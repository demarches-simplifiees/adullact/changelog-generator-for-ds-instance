# Améliorations et correctifs

## Mineurs

- fix(conditionnal): corrige les conditions sur les listes de nombre (#7746)
- fix(champ_label_component): add missing helpers (#7749)
- fix(style): applique un cursor text au textarea (#7750)
