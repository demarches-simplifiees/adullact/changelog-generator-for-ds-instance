# Technique

- Suppression du besoin d'avoir une route `/dossiers/:id` fictive
- Suppression du code pour le remplissage des formulaires usagers sur l'ancienne interface
- Amélioration de la fiabilité des tests Capybara
- Découpage de `routes.rb` en différentes sections 
