# Améliorations et correctifs

## Mineurs

- fix(revision): correction sur les révision ou un bug qui ajoutait en double les types de champ d'un bloc répétable  (#7354)
