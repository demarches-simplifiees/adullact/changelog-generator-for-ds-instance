# Améliorations et correctifs

## Mineurs

- feat(archive): prepare la prod a un plus gros usage des archives (#7068)
- Fixe la typo lorsqu'un expert veut envoyer un message (#7090)
- Corrige la visibilité des dossiers (#7093)

## Technique

- build(deps): bump puma from 5.6.2 to 5.6.4 (#7096)
- Use vite (#6787)
- Revert "Merge pull request #6787 from tchak/use-vite" (#7098)
- fix(ProcedureArchiveService.zip_root_folder): should take archive instance otherwise when we generate many archive for the same procedure, errors may occures (#7065)
- build(deps): bump minimist from 1.2.5 to 1.2.6 (#7091)
