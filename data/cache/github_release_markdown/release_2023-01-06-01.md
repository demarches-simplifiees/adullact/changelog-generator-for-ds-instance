# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- fix(template): trix will transform double spaces in &nbsp; (#8370)
- fix(graphql): fix departements with alphanumeric codes (#8374)
- refactor(repetition): add row_id to champs (#8295)
- chore(deps): bump json5 from 2.2.1 to 2.2.3 (#8376)
- fix(PiecesJustificativesService.safe_attachment): utilise la nouvelle API pour comparer le resultat du scan antivirus (#8380)

## Technique

- T

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221215131001_add_row_id_to_champs.rb`, `db/migrate/20221215131122_add_row_id_index_to_champs.rb`, `db/migrate/20221222204553_create_dossier_batch_operations.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221215135522_backfill_row_id_on_champs.rake`, `lib/tasks/deployment/20221221153640_normalize_checkbox_values.rake`, `lib/tasks/deployment/20221221155508_normalize_yes_no_values.rake`, `spec/lib/tasks/deployment/20221221153640_normalize_checkbox_values_spec.rake`, `spec/lib/tasks/deployment/20221221155508_normalize_yes_no_values_spec.rake`).
