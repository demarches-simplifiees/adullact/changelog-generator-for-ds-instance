# Améliorations et correctifs

## Majeurs

- Sur les démarches déclaratives, l'Administrateur n'est plus automatiquement affecté aux dossiers

## Mineurs

- Ajout du nouvel éditeur de champ (derrière un réglage optionnel pour l'instant)
- La page de Création de compte affiche maintenant le contexte de la démarche
- Améliorations d'usabilité pour les démarches en test

# Technique

- Refactor du layout des pages sign_in et sign_up
- Refactor de l'extraction du contexte de la démarche
- Correction des variables envoyées à SendInBlue pour suivre l'onboarding des administrateurs