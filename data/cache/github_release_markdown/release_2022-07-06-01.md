# Améliorations et correctifs

## Mineurs

- feat(demarche): add possibility to reset draft revision (#7538) (#7542)
- fix(graphql): change doc location (#7537)


## Technique

- Revert "Add id to term" (#7535)
- task(dossier): remove orphan champs (#7536)
- fix(sentry/3394049118): apparently there is an Champs::PieceJustificative without content_type validation. Should be a bug, not found in db (#7534)
- chore(npm): update dependencies (#7530)
- chore(stimulus): stimulus controllers can be lazy loaded (#7526)
- feat(task/support:delete_adminstrateurs_procedures): simple task for simple cleanup (#7539)
- feat: conditionnel, ajout du controller (#7528)

# Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220705164551_remove_unused_champs.rake`, `spec/lib/tasks/deployment/20220705164551_remove_unused_champs_spec.rb`).
