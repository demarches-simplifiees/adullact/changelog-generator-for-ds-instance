# Améliorations et correctifs

## Mineurs

- ETQ usager je peux modifier un dossier en_construction qui contient des champs en erreur (#9086)
- ETQ usager invité sur un dossier, je peux ajouter une PJ d'un dossier en construction (#9088)
- ETQ usager: débloque l'accès à des dossiers à cause d'incohérences entre les champs et les révisions associées (#9077)
- ETQ usager: modification de wording du footer dans le cadre d'une démarche (#9087)

## Technique

- API Entreprise: migration attestation sociale v3 (#9084)


# Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230522065646_fix_champs_revisions.rake`).

Si la variable d'environnement `API_ENTREPRISE_URL` est renseignée (aucune obligation, c'est optionnel), elle doit être modifiée en supprimant la version.
Ainsi, vous devez passer de 
`API_ENTREPRISE_URL="https://entreprise.api.gouv.fr/v2"`
à
`API_ENTREPRISE_URL="https://entreprise.api.gouv.fr"`

Si la variable d'environnement `API_ENTREPRISE_URL` n'est pas renseignée, vous n'avez rien à faire.



