# Améliorations et correctifs

## Majeurs

- Correction d'un bug qui empêchait d'utiliser plus d'une "liste déroulante liée"
- Correction d'un bug qui empêchait la sauvegarde des valeurs d'une "liste déroulante liée" dans les annotations privées