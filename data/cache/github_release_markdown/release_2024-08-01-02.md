# Améliorations et correctifs

⚠️  ne pas déployer  (bug dans le form de contact)

## API
-  add desarchiver mutation (#10670)

## Divers

- ETQ utilisateur, fix l'envoi à HS du form de contact quand on est déjà connecté (#10672)

