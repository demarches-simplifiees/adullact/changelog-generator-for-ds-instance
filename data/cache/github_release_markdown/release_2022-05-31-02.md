# Améliorations et correctifs


## Technique

- fix(revision): fix clone démarches with repetitions (#7426)
- bump rake (#7425)
- feat(targeted_user_link): add targeted user link to wrap expert invitation in order to avoid access issue when the expert is connected with another account (#7369)
- Technique : correction d'une exception sur la page /patron (#7422)
- Technique : correction du nom du feature-flag `procedure_estimated_fill_duration` (#7432)

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220520134041_enable_pgcrypto.rb`, `db/migrate/20220520134042_create_targeted_user_links.rb`).
