# Améliorations et correctifs

## Divers

- chore(deps): bump rexml from 3.3.0 to 3.3.2 (#10619)
- fix(intl): polyfill intl-listformat (#10621)
- Accessibilité: Amélioration de la page FAQ (#10622)
- Correction sur les avis (#10626)
- chore(champs): replace unique index (#10618)

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240716075916_add_new_champs_unique_index.rb`
- `db/migrate/20240716091043_remove_old_champs_unique_index.rb`
