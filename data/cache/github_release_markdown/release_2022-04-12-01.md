# Améliorations et correctifs

## Mineurs

- Ajoute le controle du nonce et du state dans le flow openid d'agent connect (#7141)
- refactor(pdf): accélère la génération des pdf (*2 à *4) (#7137)
- fix(archive): exporte uniquement les fichiers sans virus (#7140)
- fix(graphql): detect custom champs in fragment definitions (#7142)

## Technique

- refactor(type_de_champs): use typescript in type de champs editor (#7136)
- Mise à jour de la dépendance Nokogiri (#7143)

# Notes de déploiement

Attention, cette version est buguée, en particulier elle empèche l'acceptation de certains dossiers. Passer à la release suivante.