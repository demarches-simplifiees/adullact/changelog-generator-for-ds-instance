# Améliorations et correctifs

## Mineurs

- Améliorations de la fonctionnalité d'envoi d'un dossier à un autre instructeur :
  * il est désormais possible d'envoyer un dossier à plusieurs instructeurs en une fois
  * il est désormais possible de filtrer la liste des instructeurs à qui envoyer un dossier

# Technique

- Refactor du footer usager pour améliorer son affichage
- Dans les vues relatives aux messages, on passe désormais le `connected_user` en variable locale plutôt que son email