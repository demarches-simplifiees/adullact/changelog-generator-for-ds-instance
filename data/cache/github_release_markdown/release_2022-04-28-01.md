# Améliorations et correctifs



## Mineurs

- feat(administrateur/procedures#publication): show dubious champs to administrateur (#7194)


## Technique

- fix(js): add turbo and stimulus polyfills (#7205)
