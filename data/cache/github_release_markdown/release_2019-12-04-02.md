# Améliorations et correctifs

## Mineurs

- Amélioration des bannières affichées globalement sur le site

## Technique

- Always load IntersectionObserver to fix old browsers
