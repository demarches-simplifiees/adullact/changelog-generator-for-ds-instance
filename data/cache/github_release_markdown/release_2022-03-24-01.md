# Améliorations et correctifs

## Mineurs

- Ajout de contraintes NOT NULL sur les colonnes de AdministrateursInstructeur (#7074)
- Correction de la tâche AfterParty fix_cloned_revisions (#7037)

## Technique

- durcit la validation des emails (#7078)

# Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220322110900_add_not_null_constraints_to_administrateurs_instructeur.rb`).
