# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- feat(manager): affiche les utilisateurs de l'équipe en fonction de la valeur dans l'attribut (#8071)
- fix missing header for all demarches layout (#8077)
- feat(export): add GeoJSON export (#8045)
- add a label for api and export for sensitive data as titre_identite_champ (#8023)
- fix(cni): only allow jpg and png because of buggy pdf watermarking (#8085)
- linters: enable haml-lint for View components (#8084)
- feat(a11y): add dsfr skiplinks (#8080)
- feat(procedure_admins): permet à un admin de se retirer lui-même d'une procédure (#8083)
- ETQ admin, je peux rechercher des démarches en fonction de leur libellé parmi la liste de toutes les démarches (eventuellement  (#8089)
- refactor(operation_log): store data in jsonb instead of files (#8079)
- fix(dossier): fix dossier brouillon spec (#8106)

## Technique

- T

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220315102928_add_data_to_dossier_operation_logs.rb`, `db/migrate/20221122123809_add_libelle_index_to_procedures.rb`).
