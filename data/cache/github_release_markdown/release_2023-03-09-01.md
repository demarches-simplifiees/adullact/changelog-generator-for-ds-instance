# Améliorations et correctifs

## Mineurs

- Administrateur: permet l'import CSV de groupes instructeur avec routage lorsque la démarche n'a pas encore de routage actif (#8733)
- Fix Administrateur: le clone d'une démarche ne copie pas le cache du nombre de dossiers (#8748)
- Fix Administrateur: ne supprime pas les tags quand on n'interagit pas avec la liste de sélection (#8749)
- Fix dossier: ne numérote pas automatiquement les titres de section dans les répétitions (#8753)
- Fix typo sur la page d'accueil (#8752)


## Technique

- Jobs: rallonge durée max pour les exports, fix des timeouts, isole DossierRebase (#8750)
- chore(deps): bump rack from 2.2.6.2 to 2.2.6.3 (fix CVE-2023-27530) (#8751) 

# Notes de déploiement

Une nouvelle queue delayed_jobs a été créé: `low_priority`. Elle vise à isoler des jobs qui peuvent être empilés en masse et/ou qui mettent du temps à être traités, pour ne pas interférer avec la queue `default`.