# Améliorations et correctifs

## Mineurs

- Nouveau: API Graphql: ajoute/supprime des instructeurs (#8365)
- Correction d'une typo sur le message flash des archives (#8399)
- Amelioration (instructeurs/dossiers#show): supprime le double chargement des champs et annotations privées (#8393)
- Amélioration (instructeurs/batch_operation): n'active seulement les opérations possibles (#8385)

## Technique

- fix (mailers): observers for balancer and balanced delivery methods (#8403)
- refactor(repetition): use row_id instead of row (#8390)

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230110181415_remove_champs_row_index.rb`, `db/migrate/20230110181426_add_champs_row_id_index.rb`).
