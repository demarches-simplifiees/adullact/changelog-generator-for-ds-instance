# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- fix(procedures/all): table layout issue (#8609)
- feat(graphql): add tracing support for managers (#8635)
- a11y(main_menu): utilise aria-current=true plutot que avec page (#8592)
- Revert "Merge pull request #8635 from tchak/graphql-with-traces" (#8637)
- feat(groupe instructeur mailer): add emailing to removed instructeurs (#8626)
- prefill(dossier): prefill multiple drop down list champ (#8479)
- fix(administrateur): procedure page n+1 (#8611)
- a11y : add scrolling functionnality for description procedure Closes #8629 (#8629)
- correctif(db): index manquant sur les active_storage_attachements.blob_id -> active_storage_blob.id (#8647)
- ETQ utilisateur je peux voir le mot de passe que je choisis (#8632)

## Technique

- T

# Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230216130722_fix_active_storage_attachment_missing_fk_on_blob_id.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230216135218_reclean_attachments.rake`).
