# Améliorations et correctifs

## Mineurs

- Dans le profil administrateur, correction d'un bug lors du filtrage de la liste des instructeurs sur la page "Instructeurs" d'une démarche
- Pour les usagers, amélioration du message affiché en bas de l'email de notification de nouveau message

# Technique

- Ajout d'une tache qui migre les modules cartographiques vers les nouveaux champs carte
- Refactor de certains mailers