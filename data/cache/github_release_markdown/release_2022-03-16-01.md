# Améliorations et correctifs

## Majeurs

- ETQ qu'instructeur je veux pouvoir télécharger une archive de ma procédure même si elle fait plus de 5Go (en test) (#6989)

## Technique

- Ajout de strong_migrations (linter automatique des migrations Rails) (#7046)