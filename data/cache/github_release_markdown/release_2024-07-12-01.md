# Améliorations et correctifs

## Technique

- Technique : rattrape les erreurs active storage lors de la création des variants et previews (#10601)
- update with lock (#10604)
- fix(education): query annuaire education in the browser (#10605)
- feat(combobox): trigger menu on focus (#10606)