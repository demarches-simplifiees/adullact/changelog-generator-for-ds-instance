# Améliorations et correctifs

## Usager

- ETQ usager, j'aimerais que les champs siret soient mieux verbalisés par le screen reader (#10316)
- ETQ Usager, je souhaite que mon screen reader verbalise les erreurs sur les champs unique contenu dans un `fieldset` (#10317)

## Divers

- ETQ usage, je souhaite que les aides à la saisie soient vocalisées par le screenreader (#10313)
- tech(champs.validators): dry and standardize champs.validations (#10250)
- Tech: essaye de corriger un test non fiable relatif aux adresses (#10345)
- chore(build): use bun instead of node (#10269)
- Tech: upgrade DSFR 1.10 => 1.11 (#10341)
- Tech: replace le job de migration des logs fontionnelles sur une queue standard (#10353)
- Tech: supprime param de config `timeout` inutile pour postgresql (#10350)
- Tech: fix default url host for .gouv.fr (#10355)
