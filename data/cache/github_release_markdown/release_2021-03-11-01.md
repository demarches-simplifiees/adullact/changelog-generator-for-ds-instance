# Améliorations et correctifs




## Technique
- capital_social and numero_tva_intracommunautaire can be null
- Try to reduce the number of external data fetches
- Fix invalid GeoJSON handling
