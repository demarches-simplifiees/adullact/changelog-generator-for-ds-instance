# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- feat(dossier): prefill departements champ (#8472)
- a11y : optimization burger menu dsfr Closes #8608 (#8608)
- a11y : update label button Closes #8627 (#8627)
- correctif(procedure/all.xsls): deconnecte le lien de telechargement de toutes les demarche de turbo (#8619)
- fix(geometry): implement our own bbox to replace rgeo (#8631)
-  feat(dossier): prefill epci champ (#8576)

## Technique

- T

# Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230215100231_normalize_geometries.rake`).
