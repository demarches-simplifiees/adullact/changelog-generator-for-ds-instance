# Améliorations et correctifs

## Mineurs

- Usager : améliorations de l'affichage d'un dossier vide en PDF (#7247)
- Usager : l'accusé de dépôt affiche la première date de dépôt du dossier (#7248)

## Technique

- fix(js): prevent old Edge from crashing (#7250)
- fix(dossier): annotations privées form (#7252)
- Usager : correction du PDF de dossier vide quand un champ "Autre" est présent. (#7246)
- Improuve repetition champ (#7215)