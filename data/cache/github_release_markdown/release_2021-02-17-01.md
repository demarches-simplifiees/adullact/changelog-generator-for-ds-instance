# Améliorations et correctifs

## Majeurs

## Mineurs

- Amelioration de l'accessibilité des champs multi-select

## Technique

- README : mise à jour des informations techniques sur le déploiement de l'application
- CI : mise à jour de l'action Github utilisée pour publier les releases sur Sentry
- GraphQL : modification de l'annotation
- Sentry : ajustement du taux d'échantillonnage des traces
- préparation du mécanisme pour rendre les instructeurs notifiables (ou non) de la décision finale sur un dossier (#5904)
- Ne cache pas les classes durant les tests
- Mise à jour des dépendances Ruby
- Mise à jour de react
- Suppression de jquery et select2
- Suppression des chemins non utilisés dans la configuration de l'assets pipeline
