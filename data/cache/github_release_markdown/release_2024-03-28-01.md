# Améliorations et correctifs

💣 Ne pas déployer, bug dans les emails de dépôt de dossier, déployer [2024-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-01) ou ultérieur

## Administrateur

- ETQ admin, je veux que le champ nombre décimal ne prenne que 3 chiffres après la virgule (#10143)

## Technique

- CI: block fixup commit merge (#10233)
- tech(export_job): sometimes ExportJob are OOMed, in those cases jobs are stuck and never retried. release lock and increase attempts (#10215)
- add yabeda sidekiq to export metrics in promotheus format (#10237)