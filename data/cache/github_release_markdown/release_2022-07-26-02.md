# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- Fix(procedure library): remplace la liste infinie des procédures par un recherche simple (#7602)
- perf(graphql): benchmark demarches_publiques query (#7598)
- feat(types_de_champ): allow siret in repetition blocks (#7593)
- refactor(vite): use turbo-polyfills (#7584)
- fix(export): fix PurgeStaledExportsJob regression with `stale` scope (#7613)

## Technique

- T
