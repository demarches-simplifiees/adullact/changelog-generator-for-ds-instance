# Améliorations et correctifs

## Administrateur

- Instructeur: autocomplete les experts ne s'étant pas connectés si la liste est controllée par l'administrateur (#10358)
