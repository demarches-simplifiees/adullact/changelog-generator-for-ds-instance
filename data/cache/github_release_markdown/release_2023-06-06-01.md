# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- ETQ dev fix colonne manquante dans schema.rb (#9127)
- [refonte page accueil demarche] ajouter lien vers la notice (#9135)
- [refonte page accueil demarche] Améliorations générales design (#9136)
- ETQ Usager, je voudrais pouvoir déposer les changements apportés à la carte dans un dossier en construction (#9140)
- perf(carto): use json schema to validate geojson instead of rgeo (#9142)
- wording: affichage au clique -> au clic (#9138)
- fix(carte): no autosave on champ carte (#9144)

## Technique

- T
