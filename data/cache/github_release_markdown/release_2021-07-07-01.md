# Améliorations et correctifs

## Mineurs

- Ajouter un point sur la carte en saisissant les coordonnées
- Un usager peut accéder aux statistiques d'une démarche close

## Technique

- Erreurs ActionController::InvalidAuthenticityToken : lorsqu'il n'y a pas de cookies, la page d'erreur par défaut est affichée (#6325)
- Correction de l'affichage de couches cadastrales
