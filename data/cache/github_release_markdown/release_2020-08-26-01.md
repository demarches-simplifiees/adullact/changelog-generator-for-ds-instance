# Améliorations et correctifs

## Mineurs

- Usager : un champ radio optionnel peut maintenant être dé-sélectionné
- Usager : correction d'erreurs lors de l'enregistrement automatique d'un dossier en brouillon après la suppression d'un bloc répétable
- Super-admin : ajout de termes médicaux au détecteur automatique de données sensibles

# Technique

- build(deps): bump highcharts from 8.1.0 to 8.1.1
