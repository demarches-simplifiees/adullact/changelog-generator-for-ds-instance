# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- Backfill procedure with discarded (#8402)
- Ajoute des colonnes nécessaires au déplacement du routage comme un type de champ (#8405)
- Substitution de la variable FAQ_URL par des locales (#8401)
- fix(dossier): disable buttons when selection is canceled (#8408)
- Fix RNA Association adapter sans date_publication (#8409)
- S3 storage support (#8400)
- fix(procedures): valides uniquement le juridique a la création et à la publication (#8093)
- chore: ajoute d'un document pour les conventions de code (#8166)
- bug : améliorer l'affichage de la raison sociale pour les entreprises individuelles (#8208)
- Add prefill api to rack_attack (#8284)
- chore(npm): update dependencies (#8407)
- Us/force important email delivery (#8404)
- correctif(safe_mailer): autorise le new/create depuis le manager (#8412)
- fix(mailer): dolist header in devise mailer (#8414)

## Technique

- T

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230110153638_create_safe_mailers.rb`, `db/migrate/20230111094621_add_migrated_champ_routage_to_dossiers.rb`).
