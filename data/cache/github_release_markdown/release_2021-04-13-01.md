# Améliorations et correctifs

## Mineur

- Instructeur : le tableau de bord d'une démarche s'affiche plus rapidement
- SuperAdmin : correction de l'affichage des emails envoyés (#6076)
- SuperAdmin : en plus des emails envoyés par Sendinblue, ceux envoyés par Mailjet sont également affiché (#6076)

## Technique

- N'utilise plus flipper pour passer outre le login par jeton (1er partie)
- Remplissage des dates manquantes dans `france_connect_informations` (#6084)
- L'email de demande d'avis n'est plus envoyé si le dossier a été supprimé

## ℹ️ Notes de déploiement

Cette release contient :
- deux migrations du schéma de la base de données,
- une moulinette de migration des données (redressage des données `france_connect_information`).

Attention : sur les bases Postgres < 11, la migration de base de donnée peut potentiellement être lente.