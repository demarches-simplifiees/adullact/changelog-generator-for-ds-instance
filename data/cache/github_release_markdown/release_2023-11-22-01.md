# Améliorations et correctifs

Cette version a été déployée le lundi 20/11.

## Administrateur

- Ajout d'une page de confirmation après la publication d'une procédure - mise en avant de l'URL à partager (#9672)
- Correctif:  permet d'utiliser un tag référençant un type de champ dont le libellé contient deux espaces consécutif (#9716)

## Pour tout le monde (ou presque):

- Administrateurs/Instructeurs/Experts : lien vers la page des nouveautés et refactorise les barres de navigation principale (#9655)
- Carte de déploiement de DS par département (#9701)
- Accessibilité: mode "sombre" (en phase de test, des améliorations sont prévues) (#9705)

## Usager
- Destruction des comptes inactifs depuis au moins 2 ans (sans dossier en cours d'instruction) 💥 (#9666)

## Divers
- correctif(typo):  Quels sont des délais... ->  Quels sont les délais (#9709)

## Technique
- Migration v3 api entreprise privileges (#9713)
- Synchronize schema.rb avec la prod (#9676)

## Technique

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données : 
- `db/migrate/20231103084116_add_expired_notification_sent_at_to_users.rb`
- `db/migrate/20231108120254_add_missing_exports_instructeur_index.rb`
- `db/migrate/20231108120731_add_missing_administrateurs_groupe_gestionnaire_index.rb`
- `db/migrate/20231109190504_add_index_to_userss_on_last_sign_in_at.rb`

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20231109145911_backfill_procedure_expires_when_termine_enabled_without_dossiers.rake`
