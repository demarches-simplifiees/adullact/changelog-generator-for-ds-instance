# Améliorations et correctifs


## Mineurs

- archives : ajoute un poids moyen minimum pour les procédures sans champ pièce justificative

## Technique

- Correction d'une erreur Javascript lors de l'auto-remplissage des menus déroulants (#6323)
- Amélioration du log des erreurs ActionController::InvalidAuthenticityToken (#6321)
