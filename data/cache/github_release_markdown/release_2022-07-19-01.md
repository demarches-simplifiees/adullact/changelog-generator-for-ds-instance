# Améliorations et correctifs

## Mineurs

- feat: Champs with realistic placeholders (#7532)
- feat(admin): list procedures with updated_at & auto_archive_on (#7549)
- Rollback customized generic placeholder (#7558)
- UX/input sizes (#7555)
- feat(conditionel): version beta du conditionnel (#7544, #7582, #7561, #7486, #7565, #7569)
- feat: allows user to edit civility / siret from a dossier en brouillon (#7571)
- feat(procedure.duree_conservation_dossiers_dans_ds): decrease max duree_conservation_dossiers_dans_ds from 36 to 12 (#7551)

## Technique

- Improve perf in editor (#7578)
- Fix a11y errors (#7545)
- fix: after_stable_id pb (#7546)
- refactor(autosave): improuve events handling (#7559)
- chore: bump rails to 6.1.6.1 (#7570)
- fix(ie11): webcomponents used template interpolation (#7574)
- more fix other ie11 issues (#7583)
- Us/fix event target polyfill (#7579)
- fix(preview): gon should not crash on preview pages (#7543)
- fix(gon): matomo key can be a number (#7581)
- fix(champ): use safe_join in champ helpers (#7568)

# Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220708151654_drop_duree_conservation_dossiers_hors_ds.rb`, `db/migrate/20220708151802_add_column_duree_conservation_entendue_par_ds.rb`, `db/migrate/20220708151917_backfill_duree_conservation_entendue_par_ds.rb`, `db/migrate/20220708152039_new_default_duree_conservation_entendue_par_ds.rb`).
