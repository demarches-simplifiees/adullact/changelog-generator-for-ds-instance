# Améliorations et correctifs

## Majeurs

- Administrateurs : dans l'éditeur de champs, les nouveaux champs sont insérés à l'emplacement actuel dans le formulaire

## Mineurs

- Administrateurs : ajout de vidéos et de liens vers les webinaires
- Manager : correction de l'activation ou désactivation de fonctionnalités aux administrateurs

# Technique

- L'état des dossiers est consigné dans un journal lors de l'acceptation, du refus ou du classement sans suite
