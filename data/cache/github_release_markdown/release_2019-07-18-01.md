# Améliorations et correctifs

## Majeurs

- Usager : affiche un bouton "Répondre" dans la Messagerie
- Administrateur : ajout d'une balise `--lien document justificatif--`
- Permet d'insérer un lien vers MonAvis dans les démarches

## Mineurs

- Usager : améliore le titre des emails transactionnels
- Usager : améliore les emails de nouveau brouillon et de nouveau message

# Technique

- NodeJS v8 est maintenant le minimun requis pour faire tourner l'application
