# Améliorations et correctifs


## Mineurs

### Administrateurs

- n'envoie plus un email à tous les instructeurs lorsqu'un nouvel instructeur est ajouté (#8581) 
- permet l'import de groupes instructeurs en CSV quand la démarche est close (#8605)
- je peux cloner une démarche à partir de la vue "toutes les démarches" (#8551)
- fix erreur lors d'une modification d'une démarche en test lorsqu'elle contient des dossiers qui sont référencés dans des opérations de masse (#8585)

### Instructeurs & dossiers

- fix erreur lorsqu'on génère un export qui avait déjà été généré (#8603)
- corrections de crash de dossier suite à l'activation d'une nouvelle révision (#8587 et #8594)

### Usagers

- je peux donner mon avis sur "services publics +" depuis un mail reçu une fois mon dossier traité (#8577)

### Divers 

- API: graphql: fixing couple of n+1 (#8604)
- fix(revision): backfill missing published_at (#8607)
- fix(geo_area): backfill geo_area.geometry must be valid JSON, not nil (#8601)

## Technique

- secu(graphql): log full queries and variables (#8596)
- fix(lograge): send client_ip and request_id to es (#8599)
- chore(prod): decrease log level to info in production (#8597)

# Notes de déploiement

- Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230208154144_fix_geo_area_without_geometry.rake`, `lib/tasks/deployment/20230209093144_backfill_revisions_published_at.rake`).

- Cette version comporte une nouvelle variable d'environnement optionnelle : `SERVICES_PUBLICS_PLUS_URL`