# Améliorations et correctifs

## Majeurs

- Add revision migration and models

## Mineurs

- Administrateur : indique plus clairement si des champs de formulaire ont été ajouté à la démarche

## Technique

- Fix CI accessibility errors
- Ajout d'un test pour le telechargement de zips de dossier
- Différent fix lié au types_de_champ (surtout révisions)
