# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- fix(dossier): fix rebase with drop down options (#8383)
- refactor(batch): simplify stimulus controller (#8375)

## Technique

- T
