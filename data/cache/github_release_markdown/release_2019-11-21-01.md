# Améliorations et correctifs

## Majeurs

- Instructeur : lorsqu'un dossier est envoyé à un autre Instructeur, ce dernier est ajouté aux instructeurs qui suivent ce dossier.
- Instructeur : l'ancien format de l'export des dossiers en fichier tableur n'est plus disponible.

## Mineurs

- Usager : améliorations diverses sur l'accessibilité.

# Technique

- DossiersController : enregistre par défaut en brouillon
- DossiersController : évite de générer du HTML en cas de requête JSON
- Documentation : ajout de commentaires et d'explications pour env.example
- Javascript : les fonctions utilitaires fonctionnent lorsque l'élément n'existe pas
- Fix UnknownFormat durant les exports de procédures
