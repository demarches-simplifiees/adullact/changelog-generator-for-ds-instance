# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- fix(migration): following e764aade13985190b336bd4cbf14ac10893956e3 …we still need to remove those columns otherwise destroying a procedure fails due to index on types_de_champ.revision_id (#7684)
- fix(conditional): within section (dark blue bg) ; use white text (#7687)

## Technique

- T

# Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220822135410_remove_unused_column_on_type_de_champs.rake`).
