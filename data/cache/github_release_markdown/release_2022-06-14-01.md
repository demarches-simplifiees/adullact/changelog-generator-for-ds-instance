# Améliorations et correctifs

## Mineurs

- fix(instructeurs/avis#revive): reactiver la demande d'avis sur un dossier ne fonctionnait pas (#7461)
- fix(procedure_presentation): la personnalisation d'une liste de dossier incluant "deposé depuis" cassait l'interface (#7465)

# Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220614053743_fix_procedure_presentation_with_depose_since.rake`).
