# Améliorations et correctifs

## Mineurs

- fix typo in rgpd text (#7218)

## Technique

- fix(js): more polyfills for IE (#7219)
- fix(js): fix IE 11 fucked template element support (#7221)
