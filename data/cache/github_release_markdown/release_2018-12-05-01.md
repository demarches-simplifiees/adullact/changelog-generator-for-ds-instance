# Nouveautés

- #3046 (Instructeurs) les informations SIRET sont maintenant incluses dans l’export
- #3111 (Instructeurs) les instructeurs peuvent maintenant voir les démarches en test

# Technique

- #3118 Mémorise l’identité des auteurs des messages sur les dossiers
- #3112 script de migration des PJs vers le nouveau fournisseur de stockage
- #3121 passage de sass-rails à sassc-rail
- #3120 faire tourner after_party en post_deploy plutôt qu’au cours du script de déploiement
- #3117 mise à jour de webpacker(babel 7)
- #3114 bump de gemmes
- Suppression de code legacy
