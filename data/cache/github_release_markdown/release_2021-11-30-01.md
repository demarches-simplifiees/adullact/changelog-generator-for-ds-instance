# Améliorations et correctifs

## Mineurs

- Corrige la réappartition d'un dossier supprimé par un utilisateur mais repassé en instruction (#6691)
- Dans le manager, redirige correctement sur après avoir mergé des utilisateurs (#6692)

## Technique

- Supprime la table obsolète `feedbacks` (#6687)
- Renommage du namespace `NewAdministrateur` en `Administrateurs` (#6682)