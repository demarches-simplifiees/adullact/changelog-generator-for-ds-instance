# Améliorations et correctifs

## Administrateur

- Amélioration : ETQ admin, je veux comprendre pourquoi je ne peux pas personnaliser mes mails si l'accusé de lecture est activé (#10417)

## Usager

- Correctif : ETQ usager, je ne peux pas mettre mon dossier dans un etat invalide en essayant le bouton deposer pour un mandataire sans remplir les autres champs du mandataire (#10420)

## Divers

- chore(deps): bump nokogiri from 1.16.4 to 1.16.5 (#10416)
- refactor(champs): change views to use new urls with stable_id and row_id (#10371)
- Tech: l’affichage du bandeau .gouv.fr sticky à l'user (#10419)
- chore: expose postgres port (#10148)
- fix(dossier): fix n+1 on header sections (#10421)
