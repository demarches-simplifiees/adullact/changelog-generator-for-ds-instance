# Améliorations et correctifs

## Majeurs

- M

## Mineurs

- Précise le contenu du total des dossiers pour les instructeurs (#9073)
- correctif(users/dossiers/brouillon#submit): ETQ usager, je souhaite que chaque lien pointant vers une erreur de champ m'oriente sur le champ (#9045)
- correctif(expert/avis#index): ETQ expert, je veux retrouver mes avis donnés sur des dossiers traités (#9054)
- ETQ admin: corrige une erreur lorsque je modifie mon dossier en construction d'une démarche en test (#9072)
- Corrige un bug de numérotation automatique sur Firefox (#9078)
- Correction : pas d'erreur lors de la suppression d'un type de champ si déjà supprimé (#9080)
- chore(recovery): import/export revision (#9071)
- ETQ Opérateur, je voudrais que les erreurs dans Sentry soient liées à la version de l'application (#9079)
- ETQ Intégrateur d’API, je voudrais savoir si l’utilisateur est connecté avec FranceConnect (#9081)
- ETQ instructeur je peux filtrer les dossiers par avis (#9076)
- ETQ exploitant je veux faire remonter les contacts Dolist en erreur dans sentry (#9075)
- Usager: plus de détails sur les raisons qui expliquent pourquoi je ne reçois pas l'email "mot de passe perdu" (#9032)

## Technique

- T
