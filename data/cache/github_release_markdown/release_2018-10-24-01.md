# Améliorations et correctifs

## Mineurs

- Correction d'un bug affectant les formulaires avec plusieurs dossiers liés

# Technique

- Suppression du code de déploiement par CircleCI
- Mise en place du déploiement sur la nouvelle infra
- Refactor des scripts de déploiement
- Passage au serveur web puma
- Bump de `mina`