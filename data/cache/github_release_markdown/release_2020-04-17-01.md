# Améliorations et correctifs

# Technique

- L'éditeur de carte envoie les FeatureCollection au server
- Nettoyage des validations de procedure pour `duree_conservation_dossiers_dans_ds`
- Fix du serializer de carte pour l'API v1 suite aux modifs de carto
