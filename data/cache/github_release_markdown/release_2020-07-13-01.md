# Améliorations et correctifs

## Majeurs

- Administrateur : retour à l'ancien design de la page des détails d'une démarche (le nouveau design fonctionne mal sous Internet Explorer 11, mais va rapidement être amélioré)
