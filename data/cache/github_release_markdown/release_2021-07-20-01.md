# Améliorations et correctifs

## Mineurs

- Correction de l'affichage des cases à cocher dans les listes multiples lorsque plusieurs options ont des valeurs similaires (#6334)


## Technique

- Mise à jour de `addressable` de la version 2.7.0 à 2.8.0 (#6337)
- Mise à jour du SDK de Sentry (#6339)
