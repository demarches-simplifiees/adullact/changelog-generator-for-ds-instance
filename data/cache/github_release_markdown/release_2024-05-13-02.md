# Améliorations et correctifs

⚠️ Déployer directement [2024-05-13-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-13-03) pour les derniers correctifs

## Administrateur

- ETQ Administrateur, DS m'informe que les titres d'identité ne sont pas disponible par zip ni api (#10408)
- ETQ Administrateur, je vois le numero de ma démarche avec un separateur de millier (#10399)

## Instructeur

- Correctif: ne plante plus lors de la génération d'attestations pour les champs départements (#10409)

## Technique

- Tech: tag sentry `procedure` sur le job d'exports pour faciliter le debuggage (#10410)
