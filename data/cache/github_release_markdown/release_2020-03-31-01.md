# Améliorations et correctifs

## Mineurs

- Administrateur : correction de la prévisualisation des attestations
- Administrateur : correction des champs masqués en bas de l'éditeur de champs

# Technique

- Ajout d'une tâche `rake jobs:schedule` pour programmer les tâches récurrentes
- Tests : correction du test des dossiers expirants pour les fins de mois
- Tests : suppression du helper inutilisé `wait-for-ajax`
- Tests : simplification de la configuration rspec
- Javascript : mise à jour d'eslint et de ses dépendences
- Nettoyage des validations de PJ