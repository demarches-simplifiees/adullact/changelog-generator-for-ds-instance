# Améliorations et correctifs

## Majeurs

- Instructeur : à partir du SIRET, les effectifs mensuels de l'entreprise sont maintenant affichés dans l'interface. Les effectifs mensuels sont également disponibles dans l'API.
- Administrateur : les dossiers en construction ayant dépassé la date d'expiration sont maintenant supprimés (en conformité avec le RGPD)

## Mineurs

- Instructeur : la description de l'onglet "Tous" est plus claire

# Technique

- Correction d'une erreur sur la recherche texte libre
- Correction des règles CSP pour le développement local
- Les erreurs de lecture des pièces justificatives sont remontées dans Sentry