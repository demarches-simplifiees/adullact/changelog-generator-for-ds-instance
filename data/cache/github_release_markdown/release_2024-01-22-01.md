# Améliorations et correctifs

## Technique

- Tech: ne plante plus le job de purge des dossiers supprimés par l'usager lorsqu'un seul dossier échoue (#9917)
- chore(deps-dev): bump vite from 5.0.10 to 5.0.12 (#9924)- chore(deps-dev): bump vite from 5.0.10 to 5.0.12 (#9924)
- Exports: feature flag/Fonds verts pour trier les colonnes par révision récente puis position plutôt que mélanger les révisions entre elles (#9886)
