# Améliorations et correctifs

## Mineurs

- un administrateur qui clone une procédure y est automatiquement affecté en tant qu'instructeur
- l'interface administrateur affiche plus clairement les étapes manquantes pour pouvoir publier la démarche
- les mails de connexion sécurisée sont dorénavant uniquement envoyé toutes les 15 minutes

# Technique

- diverses nettoyages de code
- certains logs ne sont plus affichés lors de tests
- dans les tests, la création d'un administrateur créé le gestionnaire associé
- correction d'un bug qui affectait les text area dans la vue usager et instructeur