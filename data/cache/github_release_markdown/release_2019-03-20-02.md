# Améliorations et correctifs

- Usager : ajout d'un bouton d'aide contextualisé

## Mineurs

- Support : les comptes utilisateurs affichés par HelpScout sont maintenant insensibles à la casse de l'email
- API : les urls des pièces jointes sont tout le temps disponibles sauf fichier infecté