# Améliorations et correctifs

## Majeurs

- Horodatage des operations des dossiers

## Mineurs

- Correction d'un problème où la date de modification d'un dossier n'était pas mise à jour lorsque seulement les pièces justificatives étaient modifiées
- Suppression d'un paragraphe dupliqué dans le fichier README
- Petites améliorations au CONTRIBUTING.md

# Technique

- javascript: fix dependancies compilation using yarn 1.16.0
