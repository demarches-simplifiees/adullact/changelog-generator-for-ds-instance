# Améliorations et correctifs

## Majeurs

- Prend en compte les changements de ministères (#7691)

## Mineurs

- feat(conditonal): ajoute l'operator d'inclusion dans une liste pour le conditionnel (#7767)
- fix(graphql): PersonneMorale#date_creation may be null (#7808)

## Technique

- T

# Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220911134914_create_zone_labels.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220922151100_populate_zones.rake`, `spec/lib/tasks/deployment/20220922151100_populate_zones_spec.rake`).
