# Améliorations et correctifs

## Usagers

- ETQ Utilisateur, je ne veux pas de caractère invalide dans l'état du dossier en PDF (#8964)
- ETQ Usager, champ carte: ne permet pas d'enregistrer une geometry null pour ne pas casser les exports (#8948)

## Administrateurs
- ETQ administrateur empêche une condition d'égalité de s'applique à un champ choix multiple (#8967)



## Technique
- [fix] erreur dans le nom de variable (#8960)
- Corrige la logic dans le cas de comparaison avec une liste de choix multiple (#8954)


# Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230421160218_fix_geo_area_without_geometry_again.rake`, `lib/tasks/deployment/20230424154715_fix_include_in_logic.rake`, `spec/lib/tasks/deployment/20230424154715_fix_include_in_logic.rake_spec.rb`).
