# Améliorations et correctifs

## Mineurs

- amelioration(archives): documentation de la structure de l'archive (#7938)
- fix(dossier): a tag can be preceded by a - (#7950)

## Technique

- refactor(turbo): simplify custom actions implementation now that turbo has support for it (#7897)
- ci: tests against postgresql 14.3 (#7930)
