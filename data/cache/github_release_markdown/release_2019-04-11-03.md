# Améliorations et correctifs

## Mineurs

- Corrige un bug lors de l'envoi d'un email de notification si le logo de la démarche n'est pas récupérable

# Technique

- Correction d'un bug qui affectait la création de type de champ avec menu déroulant.
