# Améliorations et correctifs

## Majeurs

- Améliore l'accessibilité de la page d'accueil

## Mineurs

- GraphQL : normalise le nom des dates en français
- Corrige une erreur lors de l'affectation d'instructeur avec une adresse vide
- Corrige la fonction de sauvegarde automatique qui n'ignorait pas les bons champs
- Correction d'une petite faute
- Rename demarche archivée to démarche close
- Corrige la création d'un administrateur dans l'interface manager

## Technique

- Mise à jour de Capybara
- Clean up procedure states
- Simplify React components loader
