# 2020-06-11-01

Release [2020-06-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-11-01)  sur GitHub

## Améliorations et correctifs

## Technique

- supprime l'option `dependent: destroy` entre les groupes_instructeurs et les instructeurs
