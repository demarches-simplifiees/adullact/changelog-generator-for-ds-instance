# 2020-06-04-01

Release [2020-06-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- tri des bilans bdf CSV
- https://github.com/betagouv/demarches-simplifiees.fr/issues/5224 gestion des effectifs d'entreprise seulement pour février 2020

## Technique

- Précision des essais max par job
- #5207 - Ajout de la gestion du protocole Openstack Keystone v3
