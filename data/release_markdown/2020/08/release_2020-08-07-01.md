# 2020-08-07-01

Release [2020-08-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Adapte la première partie de la page de configuration des emails (interface administrateur) au nouveau design

