# 2020-08-03-01

Release [2020-08-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Passage de la page admin/attestation_templates/edit au nouveau design
- Ajout du fond de carte IGN pour le module cartographique

### ⚠️ Notes de versions

Cette version comporte un bogue dans la génération des attestations, et a été retirée. La version suivante corrige le problème.


