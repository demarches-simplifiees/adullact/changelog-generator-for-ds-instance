# 2020-07-21-01

Release [2020-07-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : possibilité de relancer un·e expert·e pour une demande d'avis

### Mineurs

- API GraphQL : les blocs répétables et les options sont maintenant exposés sur `champ_descriptors`

## Technique

- Activation des options par défaut de Rails 5.0
