# 2020-07-16-01-2

Release [2020-07-16-01-2](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-16-01-2)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Retire les drop_down_list inutilisées
- Ajout des annotations privées dans l'écran d'administration

