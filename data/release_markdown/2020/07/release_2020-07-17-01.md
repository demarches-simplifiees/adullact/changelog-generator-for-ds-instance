# 2020-07-17-01

Release [2020-07-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-17-01)  sur GitHub

## Améliorations et correctifs

### Mineurs


- Résoud un bug sur la nouvelle interface d'administrateur sur la possibilité de tester la démarche (en brouillon)

## Technique

- Mise à jour de lodash de 4.17.15 à 4.17.19
