# 2020-12-11-01

Release [2020-12-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ajoute dans la cartographie les couches Mnhn

### Mineurs

- présente la civilité par ordre alphabétique
- Redémarrage de delayed_job seulement pour les workers
- Supprime les titres identite une fois un dossier traité
- Affiche les titre d'identité qu'une fois le filigrane apposé.

### Technique

- #5752 - Doc: add DEMANDE_INSCRIPTION_ADMIN_PAGE_URL to env.example.optional file
- #5764 - Allow logos (mail, webapp) and favicons to be configured in .env file
- Update js dependencies
- Appelle l'API entreprise avec le token en Header
- Better graphql mutation error messages
- Bump ini from 1.3.5 to 1.3.7
- Répare et test la gestion des erreurs de fichier lors d'upload par API