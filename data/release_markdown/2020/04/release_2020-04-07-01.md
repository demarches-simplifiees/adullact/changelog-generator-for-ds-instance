# 2020-04-07-01

Release [2020-04-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-07-01)  sur GitHub

## Notes de version

Cette release corrige le problème de compilation du Javascript pour les anciens navigateurs introduit dans la version précédente (2020-04-06-01).

## Améliorations et correctifs

### Majeurs

- Administrateur : quand un administrateur clone une de ses propres démarches, les groupes instructeurs sont clonés aussi
- Usager : Désactivation des cartes Mapbox-GL, et retour vers l'ancien format de cartes

### Mineurs

- Usager : corrige une erreur Javascript au moment du Feedback, liée à l'utilisation de `window.scroll`
- Experts : dans l'email d'invitation, le lien pour donner son avis est maintenant un grand bouton bien visible

## Technique

- Ajoute une validation pour vérifier la cohérence des champs (#4994)
- Usager : les erreurs de vérification du statut de l'anti-virus ne sont plus remontées dans Sentry
- Jobs : utilisation d'expressions en pseudo-langage humain pour configurer les jobs récurrents