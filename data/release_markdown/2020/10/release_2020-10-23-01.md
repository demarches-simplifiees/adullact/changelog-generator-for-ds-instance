# 2020-10-23-01

Release [2020-10-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-23-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Fixe les CSP pour bootstrap et Jquery

