# 2020-10-30-02

Release [2020-10-30-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-30-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Accelère le temps de chargement sur la page d'accueil
- Accelère le temps de chargement sur la vue instructeur
- Cherche a télécharger l'attestation sociale tous les jours pendant 5 jours
- fix / Modifie la description dans le champ titre identité

## Technique

- Supprime la GEM rack_mini_profiler de l'environnement de production
