# 2020-10-12-01

Release [2020-10-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Revert "feat/5635 - Supprime la possibilité pour l'expert invité d'envoyer un message"

