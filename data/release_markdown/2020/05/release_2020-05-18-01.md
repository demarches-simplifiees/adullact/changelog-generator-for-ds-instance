# 2020-05-18-01

Release [2020-05-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : le service administratif de la démarche est maintenant accessible aux lecteurs d'écrans

## Technique

- Javascript : évite de remonter dans Sentry les erreurs de Direct Upload (en plus des erreurs d'auto-upload)
- Rake : ajout d'une tâche pour déclencher un rollback