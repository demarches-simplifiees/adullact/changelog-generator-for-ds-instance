# 2020-09-04-01

Release [2020-09-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Suppression des doublons d'annotations privées
