# 2020-09-30-02

Release [2020-09-30-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-30-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5643 Correction pour permettre à un administrateur d'envoyer une copie de démarche à un autre administrateur
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5547 Les exports contiennent les types_de_champ de toutes les revisions
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5627 Réduction de la taille de police dans les attestations au long footer
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5652 Correction des problèmes d'upload de champs Pièce justificative dans les champs répétition

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5650 Renvoi de l'erreur de parsing lors des appels API GraphQL incorrects
