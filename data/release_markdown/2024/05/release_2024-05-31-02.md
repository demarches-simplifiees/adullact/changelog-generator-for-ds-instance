# 2024-05-31-02

Release [2024-05-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-31-02)  sur GitHub

## Améliorations et correctifs

### Divers

- ETQ Mainteneur, les emails non vérifiés ne sont pas envoyés (#10461)
