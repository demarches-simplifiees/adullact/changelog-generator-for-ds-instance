# 2024-04-23-01

Release [2024-04-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-23-01)  sur GitHub

#:information_source: on place la feature de prévisualisation des images sous feature flag `galery_demande` afin de tester la feature plus tranquillement. 

## Améliorations et correctifs

### Divers

- Tech: codecov only informational (#10366)
- Correctif galerie : affichage pdf + feature flag sur galerie dans la page demande (#10367)
- refactor(champs): do not depend on attributes hash key in old code (#10365)
