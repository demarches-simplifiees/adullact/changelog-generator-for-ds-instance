# 2024-08-26-01

Release [2024-08-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-26-01)  sur GitHub

## Améliorations et correctifs

### Divers

- ETQ expert, je dois confirmer mon mail (#10705)

### Technique

- Correctif, ETQ Tech, je souhaite que la tache qui peuple les adresses normalisées des champs siret ne plante pas a cause de bad data sur les champs (#10714)
