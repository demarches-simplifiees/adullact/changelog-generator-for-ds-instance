# 2024-07-02-01

Release [2024-07-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-02-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, un nouveau champ est obligatoire par défaut (#10547)
- ETQ administrateur, lorsque je gère ma liste d'expert invités sur une démarche, je suis guidé pour eviter les typos (#10524, #10574)
- ETQ admin, correctif: ne détruit pas l'attestation v1 en allant sur la page pour préparer une v2 (#10573)


### Instructeur

- ETQ nouvel instructeur, je dois confirmer mon mail (#10549)
- ETQ instructeur, le sommaire sur la page demande s'affiche uniquement sur les écrans XL (#10561)

### Usager

- ETQ usager, je peux télécharger mon dossier sans avoir à passer par le bouton "imprimer" (#10559)

### Manager

- ETQ superadmin, je peux accéder aux informations essentielles des dernières démarches publiées (#10565)
- ETQ superadmin: export administrators and instructeurs list (#10489)

### Technique
- Tech (tests): default administrateur fixtures (#10483)
- Tech (helpscout): corrige encore plus mieux le rate limiting (#10568)
- Technique : crée des variants et prévisualisations des pjs pour les dossiers récents (#10556)
- Tech: corrige le nom de l'application hardcodé dans un email (#10567)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240619205011_change_types_de_champ_mandatory_default.rb`

Cette version comporte des migrations du contenu des données :
- `lib/tasks/deployment/20240625151936_create_variants_for_pjs.rake`
- `lib/tasks/deployment/20240625151948_create_previews_for_pjs.rake`
