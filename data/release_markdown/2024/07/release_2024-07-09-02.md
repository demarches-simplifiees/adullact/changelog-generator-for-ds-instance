# 2024-07-09-02

Release [2024-07-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-09-02)  sur GitHub

## Améliorations et correctifs


## Usager et instructeur 
- Corrige l'affichage du champ "choix simple" ayant plus de 20 options (#10592)
