# 2024-07-11-01

Release [2024-07-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin et instructeur, lorsque je gère ma liste d'instructeurs sur une démarche, je suis guidé pour eviter les typos (#10579)

### Expert 

- ETQ expert, la vue d'un dossier respecte le bon layout (#10593)

### Technique

- chore(npm): update dependencies (#10589)
- Technique : crée des variants et previews via une maintenance task (#10596)
- chore(npm): update coldwired (#10597)
- fix spectaql (#10598)
