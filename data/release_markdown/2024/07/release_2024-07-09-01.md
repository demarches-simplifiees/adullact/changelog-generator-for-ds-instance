# 2024-07-09-01

Release [2024-07-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-09-01)  sur GitHub

## Améliorations et correctifs

⚠️ Oops, ne déployez pas cette version ! Passer directement à [2024-07-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-09-02) 

### Administrateur

- ETQ usager et admin, je suis prévenu au moment de commencer un dossier sur une démarche ou révision en test (#10577)
- ETQ Admin, je peux associer ma démarche aux zones Autorité indépendante et Chambre parlementaire (#10583)

### Usager

- ETQ usager, corrige l'alignement de la page demande d'un dossier en construction (#10575)
- ETQ usager: je ne peux ajouter qu'une unique PJ pour les anciennes procédures qui l'exigent (#10511)
- ETQ usager, je peux saisir le numéro d'accréditation COJO avec le suffixe `-01` (#10590)

### Divers

- chore(js): update coldwired, react and combobox (#10404, #10586)
- refactor(js): use superstruct instead of zod (#10585)

### Technique

- Tech: Normalisation des champs de type number/decimal pr enlever les espace blanc à la saisi (#10555)
- Tech: cookies avec flag `secure` et `httponly` (#10576)
