# 2024-07-30-01

Release [2024-07-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-30-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin sur une démarche en brouillon, l'attestation v2 est créé en brouillon si on a déjà une v1 active (#10660)
- Correctif : ouvre l‘assistant de routage à tous les types de champ conditionnables (#10602)
- ETQ Admin (amélioration), je veux pouvoir modifier des PJ sans recharger la page (#10603)

### Instructeur

- ETQ instructeur je peux voir les pjs envoyées par la messagerie dans l‘onglet "pièces jointes" (#10572)
- ETQ instructeur, corrige un problème quand on veut enlever la dernière colonne affichée dans le tableau (#10656)
- ETQ Instructeur, je peux choisir les pjs que j'exporte (en test sous feature flag) (#10548, #10655)

### Usager

- ETQ usager : correctif pour télécharger un dossier expiré (#10667)
- ETQ usager corrige la façon dont le lien de confirmation du compte est réutilisé (#10659)
- ETQ usager l'attestation v2 n'a pas ses titres chevauchés lorsqu'ils passent sur 2 lignes (#10649)
- Clarification du message pour se créer un compte à partir d'une adresse email (#10600)

### Accessibilité
- Accessibilité: permet au markdown FAQ d'intégrer n'importe quel attribut html dans les `<img>` (#10646)
- Améliore l'accessibilité des pages de contenu de la FAQ (#10632)
- Améliore l'accessibilité de la page d'authentification (#10647)
- Améliore l'accessibilité de la page mot de passe oublié (#10654)
- Accessibilité : Assure la restitution par les TA du bloc de suggestion de l'adresse mail (#10665)

### API
- Amélioration, ETQ consommateur de l'API graphql, je souhaite pouvoir modifier une annotation de type choix simple (#10642)

### Divers
- Correctif: le changement de thème est de nouveau fonctionnel (#10652)
- fix: missing https:// sur la page de déclaration d'accessibilité (#10663)

### Technique
- fix(sva/svr): fix spec with arbitrary date (#10664)
- chore(npm): update dependencies (#10658)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240713090744_add_dossier_folder_column_to_export_template.rb`
