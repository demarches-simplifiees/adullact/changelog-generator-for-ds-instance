# 2024-03-18-03

Release [2024-03-18-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-18-03)  sur GitHub

## Améliorations et correctifs

### Divers

- Exports: ajouter une colonne "date de dernière mise à jour du dossier" #10043 (#10136)
- fix(champ): fix multiple champ copies bug (#10123)
- fix(champs): fix rows order (#10141)
- fix(champs): use approximate count in the task (#10140)
- fix(data): apply fix missing champ via task. FV dossiers are poping way too much at helpdesk (#10139)
