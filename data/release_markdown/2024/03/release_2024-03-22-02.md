# 2024-03-22-02

Release [2024-03-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-22-02)  sur GitHub

## Améliorations et correctifs

⚠️ A ne pas déployer, pb avec le dépot de nouveau dossier venant de FCI

### Divers

- fix(dosser#show.pdf): missing user indirection on fci (#10176)
