# 2024-03-07-01

Release [2024-03-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-07-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ Instructeur je veux attribuer la motivation correcte à ma décision (#10050)
- ETQ usager je vois les informations de contact de mon groupe instructeur (si elles existent) dans mon attestation de dépôt (#10067)

### API

- fix(entreprise): graphql effectif (#10053)

### Divers

- refactor(dossier): use revision as source of truth to diplay champs (#9695)
- tech(spread_dossier_deletion_task): query on date time range not on date (#10069)
