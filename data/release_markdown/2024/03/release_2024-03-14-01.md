# 2024-03-14-01

Release [2024-03-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-14-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager: Mentionne d'autres acteurs possibles dans la description de la co-construction (#9961)

### Divers

- fix(data): update procedure with duree_conservation greater than max_duree (#10107)
- fix(maintenance task): pass a collection instead of an array (#10110)
- Tech: mise à jour d'openid connect pour une compatibilité avec openssl v3 (#9908)
