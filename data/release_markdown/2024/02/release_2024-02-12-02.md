# 2024-02-12-02

Release [2024-02-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-12-02)  sur GitHub

## Correctifs

Corrige des erreurs dans l'API introduites par [la release précédente](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-12-01)

### API

- Tech: rollback graphql to 2.2 => 2.0 (#9982)
