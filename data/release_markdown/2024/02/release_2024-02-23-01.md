# 2024-02-23-01

Release [2024-02-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-23-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur: corrige une typo tier => tiers dans les exports (#10031)

### Technique

- Sécurité: fix multiple CVE in rails & rack (#10038)
