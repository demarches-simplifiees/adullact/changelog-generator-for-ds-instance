# 2024-02-09-01

Release [2024-02-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-09-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin je peux ajouter un champ carte dans un bloc répétable (#9907, #9979)


### Usager

- ETQ usager: le champ carte est un peu plus design (passage au DSFR) (#9976)
- ETQ usager je vois de nouveau le délai d'instruction pour les démarches déclaratives en instruction (#9974)

### Tech
- Instructeur: use precomputed timestamps for notifications (#9977)
- Carte: migration IGN vers Géoplatforme et corrige visibilité de certains fonds de carte (#9975)

