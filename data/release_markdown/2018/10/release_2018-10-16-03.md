# 2018-10-16-03

Release [2018-10-16-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-03)  sur GitHub

## Technique

- Scission de `lib/carto` en `lib/api_geo`, `lib/api_adresse`, `lib/api_carto`
- Utilisation de parenthèses
- Extraction de l'adresse de l'API Adresse dans une constante, et utilisation du HTTPS pour cette dernière
- Légers refactors
- Suppression d'une cassette dupliquée
- Ajout d'un délai (debounce) sur les requêtes de suggestions d'adresses pour ne pas trop solliciter notre API