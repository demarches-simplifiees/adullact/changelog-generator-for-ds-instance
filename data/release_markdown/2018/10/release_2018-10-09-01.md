# 2018-10-09-01

Release [2018-10-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #2781 Les usagers peuvent désormais déposer des dossiers sur les démarches en brouillon
- #2778 Dans l'API, l'attribut `state` des dossiers est désormais exposé

### Mineurs

- #2784 Dans le profil Administrateur, suppression du bouton de suppression d'une démarche
- #2783 La doc de l'API est désormais hébergée sur GitBook
- #2780 Dans la manager, ajout d'un bouton pour repasser une démarche en brouillon

## Technique

- #2783 Suppression de `apipie` et `maruku`
- #2787 Ajout de taches pour réenvoyer les mails envoyés par Mailjet avec une IP de confiance faible