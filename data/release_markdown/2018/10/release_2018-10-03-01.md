# 2018-10-03-01

Release [2018-10-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2733 Dans le profil instructeur, le bouton d'enregistrement des annotations privées est désormais plus visible
- #2737 Dans le manager, affichage du service des démarches
- #2736 Dans le manager, affichage des emails des démarches
- #2731 Suppression de la case d'acceptation des CGV pour l'indentification par SIRET

## Technique

- #2742 Correction de failles dans le code relatif à la personnalisation et au filtrage du tableau de dossiers instructeur
- #2734 Suppression de code relatif au profil usager désormais obsolète
- #2746 Suppression d'une vue SQL désormais obsolète