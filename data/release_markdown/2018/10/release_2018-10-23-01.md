# 2018-10-23-01

Release [2018-10-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans l'API, on expose désormais les informations récupérées pour les champs de type “SIRET”
- Dans l'API, on expose désormais les informations récupérées pour les champs de type “Carte”
- Dans l'API, on expose désormais le “state” d’une démarche
- Ajout du champ Carte, caché pour le moment

### Mineurs

- Dans le profil Usager, correction d’erreurs dans le module cartographique
- Dans le profil Instructeur, dans le header, “Contact” a été renommé en “Aide”
- Dans le profil Instructeur, le pop-over d’aide ne contient plus notre numéro de téléphone
- Dans le profil Instructeur, amélioration du message dans le pop-over d’aide
- Amélioration de l’email de création de compte administrateur
- Amélioration de l’email de création de compte instructeur
- Dans l’email de refus de création de compte administrateur, on invite désormais l’utilisateur à passer par notre formulaire de contact

## Technique

- Refactor de la gestion de la cartographie pour partager plus de code entre le champ et le module
- Explicitation d’une foreign_key sur `Champs::CarteChamp`
- Léger refactor de `ProcedureSerializer` et `Procedure`