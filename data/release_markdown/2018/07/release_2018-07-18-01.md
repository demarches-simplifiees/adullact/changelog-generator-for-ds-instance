# 2018-07-18-01

Release [2018-07-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ajout d'un champ de recherche par numéro de dossier dans la partie usager

### Mineurs

- Correction d'un bug avec le champ SIRET sur la page "Prévisualisation d'une démarche"
- Dans le manager, on affiche désormais si le compte d'un usager est confirmé ou nom
- Dans le manager, on affiche désormais 2 boutons pour renvoyer un email de confirmation ou confirmer le compte d'un usager

## Technique

- Refactor de certaines routes
- Bump de gems