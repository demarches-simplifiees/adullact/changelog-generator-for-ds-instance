# 2018-07-02-01

Release [2018-07-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-02-01)  sur GitHub

## Nouveautés

- Nouvelle présentation de la liste des dossiers pour les Usagers.

![screenshot_2018-07-02 demarches-simplifiees fr 1](https://user-images.githubusercontent.com/179923/42164203-97ddd740-7e05-11e8-8df1-2d65b95b526b.png)

## Améliorations et correctifs

### Mineurs

- Une fois qu'une procédure est archivée, il n'est plus possible de soumettre un dossier pour cette procédure, même si le dossier a été commencé avant.
- Il est désormais possible d'enregistrer un brouillon avec une pièce jointe même si tous les champs obligatoires n'ont pas été renseignés.
- La paramétrisation du nombre de résultats par page dans l'API fonctionne maintenant correctement.
- L'email de réinitialisation de mot de passe est maintenant traduit en français.