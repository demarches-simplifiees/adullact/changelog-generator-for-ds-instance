# 2018-06-26-01

Release [2018-06-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Affichage de deux établissements dans l’interface accompagnateur : l’établissement correspondant au SIRET, ainsi que le siège social (si celui-ci est différent)
- Le caractère obligatoire de la case à cocher des CGU est désormais signalé à l’utilisateur par une astérisque
- Correction du lien vers l'email de contact dans l'interface accompagnateur
- Le bouton « créer un compte » s’affiche désormais sur une seule ligne sur la page de connexion

## Technique

- Travail préparatoire sur champs conditionnels simples (deux listes déroulantes liées)
- Constantisation du numéro de téléphone de contact
