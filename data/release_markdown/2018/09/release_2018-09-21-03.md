# 2018-09-21-03

Release [2018-09-21-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-21-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les notices des démarches s'ouvrent désormais dans un nouvel onglet