# 2018-09-20-01

Release [2018-09-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug sur les champs "dossier lié"
- Correction de l'affichage des sauts de ligne dans le dialogue de confirmation de suppression d'un dossier
- Ajout de sauts de ligne dans le dialogue de confirmation de suppression d'un dossier
- Harmonisation et correction de plusieurs textes

## Technique

- Ajout et utilisation d'un linter d'editorconfig
- Flipflop est désormais remis à zero avant chaque test
- Modification du script de clone de base pour qu'il pointe vers notre nouvelle base
- Extraction des scripts de déploiement et de lint dans des fichiers
- Suppression de scripts inutiles
- Nettoyage du script de déploiement
- Modification de la clé SSH fournie à CircleCI pour le déploiement