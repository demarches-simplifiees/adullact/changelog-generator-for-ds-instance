# 2018-09-13-01

Release [2018-09-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui rendait certains boutons inactifs sur Safari iOS
- Pour les instructeurs, la rechrche est désormais insensible à la casse

### Mineurs

- Correction d'une coquille sur le formulaire de demande de compte administrateur

## Technique

- Refactor du champ SIRET
- Correction de la tache qui corrigeait la démarche FTAP