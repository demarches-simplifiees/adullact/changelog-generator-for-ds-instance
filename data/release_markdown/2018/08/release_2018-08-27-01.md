# 2018-08-27-01

Release [2018-08-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Améliorations de [la page Stats](https://www.demarches-simplifiees.fr/stats) :
  * Ajout d'un graphique avec la répartition des états des dossiers
  * Ajout d'un graphique avec l'évolution du sentiment usager
  * Pour les super-administrateurs, ajout d'un bouton pour télécharger des statistiques au format CSV

### Mineurs

- Petites améliorations au widget de collecte de sentiment usager
  * reformulation de la question pour qu'elle soit plus précise et pertinente
  * les usagers peuvent désormais évaluer une fois par mois, et pas une fois "pour toujours"
  * les usagers sont désormais incités à nous écrire un email après leur évaluation
  * après l'évaluation, la page scroll désormais en haut de page afin de voir le message de confirmation
- Petites améliorations aux deux landing pages :
  * amélioration du style d'un bouton dans la landing usagers
  * rajout d'un CTA en bas de page de la landing administateurs
  * les administrateurs sont désormais redirigés vers la landing administrateurs après soumission du formulaire de demande de compte, et non plus la landing usagers
  * le bouton vers le formulaire de demande de compte administrateur ne s'ouvre plus dans un nouvel onglet mais dans l'onglet actuel
- Ajout de points finals manquants dans des emails

## Technique

- Utilisation d'un enum textuel et non pas d'une simple colonne d'entiers pour gérer les évaluations usager
- Ajout de parenthèses dans le code
- Suppression de code CSS dupliqué
- Suppression d'un appel à Google Analytics désormais inutile