# 2018-08-30-01

Release [2018-08-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-30-01)  sur GitHub

## Nouveautés

- Ajout d'un formulaire de contact qui remplace les liens vers notre adresse email

## Améliorations et correctifs

### Mineurs

- Sur la page [Statistiques](https://www.demarches-simplifiees.fr/stats), ajout de l'évolution sur 30 jours des démarches publiées et des dossiers déposés

## Technique

- Ajout d'une API de webhook HelpScout pour pouvoir afficher des liens vers le manager dans HelpScout
- Généralisation des appels du type `MonModel.un_enum.fetch(:enum_key)` au lieu de `"enum_value"` afin d'éviter l'utilisation de valeurs inexistantes d'un enum
- Ajout d'une tache pour permettre la migration de dossiers d'une démarche Nutriscore vers une autre
- Suppression de l'API de stats, désormais inutilisée