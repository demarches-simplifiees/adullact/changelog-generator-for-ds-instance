# 2018-08-02-01

Release [2018-08-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-02-01)  sur GitHub

## Nouveautés

- Les usagers peuvent désormais inviter des personnes à modifier leur brouillon

## Technique

- Création et mise-à-jour de binstubs afin de corriger d'un bug au déploiement
- On cache désormais les packages yarn à la CI