# 2018-12-07-03

Release [2018-12-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-07-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #3144 Dans le profil administrateur, correction d'un bug affectant le clonage des démarches

## Technique

- #3145 Amélioration du script de migration des PJ