# 2018-12-19-02

Release [2018-12-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-19-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Suppression du bandeau sur le Tour de France sur la landing administrateur
- Dans le profil Administrateur, dans l'onglet "Pièces jointes" d'une démarche, ajout d'un message invitant l'administrateur à utiliser les champs "pièce justificative"
- Le lien d'inscription à la newsletter dans le footer pointe désormais vers la page d'inscription de SendInBlue

## Technique

- Utilisation d'un `link_to` dans une vue à la place d'un lien `%a`
- Ajout d'une tache de traitement de balises obsolètes
- Bump de rubocop
- Activation de plusieurs cops Rubocop
- Ajout de colonnes timestamps aux tables qui n'en avaient pas