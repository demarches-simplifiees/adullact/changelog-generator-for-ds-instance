# 2018-12-14-01

Release [2018-12-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-14-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui empêchait l'affichage de la page "annotations privées" sur un dossier avec un champ pièce justificative rempli