# 2018-12-10-01

Release [2018-12-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-10-01)  sur GitHub

## Technique
- #3138 Passage à l’API INSEE V3 pour récupérer des résultats plus complets
- #3148 Optimisation du script de migration des PJ