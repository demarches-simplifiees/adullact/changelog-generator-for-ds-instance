

--------------------------------------------

# 2024-08-26-01

Release [2024-08-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-26-01)  sur GitHub

## Améliorations et correctifs

### Divers

- ETQ expert, je dois confirmer mon mail (#10705)

### Technique

- Correctif, ETQ Tech, je souhaite que la tache qui peuple les adresses normalisées des champs siret ne plante pas a cause de bad data sur les champs (#10714)


--------------------------------------------

# 2024-08-22-01

Release [2024-08-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-22-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager, j'ai une indication sur le format de date à saisir en fonction de mon navigateur pour le champ Accréditation JO (#10709)
- ETQ usager, quand je saisis un n° RNA, les espaces sont automatiquement retirés pour ne pas provoquer d'erreur plus tard (#10708)

### Technique

- Tech(perf): active les frozen string literals partout (#10386)


--------------------------------------------

# 2024-08-21-01

Release [2024-08-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-21-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ Admin je suis prévenu que ma modification de formulaire aura besoin d'être publiée (#10702, #10707)

### Instructeur
- ETQ instructeur, je peux filtrer par les donnée de champs issues d'API, a commencer par RNA/RNF SIRET (#10693)
- ETQ instructeur, n'affiche pas 2 fois le message de réinitialisation du tableau de dossiers (#10700)

### Technique
- Correctif: tache de maintenance des données RNA pour permettre la recherche (#10704)
- refactor(columns): rename virtual to displayable (#10701)


--------------------------------------------

# 2024-08-20-02

Release [2024-08-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-20-02)  sur GitHub

## Améliorations et correctifs

### Instructeur


- ETQ instructeur, ne plante pas en essayant de formater une date quand c'est pas une date (#10698)
- Correctif: rattrape les données inconsistantes pour la présentation des tableaux instructeurs qui se font detruire a la 1ere connection (#10699)
- ETQ instructeur, corrige l'affichage expliquant la raison pour laquelle l'usager a supprimé (ou expiré) un dossier (#10697)
- ETQ instructeur : simplifie l'interface avec un onglet unique pour les dossiers supprimés (#10627)

### Divers
- Sécurité: évite une injection XSS par le nom des PJ qui trompait les navigateurs (#10696)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240718100430_rename_supprimes_recemment_from_procedure_presentation.rb`


Cette version contient une nouvelle maintenance task qui met à jour les `procedure_presentations` (cf #10699 , #10625)
- [Maintenance:: HotfixFormerProcedurePresentationNamingTask](https://github.com/mfo/demarches-simplifiees.fr/blob/3662e3836635ffeb32099ee58a22bfc92cdde8a5/app/tasks/maintenance/hotfix_former_procedure_presentation_naming_task.rb)


--------------------------------------------

# 2024-08-20-01

Release [2024-08-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-20-01)  sur GitHub

## Améliorations et correctifs

⚠️ Déployer directement [2024-08-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-20-02) (ou plus tard) avec des correctifs, et sa maintenance task associée (ainsi que les 3 introduites par cette version, voir plus bas)

### Administrateur

- ETQ admin, je veux que l'email du déposant ne puisse pas être similaire à l'email du mandataire (#10661)
- ETQ admin et usager, l'attestation v2 gère mieux les pieds de page (#10691)
### Instructeur

- ETQ Instructeur, je ne veux pas qu'on me propose de filtrer par notification des dossiers non suivi (#10388)

### API

- secu(graphql): without a token, only saved queries are allowed (#10614)

### Technique
- Tech: quand un dossier est définitivement supprimé, on ne garde que le log (DOL) concernant sa suppression (#9758)
- ETQ tech, je souhaite isoler le concept des colonnes extractable/cherchable/affichable d'une demarche (#10625)
- ETQ Tech, les adresses des champs siret / rna / rnf sont normalisées pour une recherche homogène via les filtres (#10690)
- Tech: bump rexml 3.3.2 => 3.3.5 (#10687)
- Tech: bump fugit from 1.10.1 to 1.11.1 (#10692)
- Tech: remove yabeda-rails et yabeda-graphql (#10688)

## Notes de déploiement

Cette release contient des nouvelles maintenance tasks pour préparer une future évolution (voir #10690)
- [Maintenance::PopulateSiretValueJSONTask](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/blob/92ffd69bf1a85155a8f30dbceeab57dfbbca6092/app/tasks/maintenance/populate_siret_value_json_task.rb)
- [Maintenance::PopulateRNAJSONValueTask](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/blob/92ffd69bf1a85155a8f30dbceeab57dfbbca6092/app/tasks/maintenance/populate_rna_json_value_task.rb)
- [Maintenance::PopulateRNFJSONValueTask](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/blob/92ffd69bf1a85155a8f30dbceeab57dfbbca6092/app/tasks/maintenance/populate_rnf_json_value_task.rb)


--------------------------------------------

# 2024-08-13-01

Release [2024-08-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-13-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- Correctif: permet de repousser la date d'expiration d'un dossier / restaurer un dossier récemment expiré (#10685)

### Usager

- ETQ usager, permet de saisir un champ RNF en ignorant les espaces/tabulations (#10674)
- ETQ usager, corrige un bug d'affichage de message d'information dans la liste des dossiers (#10683)


--------------------------------------------

# 2024-08-01-03

Release [2024-08-01-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-01-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Tech: permet la suppression des ContactForm d'users ayant des dossiers (#10675)


--------------------------------------------

# 2024-08-01-02

Release [2024-08-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-01-02)  sur GitHub

## Améliorations et correctifs

⚠️  ne pas déployer  (bug dans le form de contact)

### API
-  add desarchiver mutation (#10670)

### Divers

- ETQ utilisateur, fix l'envoi à HS du form de contact quand on est déjà connecté (#10672)


--------------------------------------------

# 2024-08-01-01

Release [2024-08-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-08-01-01)  sur GitHub

## Améliorations et correctifs

⚠️  ne pas déployer (bug dans le form de contact)

### Divers

- ETQ utilisateur, le form de contact détecte les typos d'email et valide les champs avant de l'envoyer à HS (#10644)

### Technique

- Tech: initialise vraiment les nouveaux feature flags (#10668)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240729160650_create_contact_forms.rb`


--------------------------------------------

# 2024-07-30-01

Release [2024-07-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-30-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin sur une démarche en brouillon, l'attestation v2 est créé en brouillon si on a déjà une v1 active (#10660)
- Correctif : ouvre l‘assistant de routage à tous les types de champ conditionnables (#10602)
- ETQ Admin (amélioration), je veux pouvoir modifier des PJ sans recharger la page (#10603)

### Instructeur

- ETQ instructeur je peux voir les pjs envoyées par la messagerie dans l‘onglet "pièces jointes" (#10572)
- ETQ instructeur, corrige un problème quand on veut enlever la dernière colonne affichée dans le tableau (#10656)
- ETQ Instructeur, je peux choisir les pjs que j'exporte (en test sous feature flag) (#10548, #10655)

### Usager

- ETQ usager : correctif pour télécharger un dossier expiré (#10667)
- ETQ usager corrige la façon dont le lien de confirmation du compte est réutilisé (#10659)
- ETQ usager l'attestation v2 n'a pas ses titres chevauchés lorsqu'ils passent sur 2 lignes (#10649)
- Clarification du message pour se créer un compte à partir d'une adresse email (#10600)

### Accessibilité
- Accessibilité: permet au markdown FAQ d'intégrer n'importe quel attribut html dans les `<img>` (#10646)
- Améliore l'accessibilité des pages de contenu de la FAQ (#10632)
- Améliore l'accessibilité de la page d'authentification (#10647)
- Améliore l'accessibilité de la page mot de passe oublié (#10654)
- Accessibilité : Assure la restitution par les TA du bloc de suggestion de l'adresse mail (#10665)

### API
- Amélioration, ETQ consommateur de l'API graphql, je souhaite pouvoir modifier une annotation de type choix simple (#10642)

### Divers
- Correctif: le changement de thème est de nouveau fonctionnel (#10652)
- fix: missing https:// sur la page de déclaration d'accessibilité (#10663)

### Technique
- fix(sva/svr): fix spec with arbitrary date (#10664)
- chore(npm): update dependencies (#10658)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240713090744_add_dossier_folder_column_to_export_template.rb`


--------------------------------------------

# 2024-07-26-01

Release [2024-07-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-26-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager, je veux pouvoir accéder plus facilement au bouton "nouveau dossier" (#10624)
- ETQ usager, je retrouve les dossiers expirés dans l'onglet `récemment supprimé` pour pouvoir les restaurer au besoin (#10488)
- ETQ usager, les options sélectionnées d'une liste à choix multiple sont stylées aux couleurs du design système de l'etat (#10635)
### Correctif
- fix(dossier): ne crash pas quand un dossier n'a pas de service (#10650)
- fix(rebase): ne crash pas la montée de version d'un dossier quand une PJ est ajouté a la version du formulaire (#10651)

### Technique
- Tech : ne liste pas 2x le même domaine dans les CSP (#10645)
- Tech : simplifie le rafraichissement du footer concernant le service/regles de routage (#10636)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240715144209_add_hidden_by_expired_at_to_dossiers.rb`


--------------------------------------------

# 2024-07-24-01

Release [2024-07-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-24-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ usager je vois les infos de contact de mon groupe instructeur avant dépôt du dossier, une fois le(s) champ(s) de routage rempli(s) (#10430)

### Manager

- ETQ superadmin je peux supprimer les pjs associées aux champs non visibles avec une maintenance task (#10634)

### Divers

- refactor: remove champ factories (#10569)
- Mainteneur: ajoute une tache pour reinitialiser et prevenir des victimes d'hameconnage (#10643)

### Technique

- Tech: essaie de contourner les crash aléatoires du nettoyage HS sur le non respect du rate limit (#10628)


--------------------------------------------

# 2024-07-22-01

Release [2024-07-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-22-01)  sur GitHub

## Améliorations et correctifs

### Divers

- chore(deps): bump rexml from 3.3.0 to 3.3.2 (#10619)
- fix(intl): polyfill intl-listformat (#10621)
- Accessibilité: Amélioration de la page FAQ (#10622)
- Correction sur les avis (#10626)
- chore(champs): replace unique index (#10618)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240716075916_add_new_champs_unique_index.rb`
- `db/migrate/20240716091043_remove_old_champs_unique_index.rb`


--------------------------------------------

# 2024-07-16-01

Release [2024-07-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-16-01)  sur GitHub

## Améliorations et correctifs

### Usager

- Amélioration, ETQ usager je souhaite pouvoir copier/coller un siret contenant des espaces (#10616)

### Divers

- ETQ Mandant, je dois confirmer mon mail avant de recevoir des notifs (#10595)
- chore(champs): task to move non unique champs to a bad_data stream (#10617)
- refactor(dossier): use each instead of filter with side effects (#10613)
- Utiliser les nouvelles combobox pour les champs de sélection multiple (#10609)


--------------------------------------------

# 2024-07-15-01

Release [2024-07-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-15-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Correctif: ETQ administrateur je souhaite qu'un type de champ expression régulière ne bloque pas l'edition de ma demarche (#10608)
- Amélioration: ETQ usager, je souhaite que les PJs d'un champ qui n'est plus visible ne soient pas soumise a l'administration (#10607)

### Divers

- fix(vite): we have to use en-US beacuse en-GB dose not exists (#10599)


--------------------------------------------

# 2024-07-12-01

Release [2024-07-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-12-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Technique : rattrape les erreurs active storage lors de la création des variants et previews (#10601)
- update with lock (#10604)
- fix(education): query annuaire education in the browser (#10605)
- feat(combobox): trigger menu on focus (#10606)


--------------------------------------------

# 2024-07-11-01

Release [2024-07-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin et instructeur, lorsque je gère ma liste d'instructeurs sur une démarche, je suis guidé pour eviter les typos (#10579)

### Expert 

- ETQ expert, la vue d'un dossier respecte le bon layout (#10593)

### Technique

- chore(npm): update dependencies (#10589)
- Technique : crée des variants et previews via une maintenance task (#10596)
- chore(npm): update coldwired (#10597)
- fix spectaql (#10598)


--------------------------------------------

# 2024-07-10-01

Release [2024-07-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-10-01)  sur GitHub

## Améliorations et correctifs

### Divers

- refactor(champ): remove call to validation from value formatting code (#10587)
- fix(react): disable aria components locales optimization (#10594)


--------------------------------------------

# 2024-07-09-02

Release [2024-07-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-09-02)  sur GitHub

## Améliorations et correctifs


## Usager et instructeur 
- Corrige l'affichage du champ "choix simple" ayant plus de 20 options (#10592)


--------------------------------------------

# 2024-07-09-01

Release [2024-07-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-09-01)  sur GitHub

## Améliorations et correctifs

⚠️ Oops, ne déployez pas cette version ! Passer directement à [2024-07-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-09-02) 

### Administrateur

- ETQ usager et admin, je suis prévenu au moment de commencer un dossier sur une démarche ou révision en test (#10577)
- ETQ Admin, je peux associer ma démarche aux zones Autorité indépendante et Chambre parlementaire (#10583)

### Usager

- ETQ usager, corrige l'alignement de la page demande d'un dossier en construction (#10575)
- ETQ usager: je ne peux ajouter qu'une unique PJ pour les anciennes procédures qui l'exigent (#10511)
- ETQ usager, je peux saisir le numéro d'accréditation COJO avec le suffixe `-01` (#10590)

### Divers

- chore(js): update coldwired, react and combobox (#10404, #10586)
- refactor(js): use superstruct instead of zod (#10585)

### Technique

- Tech: Normalisation des champs de type number/decimal pr enlever les espace blanc à la saisi (#10555)
- Tech: cookies avec flag `secure` et `httponly` (#10576)


--------------------------------------------

# 2024-07-02-01

Release [2024-07-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-02-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, un nouveau champ est obligatoire par défaut (#10547)
- ETQ administrateur, lorsque je gère ma liste d'expert invités sur une démarche, je suis guidé pour eviter les typos (#10524, #10574)
- ETQ admin, correctif: ne détruit pas l'attestation v1 en allant sur la page pour préparer une v2 (#10573)


### Instructeur

- ETQ nouvel instructeur, je dois confirmer mon mail (#10549)
- ETQ instructeur, le sommaire sur la page demande s'affiche uniquement sur les écrans XL (#10561)

### Usager

- ETQ usager, je peux télécharger mon dossier sans avoir à passer par le bouton "imprimer" (#10559)

### Manager

- ETQ superadmin, je peux accéder aux informations essentielles des dernières démarches publiées (#10565)
- ETQ superadmin: export administrators and instructeurs list (#10489)

### Technique
- Tech (tests): default administrateur fixtures (#10483)
- Tech (helpscout): corrige encore plus mieux le rate limiting (#10568)
- Technique : crée des variants et prévisualisations des pjs pour les dossiers récents (#10556)
- Tech: corrige le nom de l'application hardcodé dans un email (#10567)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240619205011_change_types_de_champ_mandatory_default.rb`

Cette version comporte des migrations du contenu des données :
- `lib/tasks/deployment/20240625151936_create_variants_for_pjs.rake`
- `lib/tasks/deployment/20240625151948_create_previews_for_pjs.rake`


--------------------------------------------

# 2024-07-01-01

Release [2024-07-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-07-01-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin/instructeur, dans les exports, je souhaite récupérer la valeur d'un champ decimal même si celle ci contient un/des espaces blanc (#10554)

### Divers
- Améliore l'accessibilité de la page de contact (#10553)
- Améliore l'accessibilité de la page des mentions légales (#10563)
- Améliore l'accessibilité, correction des listes mal structurées (#10538)

### Technique

- Tech (helpscout): améliore la gestion du rate limit (#10551)
- Tests: fix JS log events (#10546)


--------------------------------------------

# 2024-06-25-01

Release [2024-06-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-06-25-01)  sur GitHub

## Améliorations et correctifs

⚠️ Si vous testez sous feature flag l'attestation v2 en production, passez directement à [2024-07-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/2024-07-02-01) qui corrige un bug qui supprimait  l'attestation v1 avant que la v2 soit prête.

### Administrateur


- ETQ administrateur, je peux naviguer dans l'editeur des annotations en utilisant le sommaire façon editeur des champs du formulaire (#10531)
- ETQ admin je peux activer la délivrance des attestations v2 (sous feature flag) (#10465)
- ETQ admin: Corrige l'url pour accéder à la documentation sur la page expert (#10542)
- Correctif: ETQ admin, je ne peux pas activer l'inéligibilité des dossiers sans règles d'inéligibilité (#10534)
- Correctif: ETQ admin, j'aimerais pouvoir modifier le message de confirmation de dépôt de dossier (#10522)
- Adapte le titre de la page contact admin pour le dark mode (#10541)
- Correctif: ETQ Admin, Instructeur: fix traduction dans la page lien envoyé (#10517)

### Instructeur

- ETQ instructeur, je peux naviguer dans le formulaire de l'usager via un sommaire, idem pour les annotations privées. (#10536)
- ETQ instructeur le nombre de dossier par page passe de 25 a 100 (#10535)

### Usager

- ETQ usager : j'accède au bouton JDMA depuis la page Merci et depuis le mail de dépôt de dossier (#10502)
- Correctif ETQ usager : bouton "Afficher toutes les erreurs" s'affiche en français (#10519)
- Correctif ETQ usager, je souhaite pouvoir envoyer une invitation à un email qui n'est pas deja un usager DS (#10543)


### Accessibilité 
- Correction de la structure de navigation (#10530)
- Mise à jour de la décalaration d'accessibilité (#10523)
- Supprime le débordement du menu en version mobile sur les terminaux de faible largeur (#10533)
- Pied de page - correction des titres de liens (#10537)
- Restaure la visibilité de la prise de focus dans le sélecteur de langue (#10545)

### Divers
- Pages stats au DSFR, déclinées en thème sombre et adaptées aux mobiles (#10525)
- Emails: réduit légèrement la taille du nom du logo ds.fr (#10532)
- FAQ: précise périmètre d'application de DS (#10433)

### Opérateur
- ETQ opérateur, le form de contact devient asynchrone et l'antivirus passe sur les PJ (#10515)
- ETQ opérateur je peux supprimer les vieilles conversations helpscout (#10513)

### Technique

- fix(eligibilite_rules): checking for eligibilites rules should not cache champs.visible (#10527)
- Tech (kredis): configure la connection `shared` qui est celle par défaut (#10520)
- Tech: réduit le connect timeout de redis d'1s à 0.2s (#10514)
- tech(mail_checker): rescue Mail::Field::IncompleteParseError (#10526)
- Tech: lance les jobs à des horaires légèrements différents pour faciliter le débug (#10518)
- Technique : mise à jour de la galerie et transformation des images uniquement côté jobs (#10512)
- fix(graphql): use null_session forgery protection on graphql controller to allow open data requests (#10496)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240514164228_add_state_to_attestation_templates.rb`
- `db/migrate/20240522084927_add_attestation_template_unicity_index.rb`

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20240528155104_backfill_attestation_template_v2_as_draft.rake`


--------------------------------------------

# 2024-06-13-01

Release [2024-06-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-06-13-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Tech: n'indexe plus les dossiers en autosave pour ne plus pénaliser le délai de délivrance de certains emails (#10516)
- Tech : plus de simplecov en local (#10510)


--------------------------------------------

# 2024-06-11-01

Release [2024-06-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-06-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin: je souhaite que le lien JDMA soit réinitialisé lors du clonage d'une procédure (#10507)
- ETQ administrateur, je peux ajouter des conditions d'eligibilité auxquelles les dossiers doivent correspondre sans quoi l'usager ne peut déposer son dossier (#10292)
- ETQ admin, je veux que mes conditions d'annotations privées ne soient pas en erreur pour des raisons d'ordre (#10508)

### Usager

- Amélioration: ETQ usager, je souhaite pouvoir télécharger mon dossier sur la page de confirmation de dépôt (#10497)
- tech: ETQ usager je souhaite une verification de mes mails plus efficace (#10499)

### Manager

- Bouton copier/coller: améliore support quand le clipboard n'est pas disponible (#10506)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240409075536_add_transitions_rules_to_procedure_revisions.rb`
- `db/migrate/20240514075727_add_dossier_ineligble_message_to_procedure_revisions.rb`
- `db/migrate/20240516095601_add_eligibilite_dossiers_enabled_to_procedure_revisions.rb`


--------------------------------------------

# 2024-06-10-01

Release [2024-06-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-06-10-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- [admin] Simplifier interface en déplacant la tuile de modifs de l'historique du formulaire (#10464)
- Correctif: ETQ admin, je souhaite pouvoir utiliser des champs public pour conditionner une annotation privée (#10503)

### Instructeur

- ETQ instructeur, dans la galerie, je peux prévisualiser des pdfs et voir des images au format rare (#10449)

### Usager

- ETQ usager, améliore le thème sombre et responsivité de la page contact (#10296)

### API

- API v1: fix régression commentaire -> attachment url qui avait sauté (#10505)

### Divers

- Correctif (galerie) : catche les erreurs de type ActionViewTemplate (#10504)

### Technique

- Tech: log request_id enqueueing a job (#10495)


--------------------------------------------

# 2024-06-06-01

Release [2024-06-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-06-06-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ Admin, les erreurs liés aux formulaire usager et instructeurs sont plus consistantes (#10479)
- Correctif ETQ admin je peux consulter la page "toutes les démarches" sans zone sélectionnée (#10492)
- ETQ admin: amélioration visuelle dans les instructions de bienvenue (#10470)
- Admin: amélioration configuration des experts invités (#10463)

### Instructeur

- Corrige l'envoi d'emails d'ajout à un groupe instructeurs (#10485)
- Amélioration, ETQ instructeur, je souhaite que les avis soient ordonnancés dans plus recent au plus ancien (#10493)
- Instructeur: corrige quelques bugs de la feature export template (renommage dans le zip) (#10454)

### API

- API Graphql: les messages (de la messagerie) peuvent être annulés (#10487)

### Technique

- Tech (sécurité): rails 7.0.8.3 => 7.0.8.4 (#10491)
- ETQ tech, j'aimerais que la suite de test aille plus vite (#10478)
- feat(champ): add updated_by column (#10415)
- fix(api): public api v1 should not inherit from api v1 (#10486)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240513140508_add_updated_by_to_champs.rb`


--------------------------------------------

# 2024-06-03-01

Release [2024-06-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-06-03-01)  sur GitHub

## Améliorations et correctifs

### Manager

- ETQ SuperAdmin, je peux débloquer les emails d'un usager (#10481)

### Divers

- chore(js): remove vite legacy build (#10070)
- Fix BUG - Ajoute la méthode Acronomyze comme helper (#10482)

### Technique

- chore(npm): update build dependencies (#10484)


--------------------------------------------

# 2024-05-31-02

Release [2024-05-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-31-02)  sur GitHub

## Améliorations et correctifs

### Divers

- ETQ Mainteneur, les emails non vérifiés ne sont pas envoyés (#10461)


--------------------------------------------

# 2024-05-31-01

Release [2024-05-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-31-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Harmonisation de la navigation dans l'interface de configuration (#10459)

### Instructeur

- fix(instructeur): supprimes_recemment should include archived dossiers (#10466)

### API

- feat(graphql): expose last_champ_updated_at and last_champ_private_updated_at on api (#10455)

### Divers

- fix(dossier): batch operations to termine dossier should send emails (#10467)
- fix(dossier): handle missing siret information when dossier passe en instruction (#10468)
- Amélioration de la page toutes les démarches :  Quick Wins (#10437)
- chore(task): run commune code fix on all champs of a procedure (#10469)
- chore(js): lazy load lightbox and tiptap (#10474)
- fix(spec): maj broken spec on main (#10477)
- fix(css): do not inline all DSFR svg (#10476)

### Technique

- Technique : ajout de la license key pour la galerie de pièces jointes (#10379)
- Tech: corrige le nom par defaut de la bulk_email_queue (#10471)
- Tech: block l'envoi de mails à des adresses douteuses (#10472)

## Notes de déploiement

Cette version comporte des migrations du contenu des données :
- `lib/tasks/deployment/20240530090353_block_dubious_email.rake`
- `spec/lib/tasks/deployment/20240530090353_block_dubious_email_spec.rb`


--------------------------------------------

# 2024-05-28-01

Release [2024-05-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-28-01)  sur GitHub

## Améliorations et correctifs

### Divers

- refactor(champs): update champs by public_id [remove dead code] (#10340)
- Mise en place de l 'infrastructure de rejet d'envoi de mails non confirmés (#10456)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240524120336_add_email_verified_at_column_to_users.rb`
- `db/migrate/20240527090508_add_email_verified_at_column_to_individuals.rb`


--------------------------------------------

# 2024-05-27-01

Release [2024-05-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-27-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur, je peux renommer le contenu de mon export zip (#10217) (sous feature flag `export_template` pour le moment)

### Accessibilité

- Page de contact - suppression des paragraphes vides (#10414)

### Technique

- Tech: transition ImageProcessorJob to sidekiq (#10441)
- Tech (css): maintient les labels de toggle sur une seule ligne (#10443)
- Tech: ajoute une maintenance task pour recalculer les checksums de pj erronées (#10387)
- Tech: limite l'engorgement des DossierIndexSearchTermsJob et rétablit la rapidité du scan antivirus (#10448)
- chore(ts): improuve some types (#10439)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240130154452_create_export_templates.rb`
- `db/migrate/20240131094915_add_template_to_exports.rb`
- `db/migrate/20240131095645_add_export_template_fk.rb`
- `db/migrate/20240131100329_validate_export_template_fk.rb`


### Information importante : Redis

L'usage de redis s'accroit au fil des mois : passage de delayed job à sidekiq, optimisations fonctionnelles / cache sous plusieurs formes. A ce stade, la dépendance à redis reste optionnelle, c'est à dire que le code continue de fonctionner sans redis.

Depuis [la release 2024-05-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/edit/2024-05-22-01)  l'indexation des recherches devient asynchrone avec un job, après chaque opération susceptible de modifier les données nécessaires à la recherche (par exemple: changement dans un champ ou d'une annotation privée).
Pour limiter le nombre de jobs  à traiter pour un même dossier, un _debounce_ est mis en place pour n'exécuter qu'un seul job par dossier par tranche de 5 minutes. Pour fonctionner ce debounce utilise l'instance de cache de redis. S'il n'y a pas redis, il n'y a pas de debounce. Par conséquence pour un même dossier, à chaque sauvegarde du formulaire un job sera à traiter : en fonction de la volumétrie et taille des démarches, ceci pourrait provoquer un engorgement des jobs à traiter.

Si ce n'est pas encore fait, nous vous encourageons donc vivement à **mettre en place un serveur redis [au minimum pour le cache](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/blob/0b609402eea0537d2b72528e3ee31d82c4b30e85/config/env.example.optional#L232)** .


--------------------------------------------

# 2024-05-22-01

Release [2024-05-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-22-01)  sur GitHub

## Améliorations et correctifs

ℹ️ Cette version provoque des certains problèmes d'encombrement de la file `default` des jobs, privilégiez [2024-05-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/edit/2024-05-27-01) qui le résout ou plus tard


### Administrateur

- ETQ admin corrige l'acceptation de dossier d'une démarche brouillon avec accusé de lecture (#10426)
- ETQ Administrateur les actions sur un champ dans l'editeur (déplacement, suppression,...) sont rassemblés en bas de chaque champ (#10356)

### Instructeur

- ETQ instructeur, les images sont tournées dans le bon sens (#10364, #10440)

### Usager

- Correctif: ETQ usager, lorsque je clone mon dossier, je ne clone pas les PJs des annotations privées (#10435)

### Technique
- Tech (sécurité): update rails 7.0.8.1 => 7.0.8.3 (#10436)
- Tech: bump rexml from 3.2.6 to 3.2.7 (#10429)
- Tech (perf): ignore les `search_terms` colonnes pour ne plus les select à chaque query et debounce l'indexation (#10261)
- Correctif: MaintenanceTask pour corriger les dossiers declarés pour un tiers mais sans avoir renseigné les infos du tiers (#10423)
- Tech (carte): ignore silencieusement les params invalides (#10422)
- Tech (CI): agressive timeout because system tests sometimes hang forever (#10428)


--------------------------------------------

# 2024-05-16-02

Release [2024-05-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-16-02)  sur GitHub

## Améliorations et correctifs

### Divers

- Internalisation de la FAQ pour améliorer son accessibilité (#10137)


--------------------------------------------

# 2024-05-16-01

Release [2024-05-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-16-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Amélioration : ETQ admin, je veux comprendre pourquoi je ne peux pas personnaliser mes mails si l'accusé de lecture est activé (#10417)

### Usager

- Correctif : ETQ usager, je ne peux pas mettre mon dossier dans un etat invalide en essayant le bouton deposer pour un mandataire sans remplir les autres champs du mandataire (#10420)

### Divers

- chore(deps): bump nokogiri from 1.16.4 to 1.16.5 (#10416)
- refactor(champs): change views to use new urls with stable_id and row_id (#10371)
- Tech: l’affichage du bandeau .gouv.fr sticky à l'user (#10419)
- chore: expose postgres port (#10148)
- fix(dossier): fix n+1 on header sections (#10421)


--------------------------------------------

# 2024-05-13-03

Release [2024-05-13-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-13-03)  sur GitHub

## Améliorations et correctifs

### Correctifs instructeurs 
- Correction d'un crash sur les démarches avec le champ "menus déroulants liés" (#10411)
- Correction d'exports lorsqu'un champ sur une ancienne révision ne correspond pas au bon type de champ (#10412)

### Usager
- Accessibilité: Restaurer l'affichage du label "Filtrer par démarche" sur les pages Usager (#10396)
- Accessibilité: Amélioration de la hiérarchie de titres sur les pages de listing et suppression d'un lien vide (#10393)
- Corrige des fautes de frappe (#10413)


--------------------------------------------

# 2024-05-13-02

Release [2024-05-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-13-02)  sur GitHub

## Améliorations et correctifs

⚠️ Déployer directement [2024-05-13-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-13-03) pour les derniers correctifs

### Administrateur

- ETQ Administrateur, DS m'informe que les titres d'identité ne sont pas disponible par zip ni api (#10408)
- ETQ Administrateur, je vois le numero de ma démarche avec un separateur de millier (#10399)

### Instructeur

- Correctif: ne plante plus lors de la génération d'attestations pour les champs départements (#10409)

### Technique

- Tech: tag sentry `procedure` sur le job d'exports pour faciliter le debuggage (#10410)


--------------------------------------------

# 2024-05-13-01

Release [2024-05-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-13-01)  sur GitHub

## Améliorations et correctifs

⚠️ Déployer directement [2024-05-13-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-05-13-03) pour les derniers correctifs


### Administrateur
- Correctif: Une démarche modèle clonée n'est plus modèle (#10405)

### Instructeur

- ETQ Usager / Instructeur, je peux ajouter plusieurs PJ d'un coup dans la messagerie (#9986)
- ETQ Usager / Instructeur : mise à jour UI de la galerie (#10389, #10375)

### Usager

- Accessibilité:  Les champs répétables et les cases à cocher seuls n'englobent pas leurs champs enfant dans un `fieldset` si il n'y a qu'un seul champ enfant.  (#10309)
- ETQ usager: Amélioration du wording de la pagination des éléments affichés sur une seule page (#10394)
- ETQ usager: légère amélioration du message à propos d'une démarche close (#10402)
- ETQ usager, redirection vers le site de info.gouv.fr à la place de gouvernement.fr (#10407)

### Super-Admin
- ETQ super-admin, je peux conditionner l'affichage du bloc " Pour un bénéficiaire : membre de la famille, proche, mandant, professionnel en charge du suivi du dossier…" d'une démarche (#10346)


### Technique
- refactor(export): move formatting logic to type de champ (#10370)
- fix(task): fix BackfillCommuneCodeFromNameTask (#10382)
- chore(vite): use native esm modules and remove vite warning (#10380)
- chore(deps): bump sidekiq from 7.2.2 to 7.2.4 (#10385)
- fix(ci): attempt to fix ci runs (#10381, revert #10383)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240417053843_add_column_for_tiers_enabled_to_procedure.rb`


--------------------------------------------

# 2024-04-24-01

Release [2024-04-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-24-01)  sur GitHub

## Améliorations et correctifs


### Technique
- Tech: upgrade ruby 3.3.0 => 3.3.1 et quelques gems (#10378)
- chore(spec): move spec in the correct folder (#10376)
- chore(patron): build real demarche and dossier for page patron (#10377)
- doc(metrics): strongly suggest using local address in prometheus exporter doc (#10373)


--------------------------------------------

# 2024-04-23-02

Release [2024-04-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-23-02)  sur GitHub

## Améliorations et correctifs
### Instructeur

- ETQ instructeur: rétablit l'affichage du filigrane des Titres d'identité (#10374)

### API

- ETQ developpeur, je souhaite pouvoir utiliser le playground même si je ne suis pas administrateur (#10369)

### Tech

- fix(dossier): fix and optimize dossier projection service (#10372)
- chore(npm): update lock file (#10368)


--------------------------------------------

# 2024-04-23-01

Release [2024-04-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-23-01)  sur GitHub

#:information_source: on place la feature de prévisualisation des images sous feature flag `galery_demande` afin de tester la feature plus tranquillement. 

## Améliorations et correctifs

### Divers

- Tech: codecov only informational (#10366)
- Correctif galerie : affichage pdf + feature flag sur galerie dans la page demande (#10367)
- refactor(champs): do not depend on attributes hash key in old code (#10365)


--------------------------------------------

# 2024-04-22-02

Release [2024-04-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-22-02)  sur GitHub

#:warning: cette release est considérée comme instable toujours pour cause de téléchargement intempestif de grosses images.

## Améliorations et correctifs

### Divers

- Tech: deplace d'autres jobs sur sidekiq (#10361)
- Tech: corrige le chemin des tests spec/models/concerns (#10362)
- refactor(champs): update champs by public_id [controllers] (#10328)
- Tech: active le code coverage avec simplecov & codecov (#10363)


--------------------------------------------

# 2024-04-22-01

Release [2024-04-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-22-01)  sur GitHub

#:warning: cette release est considérée comme instable : la prévisualisation des images ne se faisant pas sur des vignettes (aka thumbnail aka variant) elle peut provoquer une hausse significative du trafic.


## Améliorations et correctifs

### Administrateur

- Tous les admins voient la tuile pour configurer le SVA/SVR. L'activation effective reste soumise à validation par l'équipe bizdev (#10352)

### Instructeur

- 🎉 ETQ instructeur et usager je peux voir les pièces jointes dans une galerie (#10281)

### Usager
- Correctif: utilise toujours `dossier.user_email_for` à la place de `dossier.user.email`  (#10347)

### Tech
- ETQ tech je peux re-router les dossiers d'une démarche si le routage a été configuré après publication - suite (#10311)
- Tech: ajoute metrics yabeda sur l'API graphql (#10265)
- Tech: repare le job de signature des operations (#10348)
- Tech: pour le prestataire de mail dolist, choisit le bon sender_id en fonction du domaine utilisé (migration gouv.fr) (#10357)

## Notes de déploiement 

Les instances qui utilisent Dolist peuvent paramétrer la variable `DOLIST_DEFAULT_SENDER_ID` (par défaut à 1 = le premier) pour choisir un autre "sender". Cf #10357


--------------------------------------------

# 2024-04-19-01

Release [2024-04-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-19-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Instructeur: autocomplete les experts ne s'étant pas connectés si la liste est controllée par l'administrateur (#10358)


--------------------------------------------

# 2024-04-18-01

Release [2024-04-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-18-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager, j'aimerais que les champs siret soient mieux verbalisés par le screen reader (#10316)
- ETQ Usager, je souhaite que mon screen reader verbalise les erreurs sur les champs unique contenu dans un `fieldset` (#10317)

### Divers

- ETQ usage, je souhaite que les aides à la saisie soient vocalisées par le screenreader (#10313)
- tech(champs.validators): dry and standardize champs.validations (#10250)
- Tech: essaye de corriger un test non fiable relatif aux adresses (#10345)
- chore(build): use bun instead of node (#10269)
- Tech: upgrade DSFR 1.10 => 1.11 (#10341)
- Tech: replace le job de migration des logs fontionnelles sur une queue standard (#10353)
- Tech: supprime param de config `timeout` inutile pour postgresql (#10350)
- Tech: fix default url host for .gouv.fr (#10355)


--------------------------------------------

# 2024-04-17-02

Release [2024-04-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-17-02)  sur GitHub

## Améliorations et correctifs

### Divers

- [fix] Probleme de titre de la tuile template de mail accusé de reception (#10343)
- Tech: repare le multi france connect (#10344)


--------------------------------------------

# 2024-04-17-01

Release [2024-04-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-17-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin: form des infos de démarche plus lisible (#10286)

### Instructeur

- ETQ instructeur: lorsque je demande des avis externes, ds suggere uniquement des utilisateurs s'étant connectée au moins 1 fois (#10338)
- ETQ instructeur, fix position de la pastille de notification (#10339)

### Divers

- hack: always use default redirect_uri (#10342)


--------------------------------------------

# 2024-04-16-03

Release [2024-04-16-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-16-03)  sur GitHub

## Améliorations et correctifs

### Usager

- Usager: fix champ adresse dans un TOM sans code postal dans la BAN (ex. Nouvelle-Calédonie à Nouméa) (#10336)

### Divers

- Tech: deplace la migration des données fonctionnel vers le stockage ovh sur sa propre file (#10335)
- refactor(champ): remove stable_id delegate to type_de_champ (#10327)


--------------------------------------------

# 2024-04-16-02

Release [2024-04-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-16-02)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ Admin je peux configurer une démarche avec accusé de lecture (#10190)
- Changement de l'URL de la vidéo de démo sur la page d'accueil des admins (#10331)


## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240319150925_add_accuse_lecture_to_procedures.rb`
- `db/migrate/20240321152801_add_accuse_lecture_agreement_to_dossiers.rb`


--------------------------------------------

# 2024-04-16-01

Release [2024-04-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-16-01)  sur GitHub

## Améliorations et correctifs

### Divers

- Tech: corrige un test non fiable (#10326)
- Pages d'erreur personnalisées / au dsfr (#10235)
- Tech: ne lance pas de job d'analyse antivirus pour les fichiers crés par l'application (#10329)
- perf(champs): add stable_id index on champs (#10330)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240415192417_drop_virus_scan_table.rb`
- `db/migrate/20240416062900_add_stable_id_index_to_champs.rb`


--------------------------------------------

# 2024-04-15-01

Release [2024-04-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-15-01)  sur GitHub

## Améliorations et correctifs

Nous considérons cette version suffisamment **stable** ™️ pour être un point de passage recommandé pour les autres instances.

### Usager

- ETQ usager, je ne peux supprimer l'unique element d'une repetition obligatoire (#10308)

#### Accessibilité usager
- ETQ Usager, je souhaite avoir des messages d'erreur homogènes et comprehensibles (#10318)
- ETQ usager : pas d'`aria-invalid` (#10315)
- ETQ usager, je veux que tous les champs aient un style DSFR (#10312)
- ETQ Usager je souhaite que le champ carte soit plus accessible (#10314)

### Technique

- Tech: debloque un le cron de nettoyage des blobs en limitant le périmètre à une semaine (#10310)
- Tech: ajoute une tache de maintenance pour rattraper les dossier operation log qui n'ont leur data sur le cold storage (#10321)
- Tech: pas d'erreur si un usager essaie d'ajouter une ligne d'un champ répétition sans enfant (#10325)
- fix(commune): no crash on blank codes (#10323)
- fix(schema): drop bulk_messages_groupe_instructeurs (#10305)
- chore(task): backfill stable_id with custom limit (#10322)
- chore(task): backfill commune code from name task (#10324)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240411164502_drop_bulk_messages_groupe_instructeurs.rb`

### Maintenance tasks
Cette version étant considérée stable, pensez à jeter un oeil aux **maintenance tasks** depuis le manager , lancez celles qui vous paraissent appropriées, (notamment celles sans argument). Certaines sont indispensables pour fonctionner avec le code de cette release, d'autres permettent de corriger des données sur les démarches, dossiers, champs etc…

### Jobs

Si vous avez activé **sidekiq**, voici les queues à activer avec leurs priorités respectives : 
- Process 1 : default: 4, mailers: 2, active_storage_analysis: 2, sva: 2, webhooks_v1: 2, cron: 2, api_entreprise: 1
- Process 2 : low_priority: 2, purge: 1

Nous recommandons vivement d'avoir 2 processus distincts, pour éviter que des jobs non prioritaires mais parfois très nombreux et longs viennent perturber les jobs prioritaires comme les emails.
A ce stade delayed job est toujours supporté pour l'ensemble des queues.


--------------------------------------------

# 2024-04-11-02

Release [2024-04-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-11-02)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin: améliore le style de la page oubliée "Envoyer une copie de ma démarche" (#10303)

### Divers

- Tech: corrige le job de suppression des procédures cachées sans dossiers (#10299)
- ETQ tech je peux relancer le routage pour tous les dossiers en construction d'une démarche (#10293)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240411091345_drop_attestation_template_id_from_procedure_revisions_table.rb`


--------------------------------------------

# 2024-04-11-01

Release [2024-04-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ Admin, quand je deplace les champs apres d'autre champ, ne pas griser le champs precedent (#10298)

### Instructeur

- ETQ instructeur, j'ai une colonne vide s'il n'y a pas les checkbox des actions multiples (#10270)

### Divers

- Tech: config sidekiq indépendante de redis sentinels & fix cron (#10295)
- Tech: améliore la performance du job du stockage a froid des traces fonctionnelles (#10297)
- fix(stable_id): recursive job to fill stable_ids (#10300)
- Tech: simplifie le code address dans les champs commune (#10301)
- chore(db): ignore attestation_template_id (#10302)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240410193614_add_partial_index_on_dol.rb`


--------------------------------------------

# 2024-04-10-01

Release [2024-04-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-10-01)  sur GitHub

## Améliorations et correctifs

### Usager

- [UX] Améliorer les filtres d'un dossier sur le tableau de bord d'un usager (#10280)

### Divers

- Tech: correction d un bug concernant les city_name (#10294)


--------------------------------------------

# 2024-04-09-02

Release [2024-04-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-09-02)  sur GitHub

## Améliorations et correctifs

### Divers

- Correction: ajoute une tache de maintenance pour re remplir l attribut  city_name manquant de certaines adresses (#10290)
- Tech: déplace APIEntreprise vers sidekiq (#10271)


--------------------------------------------

# 2024-04-09-01

Release [2024-04-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-09-01)  sur GitHub

## Améliorations et correctifs

### Accessibilité

- Amélioration de l'accessibilité de la page de contact. (#10266)
- Remplacement de l'attribut `aria-current="page"` par `aria-current="true"` lorsque le lien concerne un ensemble de pages (#10283)

### Technique

- Tech: correction d'une typo dans le fichier sidekiq transition (#10284)
- fix(adresse): fallback to city name if commune not found (#10282)
- Tech (jobs): (re)enqueue crons dans sidekiq (#10288)
- Tech: n'assigne pas le domaine préféré à l'inscription/connexion si la feature n'est pas activée (#10289)
- chore(npm): update dependencies (#10063)


--------------------------------------------

# 2024-04-08-01

Release [2024-04-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-08-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Tech: corrige le mécanisme de surveillance des fournisseurs d entreprise.api.gouv.fr (#10275)
- feat(champ): add paths to type_de_champ (#10234)
- Tech:  supprime les fetch_external_data_job lorsque le champ correspond n'existe plus (#10273)
- fix(i18n.format): fix date format (#10277)
- Tech: déplace les cron job sur sidekiq (#10268)
- perf(filter): we query by stable_id - no need to check type_de_champ private attribute (#10171)


--------------------------------------------

# 2024-04-05-01

Release [2024-04-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-05-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ Administrateur : Amélioration de l'accessibilité et de l'ergonomie de la liste d'édition du formulaire "déplacer après" (#10255)

### Divers

- chore(browser): be more agressive about browser deprecation (#10257)
- chore(task): another attempt to backfill stable_id (#10267)
- fix(stats): more accurat stats about deleted dossiers (#10259)
- fix(champ): do not validate hidden champs (#10272)
- Tech: empeche temporairement de setter le domain préféré (#10276)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240402212902_add_depose_at_to_deleted_dossiers.rb`


--------------------------------------------

# 2024-04-04-01

Release [2024-04-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-04-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Correctif : affiche correctement le lien vers la page de fermeture dans la liste des dossiers (#10230)

### Usager
- Correctif: redirige la page fermeture vers la page commencer si la démarche n'est plus fermée (#10232)

### Divers
- Tech: petites corrections pour le .gouv (#10231)
- Tech (mailers): `from`, liens et application en respectent le preferred domain du destinataire pour le passage au .gouv (#10224)


### Technique
- fix(champ): do not expose champs without row_id in repetitions (#10256)
- Tech: verifie que la locale est disponible (#10258)
- chore(js): use bun instead of yarn (#10248)
- Tech: renommage de la file de job `expires` en `purge` (#10262)

### Informations pour les instances
- Les dimensions du logo de l'application utilisé pour les emails, référencé par la variable `MAILER_LOGO_SRC` ont changé: plutôt qu'utiliser un format allongé (bannière), il faut maintenant utiliser un logo à peu près carré (le nom de l'application sera écrit en html à côté). Par défaut c'est le logo avec marianne et _Réplique Française_ Cf #10224

- Nous avons remplacé `yarn` par [bun](http://bun.sh/docs/installation) comme runtime JS et package manager. Sur un poste de dev, cela signifie qu'une fois bun installé, il faut supprimer le regénérer le répertoire `node_modules` en le supprimant puis faire `bun install` à la place de `yarn install` etc… Le process de déploiement est affecté de la même manière. Cf #10248

- De plus en plus de jobs otn été migrés récemment de delayed job vers sidekiq. A ce stade, les files suivantes doivent être traitées par sidekiq avec les poids, idéalement avec 2 process (queues) pour éviter que des jobs non prioritaires impactent les jobs plus prioritaires

  - Queues 1 : `default: 4, mailers: 2, active_storage_analysis: 2, sva: 2, webhooks_v1: 2`
  - Queues 2 : `low_priority: 2, purge: 2`


--------------------------------------------

# 2024-04-02-03

Release [2024-04-02-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Tech: petites corrections pour éviter les réessais inutile sur les jobs de mail et de rnf (#10243)
- chore(task): reduce batch size of fill stable id task (#10253)
- Tech: ajoute des metadata pour débugger un pb sur l'adresse controller (#10252)
- Tech (exports): télécharge les PJ > 10mb en chunks pour ne pas les mettre en mémoire (#10251)


--------------------------------------------

# 2024-04-02-02

Release [2024-04-02-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-02)  sur GitHub

## Améliorations et correctifs

### Instructeur

- Correctif(annotations privées): ETQ instructeur, je ne pouvais modifier une annotation quand un usager avait saisi des valeur de champs incorrectes (#10242)
- correctif(lien.attestation): ETQ expert, je souhaite avoir accès à l'attestation même si je ne suis pas instructeur (#10245)

### Usager

- ETQ usager : Champ choix multiple, ajouter une aide a a la saisie (#10220)

### Divers

- Tech: fix memory leak pendant le téléchargement des PJ des exports (#10247)
- Tech: ajoute plus de job a sidekiq (#10240)

### Informations pour les instances

Voir les notes de release [2024-04-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-04-01) pour le statut sur la migration delayed job => sidekiq


--------------------------------------------

# 2024-04-02-01

Release [2024-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-01)  sur GitHub

## Améliorations et correctifs

### Technique

- fix(dossier): enqueue jobs after commit (#10246)


--------------------------------------------

# 2024-03-28-02

Release [2024-03-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-28-02)  sur GitHub

## Améliorations et correctifs

💣 Ne pas déployer, bug dans les emails de dépôt de dossier, déployer [2024-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-01) ou ultérieur

### Usager

- ETQ usager : ajout d'un lien vers la messagerie dans les dossiers en attente de correction (#10238)

### Divers

- chore(task): optimize maintenance task (#10241)


--------------------------------------------

# 2024-03-28-01

Release [2024-03-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-28-01)  sur GitHub

## Améliorations et correctifs

💣 Ne pas déployer, bug dans les emails de dépôt de dossier, déployer [2024-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-01) ou ultérieur

### Administrateur

- ETQ admin, je veux que le champ nombre décimal ne prenne que 3 chiffres après la virgule (#10143)

### Technique

- CI: block fixup commit merge (#10233)
- tech(export_job): sometimes ExportJob are OOMed, in those cases jobs are stuck and never retried. release lock and increase attempts (#10215)
- add yabeda sidekiq to export metrics in promotheus format (#10237)


--------------------------------------------

# 2024-03-27-01

Release [2024-03-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-27-01)  sur GitHub

## Améliorations et correctifs

💣 Ne pas déployer, bug dans les emails de dépôt de dossier, déployer [2024-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-04-02-01) ou ultérieur

### Administrateur

- ETQ Admin, je souhaite voir l'éditeur de mail adapté au dark mode (#10216)

### Technique

- Tech: ajoute gem `rails-pg-extras` (#10173)
- Tech: supprime l'index inutilisé dossiers#hidden_at (#10207)
- chore(deps-dev): bump rdoc from 6.6.2 to 6.6.3.1 (#10212)
- Tech: les jobs de mail utilisent sidekiq (#10228)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240325161743_remove_index_dossiers_on_hidden_at.rb`


--------------------------------------------

# 2024-03-26-01

Release [2024-03-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-26-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager je vois un message d'alerte dans la liste de mes dossiers sur mes dossiers terminés sur une démarche close (#10132)

### API

- Correctif API Entreprise: transmet l'APPLICATION_NAME en `context` (#10218)

### Divers

- feat(textarea): autoresize textareas (#10079)

### Technique

- Tech: ajoute la gem reliable-fetch vendored dans gitlab (#10179)
- Tech: retire l'index inutilisé champs#private (#10205)
- Tech: bannière indiquant staging / env de test plus visible (#10203)
- fix(migration): use strong migration to add fk without validation first, then apply fk validation (#10195)
- fix(champ): use public_id in views (#10169)
- Tech: corrige protocole http -> https pour tester la connectivité sur la favicon en prod (#10183)
- Correctif : supprime les closing attributes lors du clonage d'une démarche (#10182)
- Technique : Mise à jour de l'association bulk messages - procédures (#10180)
- chore(attachment): remove unused replace code (#10196)


## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240325140537_add_procedure_foreign_key_to_bulk_messages.rb`
- `db/migrate/20240325140538_validate_fk_on_bulk_message.rb`
- `db/migrate/20240325160237_remove_index_champs_on_private.rb`


--------------------------------------------

# 2024-03-22-04

Release [2024-03-22-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-22-04)  sur GitHub

## Améliorations et correctifs

### Administrateurs
- Amélioration(ux): pour des raisons d'usage (UX, merci), deplace les champs d'un formulaire après un autre champs. 

### Divers
- task(procedure): fix open procedures with closing reason (#10181)


--------------------------------------------

# 2024-03-22-03

Release [2024-03-22-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-22-03)  sur GitHub

## Améliorations et correctifs

### Divers

- fix(dossier.pdf): code path not tested reached a typo (#10178)


--------------------------------------------

# 2024-03-22-02

Release [2024-03-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-22-02)  sur GitHub

## Améliorations et correctifs

⚠️ A ne pas déployer, pb avec le dépot de nouveau dossier venant de FCI

### Divers

- fix(dosser#show.pdf): missing user indirection on fci (#10176)


--------------------------------------------

# 2024-03-22-01

Release [2024-03-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-22-01)  sur GitHub

## Améliorations et correctifs

⚠️ A ne pas déployer, pb avec le dépot de nouveau dossier venant de FCI
### API
- feat(api): add champs.champDescriptorId so champRepetitions.rows[x] has a uniq id (#10165)

### Divers
- Correctif: un dossier peut avoir 1 à n FranceConnectInformation en passant par l'usager (#10175)
- Tech: sort le nom de l'application du cache dans la page de landing (#10174)
- Tech: ajoute des informations de debug lors de la generation de pdf utilisant weasyprint (#10078)
- Tech: fix pour la bannière du domaine (#10172)


--------------------------------------------

# 2024-03-21-01

Release [2024-03-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-21-01)  sur GitHub

#:warning: A ne pas déployer, pb avec le dépot de nouveau dossier venant de FCI

## Améliorations et correctifs

### Administrateur

- ETQ admin j'ai un message d'explication si j'utilise une URL déja utilisée lors de la publication d'une démarche (#10119)
- Correctif : ETQ admin je ne peux pas fermer une démarche en redirigeant sans choisir la nouvelle démarche (#10158)
- Correctif : un administrateur avec une procedure discarded sans dossier peut être supprimé (#10142)

### Instructeur

- ETQ instructeur, je souhaite voir la gestion des notifications instructeurs au DSFR (#10164)

### Usager

- tech(User.FranceConnect): un usager peut avoir plusieurs profils FranceConnect, pas uniquement un (#10068)

### Divers

- Tech: permet de faire marcher agent connect simultanément sur le domaine gouv et ds (#10154)
- chore(tasks): run tasks in sidekiq (#10160)
- fix(champ): fill stable_id and stream when cloning from old champs (#10145)
- refactor(dossier): label indexes based on type_de_champ not champ (#10150)
- fix(maintenance task): fix query and add a test (#10163)
- Tech: surchage des constantes relatives au nom pour permettre un double run ds et demarches.gouv hors mailers (#10157)
- Tech: suppression de code mort relatif a pipedrive (#10159)
- Tech: corrige un Current.application_base_url surnuméraire (#10170)
- Tech: deplace les variables FC dans .env.example.optional pour permettre un demarrage de l'app sans ces variables (#10168)
- Prépare la bannière informant du changement de nom de domaine, avec redirection automatique le cas échéant (#10040)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240318152314_add_preferred_domain_to_users.rb`


--------------------------------------------

# 2024-03-19-01

Release [2024-03-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-19-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur correction de l'affichage de quelques dossiers  (pas de comparaison avec champ#updated_at lorsque le champ n'est pas persisté) (#10146)

### Usager

- ETQ usager: correction du champ Annuaire Education (legacy combobox) (#10156)
- ETQ usager, pas d'erreur 500 lorsque j'accède à une démarche "fermée" qui n'existe pas (#10153)

### Tech
- fix(fix_missing_champs_task): `rake_puts` present in spec, not in prod (#10149)
- Maintenance task:  drop unusable maintenance task (#10147)
- Maintenance task: update champs for each dossier (#10152, #10155)


--------------------------------------------

# 2024-03-18-04

Release [2024-03-18-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-18-04)  sur GitHub

## Améliorations et correctifs

### Divers

- fix(dossier): select the right stable_id (#10144)


--------------------------------------------

# 2024-03-18-03

Release [2024-03-18-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-18-03)  sur GitHub

## Améliorations et correctifs

### Divers

- Exports: ajouter une colonne "date de dernière mise à jour du dossier" #10043 (#10136)
- fix(champ): fix multiple champ copies bug (#10123)
- fix(champs): fix rows order (#10141)
- fix(champs): use approximate count in the task (#10140)
- fix(data): apply fix missing champ via task. FV dossiers are poping way too much at helpdesk (#10139)


--------------------------------------------

# 2024-03-18-02

Release [2024-03-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-18-02)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur je souhaite mieux comprendre le graph "avancée des dossiers" en changeant le terme "démarrés" (#10131)
- ETQ instructeur agent connecté, me déconnecter me déconnecte également d'agent connect (#10135)

### Expert
- ETQ expert, je veux pouvoir gérer les notifications que je reçois depuis mon interface (#10127)

### Divers

- Adjust vertical spacing in segmented-control-item (#10122)
- Tech: rend les champs agent connect usual and given name optionnels (#10138)
## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240313122932_add_expert_notification_settings_to_experts_procedures.rb`
- `db/migrate/20240318092147_add_agent_connect_id_token_column_to_instructeur_table.rb`
- `db/migrate/20240318134256_remove_constraint_on_agent_connect_information.rb`


--------------------------------------------

# 2024-03-18-01

Release [2024-03-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-18-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ admin: ajoute des messages manquants dans la liste des modifications d'annotations privées (#10116)


### Instructeur
- ETQ instructeur: corrige la mise à jour automatique du bandeau de statut d'un export après sa création (#10115)

### Usager

- ETQ usager: plus de lien de téléchargement du formulaire vide PDF lorsque la démarche est close (#10112)
- ETQ usager: logos transparents de démarches plus visible avec le thème sombre (#10111)
- ETQ usager: agent de la fonction publique territoriale, je peux récupérer les dossiers d'un collègue absent (#9873)
- ETQ invité: ajouter l'owner du dossier dans la liste des dossiers (#10133)

### Manager
- ETQ Super-Admins: évite que le géocodage des services tombe sur une mauvaise commune (#10106)

### Divers
- fix(data): supprime les démarches sans administrateur ni dossier (#10125)
- fix(api_geo): include Paris, Lyon and Marseille code INSEE in local data (#10128)
- refactor(champs): if champ not found - build it (#10117)
- fix(data): update procedures with invalid mon avis (#10120)
- tech(rails-pg-extras): add rails pg extra (#10121)
- revert(pg_extra): no ops on monday, but want to fix prod (#10134)
- tech(champs): add stable_id and stream (#10109)


## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20231212133643_create_agent_connect_informations.rb`
- `db/migrate/20240316065520_champs_add_stable_id.rb`


--------------------------------------------

# 2024-03-14-01

Release [2024-03-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-14-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager: Mentionne d'autres acteurs possibles dans la description de la co-construction (#9961)

### Divers

- fix(data): update procedure with duree_conservation greater than max_duree (#10107)
- fix(maintenance task): pass a collection instead of an array (#10110)
- Tech: mise à jour d'openid connect pour une compatibilité avec openssl v3 (#9908)


--------------------------------------------

# 2024-03-12-01

Release [2024-03-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-12-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Correctif: ETQ Admin: corrige un bug ou un message d'erreur d expression regulière apparaissait plusieurs fois (#10095)
- Amelioration: ETQ Admin: lorsque je clos une démarche je peux alerter les usagers et je crée une page de fermeture si la démarche n'est pas redirigée dans DS (#9930)

### Instructeur

- Amelioration: ETQ instructeur je ne dois pas pouvoir inviter un instructeur avec une adresse email invalide (#10096)
- Correctif de l'autogestion des instructeurs (#10104)

### Expert
- Correctif: ETQ expert le téléchargeant un dossier et toutes ses PJs, la messagerie n'est pas dans l'export quand la procedure ne l'autorise pas. L'export ne contient pas les annotations privées (#10076)

### Divers

- Données : suppressions et mises à jour de bulk messages (#10071)
- Tech: ne remonte plus que 10% des InvalidAuthenticityToken errors (#10098)
- fix(rebase): destroy_all to cascade on dependencies (#10101)
- refactor(dossier_vide): use types_de_champ instead of empty dossier (#10103)
- Tech: pas d'erreur EmailEvent lorsque le message ne peut pas avoir plusieurs destinataires (#10097)
- fix(email_validation): allow email with specific format (#10100)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240201141520_add_closing_reason_and_closing_details.rb`
- `db/migrate/20240202135754_add_closing_notifications_to_procedure.rb`


--------------------------------------------

# 2024-03-11-02

Release [2024-03-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-11-02)  sur GitHub

## Améliorations et correctifs

### Divers

- fix(apply_diff): one more time (#10099)


--------------------------------------------

# 2024-03-11-01

Release [2024-03-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-11-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager, je souhaite pouvoir visualiser un formularie au format PDF (#10092)

### Divers

- fix(dossier): safely remove child champs in apply_diff (#10091)
- fix(addresse): lookup for city, departement and region name in local db (#10094)


--------------------------------------------

# 2024-03-08-02

Release [2024-03-08-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-08-02)  sur GitHub

## Améliorations et correctifs

### Divers

- Revert "Merge pull request #10082 from tchak/fix-address-city_name" (#10087)


--------------------------------------------

# 2024-03-08-01

Release [2024-03-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-08-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, je peux filtrer toutes les démarches par type d'usager (#10041)

### Instructeur

- Correctif : supprime l'information de contact à la suppression du groupe instructeur (#10080)

### Usager

- Correctif : j'aimerais que le champs de type civilité ait le même style d'erreur que les autres (#10027)
- Correctif, : je ne souhaite pas voir apparaitre des balaises HTML dans mon attestation au format pdf (#10081)

### API

- fix(graphql): no crash on address type without city_name (#10082)

### Divers

- Met à jour les zones en fonction du nouveau découpage des ministères (#10077)
- Tech: base la l'url de retour de FranceConnect sur le host de la requete (#10066)
- fix(email.validation): some nasty tests (#10065)
- fix(effectif): bug in maintenance task (#10083)
- fix(section): display sections inside repetitions (#10085)
- fix(rebase): rebase a dossier when its repetition had been removed on newer version of published_revision crashes (#10084)


--------------------------------------------

# 2024-03-07-01

Release [2024-03-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-07-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ Instructeur je veux attribuer la motivation correcte à ma décision (#10050)
- ETQ usager je vois les informations de contact de mon groupe instructeur (si elles existent) dans mon attestation de dépôt (#10067)

### API

- fix(entreprise): graphql effectif (#10053)

### Divers

- refactor(dossier): use revision as source of truth to diplay champs (#9695)
- tech(spread_dossier_deletion_task): query on date time range not on date (#10069)


--------------------------------------------

# 2024-03-04-01

Release [2024-03-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-03-04-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur je vois les messages envoyés aux usagers avec un dossier en brouillon (#10048)
- Affiche correctement les informations sur le classement sans suite (#10054)
- Corrigez les exports contenant des dossiers dont l'utilisateur a été supprimé (#10052)

### Usager

- Correction : etq usager je ne vois pas le bandeau indiquant que je suis connecté avec France Connect (#10051)
- Adresse: afficher le code postal des municipalités (#10060)

### Divers

- fix(test): fail on last month day (#10057)
- fix(dossier): no crash if condition target champ is not found (#10056)
- Tech: réduit l'impact de Flipper sur la base (#10059)
- fix(data): avoid destroying 2M dossiers in one day due to error in Cron::ExpiredDossiersTermineDeletionJob (#10062)
- fix(address): nullify data when empty or invalid address is entered (#10061)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240227163855_add_procedure_id_to_bulk_messages.rb`
Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20240301223646_clean_old_gates2024.rake`


--------------------------------------------

# 2024-02-29-01

Release [2024-02-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-29-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin ou instructeur je peux copier une liste d'emails pour inviter des experts (#9966)

### Usager
- ETQ usager, mon attestation de dépot au format PDF est plus accessible  (#10046)

### API

- feat(api.dossier.for_tiers): expose email of tier (#10047)

### Divers
- tech(redirect): ETQ usager non connecté, je suis automatiquement redirigé de demarches-simplifiees vers le nouvel host (#10042)
- Tech: chiffre le param email réaffiché dans la vue pour éviter de construire des pages de phishing (#10049)


--------------------------------------------

# 2024-02-23-01

Release [2024-02-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-23-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur: corrige une typo tier => tiers dans les exports (#10031)

### Technique

- Sécurité: fix multiple CVE in rails & rack (#10038)


--------------------------------------------

# 2024-02-22-02

Release [2024-02-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-22-02)  sur GitHub

## Améliorations et correctifs

### API

- API: fix régression d'accès aux attestations PDF (#10032)

### Technique

- Tech: supprime support de mailjet (#10035)


--------------------------------------------

# 2024-02-22-01

Release [2024-02-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-22-01)  sur GitHub

## Améliorations et correctifs

💣  Cette version introduit une bug sur l'API et une page de manager, [installez plutôt la version 2024-02-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/2024-02-22-02)

### Administrateur

- ETQ admin, quelques améliorations sur l'éditeur v2 des attestations (toujours sous feature flag) (#9995)

### Instructeur

- ETQ instructeur, message d'erreur plutôt que crash lorsqu'on ajoute un filtre trop long (plus de 100 caractères) (#10025)
- ETQ instructeur, les exports contiennent les données de mandataire, et informe si l'usager s'est connecté par France Connect (#10015)

### API

- Intègre les données de mandataire (#10015)

### Usager

- ETQ usager utilisant un lecteur d'écran (accessibilité) : améliore l'interface d'invitation à un dossier envoie d'une invitation (#10020)
- ETQ Usager : légères améliorations de lisibilité et d'espacement sur le formulaire (#10026)

### Technique

- Tech: massive gem updates, phase 2 (#9998)
- Tech: cache le bouton de c/c lorsque le navigateur bloque l'accès au clipboard (#10024)
- Tech: document STRICT_EMAIL_VALIDATION_STARTS_ON env var (#10012)
- Tech: configure les CSP pour répondre à un host défini par `APP_HOST_LEGACY` (#10022)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20240205093954_change_flipper_gates_value_to_text.rb`


--------------------------------------------

# 2024-02-21-01

Release [2024-02-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-21-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ gestionnaire d'admins: sépare les commentaires par origine + correctifs (#10011)

### Instructeur

- ETQ instructeur, je retrouve les fichiers joint a une demande d'avis expert (#10014)

### Experts
- ETQ expert: je retrouve mes avis confidentiels dans les exports  (#10014)
- ETQ expert: passe la page de création de compte expert au dsfr (#10010)

### Usager
- ETQ usager utilisant un lecteur d'écran: la page de demande de confirmation de compte ne concentre pas le lecteur d'écran sur le champ pour renvoyer un mail de confirmation (#10017)
- ETQ usager, fix sélection d'un choix multiple qui commence par `[` (#10019)
- ETQ usager je dois voir de vraies apostrophes à la place de `&#39;` dans les sujets d'email (#10021)

### Tech
- mark samsung browser as supported browser (#10023)
- secu(monavis): autorise uniquement les bouton mon avis des domaines (jedonnemonavis'|monavis|voxusagers).numerique.gouv.fr (#10016)
- fix(champ): do not reset data on champs without fetch_external_data (#10018)

## Notes de déploiement

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20240220104123_disable_invalid_monavis.rake`


--------------------------------------------

# 2024-02-19-02

Release [2024-02-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-19-02)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager: corrige la validation du champ email lorsque l'email n'est pas saisi (#10013 , bug introduit par #9978 )

### Tech

- Doc : complète le readme à propos des webdrivers (#10003)


--------------------------------------------

# 2024-02-19-01

Release [2024-02-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-19-01)  sur GitHub

## Améliorations et correctifs

💣 Introduit un bug sur le champ email, déployer directement le correctif sur [la version suivante 2024-02-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-19-02)

### Administrateur

- ETQ admin, le message de fin de dépôt d'une démarche déclarative est correct (#9971)
- ETQ admin, correction de la liste des PJ d'une démarche en brouillon (#10008)
- ETQ admin, je peux gérer la liste des instructeurs même si le routage est activé sur ma démarche (#10000)
- ETQ admin, correction des balises attestations et emails pour des champs contenant deux '-' successifs (#10009)

### Instructeur
- ETQ instructeur et consommateur API, la vue du champ RNA est harmonisée avec le champ RNF (#9957)
- ETQ instructeur, la page de gestion des groupes d'instructeur est au format du DSFR (#10002)

### Usager
- ETQ usager je peux éditer jusqu'à 50 points ou éléments sélectionnés dans le champ carte (#9989)

### Tech
- Tech: validation d'email plus stricte (#9978)
- Tech (PiecesJustificativesService): amélioration du services qui liste les fichiers pour les exports/dossiers (#9960)
- Tech:  s'assure que seuls les instructeurs d'une démarche ayant la gestion activé peuvent accéder aux écrans (#10004)
- Tech: supprime CSP_REPORT_URI env var et configuration (#9993)
- Tech: refactor groupe gestionnaire commentaire component into existing message component (#9926)
- Tech: fix flaky match_array test (#9997)
- Tech (address): use directly BAN data (#9990)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20240202133353_add_user_profile_id_user_profile_type_to_exports.rb`
- `db/migrate/20240202133417_add_administrateur_id_to_archives.rb`
- `db/migrate/20240215164247_change_column_default_procedures_instructeurs_self_management_enabled.rb`

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20240215164807_backfill_procedure_instructeurs_self_management_enabled.rake`

Nous avons supprimé le reporting d'erreurs de CSP vers l'url définie par la variable optionnelle `CSP_REPORT_URI`, qui est donc inutile et à supprimer (cf #9993)


--------------------------------------------

# 2024-02-14-01

Release [2024-02-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-14-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin je peux faire un routage simple à partir d'un type de champ communes ou epci (#9955)

### Instructeur

- ETQ instructeur: fix génération d'attestation avec une balise "nom de la commune" depuis un champ adresse (#9983)

### Usager

- ETQ Usager, je ne veux pas voir un message d'alerte indiquant que la démarche est close lorsque je modifie un dossier en construction (#9985)
- ETQ Usager, je veux trouver ma commune qui est trop bas dans la liste des résultats (#9987)

### Divers

- Tech: fix ActiveStorage::IntegrityError sur l'analyse des titres d'identité après watermark (#9984)
- refactor(logic): compute sub champ values (#9988)
- Délégation de compte: ETQ gestionnaire je peux contacter les gestionnaires des groupes parents (#9846)
- perf(mail_template/attestation): preload procedure revisions (#9991)


--------------------------------------------

# 2024-02-12-02

Release [2024-02-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-12-02)  sur GitHub

## Correctifs

Corrige des erreurs dans l'API introduites par [la release précédente](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-12-01)

### API

- Tech: rollback graphql to 2.2 => 2.0 (#9982)


--------------------------------------------

# 2024-02-12-01

Release [2024-02-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-12-01)  sur GitHub

## Améliorations et correctifs

💣 Cette release introduit un bug dans l'API, résolues [dans la version 2024-02-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-12-02)

### Usager

- ETQ usager je vois l'icône obligatoire pour une case à cocher obligatoire (#9981)

### Technique

- Tech: massive gems update, phase 1 (#9967)


--------------------------------------------

# 2024-02-09-01

Release [2024-02-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-09-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin je peux ajouter un champ carte dans un bloc répétable (#9907, #9979)


### Usager

- ETQ usager: le champ carte est un peu plus design (passage au DSFR) (#9976)
- ETQ usager je vois de nouveau le délai d'instruction pour les démarches déclaratives en instruction (#9974)

### Tech
- Instructeur: use precomputed timestamps for notifications (#9977)
- Carte: migration IGN vers Géoplatforme et corrige visibilité de certains fonds de carte (#9975)


--------------------------------------------

# 2024-02-08-01

Release [2024-02-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-08-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Correctif: ETQ admin je retrouve les annotations privées dans les archives (#9953)

### Instructeur

- Instructeur: fix bug pour imprimer un dossier rempli par un tiers (#9969)

### Usager

- ETQ usager je peux saisir un numéro RNA valide qui ne contient pas que des chiffres (#9965)
- ETQ usager, je n'ai pas besoin du détail des délais d'instruction pour les démarches déclaratives (#9970)
- ETQ usager, améliore le footer dans les emails de démarche avec un petit espace supplémentaire (#9973)

### API

- ETQ developpeur, je veux trouver facilement la doc sur Graphql dans le pied de page (#9968)

### Technique

- Tech: update administrate from 0.18.0 to 0.20.1 (manager) (#9972)
- Tech: fix maintenance task invokation `AddDossiersMissingChampsTask` (#9963)


--------------------------------------------

# 2024-02-06-01

Release [2024-02-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-02-06-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, je peux tester le nouvel éditeur d'attestation (sous feature flag) (#9903)
- Délégation de compte admin: notification par email à l'ajout d'un commentaire (#9668)
- Délégation de compte admin: affichage de l'arborescence des groupes descendants (#9837)


### Super admin
- ETQ super admin je peux voir les dossiers sur lesquels un utilisateur est invité (#9959)

### Tech
- chore(deps): bump nokogiri from 1.16.0 to 1.16.2 (#9964)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20231106104916_add_commentaire_seen_at_to_administrateurs.rb`
- `db/migrate/20231219093437_add_labels_to_attestation_templates.rb`
- `db/migrate/20240108174129_add_layout_to_attestation_templates.rb`
- `db/migrate/20240110122422_add_version_to_attestation_templates.rb`


--------------------------------------------

# 2024-01-31-02

Release [2024-01-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-31-02)  sur GitHub

## Améliorations et correctifs

### Administrateur

- tech(perf.rebase): améliore la performance pour réconcilier les dossiers d'une procédure lorsqu'un administrateur change le formulaire (donc nouveau milésime 🍷) (#9956)


--------------------------------------------

# 2024-01-31-01

Release [2024-01-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-31-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin les logos de démarches gardent une résolution convenable (#9954)

### Instructeur

- ETQ instructeur meilleur rendu du texte pour l'ajout de PJ en dark mode dans la dropdown d'instruction (#9943)
- Instructeur: corrige une redondance de texte dans le tableau quand un dossier est pour un tiers (#9950)

### Usager

- correctif: ETQ usager naviguant sur le site en anglais, je m'attends a recevoir des messages d'erreur de connexion en anglais (#9949)

### Super admin
- ETQ Super-Admin, je souhaite marquer une démarche comme modèle (#9939)

### Divers

- Tech : nettoyage du code conditionnel et routage (#9879)
- Tech: fix le job de vérification des liens externes des procédures lorsqu'un autre attribut est en erreur (#9946)
- ETQ gestionnaire, j'ai un suivi des messages au sein de mes groupes (#9664)

### Technique

- Tech (CI): update apt repos before installing packages (#9951)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20231027124906_create_follow_commentaire_groupe_gestionnaires.rb`
- `db/migrate/20240126071130_add_is_template_to_procedures.rb`


--------------------------------------------

# 2024-01-29-01

Release [2024-01-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-29-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ admin, déplacer un champ dans l'éditeur est plus rapide (#9919, #9922, #9927)
- ETQ gestionnaire je peux répondre à mon administrateur (#9645)

### Instructeur
- ETQ instructeur, je vois un badge Corrigé lorsque le dossier a été corrigé (#9936)
- ETQ instructeur, n'affiche pas encore de lien vers un export quand le fichier n'est pas encore disponible (#9934)

### Usager
- Correctif: ETQ usager, je ne peux pas soumettre 2x mes modifications en parallèle (#9932)
- ETQ usager, la page de confirmation de compte est lisible en anglais (#9942)
- ETQ usager je suis mieux prévenu lors de la suppression d'un dossier en attente de transfert (#9935, #9941)

### Expert
- ETQ expert, je suis notifié des nouveaux messages (#9890)

### API
- ETQ Admin, je suis prévenu par mail lorsque mon jeton API arrive a expiration (#9931)
- ETQ Admin: je peux modifier le nom et les réseaux d'un jeton d'API (#9940)
- ETQ Administrateur, j'ai un premier exemple d'appel api à la création d'un jeton (#9921)
- API: Ajoute la possibilité de limiter l'utilisation d'un jeton à un ensemble de réseaux précis (#9877)

### Divers

- Tech: add custom_locales folder to i18n_tasks (#9892)
- Remove user_email input autofocus on agent page (#9933)
- Tests: safer and cleaner API to create procedure with tdcs (#9929)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20231025134902_add_emails_to_commentaire_groupe_gestionnaires.rb`
- `db/migrate/20231220202003_add_ip_ranges_to_api_token.rb`
- `db/migrate/20240116155926_add_expires_at_column_to_api_token.rb`
- `db/migrate/20240123085909_add_expiration_notices_sent_at_column_to_api_token.rb`


--------------------------------------------

# 2024-01-22-01

Release [2024-01-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-22-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Tech: ne plante plus le job de purge des dossiers supprimés par l'usager lorsqu'un seul dossier échoue (#9917)
- chore(deps-dev): bump vite from 5.0.10 to 5.0.12 (#9924)- chore(deps-dev): bump vite from 5.0.10 to 5.0.12 (#9924)
- Exports: feature flag/Fonds verts pour trier les colonnes par révision récente puis position plutôt que mélanger les révisions entre elles (#9886)


--------------------------------------------

# 2024-01-17-01

Release [2024-01-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-17-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ administrateur, dans l'éditeur de formulaire  je peux déplacer un champ par une liste (#9861)
- ETQ administrateur, dans l'éditeur de formulaire un sommaire permet de naviguer de section en section (#9894)
- ETQ administrateur: légère amélioration du chargement des pages (#9916)

### Instructeur
- ETQ instructeur, la recherche de dossiers affiche les pastilles de notifications le cas échéant (#9906)

### Usager
- Le numéro de dossier apparait dans l'email de demande de transfert (#9882)

### Divers
- Perf: remplace les `OpenStruct` par des objets plus performants (#9915)


--------------------------------------------

# 2024-01-11-02

Release [2024-01-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-11-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Upgrade à ruby 3.3 avec YJIT et améliore les perfs globales (#9739)


## Informations pour les instances : 
L'activation de YJIT est optionnelle (et se fait à la compilation de ruby et s'active avec la variable d'environnement `RUBY_YJIT_ENABLE`, cf [la documentation](https://docs.ruby-lang.org/en/master/yjit/yjit_md.html).
Nous la recommandons pour les ~ 20% de réduction de temps d'exécution gagnés, mais au prix d'une consommation de RAM ~20% à 50% plus élevée. Assurez-vous de disposer d'assez de mémoire disponible, d'autant que l'augmentation est progressive au fil des jours après chaque déploiement.


--------------------------------------------

# 2024-01-11-01

Release [2024-01-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin: petites corrections de style dans l'éditeur (#9900)
- Delégation de compte admin: ETQ admin je peux contacter mon/mes gestionnaires de groupe (#9637)

### Usager
- Correctif: si le noms des EPCIs a changé, demande à resaisir le champs (#9888)
- Correctif autocomplete pour d'anciennes démarches: ignore les espaces en début/fin d'option (#9896)

## Technique :
- Sécurité : utilise un jeton dédié pour le merge de compte FC / DS par mail (#9904)
- chore(deps): bump view_component from 2.82.0 to 3.9.0 (#9895)
- chore(deps): bump puma from 6.3.1 to 6.4.2 (#9898)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20231016134902_create_commentaire_groupe_gestionnaires.rb`
- `db/migrate/20240110113623_add_email_merge_token_column_to_france_connect_information.rb`


--------------------------------------------

# 2024-01-08-01

Release [2024-01-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2024-01-08-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, creation delegation administrateur pages (#9578)
- ETQ admin, quand je configure un type de champ regexp, j'ai une petite mention sur le bornage de celle ci (#9875)
- ETQ admin je peux utiliser les champs de type dossier dans les répétitions (#9889)

### Usager
- ETQ usager naviguant sur le site en anglais, j'aimerais que le bloc pour choisir si je depose le dossier pour moi ou qqun d'autre soit traduit (#9884)
- ETQ usager invité, lorsque je crée mon compte via FC, je ne retrouve pas mes invitations (#9887)

### Divers

- Corrections de typos (#9885)

## Notes de déploiement

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20231226140149_backfill_invites_missing_existing_user.rake`


--------------------------------------------

# 2023-12-22-01

Release [2023-12-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-22-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- Correction : une condition / règle de routage du type "un département est dans une région" est valide (#9881)

### API
- API: cast le type de champ EJ (#9878)


### Tech
- API: stocke les ips utilisées pour accéder à l'API (#9880)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20231221142727_add_last_used_ips_column_to_api_token.rb`


--------------------------------------------

# 2023-12-21-01

Release [2023-12-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-21-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur, j'ai besoin d'un accès très facile à mon dernier export (#9855)
- Correctif: ETQ instructeur lorsque je sors du composant de filtrage sans choisir une option (#9839)

### API

- Expose les attributs commune et département pour les types de champ RNF (#9751)

### Usager
- Co-Construction: envoie un mail au bénéficiaire si le dossier repasse en instruction (#9874)


### Divers
- Tech: échappe les tags de données utilisateur dans les modèles pour email (#9863)


--------------------------------------------

# 2023-12-20-01

Release [2023-12-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-20-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ administrateur, je peux ajouter une notice explicative a un champ explication (#9871)
- ETQ administrateur je peux conditionner/router avec les opérateurs "est dans mon département",  "n'est pas dans mon département",  "est dans ma région" et   "n'est pas dans ma région". (#9798)

### Usager

- ETQ usager: améliore les références au Code Civil pour les mandataires (#9870)

### Divers

- Doc: met à jour policy ImageMagick pour intégrer le coder JSON (#9860)
- ETQ utilisateur avec plusieurs rôles, je m'y retrouve plus facilement (#9872)
- Tech(js) : mise a jour de vite (#9869)
- Tech : mise à jour des conditions et règles de routage suite à l'ajout de nouveaux opérateurs (#9850)


--------------------------------------------

# 2023-12-18-03

Release [2023-12-18-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-18-03)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, une alert DSFR m'informe de la difference entre un champ PJ classique et un champ titre d'identité (#9862)

### Instructeur

- ETQ instructeur je peux de nouveau passer en instruction un dossier avec corrections en attente. Nous activons le blocage à la demande, démarche par démarche. (#9868)


--------------------------------------------

# 2023-12-18-02

Release [2023-12-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-18-02)  sur GitHub

## Améliorations et correctifs

### Instructeur

- ETQ instructeur : correction de l'impression d'un dossier (#9866)


--------------------------------------------

# 2023-12-18-01

Release [2023-12-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-18-01)  sur GitHub

#:warning: Ne pas prendre cette version, régression sur l'impression d'un dossier pour un instructeur.

## Améliorations et correctifs

### Usager

- ETQ usager passant par un mandataire, je suis notifié des changements d'état de mon dossier par email (#9706)
- ETQ usager, je ne vois plus le statut "antivirus en cours" lorsque j'envoie un PJ (#9808)

### Instructeur

- ETQ instructeur, la position du pied de page pour la page de statistiques d'une procédure est corrigée (#9851)

### API

- Tech: utilsation de spectaql en remplacement de graphdoc pour la conversion du schema graphql en html (#9849)

### Divers

- Tech: mise a jour de dependances js (#9848)
- Tech : ajoute region-code aux communes et departements json (#9852)
- Tech: suppression du cron et de la tache de migration des pjs vers des buckets dédiés. (#9857)
- TECH: ajoute une variable d'environnement pour désactiver l'activation de cron job (#9859)
- Tech: sanitize les messages utilisateurs dans les emails (#9858)
- ETQ expert: lorsque je me connecte, je suis redirigé vers la page des avis, non pas vers la page de mes dossiers (#9853)
- Style: plusieurs améliorations mineures / thème sombre (#9847)
- Tech, correctif sur le service d'expiration des usagers inactif qui timeoutait via `.in_batches`. Utilise `.pluck(:id)` (#9854)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20231114140154_combine_dossier_and_individual_updates.rb`


--------------------------------------------

# 2023-12-13-02

Release [2023-12-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-13-02)  sur GitHub

## Améliorations et correctifs

### Divers

- Améliorer l'affichage des couleurs pour la page nouveautés (#9842)

### Tech

- Revert open_id connect update (#9843)


--------------------------------------------

# 2023-12-13-01

Release [2023-12-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-13-01)  sur GitHub

#:warning: A ne pas utiliser, prendre la suivante, régression sur agent connect

## Améliorations et correctifs

### Administrateur et instructeur

- ETQ admin et instructeur, j'ai un pied de page (#9817)
- ETQ instructeur je ne peux pas passer en instruction un dossier en attente de correction (#9821)
- ETQ instructeur, ma demande de correction est supprimée (résolue) lorsque je supprime son message associé (#9818)
- ETQ instructeur: j'accède à la page d'agent connect depuis un button dédié sur la page d'accueil (#9836)

### Manager

- ETQ superadmin, je peux transférer un dossier d'un utilistateur à un autre (#9754)
- ETQ superadmin je peux mettre à jour le brouillon d'une révision à partir d'un CSV (#9840)
- ETQ opérateur je peux mettre en place la délégation de comptes administrateurs (gestionnaires de groupes administrateurs ) (#9571)
- ETQ SuperAdmin: ne préremplit pas le code totp (#9832)

### Divers
- correctif(procedure/statistiques): utilise le nouveau composant pour afficher les delais de traitement (#9834)
- Tech: met a jour la dépendance openid_connect pour être compatible avec openssl v3 (#9835)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20231127102633_add_from_support_to_dossier_transfers.rb`
- `db/migrate/20231128071043_add_from_support_to_dossier_transfer_logs.rb`


--------------------------------------------

# 2023-12-11-01

Release [2023-12-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ admin je suis prévenu si des erreurs sur la démarche empêchent la publication de révision (#9800)
- ETQ admin, j'ai plus facilement accès à la bonne doc des API (#9828)

### Usagers
- ETQ usager, l information de durée de traitement des 90 percentile peut être trompeur (#9708)
- ETQ usager, je veux retrouver l'email utilisé pour mon compte dans les mails automatiques (#9802)
- ETQ usager sans dossier, je peux savoir comment trouver une démarche (#9824)
- ETQ personne morale, mon adresse email figure dans l'attestation de dépôt (#9829)
- ETQ usager ou instructeur, la motivation est bien formatée sur la page du dossier (#9831)
- ETQ instructeur, lorsque je veux filtrer les dossiers d'une démarche, je peux utiliser un autocomplete (#9820)

### Instructeurs
- ETQ instructeur, je peux accéder à la page des dossiers supprimés (#9814)
- ETQ instructeur, j'ai plus d'option pour le traitement multiple : supprimer, restaurer, desarchiver, passer en instruction depuis l'onglet "suivre",  (#9772)

### API
- amelioration(api): ajoute la configuration chorus aux API GraphQL (#9816)
- documentation(api): ou trouver l'email de l'usager qui dépose le dossier (#9813)

### Correctif
- correctif(ResetExpiringDossiersJob): timeout sur la prod, utilise une requete plus générique mais plus rapide [on ne scope pas aux dossiers ayant ete notifies, on les reset tous car ca timeouté (#9797)
- [Bug] Fix des placeholders et des icones du dropdown instruction (actions multiples) (#9812)
- Icônes: fix affichage des remix icons sur des chromes pas assez récents (#9805)

### Tech
- Tech: documente comment sécuriser ImageMagick (#9815)
- Tech: déplace les jobs de rebase des dossiers vers sidekiq (#9819)


--------------------------------------------

# 2023-12-06-01

Release [2023-12-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-06-01)  sur GitHub

## Améliorations et correctifs

### Correctif

- Tentative de correction de  l'upload de fichiers pour certains navigateurs et config réseau (chore(yarn): patch update vite) (#9803)

### Instructeur

- ETQ instructeur, lorsque je motive une décision, la motivation est désormais bien formatée (#9799)


--------------------------------------------

# 2023-12-05-01

Release [2023-12-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-05-01)  sur GitHub

## Améliorations et correctifs

### Usager
- Rend la signature dans les emails plus neutre (#9776)
- ETQ utilisateur, je voudrais que le format affiché pour les champs de date corresponde au format du champ de saisie (#9760)
- Thème sombre: adapte une bonne partie des icônes (#9755)
- ETQ usager l'affichage de la liste de mes dossiers va beaucoup plus vite (#9774)

### Administrateur
- ETQ admin, les procédures clonées conservent les features flags (#9677)
- correctif(expiration): etends la durée de conservation des dossiers quand la duree de conservation d'une procedure change (#9757)
- fix(email_template): less magic - less bugs ! (#9762)

### SuperAdmin
- feat(manager): add expert view (#9763)

### Technique
- chore(vite): update (#9761)
- Perf chargement de dossier: fix N+1 sur les champs répétitions non obligatoires (#9771)
- feat(attestation): use tiptap editor controller (#9719)
- Tech: améliorations de la configuration de skylight, outil de supervision des performances (#9753)
- Tech: cache key depends on locale (#9796)
- Tech: déplace la variable de grève de flipper a une variable d'environnement pour éviter une requete sql par page. (#9767)


--------------------------------------------

# 2023-11-27-01

Release [2023-11-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-27-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin, je peux savoir quand mon formulaire a été sauvegardé pour la dernière fois (#9710)

### Usager

- ETQ usager la liste des pièces justificatives à fournir inclut les PJ des blocs répétables (#9747)
- ETQ usager on ne m'affiche plus la raison sociale d'un établissement non diffusible (#9748)
- ETQ usager modifiant un dossier en construction je n'ai plus besoin de cocher la case de correction effectuée (#9738)

### API

- En tant que consommateur de l'API DS, je peux récupérer les infos RNF (#9740)
- API: une demande de correction de dossier peut être de type "outdated" (#9744)
- correctif(API.entreprise.enseigne): ajouter un attribut a une entreprise demande un peu plus que juste le definir sur l'object (#9749)

### Divers

- Accessibilité: améliore la lisibilité du site en mode Contraste élevé de Windows (#9752)


--------------------------------------------

# 2023-11-23-02

Release [2023-11-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-23-02)  sur GitHub

## Améliorations et correctifs

### Instructeur
- En tant qu'instructeur, je peux exporter les données RNF des dossiers (#9737)

### Technique
- correctif(manager): utilise un champ de type Field::Text plutot que Field::String, sinon administrate tente de requeter le champs/formatter (#9746)
- Tech: rajoute des logs, de l'audit de perf et du suivi de bug pour la file de job asynchrone sidekiq (#9730)


--------------------------------------------

# 2023-11-23-01

Release [2023-11-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-23-01)  sur GitHub

## Améliorations et correctifs

### Super Administrateur
- Ajout des informations sur le routage d'une procédure (#9724)

### Administrateur
- je peux conditionner / router à partir d'un champ de type choix multiple avec l'opérateur "Ne contient pas" (#9714)

### API
- API: ajoute l'enseigne aux personnes morales (#9712)

### Divers
- corrige thème sombre: ne clignote pas avec le thème clair au moment du render (#9727)
- tech(tags): all tags should have ids (#9721)
- correctif(users.expires): typos et perf (#9729)
- tech: les jobs asynchrone de controle antivirus utilise sidekiq (#9732)
- correctif(chorus.export) (#9745)


--------------------------------------------

# 2023-11-22-01

Release [2023-11-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-22-01)  sur GitHub

## Améliorations et correctifs

Cette version a été déployée le lundi 20/11.

### Administrateur

- Ajout d'une page de confirmation après la publication d'une procédure - mise en avant de l'URL à partager (#9672)
- Correctif:  permet d'utiliser un tag référençant un type de champ dont le libellé contient deux espaces consécutif (#9716)

### Pour tout le monde (ou presque):

- Administrateurs/Instructeurs/Experts : lien vers la page des nouveautés et refactorise les barres de navigation principale (#9655)
- Carte de déploiement de DS par département (#9701)
- Accessibilité: mode "sombre" (en phase de test, des améliorations sont prévues) (#9705)

### Usager
- Destruction des comptes inactifs depuis au moins 2 ans (sans dossier en cours d'instruction) 💥 (#9666)

### Divers
- correctif(typo):  Quels sont des délais... ->  Quels sont les délais (#9709)

### Technique
- Migration v3 api entreprise privileges (#9713)
- Synchronize schema.rb avec la prod (#9676)

### Technique

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données : 
- `db/migrate/20231103084116_add_expired_notification_sent_at_to_users.rb`
- `db/migrate/20231108120254_add_missing_exports_instructeur_index.rb`
- `db/migrate/20231108120731_add_missing_administrateurs_groupe_gestionnaire_index.rb`
- `db/migrate/20231109190504_add_index_to_userss_on_last_sign_in_at.rb`

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20231109145911_backfill_procedure_expires_when_termine_enabled_without_dossiers.rake`


--------------------------------------------

# 2023-11-16-01

Release [2023-11-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-16-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- ETQ admin je peux conditionner / router avec un champ de type nombre décimal (#9694)

### Instructeur

- correctif(instructeurs/procedures/filters): ETQ instructeur, je ne peux filtrer les dossiers d'une procedure par type de champs choix simple (#9704)

### Usager

- correctif(dossiers): ETQ usager, le menu d'invitation etait cassé sur mobile (#9700)

### Technique

- Test: fix flaky brouillon on repetition (#9703)
- détruit les dossier operation logs with dossier (#9599)
- fix(dossier): use traitements to send repasser_en_instruction template (#9697)
- Tech : supprime des features flags liés au routage (#9707)
- Prototype de l'éditeur d'attestation v2 (#9612)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20231114113317_add_json_body_column_to_attestation_template.rb`
Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20231011144554_remove_orphan_dossier_operation_logs.rake`

## :information_source:  Information pour les instances

Cette PR introduit l'usage optionnel de sidekiq + redis pour la gestion des jobs asynchrone. Elle réalise aussi les premiers pas pour la génération d'une nouvelle attestation pdf utilisant le middleware weasyprint à des fins de tests uniquement pour l'instant.


--------------------------------------------

# 2023-11-14-01

Release [2023-11-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-14-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ Admin, fix l'affichage du choix de la limite de caractères d'un champ texte (#9693)

### Usagers
- ETQ usager, lorsqu'une demarche n'a pas activé les PJ multiples, je ne dois pas pouvoir uploader plus d'une pj (#9691)
- ETQ usager, les combobox sur mobile n'etaient pas utilisable (#9698)
- ETQ usager, les combobox de DS se comportent comme un `<select>` (#9699)

### Instructeur
- ETQ instructeur, je souhaite que l'email de réexamen du dossier soit affiché dans la messagerie (#9627)
- ETQ instructeur, je ne pouvais pas instruire certains dossiers car ils y avait des champs commune non normalisé (#9689)


### Technique
- Tech: fix multiple deprecation warnings (#9692)
- Suppression de l'adresse email de contact dans la page 500 (#9686)
- Tech: fix log without to_log key (#9702)
- Perf: accélération du chargement des dossiers visibles (#9696)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20231107130640_add_re_instructed_at_to_dossiers.rb`, `db/migrate/20231107150217_create_re_instructed_mails.rb`, `db/migrate/20231110135532_alter_dossiers_for_procedure_preview_not_nullable.rb`, `db/migrate/20231110135533_validate_alter_dossiers_for_procedure_preview_not_nullable.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20231110092044_fix_not_normalized_champs_commune.rake`).


--------------------------------------------

# 2023-11-10-01

Release [2023-11-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-10-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- Chorus: je peux ajouter un type de champ EngagementJuridique (#9621)
- Gestionnaire: Creation delegation page children management (#9566)

### API
- Ajoute l'identifiant du jeton api utilisé lors de requête à l'api graphql (#9681)

### Divers
- Tech: log des informations sur les navigateurs opérant les Traitements (#9663)
- Tech: active le cache pour le geocoder pour contourner le rate limiting dans certains contextes (#9687, #9688)
- Tech: infrastructure pour utiliser sidekiq (#9519)
- Super-Admin: fix layout des gros nombres de la page stats (#9680)
- Super-Admin: je peux informer les administrateurs, instructeurs et experts des évolutions du site (#9638)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20230912155425_create_release_notes.rb`
- `db/migrate/20230929091948_add_ancestry_to_groupe_gestionnaires.rb`
- `db/migrate/20231017092437_add_browser_to_traitements.rb`
- `db/migrate/20231017162057_add_announces_seen_at_to_users.rb`


--------------------------------------------

# 2023-11-08-01

Release [2023-11-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-11-08-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- En tant qu'admin, je peux filtrer les démarches par département (#9649)
- ETQ administrateur je n'ai plus d'avertissement si ma démarche contient un champ RIB (#9661)
- ETQ Administrateur, je voudrais pouvoir conditionner les annotations en utilisant des champs du formulaire (#9660)
- [Fix] ETQ admin, je souhaite associer une démarche existante depuis le bouton "clore" du tableau de bord de la démarche (#9653)
- ETQ admin je peux créer une règle de routage de plusieurs lignes (#9604)

### Instructeur

- correctif(pdf): ETQ instructeur, lorsque je télécharge un dossier et ses PJs, les \t deviennent des ? (#9656)
- ETQ Instructeur les urls de mes messages dans la messagerie sont converties en lien (#9673)

### Usager

- Usager: fix alignement du champ "autre" des listes de choix (#9652)
- Usager: légères harmonisations visuelles de la combobox (#9654)

### API

- log(graphql): log deprecated order argument usage (#9662)
- feat(graphql): expose prefilled flag on dossier (#9674)

### Divers

- tech(maintenance): ajoute `maintenance_task` une  pour ajouter des champs manquant a un dossier (#9657)
- feat(dossier): validate on change and revalidate on input (#9643)
- amelioration(export): suggère l'usage de 7zip et de renommer l'archive pour un nommage plus court sur la page des exports afin d'eviter les problèmes au support (#9659)
- doc(.env.optional): documente BULK_EMAIL_QUEUE (#9667)
- fix(dropdown_multiple): options -> enabled_non_empty_options (#9675)
- feat(dossier): commune champ is an autocomplete now (#9605)
- Ajout d'un nouveau référentiel : le Répertoire National des Fondations (RNF) (#9648)

### Technique

- Tech: retry system tests JS seulement sur CI pour avoir les erreurs rapidement en local (#9669)


--------------------------------------------

# 2023-10-30-01

Release [2023-10-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-30-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Feature ouidou/admin creation delegation gestionnaire page group gestionnaire management (#9565)
- Sécurité/2FA: ne connecte pas automatiquement un super-admin après réinitialisation du mot de passe (#9650)
- ETQ superadmin, je peux supprimer un admin depuis le manager (#9616)

### API

- fix(graphql): fix pagination with order desc (#9644)
- fix(graphql): fix preloader with pagination (#9646)

### Divers

- can reset file input in messagerie (#9636)
- amelioration(dossier.pdf): pour les champs de type Champs::AddressChamp, ajoute le code insee de la commune sous l'adresse complete (#9624)
- tech: ajoute le departement aux services (#9647)
- fix(drop_down_list): fix other option with combobox and some cleanup (#9651)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20231025161609_add_departement_to_services.rb`


--------------------------------------------

# 2023-10-25-01

Release [2023-10-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-25-01)  sur GitHub

## Améliorations et correctifs

### Usager
- ETQ usager, correctif, lorsque j'upload un fichier, ca bloque le bouton pour deposer un dossier (#9639)

### Administrateur 
- ETQ admin, je peux saisir le cadre budgetaire d'une demarche de subvention pour faciliter le rapprochement d'un export DS a un export Chorus (#9420)

### Technique
- Sécurité (champ regex): timeout plus agressif à 1 seconde (#9634)
- feat(attestation): add tiptap json to html converter (#9626)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230828131618_add_chorus_column_to_procedure.rb`).


--------------------------------------------

# 2023-10-24-01

Release [2023-10-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-24-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usagers, mise à jour du format attendu pour les champs de type nombre décimal car on utilise les `.` en lieu et place des `,` (#9633)

### Administrateur
- ETQ administrateur, le footer pour soumettre un formulaire venait en sur-elevation sur certaines page (empechant par exemple la saisie des options avancées d'une démarche) (#9635)


--------------------------------------------

# 2023-10-23-01

Release [2023-10-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-23-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ administrateur: les boutons d'actions de la page "création du formulaire" et "annotations" sont plus visibles (#9603)

### Usagers
- ETQ usager: correctif, lorsque je saisi une valeur invalide sur un champs de type expression régulière, je vois le message d'erreur configuré par l'admin (#9622)
- ETQ usager: correctif, je ne vois pas le champs departement tant que je n'ai pas saisi le champs EPCI (#9628)
- ETQ usager: correctif, je peux saisir une adresse qui ne fait pas partie de la Banque d'Adresse National (#9630)
- ETQ usager: correctif, je peux saisir des nombre decimaux avec comme séparateur de decimal un `.`, une `,`. Je peux aussi saisir des nombre avec un decimal commençant par un `0` (#9632)

### Tech/Correctif
- tech: utilise l'api entreprise v3 pour récupérer les effectifs mensuels et annuels (#9613)
- use maintenance tasks gem (#9584)

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20231010093014_create_maintenance_tasks_runs.maintenance_tasks.rb`, `db/migrate/20231010093017_add_arguments_to_maintenance_tasks_runs.maintenance_tasks.rb`, `db/migrate/20231010093018_add_lock_version_to_maintenance_tasks_runs.maintenance_tasks.rb`, `db/migrate/20231010093021_add_metadata_to_runs.maintenance_tasks.rb`).


--------------------------------------------

# 2023-10-19-01

Release [2023-10-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-19-01)  sur GitHub

## Améliorations et correctifs

### Administrateurs
- ETQ administrateur, je veux pouvoir valider des champs avec des règles simples (Regexp) (#9535)
- ETQ admin je peux conditionner sur les champs communes, EPCI et région (#9614)
- [Fix Administrateur] Retirer le soulignement du dernier item du fil d'ariane (#9588)
- Landing administrateur : fix design des éléments "DS en chiffres" (#9587)

### Export
- Expert: accélère les pages de liste d'avis et d'affichage des dossiers (#9586)

### Usagers
- fix(dossier): delay submit to autosave end (#9591)
- wip(autocomplete): autocomplete from url (#9495)
- chore(browser): show browser outdated message to more (very) old browsers (#9600)

### Instructeur
- fix(search): show a message if searching for deleted dossier (#9618)
- ETQ instructeur si j'utilise un filtre de type choix, les valeurs possibles s'affichent dans un select (#9560)
- [amélioration] Affichage de plus d'info RNA coté usager + amélioration affichage coté instructeur/usager (#9597)

### SuperAdmin
- Feature ouidou/admin creation delegation manager page (#9564)
- Feature ouidou/admin creation delegation gestionnaire page gestionnaire management (#9538)

### Tech
- Tech: mise à jour de flipper (#9590)
- fix suivi on mobile by using ds fr (#9589)
- Tests: fix SVA/SVR calculator specs to handle time zones (#9592)
- Tech: documente variable optionnelle ROUTAGE_URL manquante (#9595)
- feat: add env var CERTIGNA_ENABLED to disable certigna if not used (#9601)
- fix(api_client): fix some edge cases (#9608)
- fix(dossier): remove extra input event on page load (#9607)
- Correction du lien vers la doc du routage (#9606)
- amelioration(email): passe les jobs non prioritaire [appelons ça des bulk email], dans la queue de low_priority (#9617)
- chore(deps): bump @babel/traverse from 7.20.12 to 7.23.2 (#9619)

### API
- fix(graphql): implement real cursor pagination (#9387)

### Data
- Correction : tâche Rake pour corriger les numéros de téléphone invalides (#9602)
- correctif(data): tâche rake recréant les champs manquant à un dossier ayant subi une perte de données (#9610)

### Infra

- Ajout de la var d'env pour `CERTIGNA_ENABLED` pour activer/desactiver l'horodatage, cf (#9601)
- Ajout de la var d'env pour `BULK_EMAIL_QUEUE` pour isoler les mails dans une fil d'attente non priorizé, cf  (#9617)


--------------------------------------------

# 2023-10-10-01

Release [2023-10-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-10-01)  sur GitHub

## Améliorations et correctifs

### Administrateur 

- ETQ admin, je suis averti que je ne peux pas réactiver une démarche tant que la date limite de dépôt de dossiers n'est pas dans le futur (#9550)

### Usager
- ETQ usager, lorsque j'ai déjà des dossiers sur une procédure, la page de garde me donne de meilleurs liens (#9579)

### Correctis
- fix linter (#9581)
- fix: corrige la remontée d'infos à Sentry pour les appels webhooks (#9580)
- Schema: fixup failed migrations, ensure exports->instructeurs FK exists (#9583)
- Lors de la migration des pjs, détruit les blobs qui n'existent pas dans l'espace de stockage (#9523)
- correctif(tech.export): il arrive que des exports soient mal identifié (le content-type), ce qui par la suite renvoie des exports vide (0kb) (#9582)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20231009070354_add_dossiers_count_to_exports.rb`, `db/migrate/20231010083144_add_instructeur_foreign_key_to_exports.rb`).


--------------------------------------------

# 2023-10-09-01

Release [2023-10-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-09-01)  sur GitHub

## Améliorations et correctifs

💣 Le déploiement de cette version pourrait échouer avec les migrations. Déployer directement [2023-10-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/edit/2023-10-10-01) qui les corrige.

### Administrateur

- **Nouveau :** active le conditionnel dans les annotations privées  (#9546)
- Administrateur: petites corrections visuelles sur l'éditeur de champs (#9537)
- Administrateur : permet  le conditionnel dans un champ d'un bloc répétable (#9556)
- Corrige l'envoi d'email aux admins avec service sans siret (#9569)
- Documentation routage: ajoute lien vers la doc sur la page des options (#9547)

### Instructeur
- **Nouveau :** Le PDF d'un dossier intègre les questions et messages des avis experts (#9555)
- **Nouveau :** Ajoute les pieces justificatives des avis dans les exports (#9558)
- ETQ instructeur: corrige le décompte du nombre de dossiers exportés une fois l'export généré (#9575, #9577)
- ~~Tentative de correction du problème de génération d'exports > 4 Go~~ (#9552)

### Usager

- **Important** ETQ usager, plus de mention "facultatif" sur les champs optionnels (#9553)
- ETQ usager, le formulaire d'identité est un peu plus accessible (#9549)
- ETQ usager, accélère les affichages de la demande d'un dossier et de son PDF (#9559)
- Correction : ETQ usager je peux saisir des nombres négatifs (#9570)
- ETQ usager le form n'accumule pas l'espacement vertical des champs conditionnés consécutifs (#9576)
- Correctif: certains formulaires de gestion de compte ne respectaient pas la chartre du DSFR (#9557)

### Gestionnaire d'instance
- Initialisation des gestionnaires de groupes (#9418)

### Divers

- Tech: tâche consolidant les établissements en mode dégradé d'une démarche donnée (#9551)
- chore(deps-dev): bump postcss from 8.4.29 to 8.4.31 (#9572)
- remonte les erreurs dans Sentry lorsqu'un appel webhook est en erreur (#9573)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20230813091937_create_gestionnaires.rb`
- `db/migrate/20230813091938_create_groupe_gestionnaires.rb`
- `db/migrate/20230813091946_add_groupe_gestionnaire_to_administrateurs.rb`
- `db/migrate/20230813091947_add_foreign_key_groupe_gestionnaire_to_administrateurs.rb`
- `db/migrate/20230813091948_validate_foreign_key_groupe_gestionnaire_to_administrateurs.rb`
- `db/migrate/20231009070354_add_context_to_exports.rb`
- `db/migrate/20231009143331_add_reference_to_exports_instructeur.rb`

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20231003123111_delete_optional_champ_feature_flag.rake`

Cette version comporte une nouvelle variable d'environnement optionelle : 
- `ROUTAGE_URL` cf #9547


--------------------------------------------

# 2023-10-02-01

Release [2023-10-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-10-02-01)  sur GitHub

## Améliorations et correctifs

### Administrateur

- **Nouveau:** ETQ instructeur ou admin, possibilité d'apposer un tampon dédié à un groupe instructeur sur une attestation (#9507)
- Notification par mail des admin de services sans siret (#9524)
- Message d'alerte avant publication de modification d'un formulaire rendu moins anxiogène (#9545)
- Désactive le routage de procédures clonées si l'admin n'est pas aussi admin de la procédure parente (#9534)
- Le bouton de publication des modifications est déplacé à gauche (#9539)

### Instructeur
- **Nouveau:** Création d'une page qui liste les **exports** ; retrait des liens des menus déroulants (#9473)
- ETQ instructeur, réorganisation des onglets et mise en avant des démarches closes avec des dossiers à traiter (#9517, #9543)

### Usager

- ETQ usager, lors de la soumission d'un dossier avec des erreurs, le format des erreurs est conforme au DSFR (#9491, #9544)
- ETQ usager, aération du formulaire grâce au bon markup du DSFR (#9531)
- ETQ usager, la saisie d'un nombre invalide affiche une erreur (#9516)

### Technique

- Mise à jour de `get-func-name` de la version 2.0.0 à 2.0.2 (#9532)
- Test : utilisation de match_array (#9533)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données 
- `db/migrate/20230928083809_remove_exports_unicity_constraint.rb`


--------------------------------------------

# 2023-09-27-01

Release [2023-09-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-27-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ admin, mise en place du conditionnel sur un champ "département" (#9506)
- Correctif : désactivation du routage pour les procédures clonées si l'administrateur n'est pas également administrateur de la procédure parente (#9504)

### Instructeur
- ETQ instructeur,  filtre les dossiers "en construction" sans inclure ceux "en attente de corrections" (#9520)

### Usager
- ETQ usager, bénéficie d'une aide à la saisie grâce à une description détaillée du format attendu pour certains champs (#9518)
- Corrections d'erreurs 500 liées à l'affichage d'informations de contact de groupes instructeurs (#9529)
- Lorsqu'un dossier est dupliqué, celui-ci est systématiquement rebasé sur la dernière version de la démarche (#9493)

### Super-Admin
- ETQ manager, possibilité de masquer une démarche de la liste des démarches disponibles à la création (#9345)

### Technique
- Mise à jour de la gem `graphql` de la version 16.8.0 à 16.8.1 (#9505)



## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20230720103932_add_hidden_at_as_template_to_procedure.rb`

Cette version comporte une migration du contenu des données :
- `lib/tasks/deployment/20230921132123_unroute_cloned_procedures_from_diffent_admin.rake`


--------------------------------------------

# 2023-09-21-01

Release [2023-09-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-21-01)  sur GitHub

## Améliorations et correctifs

### Usagers

- correctif(repetition.commune): ETQ usager, lorsque j'ai plus d'une repetition ayant un champ commune séléctionnée avec le meme code postal, les ids des options des input[type=radio] communes partageant le meme code postal ne sont pas uniques, on boucle donc sur la 1ere repetition quand on veut en selectionner un 🤯 (#9501)

### Technique

- Tech: suppression du code d'authentification des jetons v1 et v2 (#9391)


--------------------------------------------

# 2023-09-20-01

Release [2023-09-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-20-01)  sur GitHub

## Améliorations et correctifs

## ETQ Usager
- amelioration(usager/recherche): Tableau de bord - rendre recherche complémentaire avec filtre par procédure (#9363)
- amelioration(dropdown_list_component): nouvelle version de la dropdown au format DSFR et avec une nouvelle archi (#9152)
- amelioration(champ.siret): ETQ usager le form pour renseigner son SIRET passe au DSFR et est dispo en anglais (#9489)

## ETQ Admin
- amelioration(routage) : ETQ admin je peux router par département depuis un type de champ communes (#9474)
- amelioration(routage): ETQ admin je peux router par département depuis le champ EPCI (#9477)

## ETQ instructeur
- amelioration(agent-connect): ETQ tous les agents peuvent s'agent connecté (#9480)
- amelioration(tableau.de.bord): Afficher le bouton "personnaliser" en entier dans le tableau des instructeurs (#9494)

## ETQ Operateur
- correctif(manager#dossiers/show): ETQ superadmin, l'affichage d'un dossier pouvait timeouter car non préloadé (#9478)

## API
- fix(graphql): improuve sort for pending deleted dossiers (#9479)

## Correctif
- Fix dossiers préremplis avec identité individuelle vide de démarches pour établissements (#9488) 
- correctif(expert.invite-autre-expert): ETQ expert, je ne pouvais pas demander d'autres avis d'expert (#9487)
 - fix(champs-editor): remove unnecessary background color (#9490)

## Technique
- tech(skylight): bump skylight version to fix proxy issue with skylightd (#9496)


## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230918143300_fix_prefilled_empty_individual.rake`).


--------------------------------------------

# 2023-09-13-01

Release [2023-09-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-13-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ admin, possibilité de router depuis un champ de type "régions" (#9472)
- Activation de la fonctionnalité SVR pour les administrateurs (#9372)

### Instructeur
- ETQ instructeur, ajout d'une fonction d'auto-complétion lors de la demande d'avis aux experts (#9471)
- ETQ instructeur, nouveau filigrane pour les titres d'identité améliorant la lisibilité des documents Titres d'identité (#9470)

### Correctif
- Correction d'un bug dans la génération d'archives périmées : évite les réessais inutiles (#9469)
- Correctif concernant le menu "burger" du DSFR sur mobile  (#9476)
- Mise à jour et ajout d'icônes de favoris, (notamment icône Apple Touch) en accord avec les bleus et rouges de la charte graphique (#9475)

### Informations pour les instances  : 
- La variable `WATERMARK_FILE` et le fichier associé peuvent être supprimés : le filigrane des titres d'identité utilise maintenant l'`APPLICATION_NAME` en guise de texte. Les paquets `imagemagick` et `gsfonts` deviennent une dépendance  pour générer les filigranes (probablement déjà installés) (cf #9470)
- Optionnel: ajout de la favicon en HD pour Safari et les raccourcis sur mobile. Elle est personnalisable avec la variable `FAVICON_APPLE_TOUCH_152PX_SRC`, ou désactivable complètement si cette variable est un string vide (`""`) (cf #9475)


--------------------------------------------

# 2023-09-11-01

Release [2023-09-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-11-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ admin je peux router avec des règles dont l'opérateur est "n'est pas" (#9423)

### Usagers
- ETQ Usager, je veux voir le champ Civilite au DSFR (#9451)
- ETQ usager, fix position du spinner pour les champs conditionnels (#9461)
- ETQ Usager, je veux voir dans mon dossier les informations de contact de mon groupe instructeur (#9425)

### Instructeur
- [fix] Selecteur de filtres pour instructeurs - le champ est invisible (#9463)
- Fix brouillon dossiers with forced_groupe_instructeur (#9466)
- Reorganisation des boutons sur l'interface instructeur (#9457)

### technique
- changed(brevo): use env vars for smtp relay address and port. (#9462)
- Tech: log technique de la destruction de dossiers (#9230)
- Tech: mise à jour de la gem activestorage-openstack (#9468)

### Exploitant
- ETQ exploitant: amélioration du log de destruction de dossier (#9465)

### API
- Prefill identity pour les procédures individuelles (#8719)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230809151357_create_contact_informations.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230906124541_update_brouillon_dossiers_with_forced_groupe_instructeur.rake`).


--------------------------------------------

# 2023-09-05-02

Release [2023-09-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-05-02)  sur GitHub

## Améliorations et correctifs

### Correctifs
- Corrige accordéons et menus déroulants pour les navigateurs <= 2020 (#9455)

### Usager
- ETQ usager je veux toujours avoir une liste déroulante sans choix présélectionné (#8991)


--------------------------------------------

# 2023-09-05-01

Release [2023-09-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-05-01)  sur GitHub

## Améliorations et correctifs

💣 Instable sur vieux navigateurs, passez directement à 2023-09-05-02

### Correctifs
- ~~Amélioration de l'utilisation des menus déroulants sur les anciens navigateurs (#9452)~~
- Correction du lien qui renvoie vers la FAQ dans le message de confirmation de compte à chaque connexion (#9447)

### Instructeur
- Dans une démarche SVA/SVR, meilleur suivi des dossiers terminés repassés en instruction (#9450)

### Administrateur
- Correction UX du formulaire de sélection de zones (#9453)


### Manager
- Visualisation de la liste des instructeurs et des groupes d'instructeurs pour une procédure spécifique (#9448)

### Technique
- Mise à jour des dépendances npm (#9454)


--------------------------------------------

# 2023-09-04-01

Release [2023-09-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-04-01)  sur GitHub

## Améliorations et correctifs

💣 Instable sur vieux navigateurs, passez directement à 2023-09-05-02

### Usager
- ETQ usager, fix layout de 3 forms de gestion de compte (#9433)
- ETQ usager,  should refresh dossier footer when removing options from multiselect (#9438)
- ETQ usager, corrige un texte sur la configuration de firewall en cas d'erreur de pjs (#9440)
- ETQ usager, fix le style pagination quand affichée dans .card (#9441)
- ETQ usager, passe certains champs au DSFR (#9258)

### Instructeur
- ETQ instructeur, exclut les dossiers en brouillon de la recherche (#9414)
- ETQ instructeur, je vois un badge d'alerte quand le dossier a été réaffecté (#9286)
- ETQ instructeur, corrige la fusion de compte dans le cas de procédure supprimée (#9439)

### Administrateur
-  ETQ administrateur, je peux ajouter un champs sous un autre (#9445)

### Correctif
- Correctif : petite typo (#9434)
- Correctif : mise à jour d'un dossier réaffecté et sans groupe instructeur (#9431)
- Correctif : Commune exemple (#8843)
- Correctif : initialize map only when container is visible (#9442)

### API
- API: expose les 2 dates utilisées par le SVA (#9362)

### Technique
- essaye de diminuer les tests non fiables en ajoutant des retry sur tous les end2end avec du js (#9437)
- chore(tools): mise a jour de la procédure d'installation de chrome/chromedriver (#9430)
- Update CONTRIBUTING.md (#9427)
- Tech : déplace une validation sur le groupe d'instructeurs défaut du controleur au modèle (#9392)
- chore(deps-dev): bump vite from 4.3.4 to 4.3.9 (#9139)
- add default values for some of the config options (#9161)
- add a constant for the most used test password (#8740)
- fix(coldwire): bump @coldwired/actions to fix event dispatch (#9443)


--------------------------------------------

# 2023-08-30-01

Release [2023-08-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-08-30-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- Amélioration: ETQ admin je peux router depuis un champ département (#9406)
- Correctif : ETQ admin, corrige l'affichage du status d'une procedure depubliee (#9400)
- Correctif : ETQ admin je ne vois pas d'alerte sur les groupes d'instructeurs à la publication d'une révision d'une démarche non routée (#9407)
- Amélioration: ETQ admin, dans les personnes impliquées, je vois la liste des instructeurs suivants le dossier triée par ordre d'envoi (#9412)

### Instructeur
- Amélioration : ETQ instructeur, je peux redemander un lien de connexion securisé (#9424)
- Correctif : ETQ instructeur je peux envoyer un message de groupe aux dossiers sans groupe d'instructeurs (donc les dossiers en brouillon)

### Technique
- Chore(deps) : bump puma from 6.1.1 to 6.3.1 (#9403)
- Correctif : corrige l'url du logo public (#9408)
- Test(flacky) : essaie de rendre plus fiable le test system/administrateurs/types_de_champ_spec.rb ligne 199 (#9421)
- Test(flacky): fix flaky test et tri des dossiers par avis (#9426)
- Test : fix SVA decision date when start date is at end of month and with correction delay (#9419)
- Tech : ajout de rspec retry pour fiabiliser la CI (#7904)
- Tech(clean) : retire un params inutile dans wait for autosave (#9422)


--------------------------------------------

# 2023-08-21-02

Release [2023-08-21-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-08-21-02)  sur GitHub

## Améliorations et correctifs

### Correctif
- Revert "tech(gemfile): met à jour activestorage-openstack 1.5.1 vers … (#9405)


--------------------------------------------

# 2023-08-21-01

Release [2023-08-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-08-21-01)  sur GitHub

## Améliorations et correctifs

### EQT instructeur

- ETQ instructeur, je peux envoyer un message a un utilisateur ayant un dossier qui n'a pas encore de groupe d'instructeur (#9314)



### correctif
- correctif(dolist): utilise des liens vers les logos des procedures plutôt que des attachements.inlined (#9369)
- correctif(export): corrige bug création archives (#9402)
- correctif(email.dolist): expose public logo url (#9376)

### technique
- chore(pipedrive): remove pipedrive (#9396)
- fix(after_party): this job timouts in prod. Drop it for now. (#9399)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230718143350_reset_dossier_brouillon_groupe_instructeur_id.rake`).


--------------------------------------------

# 2023-08-10-01

Release [2023-08-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-08-10-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Corrige un test api entreprise (#9374)
- Tech: supprime la colonne dossier_corrections#kind (#9316)
- Corrige le routage si option "autre" sélectionnée (#9366)
- Avertit dans le manager si l'utilisateur est bloqué (#9373)
- chore(graphql): update stored query to include corrections (#9349)
- feat(graphql): use camelize with inflection (#9367)
- Améliore l'affichage de plusieurs petits formulaires résiduels (#9375)
-   Nettoyage du code de l'ancien système de routage (suite) (#9378)
- chore(annotate): remove annotate gem (#9381)
- ETQ Intégrateur API, je voudrai que le timeout de l'API soit plus long (#9380)
- ETQ Administrateur, je voudrais que mon compte soit considéré comme actif si je n'utilise que l'API (#9377)
- amélioration des affichages de numéro de dossiers et de procédure (#9382)
- [Refonte page accueil demarche] Infos sur les delais d'instruction (#9371)
- Fix datetime_champ validation with negative time zone (#9280)
- Tech : supprime la colonne procedures#routing_criteria_name (#9386)
- ETQ admin d'une démarche routée, je suis alerté à la publication si des groupes n'ont pas de règle de routage valide (#9388)
- fix(dossier): removing options from multiselect should update conditions (#9389)
- chore(coldwired): update to enable style attribute preservation (#9390)
- graphql(attachment): prevent null errors (#9395)
- Ameliore le wording (#9398)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230713163641_remove_dossier_corrections_kind.rb`, `db/migrate/20230801102740_add_last_authenticated_at_to_api_tokens.rb`, `db/migrate/20230801121131_remove_migrated_champ_routage_columns.rb`, `db/migrate/20230802161011_remove_routing_criteria_name_column.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230728085422_update_routing_rule_for_groups_routing_from_drop_down_other.rake`).


--------------------------------------------

# 2023-07-27-01

Release [2023-07-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-27-01)  sur GitHub

## Améliorations et correctifs

### ETQ admin
- ETQ admin, je suis alerté lorsque mes demarches publiées ont des services sans siret ou pas de service (#9353)
- ETQ admin, je peux saisir des liste ordonnées avec des paragraphes entre cette liste ordonnées (#9370)

### ETQ instructeur 
- ETQ Instructeur, je voudrais pouvoir créer des listes avec une numérotation personnalisée et sur plusieurs lignes (#9364)

### ETQ Usager/invité
- ETQ Invite, le fait que c'est au titulaire du dossier de deposer le dossier est plus visible (#9322)
- ETQ Usager, je retrouve les erreurs sous les champs + les champs de type text/number sont au format dsfr  (#9004)
- ETQ Usager, affiche l'email du demandeur dans l'attestation de dépôt (#9354)

### Correctifs
- champs(commune): ETQ usager, le code insee des villes de Paris, Marseille and Lyon ne me sont pas proposés, je peux saisir uniquement les arrondissement (#9355)
- champs(carto): ETQ usager, lorsque je modifie un dossier en construction et lorsqu'un objet associé (geo area…) n'est pas valide (#9361)

### Technique
- APIEntreprise: envoie le siret par défaut si le siret du service est identique au siret de l'établissement demandé (#9365)
- tests: utilise selenium-webdriver gem plutot que webdrivers qui est actuellement cassé (#9360)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230725101602_remove_toplevel_communes.rake`).


--------------------------------------------

# 2023-07-25-01

Release [2023-07-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ administrateur, je souhaite que la page "publier" soit plus claire (#9323)
- ETQ operateur, je peux piloter le niveau de log à partir de la variable DS_LOG_LEVEL (#9342)
- rend plus actionnable message d'erreur (#9332)
- fix(views): fix dead link to configure API entreprise token (#9343)
- Ameliore le wording des personnes impliquées (#9340)
- API graphql: expose les demandes de corrections (#9310)
- build(deps): bump word-wrap from 1.2.3 to 1.2.4 (#9327)
- chore(deps): bump semver from 5.7.1 to 5.7.2 (#9302)
- ETQ instructeur, je veux que la motivation soit effacée lorsque je repasse un dossier en instruction (#9329)
- [fix] Les demarches supprimées s'affichent encore dans l'onglet "en test" pour les instructeurs (#9337)
- refactor: move submit en_construction logic to the model (#9348)
- [refonte usager] Tableau de bord - filtrer les dossiers par démarche (#9338)
- feat(administrateur): add environment variable for Administrateur::UNUSED_ADMIN_THRESHOLD (#9352)
- chore(geo_area): purge invalid geo_areas (#9346)
- fix spec (% 10000 and assert_performed_jobs) (#9350)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230712095037_add_reason_to_dossier_corrections.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230712095348_backfill_dossier_correction_reason.rake`, `lib/tasks/deployment/20230721142825_purge_motivation_dossiers_not_termine.rake`, `lib/tasks/deployment/20230721145042_purge_invalid_geo_areas.rake`).


--------------------------------------------

# 2023-07-19-01

Release [2023-07-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- indique nom et prénom de l'EI comme raison sociale (#9317)
- tech(smtp): passe du relai SMTP de SendInBlue vers le relai SMTP Brevo (#9320)
- ETQ instructeur je vois les réaffectations d'un dossier (#9270)
- fix(procedure): accept dpo emails with accents (#9325)
- ETQ Instructeur, je veux que les GeoJSON déposés par les utilisateurs soient valides (#9275)
- send service siret as recipient (#9326)
- amelioration(archive-uploader): capture avec sentry les erreurs du swift_wrapper (#9328)
- [fix] manque de la traduction du role invité dans le dropdown de comptes (#9333)
- Perf: améliorer la perf de la page admin en cachant le résultat du parseur de template (#9311)
- conf: ajoute redis comme backend de cache (#9312)
- ajoute les jetons api lors de la fusion d'un compte administrateur (#9259)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230630091637_create_dossier_assignments.rb`, `db/migrate/20230704093503_enable_postgis.rb`).


--------------------------------------------

# 2023-07-18-02

Release [2023-07-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-18-02)  sur GitHub

## Améliorations et correctifs

### Administrateur
- [fix] Logo ne s'affiche pas correctement dans les cartes sur safari (admin) (#9319)

### Technique
- un opérateur peut bloquer un compte (#9324)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230718113720_add_blocked_at_block_reasonto_user.rb`).


--------------------------------------------

# 2023-07-18-01

Release [2023-07-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-18-01)  sur GitHub

## Améliorations et correctifs

### Usager
- ETQ usager: correctifs visuels mineurs (#9304)
- correctif(champs.iban): ETQ usager, le copier/coller sur un champs iban avec le code pays en minuscule perdait les caractères alphabétiques en debut d'IBAN (#9321)
- correctif(notifications): ETQ usager, j'aimerais que les notifications soient fiable (#9285)
- feat(dossier): enable refresh after update on more champs (#9299)

### Instructeur
- [fix] Changer message à propos des corrections en attente (#9303)
- [Actions multiples] Ajouter la possibilité pour les instructeurs de classer sans suite et refuser (#9274)
- ETQ instructeur, le badge SVA d'un dossier intègre la date prévisionnelle de décision au survol (#9305)
- ETQ instructeur mes démarches sont filtrées par onglet pour les retrouver plus facilement (#9308)

### Api
- feat(graphql): add timestamps to files and champs (#9306)

### Technique
- Ajout d'un cron pour maintenir le nombre de pjs en cours de migration entre 0 et 200K (#9309)
- [bug] fix de la barre de recherche pour les dossiers invités (#9298)
- [fix] le dropdown pour les utilisateurs connectés (#9313)


--------------------------------------------

# 2023-07-11-01

Release [2023-07-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ETQ admin je peux configurer ma démarche en SVA. En test, à demander au support pour activation de l'interface (#9104) 

### Usager
- Corrige l'affichage des liens de notices des vieilles démarches (#9297)
- Correctif Champ COJO: une accréditation invalide doit être assimilé à un champ "vide" (#9296)

### API
- Sécurité: bloque les jetons des anciennes versions de jetons au format v1 et v2 (#9293)

### Gestionnaire d'instance
- Stats: ignore aperçus, brouillons en construction & dossiers des démarches en brouillon (#9291)


## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données:

- `db/migrate/20230525130349_add_sva_svr_to_procedures.rb`
- `db/migrate/20230601080235_add_sva_svr_decision_on_to_dossiers.rb`
- `db/migrate/20230614093944_add_kind_to_dossier_corrections.rb`


--------------------------------------------

# 2023-07-10-02

Release [2023-07-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-10-02)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ administrateur, je voudrais pouvoir utiliser le numéro d'accréditation Paris 2024 (#9101)

### Instructeur
- Correctif (demande): pas d'erreur lorsque l'entreprise n'a pas de SIRET associé au siège social (#9294)

### Usager
- Refonte usager: Tableau de bord - Changer design de tableau à tuiles (#9256)

### Technique
- Change le nommage des pjs à %Y/%m/%d/sd/sd..... (#9289)


--------------------------------------------

# 2023-07-10-01

Release [2023-07-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-10-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- ETQ Admin je peux filtrer les groupes d'instructeurs à configurer (#9164)
- ETQ Admin je peux importer des instructeurs depuis la page d'un groupe (#9168)
- Vérifie que le lien vers la notice de la démarche soit valide  (#9264, #9282)

### Instructeur
- Page demande: augmente le contraste entre libellés et réponses (#9281, #9272)
- ETQ instructeur: intègre les demandes de corrections dans les PDF (#9260)

### Usager
- Email: utilise le template de notification par défaut quand la démarche n'a pas personnalisé le sien (#9279)
- ETQ usager je peux corriger l'identité de mon dossier qui a pu être remplie sans qu'elle soit complète (#9288)

### Tech
- Nettoyage du code de l'ancien système de routage (#9237)
- Tech: mise à jour du routage sans callback (#9244)
- API entreprise: contexte dépendant de l'instance (#9287)
- Test: merge_fork d'un dossier étant sur une revision passée avec une répétition qui a été supprimée (#9269)
- chore(dev): use overmind instead of foreman (#9276)
- chore(npm): update javascript dependencies (#9278)
- chore(deps): bump sanitize from 6.0.1 to 6.0.2 (#9283)



## Notes de déploiement

Cette version comporte une migration du schéma de la base de données :
- `db/migrate/20230629102031_add_lien_notice_and_dpo_errors_to_procedures.rb`

Cette version comporte des migrations du contenu des données: 
- `lib/tasks/deployment/20230615182245_validate_procedure_liens.rake`
-  `lib/tasks/deployment/20230707121855_force_dossiers_to_fill_missing_individual.rake`


--------------------------------------------

# 2023-06-30-01

Release [2023-06-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-30-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(dossier.en_construction): ETQ usager, je souhaite pouvoir modifier un dossier ayant des repetition ayant des enfants (#9268)

### Technique

- T


--------------------------------------------

# 2023-06-29-02

Release [2023-06-29-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-29-02)  sur GitHub

## Améliorations et correctifs

### Admin
- amelioration(admin): ETQ admin, je peux de desactiver le suffix "facultatif" sur les labels des champs non obligatoires (demander au support) (#9263)

### Tech
- Tech: permet un fichier local de dev de config ou helpers ruby (#9265)

### Usager, Instructeur
- Harmonisation des espaces du sub header pour les differents profils (#9266)
- correctif(demande): ETQ usager, instructeur, je ne souhaite pas voir les champs conditionné et non visible dans un bloc repetable (#9267)


--------------------------------------------

# 2023-06-29-01

Release [2023-06-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-29-01)  sur GitHub

## Améliorations et correctifs

### Operateur

- ETQ Opérateur, je souhaite mettre à jour les données de l'API GEO (#9241)
- fix(typo) adresse électronique (#9254)

### Usager/Instructeur
- ETQ usager/instructeur: fix date de modification du dossier (#9252)
- ETQ Instructeur, je voudrais que les filtres fassent la différence entre les Bouches-du-Rhône et le Rhône (#9219)
- ETQ jusager/instructeur: n'affiche pas le titre de section deux fois (#9255)
- ETQ instructeur/usager: propositions d'améliorations visuelles de la page de demande (#9261)

### Technique
- Tech: update rails 7.0.4.3 => 7.0.5.1 fix CVE-2023-28362 in redirect_to (#9253)
- tech: re-active l'expiration des dossiers termines (#9262)


--------------------------------------------

# 2023-06-27-02

Release [2023-06-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-27-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ usager/instructeur: améliorations visuelles sur la page récap d'un dossier (#9249)
- tech(active-storage.queues.purge): place les jobs de purge ds low_priority (#9250)

### Technique

- T


--------------------------------------------

# 2023-06-27-01

Release [2023-06-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-27-01)  sur GitHub

## Améliorations et correctifs

### Instructeur

- correctif(champ.pj): ETQ usager et instructeur, l'affichage des PJs etait cassé (#9246)


--------------------------------------------

# 2023-06-26-02

Release [2023-06-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-26-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Tech: corrige une typo dans le job de migration des pjs (#9245)


--------------------------------------------

# 2023-06-26-01

Release [2023-06-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-26-01)  sur GitHub

## Améliorations et correctifs

### Usager

- ETQ usager, la page resumant ma demande est aux couleurs du DSFR (#9030)

### Administrateurs

- Mettre le libellé de la procédure en h1 et harmoniser le header (#9210)

### API
- feat(API): notify instructeurs when added or removed from procedure (#8658)

### Technique

- tech: place les pjs sous un namespace de type année/aa/bb/token (#9228)
- tech(expiration.dossiers): evite d'envoyer tous les mails d'expiration d'un coup (#9240)

## Notes de déploiement

:information_source: A partir de cette release, les nouveaux fichiers stockés dans l'object storage (pj, attestation ...) auront pour clé : `année/aa/bb/token` , voir #9228 . Un tache est disponible dans cette mm pr pour migrer les anciens fichiers sous ce nouveau format. Ce rattrapage est facultatif.

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230623160831_add_index_to_parent_dossier_id.rb`).


--------------------------------------------

# 2023-06-23-01

Release [2023-06-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-23-01)  sur GitHub

## Améliorations et correctifs

### UI
- Remplacer les cartes bleu et orange par des composants du DSFR (#9225)

### Instructeur
- améliore le wording du sous-titre de l'action "repasser en construction" (#9222)

### Usagers
- ETQ Usager, je ne veux pas recevoir d'email si j'ai supprimé mon compte (#9221)

### Administrateur
- [Page accueil demarche] Ajouter la possibilité de créer sa propre liste de PJ pour les Admins (#9234)


### Technique
- tech(logs): prevent /ping requests from being written in centralized logs (#9231)
- amelioration(email_event): re-lever une erreur dans un rescue_from ne la fait pas remonter. change de stratégie pour savoir si oui ou non un mail a ete envoye avec success. (#9209)
- correctif(dossiers.expirations): sur de gros volumes de données, le fait de mettre a jours tous les dossiers a supprime timeout face a PG. batch la maj (#9220)

### Correctif 
- Tech: fix missing column procedures#description_pj (#9238)
- ETQ Usager: fix dépot en construction après qu'un champ d'une répétition a été ajouté/modifié par une révision (#9239)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230621161733_add_pj_field_for_description_to_procedure.rb`).


--------------------------------------------

# 2023-06-20-02

Release [2023-06-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-20-02)  sur GitHub

## Améliorations et correctifs

### Administrateur

- Correction du reroutage - supprime la vue en doublon (#9218)

### Technique

- Test: update selenium & capybara pour meilleure compatabilité avec chrome 114 (#9217)


--------------------------------------------

# 2023-06-20-01

Release [2023-06-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-20-01)  sur GitHub

## Améliorations et correctifs

### Usager

-  [refonte usager] Tableau de bord - remonter les dossiers à corriger (#9201)

### Instructeur

- ETQ instructeur je peux réaffecter un dossier à un autre groupe d'instructeurs (#9093)

### Adminsitrateur

- ETQ admin, je ne veux plus voir l'IBAN comme un champ "suspect" (#9208)

### Technique

- fix(routing): migrate remaining data (#9132)
- Jobs: traite en isolation les dossiers bloqués de démarches déclaratives (#9195)
- Tests: fix tests instables et colonne manquante dans le schema.rb (#9215)
- Tech (seo): disallow /super_admins/ (#9213)
- tech(doc): documente le processus pour re-importer des dossiers perdus (#9216)
- Outillage: ajout d'une tache qui permet d'invalider des blob signed_ids (#9214)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230517085816_add_forced_groupe_instructeur_to_dossier.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230602165134_migrate_remaining_data_for_routing_with_dropdown_list.rake`).


--------------------------------------------

# 2023-06-16-01

Release [2023-06-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-16-01)  sur GitHub

## Améliorations et correctifs

### Usager

-  [Refonte page accueil demarche] Ajouter les infos concernant les PJ (#9121)

### Technique

- fix(commune): a commune can not be located in 99 (#9202)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230615175221_fix_champs_communes_99.rake`).


--------------------------------------------

# 2023-06-15-01

Release [2023-06-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-15-01)  sur GitHub

## Améliorations et correctifs

### Usagers
- Usager: met à jour le taux de conformité d'accessibilité à 80% #9192

### Instructeurs
- #9199 fix(dossier): only show optional text on public champs
- #9198 fix and test hack for procedures using groupe instructeur api

### Administrateurs

- Administrateur, je voudrais proposer de pré-remplir les annotations privées #9197 
- #9188  je vois une modale pour confirmer la réinitialisation des modifications de la procédure
-  #9166 je voudrais pouvoir pré-remplir un dossier sur une démarche en teste
-  #9196 je veux voir les erreurs lorsque la (re-)publication échoue


--------------------------------------------

# 2023-06-13-02

Release [2023-06-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-13-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Correction : rejoue le moteur de routage après ajout des règles de routage sur une procédure clonée (#9194)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (qui peut être ignorée par les autres instances): 
- [lib/tasks/deployment/20230613114744_replay_routing_engine_for_a_cloned_procedure.rake](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/pull/9194/files#diff-08bacdb3a74cc58f1de6deac5f8ade1b9767c8bfd888c7386a4f68266ee0f2cf)


--------------------------------------------

# 2023-06-13-01

Release [2023-06-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-13-01)  sur GitHub

## Améliorations et correctifs

### Usager

- fix(carto): no crash on points with elevation (#9171)
- Correction de coquilles et fautes d'orthographe (#9153)
- ETQ usager je suis guidé dans le formulaire lorsque les champs sont obligatoires ou facultatifs (#9050)

### Instructeurs

- ETQ instructeur: uniformise le wording "informé" sur les actions d'instruction et de correction (#9187)
- Design: corrige badge en_instruction & accepte pour avoir les badges, contrairement aux autres états (#9186)
- Ajoute un lien dans le menu d'export vers la doc sur les macros (#9155)

### Administrateurs

- Mise a jour de l'UX de la page groupe d'instructeurs (#9146)

### Technique

- refactor: use persisted? instead of created_at.present? (#9167)
- fix: exclue les chemins /rails/ des moteurs de recherche (#9190)


--------------------------------------------

# 2023-06-12-01

Release [2023-06-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-12-01)  sur GitHub

## Améliorations et correctifs

### Utilisateur

- [design] Changement de couleurs pour les bagdes de statuts des dossiers (#9156)
- Corrige le tableau de bord usager lorsqu'il y a un dossier supprimé sur une démarche en cours de suppression (#9158)

### Instructeurs

- ETQ instructeur je lis la date d'un filtre dans un format "humain" (#9160)

### Technique

- refactor(carto): geo area as component (#9165)
- ETQ utilisateur je veux que mes PJ soient supprimées en turbo (#9154)


--------------------------------------------

# 2023-06-09-01

Release [2023-06-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-09-01)  sur GitHub

## Améliorations et correctifs

 ## Technique

fait passer explicitement sentry par le proxy s'il existe (#9163)


--------------------------------------------

# 2023-06-07-01

Release [2023-06-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-07-01)  sur GitHub

## Améliorations et correctifs

### Admin

- Ajustements d'UI dans la liste et le détail des groupes d'instructeurs (#9133)

### Technique

- Correction rapide des pages de présentation pour les pages avec une notice_url mais pas de notice attaché (#9145)


--------------------------------------------

# 2023-06-06-01

Release [2023-06-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ dev fix colonne manquante dans schema.rb (#9127)
- [refonte page accueil demarche] ajouter lien vers la notice (#9135)
- [refonte page accueil demarche] Améliorations générales design (#9136)
- ETQ Usager, je voudrais pouvoir déposer les changements apportés à la carte dans un dossier en construction (#9140)
- perf(carto): use json schema to validate geojson instead of rgeo (#9142)
- wording: affichage au clique -> au clic (#9138)
- fix(carte): no autosave on champ carte (#9144)

### Technique

- T


--------------------------------------------

# 2023-06-05-01

Release [2023-06-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-05-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ETQ Instructeur je peux marquer un dossier "à corriger" par l'usager (#8714, #9134)

### Mineurs

- ETQ Admin: Formulaire de création admin full page (plus de prévisualisation en direct) (#9107)
- ETQ Usager [refonte page d'accueil démarche]: ajout du détail sur le temps d'estimation (#9116)
- ETQ Instructeur: correctif pour afficher sans erreur la colonne "avis" (#9128)

### Technique

- ETQ dev je ne veux plus de requêtes infinies sur /csp (#9124)
- Tech: dépendances yarn: update coldwired to 0.11 (#9123)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230228134859_create_dossier_corrections.rb`, `db/migrate/20230228134900_add_foreign_keys_to_dossier_corrections.rb`).


--------------------------------------------

# 2023-06-01-02

Release [2023-06-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-01-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ admin je ne veux pas pouvoir saisir une date d'auto archivage dans le passé (#9120)
- Routage: corrige l'autogestion des instructeurs (#9119)

### Technique

- API Entreprise: migration RNA (associations) (#9102)
- API Entreprise: migration "Etablissement" (#9106)
- API Entreprise: migration "bilans bdf" (#9100)
- API Entreprise: raison_sociale peut être null (#9118)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230601121416_fix_instructeurs_self_management_for_routed_procedures.rake`).


--------------------------------------------

# 2023-06-01-01

Release [2023-06-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-01-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- ETQ admin, j'ai du mal a comprendre l'interface qui me permet de gerer les autorisations qu'un jeton a (#9097)
- ETQ usager je peux joindre une pièce justificative audio en .m4a, .aac, .wav (#9108)
- Correctif API Entreprise: nom_commercial peut être vide (fix l'API et le comportement des démarches déclaratives) (#9117)


--------------------------------------------

# 2023-05-31-02

Release [2023-05-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-31-02)  sur GitHub

## Améliorations et correctifs

### Administrateurs

- Majeur : - Nouvelle UX pour le routage (#8940) !

### Technique

- tech(recovery.list_blob_ids): ajoute une tache pour exporter les clés des fichiers a restaurer (#9090)


--------------------------------------------

# 2023-05-31-01

Release [2023-05-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-31-01)  sur GitHub

## Améliorations et correctifs


### Usager 
- Merge branch 'main' into 8054-a11y-ways-of-navigating
- a11y : 8054 a11y ways of navigating (#8979)
- 8588 search engine footer optimization (#9096)
- [Refonte page accueil demarche] Détailler la description pour plus de clarté pour l'usager (#9074)

### Instructeur

### Administrateur
- Etq admin, lors de la création ou modification d'une démarche, des zones par défaut me sont suggérées (#9014)

### Super Admin
- ETQ SuperAdmin, je vois le groupe dans le détail d'un dossier (#9091)

### Technique
- ETQ intégrateur d’API, je voudrais voir un message d’erreur si le changement d’état échoue (#9085)
- ETQ tech je mets à jour Sentry pour essayer d'avoir de meilleures traces (#9089)
- ajoute un exemple de config pour utiliser l'environnement de staging d'API Entreprise (#9094)
- Api Entreprise : Migration Entreprise Tva  et Kbis (#9092)
- API Entreprise : migration "exercices" (#9099)
- API Entreprise : migration "attestation fiscale" (#9098)
- Migre les données pour le nouveau mode de routage (#8923)

## Notes de déploiement

:information_source: Il y a une grosse migration du a nouveau système de routage : un champ est ajouté à tous les dossiers routés, voir #8923

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230508103932_add_tchap_hs_to_zones.rb`, `db/migrate/20230508160551_create_default_zones_administrateurs.rb`, `db/migrate/20230516132925_add_description_target_audience_to_procedure.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230322172910_populate_zones_with_tchap_hs.rake`, `lib/tasks/deployment/20230417083259_migrate_data_for_routing_with_dropdown_list.rake`, `spec/lib/tasks/deployment/lib/tasks/deployment/20230417083259_migrate_data_for_routing_with_dropdown_list_spec.rake`).


--------------------------------------------

# 2023-05-25-01

Release [2023-05-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ usager je peux modifier un dossier en_construction qui contient des champs en erreur (#9086)
- ETQ usager invité sur un dossier, je peux ajouter une PJ d'un dossier en construction (#9088)
- ETQ usager: débloque l'accès à des dossiers à cause d'incohérences entre les champs et les révisions associées (#9077)
- ETQ usager: modification de wording du footer dans le cadre d'une démarche (#9087)

### Technique

- API Entreprise: migration attestation sociale v3 (#9084)


## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230522065646_fix_champs_revisions.rake`).

Si la variable d'environnement `API_ENTREPRISE_URL` est renseignée (aucune obligation, c'est optionnel), elle doit être modifiée en supprimant la version.
Ainsi, vous devez passer de 
`API_ENTREPRISE_URL="https://entreprise.api.gouv.fr/v2"`
à
`API_ENTREPRISE_URL="https://entreprise.api.gouv.fr"`

Si la variable d'environnement `API_ENTREPRISE_URL` n'est pas renseignée, vous n'avez rien à faire.


--------------------------------------------

# 2023-05-24-01

Release [2023-05-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Précise le contenu du total des dossiers pour les instructeurs (#9073)
- correctif(users/dossiers/brouillon#submit): ETQ usager, je souhaite que chaque lien pointant vers une erreur de champ m'oriente sur le champ (#9045)
- correctif(expert/avis#index): ETQ expert, je veux retrouver mes avis donnés sur des dossiers traités (#9054)
- ETQ admin: corrige une erreur lorsque je modifie mon dossier en construction d'une démarche en test (#9072)
- Corrige un bug de numérotation automatique sur Firefox (#9078)
- Correction : pas d'erreur lors de la suppression d'un type de champ si déjà supprimé (#9080)
- chore(recovery): import/export revision (#9071)
- ETQ Opérateur, je voudrais que les erreurs dans Sentry soient liées à la version de l'application (#9079)
- ETQ Intégrateur d’API, je voudrais savoir si l’utilisateur est connecté avec FranceConnect (#9081)
- ETQ instructeur je peux filtrer les dossiers par avis (#9076)
- ETQ exploitant je veux faire remonter les contacts Dolist en erreur dans sentry (#9075)
- Usager: plus de détails sur les raisons qui expliquent pourquoi je ne reçois pas l'email "mot de passe perdu" (#9032)

### Technique

- T


--------------------------------------------

# 2023-05-19-01

Release [2023-05-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ usager je veux voir un exemple d'url sans erreur lorsque la démarche n'existe pas (#9049)
- tech(recuperation-de-données): en cas de bug critique les techs DS aimeraient faciliter le re-import de données depuis un backup (#9055)
- fix(spec): specify vcr cassette for the whole scenario (#9060)
- amelioration(recovery:import): meilleure log et gestion de edge cases (#9057)
- chore(github): remove unused actions (#9059)
- fix(revision): backfill missing published_at on published revisions (#9061)
- fix(champs): update task to fix broken champ types (#9058)
- [fix] Vider les champs invalides pour permettre de cloner une procedure (#9051)
- chore(afterparty): do not delete dossiers on draft revisions (#9070)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230515155606_fix_champs_after_merge.rake`, `lib/tasks/deployment/20230516152457_fix_procedure_revision_publiee_without_published_at.rake`).


--------------------------------------------

# 2023-05-15-01

Release [2023-05-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-15-01)  sur GitHub

## Améliorations et correctifs

### Instructeurs 
- Actions multiples: corrige le compteur dans les alertes + ajouter des infos sur le créateur du batch (#9037)

### Usager 
- Correctif: le champ PJ d'un dossier en construction ne doit pas être marqué "à déposer" sans raison (#9035)
- Correctifs pour les modifications en construction (#9036)
- Amélioration des messages du compteur de caractères (#9034)

### Technique
- Corrige des tests instables (#9026)
- chore(procedure): log dossiers before reset (#9047)
- ETQ Opérateur, je ne veux pas voir de log de timeout d'API dans les log Sentry (#9046)


--------------------------------------------

# 2023-05-12-05

Release [2023-05-12-05](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-12-05)  sur GitHub

## Améliorations et correctifs

💣 instable — ne pas déployer

### Mineurs

- fix(after_party): type_de_champ can be nil (#9044)


--------------------------------------------

# 2023-05-12-04

Release [2023-05-12-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-12-04)  sur GitHub

## Améliorations et correctifs

💣 instable — ne pas déployer

### Mineurs

- fix(after_party): do not crash if missing champs for dossier (#9043)


--------------------------------------------

# 2023-05-12-03

Release [2023-05-12-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-12-03)  sur GitHub

## Améliorations et correctifs

💣 instable — ne pas déployer

### Mineurs

- Correctif: force le rebase du dossier origin en construction avant merge du fork (#9042)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230512155606_fix_champs_after_merge.rake`).


--------------------------------------------

# 2023-05-12-02

Release [2023-05-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-12-02)  sur GitHub

## Améliorations et correctifs

💣 instable — ne pas déployer

### Mineurs

- fix(dossier): task to fix cloned published revisions (#9041)


## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230512103830_fix_cloned_published_revisions.rake`, `spec/lib/tasks/deployment/20230512103830_fix_cloned_revisions_spec.rb`).


--------------------------------------------

# 2023-05-12-01

Release [2023-05-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-12-01)  sur GitHub

## Améliorations et correctifs

💣 instable — ne pas déployer


### Mineurs

- Tech: notifie les devs lorsque le status d'un contact n'existe pas chez Dolist (#9033)
- Ajouter des bannières visant soit les instructeurs / admins soit les usagers (#9020)
- Usager: améliore la visibilité du bouton action dans le tableau dossiers des usagers (#9029)
- Revert "Merge pull request #9002 from tchak/feat-procedure-clone-prev… (#9040)


--------------------------------------------

# 2023-05-11-01

Release [2023-05-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-11-01)  sur GitHub

## Améliorations et correctifs

💣 instable — ne pas déployer, aller directement à [2023-05-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-15-01)

### Administrateurs
- Correctif:  je souhaite pouvoir cloner une procedure ayant deux groupes d'instructeur ayant le dernier fermé (#9005)
- Correctif: je veux cloner la version publiée des démarches en production (#9002)
- Nouveau: je veux pouvoir limiter le nombre de caractères dans les champs "texte long" (#8990)
- Nouveau: je veux avoir une première démarche pré remplie lorsque j'arrive sur ds (#8997)
- Correctif:  je souhaite que la numerotation automatique des titres de section fonctionne meme quand si deux titres de sections se succèdent (#9022)
- Correctif d'affichage d'un avertissement dans le résumé des changements bloquants d'une nouvelle révision (#9024)

### Instructeurs
- Correctif: je ne devrai pas pouvoir sélectionner des champs non filtrables/affichables (#9018)
- Correctif: je souhaite avoir les mêmes compteurs entre les pages listant mes demarches et la page pour visualiser une démarches (#9023)
- Correctif: affichage du statut de l'entreprise quand elle est fermée/a cessé (correctif pour les nouveaux dossiers uniquement) (#9031)

### Usagers
- Amélioration: je modifie et soumets à nouveau mon dossier “en construction", et je peux supprimer une PJ obligatoire (#9021)
- Amélioration: je souhaite pouvoir acceder aux champs en erreur facilement (#8987)
- Refonte Tableau de bord; Ajouter bouton filtres simples (#8994)

### Technique
- ETQ développeur, je veux que les dépendances npm soient à jour (#9000)
- ETQ développeur je veux pouvoir utiliser les urls d'attachments en local (#9001)
- Email (Dolist): pas d'erreur lorsqu'un mail n'a pas pu être envoyé vers un nouveau contact (#9016)
- Email: étale un peu dans le temps l'envoi des rapports aux instructeurs pour éviter le quota Dolist (#9017)
- Ajoute la liste de boutons et le composant toggle au format ds-fr (#9015)
- Essaye de corriger des tests instables (#9025)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230113165022_add_editing_forks_to_dossiers.rb`).


--------------------------------------------

# 2023-05-04-03

Release [2023-05-04-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-03)  sur GitHub

## Améliorations et correctifs

### Correctifs

- ETQ Instructeur, je ne veux pas voir les dossiers terminés dans l'onglet "suivis" (régression rails 7) (#9003)
- ETQ Instructeur, je ne veux pas d'erreurs quand les filtres ne sont pas trouvés (#8989)
- ETQ instructeur, ne plante pas un export s'il contient des PJ avec des noms de fichiers invalides (#8999)

### Nouveau
- ETQ administrateur, je peux prévisualiser la démarche depuis la page "toutes les démarches" (#8995)


--------------------------------------------

# 2023-05-04-02

Release [2023-05-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-02)  sur GitHub

## Améliorations et correctifs

💣 Ne pas déployer cette version, mais la 2023-05-04-03 qui comprend des correctifs.

### Instructeur et Usager

- Corrige le téléchargement des attestations (régression introduite par Rails 7) (#8998)


--------------------------------------------

# 2023-05-04-01

Release [2023-05-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Tech: migration vers Rails 7, ruby 3.2 (#8736)

## Notes de déploiement

💣 Ne pas déployer cette version, mais la 2023-05-04-03 qui comprend des correctifs.

D'autres releases viendront compléter cette migration Rails 7 dans les prochaines semaines pour incorporer quelques changements de config.

Cette version comporte des migrations du schéma de la base de données

- `db/migrate/20230328125207_remove_not_null_on_active_storage_blobs_checksum.active_storage.rb`
- `db/migrate/20230502160046_add_otp_secret_to_super_admin.rb`


Deux nouvelles variables d'environnement sont nécessaires :   `AR_ENCRYPTION_PRIMARY_KEY` et `AR_ENCRYPTION_KEY_DERIVATION_SALT`.
Cf https://github.com/demarches-simplifiees/demarches-simplifiees.fr/blob/f4ccc5c717b0c4f266e5caa70667a5340f3fa730/config/env.example#L145


--------------------------------------------

# 2023-04-28-02

Release [2023-04-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-28-02)  sur GitHub

## Améliorations et correctifs

### Usagers

- [fix] Pagination interface usager - onglets disparaissent si pas sur la page 1 (#8971)
- correctif(titre-de-section): ETQ usager, je souhaite que les titres de sections ne soient pas préfixé d'un 0. (#8983)

### Technique

- Corrige des tests peu fiables (#8986)
- ETQ opérateur, je veux revert les validations cassées (#8988)
- ETQ usager, je veux être avertie si l'API entreprise est HS (#8984)


--------------------------------------------

# 2023-04-28-01

Release [2023-04-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ usager, quand une les balises `<select>` avec une `<option>` ayant un libelle tres long, le select prends la largueur du contenu de l'option et force un scroll horizontal sur la page (#8961)
- ETQ usager, je veux que le focus suive les changement d'interface (#8956)
- Tech: ne mélange pas class & module Dolist, supprime adapter SMTP (#8978)
- ETQ instructeur, je veux que tous les dossiers aient un demandeur (#8980)
- ETQ opérateur, je souhaite corriger des valeurs invalides de codes départementaux (#8981)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230427090118_fix_dossiers_with_missing_identification.rake`, `lib/tasks/deployment/20230427094648_normalize_commune_code_departement.rake`).


--------------------------------------------

# 2023-04-27-01

Release [2023-04-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-27-01)  sur GitHub

## Améliorations et correctifs

### Instructeurs
- ETQ instructeur, je peux relancer une demande d'avis meme si celui ci contient une question (#8966)
- [instructeur] ajout du nb de réponses oui/non aux avis dans le tableau d'une procedure (#8936)

### Administrateurs
- ETQ administrateur, je veux être averti si des données seront supprimées sur les dossiers lors de la publication d’une nouvelle révision (#8963)
- Correction de faute de frappe sur la page de présentation aux administrations (#8959)
- Filtrer les démarches par service (#8887)

### Usagers
- ETQ usager, je veux que les champs de type adresse électronique soit validé (#8899)
- ETQ usager, je veux que mon interface soit traduite en anglais (#8921)
- ETQ usager utilisant microsoft Edge, les champs de type date sont mal alignés (#8962)
- ETQ usager, je trouve que les titres de section sont trop fort en graisse de caractère (#8952)
- ETQ usager, je ne veux pas bypasser la vérification de SIRET (#8976)


### Technique
- Correctif: definit le groupe instructeur par defaut pour certaines procedure clonées (#8968)
- Corrige un test non fiable (#8977)
- spec: fix flaky test (#8975)


--------------------------------------------

# 2023-04-26-01

Release [2023-04-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-26-01)  sur GitHub

## Améliorations et correctifs

### Usagers

- ETQ Utilisateur, je ne veux pas de caractère invalide dans l'état du dossier en PDF (#8964)
- ETQ Usager, champ carte: ne permet pas d'enregistrer une geometry null pour ne pas casser les exports (#8948)

### Administrateurs
- ETQ administrateur empêche une condition d'égalité de s'applique à un champ choix multiple (#8967)



### Technique
- [fix] erreur dans le nom de variable (#8960)
- Corrige la logic dans le cas de comparaison avec une liste de choix multiple (#8954)


## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230421160218_fix_geo_area_without_geometry_again.rake`, `lib/tasks/deployment/20230424154715_fix_include_in_logic.rake`, `spec/lib/tasks/deployment/20230424154715_fix_include_in_logic.rake_spec.rb`).


--------------------------------------------

# 2023-04-25-01

Release [2023-04-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-25-01)  sur GitHub

## Améliorations et correctifs

### Usager

- [refonte usager] Tableau de bord - Améliorer pagination (#8947)
- [refonte usager] Tableau de bord - Ajouter continuer à remplir dernier dossier (#8894)

### Instructeur

- ETQ instructeur, je veux pouvoir accéder à mes annotations privées (#8955)


--------------------------------------------

# 2023-04-24-01

Release [2023-04-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-24-01)  sur GitHub

## Améliorations et correctifs

### Administrateur
- Clone de démarche: corrige `defaut_groupe_instructeur_id` (fix de la release 2023-04-17-01) (#8949)

### Usager
- ETQ usager, correction sur les titres de section conditionnés (#8950)

### Technique
- correctif(procedure.declarative): ETQ administrateur d'une procedure declarative, certains de mes dossiers restent en construction [ex: l'object storage est down, le dossier reste bloqué] (#8941)


--------------------------------------------

# 2023-04-21-01

Release [2023-04-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-21-01)  sur GitHub

#:information_source:  A ne pas déployer, la tache de correction des démarches clonées est modifiée dans la prochaine release.

## Améliorations et correctifs

### Administrateur

- Fix: inclus au moins 3 répétitions dans le formulaire au format PDF (#8937)
- Clone de démarche: corrige `defaut_groupe_instructeur_id` (fix de la release 2023-04-17-01) (#8942, #8945)

### Instructeurs
- Fix: Lorsque je saisie une autre valeur dans un champ de type choix simple, celle ci n'était pas sauvegardé (#8938)
- Fix: ETQ utilisateur ou instructeur je ne veux pas perdre le caractère `_` au sein d'urls dans mes messages (#8934)
- Corrige des utilisations incorrectes du verbe notifier (#8939)

### Usager
- ETQ usager, je peux créer de nouveaux dossiers sur la démarche qui remplace la démarche fermée (#8943)

### Divers
- ETQ super-admin je peux éditer l'attribut d'une démarche qui pointe vers la démarche qui la remplace (#8944)

## Notes de déploiement

Cette version comporte des migrations du contenu des données: 
- `lib/tasks/deployment/20230421091957_fix_defaut_groupe_instructeur_id_for_cloned_procedure.rake`


--------------------------------------------

# 2023-04-20-01

Release [2023-04-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-20-01)  sur GitHub

## Améliorations et correctifs

### Instructeurs
- ETQ instructeur, je veux voir l'état des dossiers affiché sur une seule ligne (#8920)

### Administrateurs
- ETQ administrateur, je peux specifier un niveau de titre a mes titres de sections (#8695)
- ETQ administrateur, je peux changer le groupe d'instructeur par défaut (#8916)

### Usagers
- ETQ usager, Tableau de bord - Déplacer la barre de recherche proche des dossiers (#8912)
- ETQ usager, Simplifie l'implémentation des champs "liste d'options" et "listes d'options liées" (#8850)
- ETQ usager, je voudrais avoir accès aux statistiques des démarches closes (#8928)
- ETQ usager, regroupe les champs précédés d'un titre de section dans un fieldset (#8695)
- ETQ usager, je voudrai pouvoir remplir un code postal avec des espaces (#8930)

### API 
- ETQ intégrateur API, je ne veux pas avoir d'erreurs lorsque j'interroge les métadonnées de certains fichiers (#8919)
- ETQ intégrateur API, je voudrais avoir accès aux dossiers récemment supprimés d’un groupe instructeur (#8888)
- ETQ intégrateur API, je voudrais avoir des codes d'erreur plus précis (#8918)
- ETQ DS API, log dossier and procedure id on dossier fetch endoint (#8927)

### Correctif
- ETQ usager, Améliorer l'affichage des resultats de recherche (#8913)
- ETQ visiteur, ne plus afficher le telephone de la dinum sur la page declaration d'accessibilite (#8922)
- ETQ usager, désactive la checkbox pour tout séléctionner quand une action mutliple est en cours (#8902)



### SuperAdmin
- ETQ opérateur, je voudrais pouvoir consulter plus facilement l’état d’une démarche (#8929)

### Technique
- amelioration(dolist): ne log erreurs pas les erreurs dans sentry lorsque le contact chez dolist est injoingable ou hardbounce (#8907)
- chore(procedure): remove duplicate code (#8925)
- Tech: update rubocop, active nouveaux cops Rails/* (#8924)


--------------------------------------------

# 2023-04-17-01

Release [2023-04-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- amelioration(dolist): desactive le tracking http (#8908)
- feat(routing): nicer and safer ? (#8836)
- chore(api-geo): bump cache keys (#8917)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230331124617_add_defaut_groupe_instructeur_id_to_procedures.rb`, `db/migrate/20230331125409_add_defaut_groupe_instructeur_foreign_key_to_procedures.rb`, `db/migrate/20230331125931_validate_add_defaut_groupe_instructeur_foreign_key_to_procedures.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230407124517_back_fill_procedure_defaut_groupe_instructeur_id.rake`, `spec/lib/tasks/deployment/20230407124517_back_fill_procedure_defaut_groupe_instructeur_id_spec.rake`).


--------------------------------------------

# 2023-04-13-01

Release [2023-04-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix link for csv example file (#8891)
- chore(deps): bump nokogiri from 1.14.2 to 1.14.3 (#8898)
- chore(dossier): remove fallback from commune champ (#8889)
- fix(dossier): delete all champs starting with children (#8897)
- fix(dossier): find communes outside of departement (#8901)
- chore(npm): update dependencies (#8869)

### Technique

- T


--------------------------------------------

# 2023-04-11-01

Release [2023-04-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dossier): normalize champs commune data (#8885)
- fix(admin): peut supprimer un admin sans démarche publiée, sans service (#8886)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230407132756_normalize_communes.rake`).


--------------------------------------------

# 2023-04-07-01

Release [2023-04-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-07-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(graphql): pendingDeletedSince should not crash (#8873)
- fix(dossier): selecting blank option should not empty selection (#8872)
- feat(dossier): use turbo to select linked dossier (#8858)
- fix(dossier): instructeur actions should be buttons and not links (#8874)
- feat(graphql): expose commune and departement information on address (#8867)

### Technique

- T


--------------------------------------------

# 2023-04-06-01

Release [2023-04-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- amelioration: ajoute la possibilité d'utiliser sendmail pour envoyer les emails (#8866)
- amelioration(a11y): actions multiples (#8860)


### Technique
- correctif(api-entreprise): ne crash pas quand le ne peut etre trouvé est absent (#8868)
- correctif(firefox.68): does not like latest zod version (#8871)


--------------------------------------------

# 2023-04-05-01

Release [2023-04-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-05-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(graphql): demandeur personne morale can be in degraded mode (#8861)
- fix(turbo): add getRootNode polyfill (#8863)
- fix(date): ne crash pas lorsqu'un champ date n'as pas une date standard (#8864)
- feat(graphql): improuve logs (#8865)

### Technique

- T


--------------------------------------------

# 2023-04-04-01

Release [2023-04-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(dossier): multiselect without react (#8824)
- [bug] retirer les notifications + compteur des avis pour les dossiers terminés (#8845)
- fix(condition_check_on_children): give proper upper tdc to child cond… (#8851)
- a11y : add mobile environment test to accessibility statement (#8849)
- [bug] Changer couleur de fond pour faire apparaitre bouton conditionnelle sur les sections (#8852)
- fix(dossier): an invited user can remove (leave) a dossier (#8847)
- fix(dossier): improuve commune champ rendering in pdf (#8853)
- bug(administrateur.merge): la fusion de compte entre admin devrait aussi dissocier les services des procedures supprimées (#8856)
- correctif(data): certaines procedures supprimées etaient invalides du fait qu'elle n'avaient pas ete mise a jour sur le critere du nom de routage (#8857)
- WIP [manager] affiche les zones d'une procédure donnée (#8855)
- fix(graphql): return better errors when dossiers cannot change state (#8846)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230404141339_fix_backfill_procedure_routing_criteria_name.rake`).


--------------------------------------------

# 2023-04-03-01

Release [2023-04-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Webinaire link fix (#8814)
- correctif(export.pdf): ETQ expert, lorsque j'exporte un dossier au format PDF, celui ci contenient les avis non confidentiels ainsi que mes avis (#8817)
- Fix: autolink manuellement les liens qui ne sont pas des urls (#8820)
- Fix avis: n'affiche pas le form de réponse à la question s'il n'y a pas de question (#8816)
- FIX - Garde les tags dans les params lors de la pagination (#8815)
- Webinaire link fix (#8814)
- correctif(export.pdf): ETQ expert, lorsque j'exporte un dossier au format PDF, celui ci contenient les avis non confidentiels ainsi que mes avis (#8817)
- Fix: autolink manuellement les liens qui ne sont pas des urls (#8820)
- refactor(commune): choisir la commune par son code postal (#8783)
- feat: allow classic SMTP for email sending (#8818)
- feat(graphql): log type and timeout errors (#8822)
- chore(npm): update dependencies (#8823)
- fix(rails): smtp config should be optional (#8825)
- Fix champ explication: cache le texte qui doit être caché (#8826)
- Fix (manager): ne trie pas par created_at pour ne pas casser la prod (#8828)
- [Fix] Petites améliorations de la vue expert (#8829)
- Fix: met à jour des liens du pied de page des démarches (#8830)
- ETQ instructeur je veux pouvoir supprimer la pj pendant l'instruction (#8811)
- patch(demarche.lien_demarche): ignore cette colonne pr la supprimer plus tard (#8649)
- Add routing rules to groupe instructeurs (#8774)
- a11y(bloc-repetable): amélioration des interactions avec les répétitions d'un bloc répétable (#8699)
- chore(données): supprime la table `drop_down_lists` qui est inutilisée (#8832)
- correctif(revision.validation-des-conditions): les conditions dans un bloc répétable ne remontenpt pas dans le composant ErrorsSummary (#8833)
- [fix] bug dans le sujet des mails de notification si le libelle de la procedure contient un apostrophe (#8837)
- fix(commune): improuve label and error message (#8835)
- fix(dossier): add value_json to dossier projection (#8842)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230228073530_add_routing_column_to_groupe_instructeur.rb`, `db/migrate/20230331075755_drop_table_drop_down_lists.rb`).


--------------------------------------------

# 2023-03-29-01

Release [2023-03-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-29-01)  sur GitHub

## Améliorations et correctifs

💣 **Instable**: un correctif sera disponible dans la prochaine release.

### Mineurs

- Instructeur: Je peux poser une question dans une demande d'avis (#8743, #8789, #8816)
- Instructeur: amélioration UX de la demande d'avis (#8770, #8810)
- instructeur: Ajoute un raccourci pour copier un SIRET (#8781)
- Champ: les champs de type decimaux ont comme placeholder 49.3 (#8773)
- Champ carte: n'affiche pas toutes les "features" s'il yen a plus que 20 (#8782)
- Administrateur: je peux bloquer l'accès aux experts à la messagerie (#8805)
- Administrateur: conserve les filtres dans de la pagination de "toutes les démarches" (#8815)
- Fix: le "détail" du champ explication peut être formatté avec des liens (#8799)
- Fix: les annotations privées n'étaient pas correctement rebasées après modif de leur formulaire (#8796)
- Fix: affichage plus propre du multi-select (#8792)
- Accessibilité: ajoute l'attribut title précisant l'ouverture dans un nouvel onglet dans le simple renderer (#8798)
- Accessibilité: internalise la page de mentions légales (#8762)
- Accessibilité: internalise la page de déclaration d'accessibilité (#8800)
- Accessibilité: augmente le contraste de la barre de progression lorsqu'elle a le focus (#8794)
- API: expose pendingDeletedDossiers (#8786)
- API: ameliore la documentation de certains champs graphql (#8788)
- Manager: permet l'override de l'upload multiple de PJ (#8808)
- Manager: créé un dashboard exports (#8791)
- Manager: desactive le 2FA suivant la config (#8726)

### Technique

- Fix: corrige peut-être des deadlocks postgresql sur le champ SIRET (#8776)
- Fix champ: use champ.required? (#8784)
- chore(npm): update vitejs to fix legacy build issues (#8813)
- chore(npm): update dependencies (#8793)
- fix(api_token): improuve flaky test (#8795)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230303094613_add_question_columns_to_avis.rb`, `db/migrate/20230322150907_add_allow_expert_messaging_to_procedures.rb`).


--------------------------------------------

# 2023-03-16-02

Release [2023-03-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-02)  sur GitHub

## Améliorations et correctifs

### Technique

Fix déploiement de la release précédente [2023-03-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-01) (#8772)

Cette version peut être considérée comme **stable**.

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230316125244_fix_private_champ_type_mismatch.rake`).


--------------------------------------------

# 2023-03-16-01

Release [2023-03-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-01)  sur GitHub

## Améliorations et correctifs 

💥 Cette version comporte un bug de déploiement, déployez [2023-03-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-02) directement
⚠️ Mise à jour puma de v5 à v6: les scripts de déploiement nécessitent peut-être d'être ajustés

Cette version déprécie les jetons d'api v1 et v2. La tache rake `token_expiration_email:send_warning` envoie un mail aux personnes concernées. On vise une suppression des jetons concernés le 1 juillet 2023.

### Mineurs


- Nouveau: active la sauvegarde automatique des annotations privées (#8727)
- Usager: Corrige l'alignement des badges du dossier côté usager (#8755)
- Amélioration dossier pdf: remplace les "feature collections" du champ carte par les libellés de sélection utilisateur (#8765)
- API: active tokens avec droits d'accès ciblés (#8623)
- API: fix return empty array when no attachments (#8768)
- API: déprécie les jetons d'api v1 / v2 (#8771)
- Fix: crash d'annotations privées à cause d'une incohérence avec leur type de champ (#8769)
- Fix: masque le nom de l'entreprise aux usagers lorsqu'elle exerce sont droit de non diffusion (#8761)
- Manager: liens directs vers les pages ETQ admin et ETQ instructeur d'une démarche (#8757)
- Job: détruit en batch les dossiers en brouillon expirés (#8756) 

### Technique

- update to puma 6.1.1 (#8764)
- Better api log (#8766)
- Infra: log process id traitant chaque requête (#8763)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230213123229_add_read_only_and_procedure_ids_to_api_tokens.rb`).

Cette version comporte une migration du contenu des données qui ne peut pas être lancée dans cette release, mais qui est corrigé dans la release [2023-03-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-02) (`lib/tasks/deployment/20230316125244_fix_private_champ_type_mismatch.rake`).


--------------------------------------------

# 2023-03-10-01

Release [2023-03-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-10-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- [usager] peut révoquer une demande de transfert (#8728)
- correctif(procedure.maj): quand la duree conservation dans ds est supérieur a 12, on ne pouvait plus mettre a jour la procedure (#8754)

### Technique

- T


--------------------------------------------

# 2023-03-09-01

Release [2023-03-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur: permet l'import CSV de groupes instructeur avec routage lorsque la démarche n'a pas encore de routage actif (#8733)
- Fix Administrateur: le clone d'une démarche ne copie pas le cache du nombre de dossiers (#8748)
- Fix Administrateur: ne supprime pas les tags quand on n'interagit pas avec la liste de sélection (#8749)
- Fix dossier: ne numérote pas automatiquement les titres de section dans les répétitions (#8753)
- Fix typo sur la page d'accueil (#8752)


### Technique

- Jobs: rallonge durée max pour les exports, fix des timeouts, isole DossierRebase (#8750)
- chore(deps): bump rack from 2.2.6.2 to 2.2.6.3 (fix CVE-2023-27530) (#8751) 

## Notes de déploiement

Une nouvelle queue delayed_jobs a été créé: `low_priority`. Elle vise à isoler des jobs qui peuvent être empilés en masse et/ou qui mettent du temps à être traités, pour ne pas interférer avec la queue `default`.


--------------------------------------------

# 2023-03-07-01

Release [2023-03-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-07-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(dossier.export.pdf): l'export au format pdf d'un dossier ayant un champ de type repetition contenant un titre etait mal rendu (#8746)
- correctif(cadastre): certaines parcelles cadastrales ne contiennent pas la surface. (#8747)
- fix(a11y): improve Explication "lire plus"  accessibility (#8678)

### Technique

- T


--------------------------------------------

# 2023-03-06-01

Release [2023-03-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-06-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Administrateur:  je peux filtrer avec plusieurs tags (#8720)
- Instructeur: Uniformise les actions pour les instructeurs sur la page tableau et dossier (#8630)
- instructeur: amélioration UX du bouton d'instruction (#8701)
- Instructeur: integre les avis dans l'export au format pdf du dossier (#8744)
- Instructeur & Usager: implémentation des classes DSFR pour les badges d'état des dossiers (#8710)
- Fix: un groupe instructeur inactif est valide tant qu'il reste un groupe actif (#8729)
- Fix: affichage de yes/no en anglais (#8735)
- Préremplissage: fait du get sans stored query  (#8622)

### Technique
- Corrige des erreurs récurrentes dans des jobs, notamment dans les jobs type "cron" (#8723)
- Schema: ajoute la clé etrangère entre active_storage_attachements et active_storage_blobs uniquement si elle est manquante (#8721)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230302161322_fix_again_hidden_by_reason_nil.rake`).


--------------------------------------------

# 2023-03-02-01

Release [2023-03-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-02-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(dossier): prefill address champ (#8707)
- fix(graphql): fix etablissement in degraded mode (#8724)
- feat(groupe instructeur): do not notify group when instructeurs removed (#8722)
- correctif(admin/procedures#champs): ajoute le formulaire pour supprimer une PJ (#8725)

### Technique

- T


--------------------------------------------

# 2023-03-01-01

Release [2023-03-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-01-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

#### Administrateurs
- Liste toutes les démarches, même celles non associées à des zones (#8706)
- Ne publie pas le descriptif de toutes les procedures sur datagouv (#8711)
- Passe le flag opendata à false pour certaines procédures (#8717)
- Améliore turbo refresh sur la page "toutes les demarches" (#8709)

#### Instructeurs

- Fix numérotation automatique des annotations privées (#8712)

#### Champs / usager
- Accessibilité : instructions de l'autocomplete pour lecteurs d'écran et annonce l'arrivée des résultats (#8677)
- Champ nombre : désactive le changement au scroll (#8716)
- Champ Date et heure : amélioration pour les vieux navigateurs (#8718)
- Pré-remplissage: support du champ Annuaire Education(#8625)
- Refacto: déplace le modèle de barre de progression en composant haml (#8639)

#### API
- add dossier state api (#8690)
- fix pour etablissements incomplets issus du mode dégradé (#8713)

### Technique

- chore(env): add ELASTIC_APM_* environment variable to env.example.optional file (#8688)
- Fix dossier controller spec (#8703)


--------------------------------------------

# 2023-02-28-01

Release [2023-02-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-28-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(combo-address): provide a valid empty response (#8702)
- fix prefill possible values (#8704)


--------------------------------------------

# 2023-02-27-01

Release [2023-02-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-27-01)  sur GitHub

## Améliorations et correctifs

Cette version est considérée comme **instable**.

### Mineurs

- Titres de section: la numérotation automatique tient compte du conditionnel (#8659)
- Accessibilité: traduit les menus contextuels d'aide, normalise l'affichage et les icônes (dsfr) (#8673)
- fix(autocomplete): avoid double escape of query params (#8698)
- fix(procedure): fix translations in changes component (#8689)
- fix(filter): fix find type de champ by stable_id (#8694)
- Prefill repeatable (#8513)
- feat(dossier): prefill communes champ (#8685)
- feat(dossier): prefill siret champ (#8388)
- feat(dossier): prefill rna champ (#8424)
- feat(dossier): prefill dossier link champ (#8679)


## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230221100840_strip_type_de_champ_libelle.rake`).


--------------------------------------------

# 2023-02-24-01

Release [2023-02-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-24-01)  sur GitHub

## Améliorations et correctifs

Cette version est considérée comme **instable**.

### Mineurs

- fix(demarche): exclude preview and deleted dossiers from stats (#8686)
- correctif(annotations-privee): ajout du delete form component pour supprimer une annotation privee (#8693)
- correctif(filtres): les filtres par colonne se font par stable_id, non pas par id (#8692)
- feat(apigeo): enable all zones on departements api (#8680)

### Technique

- T


--------------------------------------------

# 2023-02-23-01

Release [2023-02-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(instructeurs): combobox layout in "personnes impliquees" tab (#8666)
- feat(groupe_instructeurs): import instructeurs in unrouted procedures with a proper CSV (#8396)
- fix(a11y/upload): role=status on progress antivirus & watermark (#8674)
- ci: enable merge queue (merge group) (#8669)
- fix(flash messages): allow links in flash messages (#8675)
- feat(instructeurs import): notify added instructeurs from import (#8681)
- docs: add DATAGOUV_STATISTICS_DATASET environment variable  to env.example.optional (#8616)
- amelioration(a11y): rend du texte saisi (par les admin/instructeur/expert) en html compatible avec les contraintes d'accessibilités (#8670)
- publish opendata demarches only if they are publiees or closes (#8687)

### Technique

- T


--------------------------------------------

# 2023-02-22-01

Release [2023-02-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-22-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- en tant qu'admin, je peux voir le nb de dossiers par démarche dans la liste de toutes les démarches (#8369)
- correctif(attachement_edit): ajoute le composant <form> pour supprimer un attachment via le nouveau <button> (#8667)

### Technique

- fix(graphql): context should correctly preserve demarche authorization state (#8668)
- fix(graphql): fix demarcheUrl (#8636)
- chore(bundle): setup elastic_apm, disabled by default (#8655)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230217094119_add_estimated_dossiers_count_and_dossiers_count_computed_at_to_procedures.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230223103427_update_procedure_dossiers_count.rake`).


--------------------------------------------

# 2023-02-21-01

Release [2023-02-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- remove departements and regions migration specs (#8624)
- correctif(procedure#filtre): passe a l'usage de nos filtres par enum pour la recherche par departement (#8652)
- feat(groupe instructeur mailer): notify instructeurs when added to procedure (#8653)
- correctif(a11y): utilise un <button> a la place d'un <a> lorsqu'on effectue l'action de supprimer un attachement (#8657)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230216041517_remove_champs_external_id_index.rb`).


--------------------------------------------

# 2023-02-16-02

Release [2023-02-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-16-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Merge branch 'main' into ensure-pg-fk-between-attachments-and-blobs
- correctif(db): force active_storage_attachments.blob fk on active_storage_blobs (#8648)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230216141558_validate_foreign_key_between_attachments_and_blobs.rb`).


--------------------------------------------

# 2023-02-16-01

Release [2023-02-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-16-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(procedures/all): table layout issue (#8609)
- feat(graphql): add tracing support for managers (#8635)
- a11y(main_menu): utilise aria-current=true plutot que avec page (#8592)
- Revert "Merge pull request #8635 from tchak/graphql-with-traces" (#8637)
- feat(groupe instructeur mailer): add emailing to removed instructeurs (#8626)
- prefill(dossier): prefill multiple drop down list champ (#8479)
- fix(administrateur): procedure page n+1 (#8611)
- a11y : add scrolling functionnality for description procedure Closes #8629 (#8629)
- correctif(db): index manquant sur les active_storage_attachements.blob_id -> active_storage_blob.id (#8647)
- ETQ utilisateur je peux voir le mot de passe que je choisis (#8632)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230216130722_fix_active_storage_attachment_missing_fk_on_blob_id.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230216135218_reclean_attachments.rake`).


--------------------------------------------

# 2023-02-15-01

Release [2023-02-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-15-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(dossier): prefill departements champ (#8472)
- a11y : optimization burger menu dsfr Closes #8608 (#8608)
- a11y : update label button Closes #8627 (#8627)
- correctif(procedure/all.xsls): deconnecte le lien de telechargement de toutes les demarche de turbo (#8619)
- fix(geometry): implement our own bbox to replace rgeo (#8631)
-  feat(dossier): prefill epci champ (#8576)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230215100231_normalize_geometries.rake`).


--------------------------------------------

# 2023-02-13-01

Release [2023-02-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- a11y : optimization return to the line of longs links (#8606)
- feat(prefill): harmonize prefill buttons (#8598)
- migrate(champs): normalize regions (#8593)
- typo(fix): text+e (#8618)
- correctif(messagerie): autorise l'usage des balises <a> dans la messagerie quand les messages viennent de l'administration (#8617)
- migrate(champs): normalize departements (#8595)
- Email: limite le sujet généré à 100 caractères et utilise le sujet par défaut si le modèle est vide (#8613)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230207144243_normalize_regions.rake`, `lib/tasks/deployment/20230208084036_normalize_departements.rake`, `spec/lib/tasks/deployment/20230207144243_normalize_regions_spec.rake`, `spec/lib/tasks/deployment/20230208084036_normalize_departements_spec.rake`).


--------------------------------------------

# 2023-02-09-01

Release [2023-02-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-09-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

#### Administrateurs

- n'envoie plus un email à tous les instructeurs lorsqu'un nouvel instructeur est ajouté (#8581) 
- permet l'import de groupes instructeurs en CSV quand la démarche est close (#8605)
- je peux cloner une démarche à partir de la vue "toutes les démarches" (#8551)
- fix erreur lors d'une modification d'une démarche en test lorsqu'elle contient des dossiers qui sont référencés dans des opérations de masse (#8585)

#### Instructeurs & dossiers

- fix erreur lorsqu'on génère un export qui avait déjà été généré (#8603)
- corrections de crash de dossier suite à l'activation d'une nouvelle révision (#8587 et #8594)

#### Usagers

- je peux donner mon avis sur "services publics +" depuis un mail reçu une fois mon dossier traité (#8577)

#### Divers 

- API: graphql: fixing couple of n+1 (#8604)
- fix(revision): backfill missing published_at (#8607)
- fix(geo_area): backfill geo_area.geometry must be valid JSON, not nil (#8601)

### Technique

- secu(graphql): log full queries and variables (#8596)
- fix(lograge): send client_ip and request_id to es (#8599)
- chore(prod): decrease log level to info in production (#8597)

## Notes de déploiement

- Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230208154144_fix_geo_area_without_geometry.rake`, `lib/tasks/deployment/20230209093144_backfill_revisions_published_at.rake`).

- Cette version comporte une nouvelle variable d'environnement optionnelle : `SERVICES_PUBLICS_PLUS_URL`


--------------------------------------------

# 2023-02-08-01

Release [2023-02-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-08-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- add external id index to champs (#8590)
- fix(dolist): n'envoie pas de mail par Dolist API s'il contient des destinataires cachés (#8591)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230207105539_add_external_id_index_to_champs.rb`).


--------------------------------------------

# 2023-02-07-02

Release [2023-02-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-07-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dolist): dolist method not defined (#8583)

### Technique

- T


--------------------------------------------

# 2023-02-07-01

Release [2023-02-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- amelioration(traitement.multiple): permet de selectionner plus d'une page pour le traitement multiple
- fix(instructeurs): breadcrumbs with right wordings and links (#8547)
- fix(dossier): do not try to rebase if procedure is not published (#8546)
- amelioration(a11y): optimization burger menu (#8565)
- amelioration(a11y) : optimization navigation accessibility (#8564)
- fix(dossiers_transfers): users can reject dossiers_transfers (#8561)
- secu: remove a balise from sane user input (#8556)
- feat(dossier): optional repetition champ should not add first row (#8554)
- amelioration(a11y): meilleur navigation au clavier sur les invitations et l'upload (#8351)

### Technique
- Revert "migration: normalize regions (#8521)" (#8578)
- Revert "migrate(champs): normalize departements (#8505)" (#8579)
- chore(sentry): more traces (#8574)
- secu(sinatra): passe la grappe des dependances limitante pour embarquer la bonne version de sinatra (#8573)
- amelioration(dolist): ajoute dolist_api a notre routage des emails. (#8571, #8533)
- feat(log): log ip et correlation id (#8548)
- fix(task): improve after party backfill_dossiers_repetitions (#8545)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230203134127_add_message_id_to_email_event.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230201090535_backfill_dossiers_repetitions.rake`, `lib/tasks/deployment/20230203155423_rename_email_event_dolist_method.rake`).


--------------------------------------------

# 2023-02-01-01

Release [2023-02-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-01-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(avis): display remind in instructor view (#8484)
- fix(dossier): fix adding repetition in a new revision (#8535)
- Manager: permet de masquer l'estimation de durée de remplissage à la demande (#8542)
- fix(tags): can edit tags on published demarches (#8543)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230131172119_add_estimated_duration_visible_to_procedures.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230201090534_backfill_dossiers_repetitions.rake`).


--------------------------------------------

# 2023-01-31-02

Release [2023-01-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-31-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dossier): fix parser with dashes (#8532)
- fix(instructeurs): filtre "Groupe instructeur" avec seulement les groupes de la démarche (#8536)
- correctif(dossiers.transfert): ne fonctionne pas quand on saisi un mail avec une majuscule (#8538)
- correctif(invitations): peuvent reférencer le mauvais usager (#8539)
- Upload: prévient qu'une erreur d'envoi peut être causée par un pare-feu bloquant l'envoi de fichiers (#8540)
- correctif(invitations): reprise des donnees d'invitations en erreur pointant sur le mauvais model (#8541)

### Technique

- fix(menu): js error on click outside before unloading menu (#8531)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230131132616_fix_dossier_transfer_with_uppercase.rake`, `lib/tasks/deployment/20230131154015_fix_bad_invitation_targeted_user_link.rake`).


--------------------------------------------

# 2023-01-31-01

Release [2023-01-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-31-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(typo): remplace le dernier usage de morph par replace (#8528)
- fix(instructeurs): exports PDF avec annotations privées (#8529)
- fix(only_present_on_draft?): broken when type_de_champ is removed from later revisions (#8494)
- fix(combobox): form submit when form has combobox and file input without file selected (#8530)

### Technique

- T


--------------------------------------------

# 2023-01-30-01

Release [2023-01-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-30-01)  sur GitHub

## Améliorations et correctifs

### Sécurité

- correctif(patron): limite l'usage de la page patron aux devs (#8482)

### Mineurs

#### Administrateurs : 
- Tags : Ne prend pas en compte les tags sur les démarches en brouillon (#8481)
- Améliorations sur la validation des balises dans les attestations (#8515)

#### Instructeurs :

- perf: gros gain sur les pages dossier des instructeurs (#8508)
- fix: attend que les uploads se terminent avant de soumettre un formulaire ayant un menu déroulant (#8485)

#### Usagers : 
- Conditionnel: affiche un loader après un champ dont l'affichage d'autres champs peuvent dépendre (#8294)
- Dossier dupliqué: fix pré-découpage de l'adresse (#8516)
- Champ liste déroulante: fix champ "autre" qui pouvait écraser une option de la liste (#8491)
- Champ date: renforce la validation de la date (#8504)
- Pré-remplissage: ouvre la page des valeurs possibles dans un nouvel onglet (#8525, #8526) 

#### Accessibilité : 

- a11y : update styles actions buttons list folders (#8489)
- a11y : update style link dsfr (#8490)
- a11y : add label textarea message (#8492, #8502)
- a11y: update-style-link-sens-message (#8151) 
- a11y: update-styles-actions-button (#8224)
- a11y: messaging-input-control (#8181) 
- a11y : footer remove br tags (#8496)
- a11y : update html tags card-title (#8509)
- a11y : sign in remove tags used only for presentation (#8510)
- a11y : contact standardize links (#8498)
- a11y : update structure search engine (#8507)



### Technique
- amelioration(menu): extraction des menu dans un composant ruby pour ne pas dupliquer les changements aria partout ds la codebase (#8360, #8525)
- chore(deps): bump sanitize from 6.0.0 to 6.0.1 (#8519)
- amelioration(mail.prioritaire): en plus des mails de devise, ajoute la gestion du routage des autres mails critique (#8480)
- chore(turbo): morph all the things (#8453)
- fix(morph): ids should not start with numbers (#8503)
- fix(dossier): set rebased_at only on changed champs (#8514)
- feat(avis): add reminded_at field to avis (#8511)
- fix(stimulus): "on" handler having target (#8520)



## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230126145329_add_reminded_at_to_avis.rb`).


--------------------------------------------

# 2023-01-23-01

Release [2023-01-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- amélioration(export.pdf): n'affiche pas les champs conditionnels dans l'export PDF d'un dossier (#8476)
- amelioration(modele_attestation): re-tente a génération de l'attestation dans le cas ou notre objectstorage est en bagotage (#8478)
- amélioration(types de champ): ajout d'un nouveau type de champ EPCI (#8454)

### Technique

- Fix du support d'une durée personnalisée de conservation des dossiers (#8425)


--------------------------------------------

# 2023-01-20-01

Release [2023-01-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-20-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- chore(deps): bump globalid from 1.0.0 to 1.0.1 (#8471)
- fix(graphql): remove deprecated options from introspection query (#8470)

### Technique

- T


--------------------------------------------

# 2023-01-19-03

Release [2023-01-19-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-19-03)  sur GitHub

## Améliorations et correctifs

Cette version etait considérée comme stable 🧘. 

### Majeurs

- secu(api_token_v3): amélioration du controle d'accès sur les token v3 (#8469)

### Mineurs

### Technique


--------------------------------------------

# 2023-01-19-02

Release [2023-01-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-19-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- amélioration(infra): ajout d'un webhook pr diffuser les incidents SIB sur nos channel de support (#8434)
- correctif(champ.repetition) ; les champs répétitions n'étaient pas vraiment supprimé (#8465)
- amélioration(retraites): ajoute la bannière de grève (#8463)
- correctif(expert.export): ETQ expert, les exports ne contiennent pas les annotations et avis (#8455)


--------------------------------------------

# 2023-01-19-01

Release [2023-01-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(graphql): expose more information on demarche descriptor (#8117)
- fix(after_party): backfill children Champs without row_ids (#8460)
- fix(graphql): demarche with pj should return schema (#8461)
- ETQ Administrateur, je souhaite filtrer mes démarches par tag (#8392)
- Fix: prévient l'instructeur lorsqu'un dossier n'est pas terminable à cause de champ SIRET incomplet (#8462)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230103170917_normalize_pays_values.rake`, `lib/tasks/deployment/20230118102035_backfill_repetition_champ_without_row_id.rake`).


--------------------------------------------

# 2023-01-18-01

Release [2023-01-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-18-01)  sur GitHub

## Améliorations et correctifs

Cette mise à jour adresse les vulnérabilités suivantes : 
https://rubyonrails.org/2023/1/17/Rails-Versions-6-0-6-1-6-1-7-1-7-0-4-1-have-been-released

### Mineurs

- feat(autosubmit): data-no-autosubmit can disable only some event types (#8441)
- feat: ETQ super-admin je peux recevoir un rapport d'analyse d'emails envoyés avec Dolist (#8451)
- fix affichage de l'erreur lors de mises à jour de groupe instructeurs (#8364)

### Technique

- chore(bundle): rails 6.1.7.1 — fix multiples CVE (#8452)


--------------------------------------------

# 2023-01-17-02

Release [2023-01-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-17-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Fix: affichage du nombre d'admins d'une démarche sur la page "toutes les démarches" (#8444)
- fix(after_party): DateTimeChamp -> DatetimeChamp (#8443)
- fix(email-event): apparently mail subject could be null (#8447)
- restreint le nombre d'instructeur invitable (#8446)

### Technique

- T


--------------------------------------------

# 2023-01-17-01

Release [2023-01-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(js): fix submitter attributes not recognised by safari (#8427)
- Chaque lien est-il explicite (hors cas particuliers) ? (#7484)
- feat(dossier): use select in filters by groupe instructeur (#8432)
- refactor(js): more forms to use autosubmit controller (#8428)
- feat(stimulus): allow `on` with target (#8430)
- refactor(js): add generic "format" controller (#8429)
- refactor(turbo): use @coldwired/actions (#8377)
- fix(graphql): n+1 on single dossier loading (#8439)
- 8205 home remove tags presentation (#8373)
- chore(db): remove unused columns (#8438)
- fix(email): ré-essai si une erreur intervient à l'envoi (#8435)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230117094309_remove_row_from_champs.rb`, `db/migrate/20230117094317_remove_encrypted_token_and_active_from_administrateurs.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221221170137_normalize_datetime_values.rake`).


--------------------------------------------

# 2023-01-12-02

Release [2023-01-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-12-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(groupe_instructeur): only show to administrators the number of visible files (#8413)
- feat(graphql): add demarcheCloner mutation (#8108)
- fix(procedure): return most recent demarche for a given path (#8420)
- typo(page.dossier-envoye): typo (#8421)
- refactor(demarches): use turbo-frame (#8410)
- refactor(autosubmit): split and improuve autosubmit and turbo controller (#8416)
- chore(npm): update build dependencies (#8422)

### Technique

- T


--------------------------------------------

# 2023-01-12-01

Release [2023-01-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-12-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(safe_mailer): fix bug sur de la synthax (#8415)

### Technique

- T


--------------------------------------------

# 2023-01-11-02

Release [2023-01-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-11-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Backfill procedure with discarded (#8402)
- Ajoute des colonnes nécessaires au déplacement du routage comme un type de champ (#8405)
- Substitution de la variable FAQ_URL par des locales (#8401)
- fix(dossier): disable buttons when selection is canceled (#8408)
- Fix RNA Association adapter sans date_publication (#8409)
- S3 storage support (#8400)
- fix(procedures): valides uniquement le juridique a la création et à la publication (#8093)
- chore: ajoute d'un document pour les conventions de code (#8166)
- bug : améliorer l'affichage de la raison sociale pour les entreprises individuelles (#8208)
- Add prefill api to rack_attack (#8284)
- chore(npm): update dependencies (#8407)
- Us/force important email delivery (#8404)
- correctif(safe_mailer): autorise le new/create depuis le manager (#8412)
- fix(mailer): dolist header in devise mailer (#8414)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230110153638_create_safe_mailers.rb`, `db/migrate/20230111094621_add_migrated_champ_routage_to_dossiers.rb`).


--------------------------------------------

# 2023-01-11-01

Release [2023-01-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Nouveau: API Graphql: ajoute/supprime des instructeurs (#8365)
- Correction d'une typo sur le message flash des archives (#8399)
- Amelioration (instructeurs/dossiers#show): supprime le double chargement des champs et annotations privées (#8393)
- Amélioration (instructeurs/batch_operation): n'active seulement les opérations possibles (#8385)

### Technique

- fix (mailers): observers for balancer and balanced delivery methods (#8403)
- refactor(repetition): use row_id instead of row (#8390)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230110181415_remove_champs_row_index.rb`, `db/migrate/20230110181426_add_champs_row_id_index.rb`).


--------------------------------------------

# 2023-01-10-01

Release [2023-01-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Email events: premières briques pour un monitoring — emails dispatchés par ActionMailer (#8389)
- fix: gros gain de perf sur l'affichage des dossiers ayant beaucoup de conditionnel (#8391)
- refactor(demarches): use turbo_stream template (#8384)

### Technique

- fix(instructeurs/dossier#show): missing preload on dossier (#8387)
- chore(sentry): ignore error on password input with password manager (#8398)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230109140138_create_email_events.rb`).


--------------------------------------------

# 2023-01-06-02

Release [2023-01-06-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-06-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dossier): fix rebase with drop down options (#8383)
- refactor(batch): simplify stimulus controller (#8375)

### Technique

- T


--------------------------------------------

# 2023-01-06-01

Release [2023-01-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(template): trix will transform double spaces in &nbsp; (#8370)
- fix(graphql): fix departements with alphanumeric codes (#8374)
- refactor(repetition): add row_id to champs (#8295)
- chore(deps): bump json5 from 2.2.1 to 2.2.3 (#8376)
- fix(PiecesJustificativesService.safe_attachment): utilise la nouvelle API pour comparer le resultat du scan antivirus (#8380)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221215131001_add_row_id_to_champs.rb`, `db/migrate/20221215131122_add_row_id_index_to_champs.rb`, `db/migrate/20221222204553_create_dossier_batch_operations.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221215135522_backfill_row_id_on_champs.rake`, `lib/tasks/deployment/20221221153640_normalize_checkbox_values.rake`, `lib/tasks/deployment/20221221155508_normalize_yes_no_values.rake`, `spec/lib/tasks/deployment/20221221153640_normalize_checkbox_values_spec.rake`, `spec/lib/tasks/deployment/20221221155508_normalize_yes_no_values_spec.rake`).


--------------------------------------------

# 2023-01-04-01

Release [2023-01-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- accessibilite(pages-authentification): evolutions des pages de connexion/creation de compte pour respecter le DSFR et supporter une meilleure accessibilite (#8316)
- amelioration(role): utilise le role de status plutot que celui de alert afin d'eviter les confusion sur les role aria (#8356)
- Email: améliore le wording des emails de dossiers arrivant à expiration, et de suppression automatique (#8357)

### Technique

- refactor(virus_scan_result): use column instead of metadata on blob (#8331)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221222163907_add_watermarked_at_active_storage_blobs.rb`, `db/migrate/20221222170319_add_virus_scanned_at_active_storage_blobs.rb`, `db/migrate/20221226221025_add_virus_scan_result_index.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221222164435_backfill_watermarked_blobs.rake`, `lib/tasks/deployment/20221222173733_backfill_virus_scan_blobs.rake`).


--------------------------------------------

# 2023-01-03-01

Release [2023-01-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- améliore le texte de description lorsque le dossier est pre-rempli (#8343)
- ETQ admin, ajoute un lien pour permettre de générer un lien avec la démarche pré-remplie (#8345)
- ETQ admin, ajoute le nombre de dossiers qui seront deplacés dans le groupe d'instructeur (#8348)

### Technique
- correctif(api_entreprise_token): les nouveaux tokens stockent les roles sous la clé scopes et non plus roles (#8355)
- accessibilite(dossier): amélioration la structure  html dans le resumé d'un dossier (#8350)
- accessibilite((home/i18n): améliorate la traduction des dates relative sur la home (#8352)
- tech: Supprime le feature flag administrateurs Tags (#8346)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221213084333_add_prefill_fields_to_dossiers.rb`, `db/migrate/20221213084442_add_prefill_token_index_to_dossiers.rb`).


--------------------------------------------

# 2022-12-28-02

Release [2022-12-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-28-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(PersonneMoraleType): il arrive que le code naf d'une entreprise puisse ne pas exister [trop vieux pour etre mappable] (#8342)

### Technique

- T


--------------------------------------------

# 2022-12-28-01

Release [2022-12-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- sentry(3830513995): champs pays peut avoir un external_id a '', test la presence d'external_id sinon on peut renvoyer du nil via Champs::PaysChamp.name (#8340)

### Technique

- T


--------------------------------------------

# 2022-12-27-01

Release [2022-12-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Merge branch 'main' into feature/prefill_civility
- Allow prefill civility (#8305)
- remove useless DOSSIER_DEPOSIT_RECEIPT_LOGO_SRC env var (#8333)
- accessibilite(page#dossiers): utiliser les role=alert sur le status des dossiers (#8267)
- a11y(dossier.status): ajout de l'attribut role=alert aux status du dossier (#8266)
- fix(pays): some Île are written with î for some reason… (#8328)
- fix(dossier): explicitly send draft notification email (#8321)
- feat(prefill): allow to prefill private annotations (#8322)
- correctif(instructeurs/dossier#telecharger_pjs): ne pas inclure les bills et horodatage quand on telecharge un dossier unitairement (#8337)
- feat(revision): show number of pending dossiers for each revision (#8335)
- Us/fix departement champs (#8339)

### Technique

- T


--------------------------------------------

# 2022-12-26-01

Release [2022-12-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-26-01)  sur GitHub

## Améliorations et correctifs

### Technique

- correctif(email.sans-confirmation): manque de la maj des traductions des mails sans confirmations (#8336)


--------------------------------------------

# 2022-12-22-01

Release [2022-12-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- refactor(champs): pays, regions et departements as simple select (#8246)
- correctif(visuel): les textarea surchargé par notre trix-editor avec un contenu long ont un max-height. le desactive (#8325)
- feat(dossier): allow to rebase champs with value made mandatory (#8326)
- feat(dossier): rebase after repasser en instruction (#8327)
- feat(graphql): add pays champ to API (#8323)

### Technique

- T


--------------------------------------------

# 2022-12-21-01

Release [2022-12-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-21-01)  sur GitHub

## Améliorations et correctifs

Cette version etait considérée comme _stable_ 🧘. Un patch de sécu est necessaire : https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-21-01). la nouvelle version considérée comme stable est : https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-19-03

### Mineurs

- Les conditions dans les répétitions sans le n+1 cette fois-ci :) (#8280)
- amelioration(attestation.et.mail): clarifie les problèmes de tag sur les attestations & mail (#8289)
- ETQ SuperAdmin, je souhaite associer en masse des tags avec des démarches (#8070)
- ETQ admin, je peux exporter la liste des demarches (filtrées ou non) (#8193)
- fix(instructeurs): behavior of sort by notifications checkbox (#8308)
- style(instructeurs): improve dossier header layout (#8314)
- fix(dossier): clone should include more attachments (#8307)
- correctif(a11y.contact-page): #8058 (utiliser le type email sur l'input prenant l'email de l'usager), #8056 (ajuster les erreurs de contraste par l'usage des composants du DSFR) (#8300)
- feat(type_de_champ) update wording and order in select (#8147)
- feat(pdf): list department with champ commune (#8290)
- fix(geo_area): missing source on 1 record (#8319)

### Technique

- fix(dossier): schedule a rebase to all pending dossiers (#8318)
- fix batch operation: ensure PostgreSQL 13 compatibility (#8297)
- schedule job during a no-busy time (#8296)
- Remove feature flag opendata (#8302)
- Remove feature flag zonage (#8306)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221221090151_schedule_rebase_for_all_dossiers.rake`, `lib/tasks/deployment/20221221115241_fix_geo_area_missing_source.rake`).


--------------------------------------------

# 2022-12-15-01

Release [2022-12-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-15-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- Replace mandatory attachments when dossier is "en construction" (#8271)
- feat(revision): allow more dossiers to rebase (#8272)
- fix(tags): relax parser roules (#8283)
- style(instructeurs): table dossiers in full width in a fluid container (#8281)
- fix(dossier): authorize passage en instruction if auto archive is on and is in the past (#8285)
- fix(dossier): parent_dossiers is really cloned_dossiers (#8274)
- style(instructeurs): dossiers table with dropdown DSFR (#8286)
- Us/poc multiple passer en instruction (#8268)
- fix(fetch): always use post http method (#8287)
- US/po-multiple (fix) Afficher les checkbox uniquement sur les pages des batchs (#8291)

### Technique
- Merge branch 'main' into pj-en-construction-replace
- refactor(dossier): flatten champ attributes (#8101)
- chore(deps): bump rails-html-sanitizer from 1.4.3 to 1.4.4 (#8278)
- fix(schema): champs.prefilled default (#8276)


--------------------------------------------

# 2022-12-14-02

Release [2022-12-14-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-14-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Revert "Merge pull request #7907 from tchak/feat-cond-repetitions" (#8277) (fix perf issue)
- chore(deps): bump loofah from 2.19.0 to 2.19.1 (#8273)


--------------------------------------------

# 2022-12-14-01

Release [2022-12-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-14-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(timestamp): utilise l'horodatage de certigna et vérifie par openssl la validité du jeton (#8206)
- feat(cond): enable conditional on champs in repetitions (#7907)
- feat: attachments viewable after upload (dossier en brouillon) (#8221)
- fix(annuaire-education): schema less strict (#8210)
- feat(dossier): prefill dossier from query params (#8145)
- amelioration(administrateur/carte-email): la carte des emails affiche en permanence à configurer (#8234)
- clean(api_token): remove administrateur token support (#8218)
- fix(admin): go to the right tab when click on breadcrumb link (#8243)


### Technique

- chore(deps): bump nokogiri from 1.13.9 to 1.13.10 (#8219)
- chore(deps): bump qs from 6.5.2 to 6.5.3 (#8222)
- fix(schema): add missing team_account attribute (#8230)
- refactor(dossier): improuve dossiers_safe_scope on batch operation (#8270)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221201103802_add_prefilled_to_champs.rb`).


--------------------------------------------

# 2022-12-07-01

Release [2022-12-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(graphql): prepare avis and messages to handle multiple files (#8200)
- feat(api): api tokens can be rotated (#8140)

### Technique

- fix(dossier): send notifications on declarative dossiers (#8209)
- fix(notifications) : réintégrer les pastilles dans les tableaux avec actions multiples (#8216)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221129104327_create_api_tokens.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221129162903_migrate_api_tokens.rake`).


--------------------------------------------

# 2022-12-06-01

Release [2022-12-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(admin/procedures/configuration): pour une procedure, la tuile instructeurs est considérée valide si le routage est activé ou si il y a des instructeurs declarés (#8198)
- amélioration(instructeur/dossiers): ETQ instructeur je peux archiver en masse des dossiers (#8081)
- correctif(export.pjs): la liste des pjs doit etre une chaine de caractère pas un tableau (#8203)
- fix(apiv1):  piece_justificative continue to returns a single attachment (#8202)
- correctif(api-v1): dossier -> avis -> piece_justificative_file_attachment relation (#8197)
- style: minor attachments improvements in edge cases (#8207)

### Technique

- refactor(demarche): make declarative demarche processing syncroneous (#8190)
- refactor(groupe_instructeur): improve controller (#8176)
## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221118133711_create_table_batch_operation.rb`, `db/migrate/20221118134105_add_batch_operation_id_to_dossiers.rb`, `db/migrate/20221118134156_add_foreign_key_to_batch_operation_id.rb`, `db/migrate/20221118134231_validate_foreigh_key_to_batch_operation_id.rb`, `db/migrate/20221119050905_add_foreign_key_to_batch_operation_instructeur.rb`, `db/migrate/20221119050928_add_validate_key_to_batch_boperation_instructeur.rb`, `db/migrate/20221122123721_add_index_to_dossiers_batch_operation_id.rb`, `db/migrate/20221201091240_create_batch_operation_groupe_instructeur_join_table.rb`, `db/migrate/20221201091658_add_seen_at_to_batch_operations.rb`).


--------------------------------------------

# 2022-12-05-03

Release [2022-12-05-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-05-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix: multipart upload on instructeurs import & acceptation avec PJ (#8195)
- fix(clone-rescue): when record was invalid (#8196)


--------------------------------------------

# 2022-12-05-02

Release [2022-12-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-05-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- amelioration(liens-de-telechargement): permet de specifier si le lien  de téléchargement se fait sur l'onglet actuel ou vers un nouvel onglet (#8194)

### Technique

- T


--------------------------------------------

# 2022-12-05-01

Release [2022-12-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-12-05-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- feat(dossier): piece justificative allows multiple attachments for new procedures (#7958)

### Mineurs

- Update explanation for csv import of instructeurs when routing is not enabled (#8182)
- fix(dossier): fix links to empty pdf templates (#8183)
- amelioration(sanitize): assainit aussi les balises <img> (#8185)
- fix(typography): fix quotes (#8184)
- feat(instructeurs): list archiver email (#8143)
- ETQ Instructeur, je souhaite voir : le numéro de dossier, le lien vers la démarche, la liste des administrateurs sur mon interface (#8150)
- i18n: extraction home + meta titles (#8175)
- fix(pj_template): proxy template url so their links are non expirable (#8186)
- fix(a11y/identite): indicates that all fields are required (#8188)

### Technique

- feat(dossier): add some metadata to archive and remove operation log (#8167)
- clean(type_de_champ): remove magic factory (#8165)
- fix(dev): fix helo integration (#8191)
- clean(flipper): retire des anciennes conditions (#8149)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221107170011_add_piece_justificative_multiple_on_procedures.rb`, `db/migrate/20221130113745_add_archived_at_and_archived_by_to_dossiers.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221128205201_clean_old_gates.rake`).


--------------------------------------------

# 2022-11-30-01

Release [2022-11-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(dossier): exclue les dossier du résumé de la démarche (#8155)
- Fix(groupe_instructeurs) : with pagination display group even if one per page (#8158)
- a11y: fix non explicit links (criteria 6.1) (#8146)
- fix(patron): dossier should have a procedure and champ should delegate to dossier (#8163)
- fix(locale-switcher): attributes & wording issues (#8160)
- a11y: fix for tables (#8159)

### Technique

- chore(deps): bump minimatch from 3.0.4 to 3.1.2 (#8121)
- chore(bin/update): allow to ignore automatic webdriver update (#8144)
- chore(dossier): cleanup champs nested attributes (#8100)
- clean(dossier): retire du code mort (#8152)
- fix(avis): corrige des specs (#8153)
- refactor(attestation_template): remove unused code (#8134)
- spec(graphql): add tests to ensure that stable ids are stable (#8139)


--------------------------------------------

# 2022-11-28-01

Release [2022-11-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(graphql): fix playground when admin has no procedures or dossiers (#8127)
- chore(dossier): cleanup clone champ (#8099)
- feat(demarche): enable revisions for all (#8133)
- feat(graphql): make demarche schema public (#8124)
- fix(pdf): strip html tags in type_de_champ descriptions (#8137)
- fix: escape characters when showing page title (#8102)
- chore: instructions sur le transfert de dossier plus précises (#8138)
- fix(graphql): check if tokens are revoked (#8141)

### Technique

- T


--------------------------------------------

# 2022-11-24-02

Release [2022-11-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-24-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(super_admin): crée un compte admin pour les super admins (#8129)
- amelioration(avis): sur une page listant les avis en attente sur uneprocedure, on remonte le dernier avis demandé en premier (#8130)
- feat(team_user): affiche le nombre de procedure associés aux comptes admins (#8132)
- ETQ admin, je peux rechercher des administrateurs via leur email parmi la liste de tous les administrateurs de toutes les démarches (eventuellement filtrées) (#8097)

### Technique

- feat(graphql): expose groupe_instructeur state and update mutation (#8103)
- fix(stats): exported file should be csv (#8131)
- fix(a11y): erreurs code source (Critère 8.2) (#8116)


--------------------------------------------

# 2022-11-24-01

Release [2022-11-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(procedure): rattrape la donnée manquante relative à la duree de conservation (#8091)
- feat(attestation): validate attestation tags (#8003)
- chore: maj skylight (#8120)
- fix(procedure): ajoute des contraintes not null sur les colonnes duree_conservation (#8092)
- fix(invite): do not render menu when invite not found (#8126)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221122145030_clean_duree_conservation.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221108114545_assign_attestation_templates_to_procedures.rake`, `lib/tasks/deployment/20221121163201_clean_invalid_procedures.rake`, `spec/lib/tasks/deployment/20221108114545_assign_attestation_templates_to_procedures_spec.rb`).


--------------------------------------------

# 2022-11-23-01

Release [2022-11-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(manager): affiche les utilisateurs de l'équipe en fonction de la valeur dans l'attribut (#8071)
- fix missing header for all demarches layout (#8077)
- feat(export): add GeoJSON export (#8045)
- add a label for api and export for sensitive data as titre_identite_champ (#8023)
- fix(cni): only allow jpg and png because of buggy pdf watermarking (#8085)
- linters: enable haml-lint for View components (#8084)
- feat(a11y): add dsfr skiplinks (#8080)
- feat(procedure_admins): permet à un admin de se retirer lui-même d'une procédure (#8083)
- ETQ admin, je peux rechercher des démarches en fonction de leur libellé parmi la liste de toutes les démarches (eventuellement  (#8089)
- refactor(operation_log): store data in jsonb instead of files (#8079)
- fix(dossier): fix dossier brouillon spec (#8106)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220315102928_add_data_to_dossier_operation_logs.rb`, `db/migrate/20221122123809_add_libelle_index_to_procedures.rb`).


--------------------------------------------

# 2022-11-17-01

Release [2022-11-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-17-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- ETQ admin, je peux lister tous les administrateurs de l'ensemble des démarches filtrées. (#8031)
- amélioration(dossier.clone): permettre a un usager de cloner un de ses dossiers (#8015)
- it displays a message if instructor is looking for a dossier that is not in his instructor group (#8002)
- feat(support): ajoute une tache pour supprimer les anciens admins de l'équipe (#8043)
- style: fix old webkits radio & checkbox appearance (#8035)

### Technique
- refactor(procedure): types_de_champ -> active_revision.types_de_champ_public (#8066)
- amelioration(packaging.js): ajoute une dependance qui semble manquer (#8069)
- feat(web-console): allow private & loopback ranges (#8042)
- Accepter les instances auto-hébergées de Sentry dans la Content Security Policy (#8017)
- fix(turbo-stream): read delay on turbo-stream element (#8044)
- chore(vitejs): update (#8039)
- perf(manager): ne fait pas de .count sur les tables massives (#8041)
- feat(manager): améliore la liste des champs d'un dossier (#8040)
- feat(graphql): upgrade playground version (#8038)
- fix(graphql): load playground from CDN (#8075)
- Merge pull request #8032 from tchak/feat-better-as-clone (#8032)
- refactor(dossier): champs -> champs_public (#8034)
- fix(manager): missing renaming champs -> champs_public (#8036)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221107163131_add_parent_dossier_id_to_dossier.rb`, `db/migrate/20221110100622_add_foreign_key_to_parent_dossier_id.rb`, `db/migrate/20221110100759_validate_foreign_key_to_parent_dossier_id.rb`).


--------------------------------------------

# 2022-11-09-02

Release [2022-11-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-09-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- En tant qu'admin, je peux filtrer la liste de toutes les démarches par date de publication (#8020)
- improve wording (#8024)

### Technique

- fix(groupe_instructeur): add explicit order to groupe_instructeurs (#8019)
- fix padding for checkbox group (#8026)
- Fix procedure with only inactive group (#8025)
- Fix procedure with active group and routing nil (#8027)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221109115352_update_procedure_with_only_inactive_instructor_group.rake`, `lib/tasks/deployment/20221109124456_update_procedure_with_active_group_and_routing_nil.rake`).


--------------------------------------------

# 2022-11-09-01

Release [2022-11-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Feat(routing) Remove routing button (#7948)
- feat(graphql): improve groupe instructeur query (#7993)
- fix(merge): transfere les procédures cachées (#8013)
- Update estimated_fill_duration_component.fr.yml (#8011)
- fix(publication): improve publication warnings (#8012)
- ETQ admin, je peux supprimer le filtre statut de la liste de toutes les démarches (#8005)
- Précisions sur le texte d'import des groupes par fichier csv (#8021)
- Feat(FranceConnect): liste les comptes FC liées et permet de les délier (#8014)
- amelioration(Champ.titre-d-identite): ajout du support pour les .pdf (#8022)
- evolution(helpscout.webhooks): mise en place d'un rappel web afin de notifier l'equipe tech des bug identifies par le support (#7997)
- amelioration(types_de_champ/explication): pouvoir deplier ou pas l'option d'explication (#8006)

### Technique

- tech(dsfr): passe de la version 1.7.2 a la version 1.8.1 (#8007)
- Us/cleanup ops account (#8001)
- feat(conditional): remove conditional feature flag (#8004)
- Us/stress wcag tests (#7863)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221104071959_add_team_account_to_users.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221026074507_update_procedure_routing_enabled.rake`, `lib/tasks/deployment/20221104122104_backfill_users_team_account.rake`).


--------------------------------------------

# 2022-11-04-01

Release [2022-11-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- correctif(Administrateurs::Archives#index): mauvaise comparaison sur les date des archives (#7994)
- amelioration(archive.nom-de-fichier): ameliore le nom du fichier d'une archive mensuelle pour eviter les confusion (#7995)
- correctif(typo): étendre plutôt que entendre pour la durée de conservation d'un dossier (#7996)
- Ajoute les dossiers déposés dans les statistiques Datagouv (#7992)
- Remove categories_type_de_champ feature flag (#7998)
- chore(capybara): augmente le temps d'attente de capybara car les workers github sont lents (#7999)
- feat(emails): validate tags in notification emails (#7657)

### Technique

- T


--------------------------------------------

# 2022-11-02-01

Release [2022-11-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-02-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(merge): s'assure que les transfert de service lors des merges d'administrateur fonctionnent (#7965)
- design(page de configuration de procedure): passage de la grille au DSFR (#7921)
- fix(invitation): don't fail when dossier is hidden & user signed on another account (#7954)
- feat(graphql): on api exceptions log query and variables (#7970)
- fix(spec/system): stabilise encore un peu les tests de bout en bout (#7969)
- bug(export): la creation d'un export avec un filtre different devrait etre effective, ce n'est pas le cas (#7951)
- fix(graphql): do not load brouillon or deleted linked dossier (#7977)
- fix(graphql): properly handle variables json parse errors (#7979)
- fix(rna): date_creation may be null (#7978)
- feat: improve estimated fill duration with text reading time (#7967)
- chore(manager): activate js (#7981)
- correctif(export.global): les exports globals ne sont pas trouvés parla methode find_for_groupe_instructeurs (#7980)
- ETQ Administrateur, je peux voir la liste de toutes les démarches. (#7976)
- chore(graphql): update (#7982)
- refactor(procedure): expose procedure tags on model (#7986)
- fix(clone): lors du clone d'une procédure, ignore les fichiers corrompus (#7971)
- fix(css): corrige un dépassement dans l affectation des instructeurs (#7973)
- correctif(administrateur/procedure/archives#index): les compteurs sont desynchronisés (#7983)
- fix(procedure): only set replaced_by if the procedure is effectively published (#7990)
- fix(messages): retry stale blobs (#7987)
- fix(procedure): fix procedure cards grid view (#7988)
- ETQ Super Admin, je veux ajouter / Supprimer des tags à une démarche (#7985)
- feat(graphql): improuve reptition champs schema (#7989)
- feat(graphql): implement stored queries (#7984)

### Technique

- T


--------------------------------------------

# 2022-10-27-01

Release [2022-10-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Administrateur je peux associer des tags à une démarche (#7864)
- fix(editeur de champ): corrige la perte de focus lors de l'édition de champ (#7960)

### Technique

- chore(capybara): nouvelle essai pour stabiliser les deconnexions sur les tests bout en bout (#7916)
- chore(dev): yarn clean should use vite clobber (#7957)
- chore(bundle): update strscan 3.0.3 => 3.0.4 (#7959)
- fix(editeur): supprime un 'end' surnuméraire (#7956)
- chore(sentry): regularly notify delayed jobs errors (#7894)
- refactor(rails/ujs): don’t use @rails/ujs in our own code (#7949)
- fix(silent_error_in_merge): leve une exception si une mise a jour dans l'update échoue (#7947)
- fix(migration): ignore strong migration pour la petite table procédures (#7964)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221006074425_add_tags_to_procedures.rb`).


--------------------------------------------

# 2022-10-21-01

Release [2022-10-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- amelioration(archives): documentation de la structure de l'archive (#7938)
- fix(dossier): a tag can be preceded by a - (#7950)

### Technique

- refactor(turbo): simplify custom actions implementation now that turbo has support for it (#7897)
- ci: tests against postgresql 14.3 (#7930)


--------------------------------------------

# 2022-10-20-02

Release [2022-10-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-20-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- amelioration(export): clarifier l'usage des exports pour les instructeurs pour eviter le téléchargement des export zip pour de l'archivage (#7939)
- amelioration(export): renommer le fichier lisez-moi et le positionner en tête du répertoire (#7937)

### Technique

- chore(actions): update github actions (#7943)
- chore(deps): bump nokogiri from 1.13.8 to 1.13.9 (#7944)
- fix(message): add instructeurs foreign key (#7941)
- fix(dolist): réduit le nombre de  'Message-Name' possible chez dolist (#7945)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221020094031_add_foreign_key_to_commentaires_instructeur_id.rb`).


--------------------------------------------

# 2022-10-20-01

Release [2022-10-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-20-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dossier): nullify instructeur and expert id on messages when they are deleted (#7933)
- fix(admin_merge): résout un pb lorsque les 2 admins ont un service avec le mm nom (#7935)
- amelioration(export): augmente la duree de conservation des exports encours de generation passant de 12 a 16h. augment la duree de conservation des exports generés passant de 16 à 32h (#7926)
- fix(revision): fix changes list style (#7917)
- fix(invitation): when dossier is not visible anymore (#7927)
- style(admin): suggest auto expiration converted to DSFR (#7929)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221019094123_nullify_commentaire_deleted_instructeurs.rake`).


--------------------------------------------

# 2022-10-18-01

Release [2022-10-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-18-01)  sur GitHub

## Améliorations et correctifs

### Technique

- feat(manager/outdated_procedure): enhance procedure cleanup (#7856)
- fix(translation): typo dans le footer (#7923)
- fix(drop_down_list): corrige l'apparition de l'option 'non renseigné' dans une liste de choix obligatoire (#7922)
- fix(export): corrige un bug apparaissant avec des revisions et du conditionnel (#7928)
- fix(task): ajoute un controle des exception dans la tache d'action d'expiration des procedures (#7931, #7932)


## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221005142610_enable_procedure_expires_when_termine_enabled_on_procedure_without_dossiers.rake`).


--------------------------------------------

# 2022-10-17-01

Release [2022-10-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dossier): we should never detroy user’s dossiers in cascade (#7906)
- fix(routage): fill dossier groupe instructeur in some edge cases (#7918)

### Technique

- T


--------------------------------------------

# 2022-10-14-01

Release [2022-10-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-14-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajoute le type de champ relatif au association utilisant le répertoire national des association (RNA) (#7790)
- feat(groupe_instructeur) n'affiche pas de choix de groupe s'il n'y en a qu'un (#7881)

### Technique

- perf(dossier brouillon): petites optimisations (#7903)`
- spec(system): essaye de corriger le timeout sur les tests de déconnexion (#7876)
- refactor(spec): use match_array (#7914)
- fix(export): met une valeur null aux champs cachés lors des exports (#7909)
- fix(graphql): hide champs based on conditions (#7915)
- perf(user dossier): améliore la gestion des conditions coté js (#7891)
- fix(spec): fix dubious procedure spec (#7892)
- fix(api_address): support Adresse BAN without postcode (#7890)
- fix(dossier): en_construction expiration is counted from submit date (#7889)
- fix(editor champ): ne leve pas d'exception si un type de champ est supprimé 2 fois (#7893)
- chore(webdriver): update (#7901)
- chore(flipper): always use feature_enabled? helper (#7896)
- feat(jobs): add request_id to jobs payload (#7895)
- style(avis): fix font-size & font-weight (#7900)
- style(repetition + conditionnel): fix button styles (#7902)
- feat(dossier): do not update dossier champs on submit (#7460)
- correctif(fusion de compte): correctif sur les comptes usagers ayant initiés la fusion de leur compte sur lui même [ce qui detruit la donnée] (#7905)
- fix(iban): format iban after validation (#7866)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221006134215_add_index_on_type_to_champs.rb`, `db/migrate/20221013142432_clean_pending_user_with_requested_merge_into_id_similar_to_their_id.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221006135016_format_iban_champ_values.rake`).


--------------------------------------------

# 2022-10-11-02

Release [2022-10-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-11-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- style(dsfr) : on supprime notre reset CSS et on adopte le DSFR par défault (#7859)
- fix(routage): typo nommination => nomination (#7888)
- correctif(routing_criteria_name ne doit pas etre vide): ne pas oublier les procedures.routing_criteria_name ayant une chaine de caractère vide ex: ' ' (#7887)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221011075758_backfill_procedure_routing_criteria_name_blank.rake`).


--------------------------------------------

# 2022-10-11-01

Release [2022-10-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-11-01)  sur GitHub

## Améliorations et correctifs

### Technique

- colinux/fix-address-debounce (#7885)
- correctif(routing_criteria_name doit etre rempli): données inconsistantes (#7886)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20221011071912_backfill_procedure_routing_criteria_name.rake`).


--------------------------------------------

# 2022-10-10-01

Release [2022-10-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix améliore la validation sur le libellé de routage (#7827)
- fix(export): quand on essaie d'exporter la liste des dossiers recemment supprimé, celle ci est vide (#7879)

### Technique

- fix: supprime les types de champ orphelins due au reset de la version brouillon (#7875)
- fix(data): move type de champs engagement to checkbox (#7871)
- chore(clean) : drop useless procedures.direction (#7861)
- perf(dossier_controller): améliore la performance d'affichage d'un dossier (#7832)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221007134731_fix_clean_migrate_type_de_champs_engagement.rb`).


--------------------------------------------

# 2022-10-07-02

Release [2022-10-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-07-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- a procedure without zone is finally valid (#7873)

### Technique

- T


--------------------------------------------

# 2022-10-07-01

Release [2022-10-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-07-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- fix(instructeur dashboard): permets de filtrer les dossiers sur plusieurs états (#7833)
- fix(typo): corrige des erreurs d'orthographe (#7834, #7837, #7845)
- fix(web_graphql): corrige l'authorisation d'acces au bac à sable graphql (#7836)
- perf: amélioration de la rapidité de la page admin/procedure/show (#7828)
- clean(engagement): finalise la suppression du type de champs engagement (#7825)
- fix(sentry/3422144920): netoie les anciens liens de partage (#7838)
- feat(procedure.max_duree_conservation_dossiers_dans_ds): make it more flexible (#7860)
- feat(type_de_champ.select): optimise la liste déroulante concernant les invités (#7848 #7798)
- fix(preview): aperçu des mails (#7867)
- fix(migration): backfill max_duree_conservation_dossiers_dans_ds with… (#7868)
- feat(zone): Un administrateur peut associer plusieurs zones à une démarche (#7815, #7844)
- fix(type_de_champ.donnée): supprime les orphelins des tables type_de_champs et champs n'étant pas rattaché à une révision et étant attaché à un dossier (#7870)


### Technique
- feat(tags): replace regexp based parser with a parser combinator (#7842)
- clean(procedure.columns): :durees_conservation_required, :cerfa_flag, :test_started_at (#7862)
- fix(spec.flacky): maybe flacky due to css animation not loved by capy (#7840)
- fix(spec): missed a failed spec previously. sorry (#7865)
- chore: bump les gems de profilage (#7847)
- chore(js): remove CheckConditionsController (#7823)
- chore: bump haml (#7849)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20221005090004_add_index_on_dossier_id_to_invites.rb`, `db/migrate/20221005145531_max_column_duree_conservation_dossiers_dans_ds_to_procedure.rb`, `db/migrate/20221005145646_backfill_procedure_max_duree_conservation_dossiers_dans_ds.rb`, `db/migrate/20221006161143_re_backfill_procedure_max_duree_conservation_dossiers.rb`, `db/migrate/20221006190110_create_procedures_and_zones.rb`, `db/migrate/20221006193737_backfill_procedures_zones.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221004112700_clean_orphaned_targeted_user_links.rake`, `lib/tasks/deployment/20221007085741_clean_champs_and_type_de_champ_with_no_revision.rake`).


--------------------------------------------

# 2022-10-03-01

Release [2022-10-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-10-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- bug(multiple_drop_down_list): when their is a coma in label, comas is splitted as an item (#7826)

### Technique

- update saml idp config (#7831)


--------------------------------------------

# 2022-09-30-01

Release [2022-09-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat: précise aux administrateurs que les liens PJ + notice ne sont pas partageables (#7758)
- feat(instructeur/procedure#show): enhance sort by notifications as planned by UX (#7809)

### Technique

- Fixe le champ checkbox lorsqu'il est obligatoire et non séléctionné (#7814)
- fix(profile): fix css (#7816)
- fix(instructeur/procedures#show): Procedure presentation filterable state field is in conflict with projected state field. quick fix, maybe will refactor both to align them (#7817)
- typo readme (#7824)
- fix-ux(issue#7774) : indicateur menu email navbar (#7785)
- feat(graphql): new tokens should carry administrateur_id (#7765)
- fix(linked_drop_down): corrige l'enregistrement des dropdown liés dans des blocs répétables (#7820)
- refactor(ujs): remove old ujs helpers (#7572)
- fix(system spec): augmente le timeout de la déconnexion (#7821)
- refactor(block): use block? instead of repetition? (#7595)
- unschedule export demarches opendata (#7822)


--------------------------------------------

# 2022-09-28-01

Release [2022-09-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-28-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(filter): active les filtres instructeurs sur les états des dossiers, leurs dates de traitement (#7755)
- clean(Champs::Engagement): transforme le champ engagement en checkbox (#7606)
- Fix(conditional and revision): restreint les calculs conditionels a la révision courante (#7810)
- Fixe le footer DSFR visiteur (#7812)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220725075951_migrate_champs_engagement_to_checkbox.rake`).


--------------------------------------------

# 2022-09-27-01

Release [2022-09-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Prend en compte les changements de ministères (#7691)

### Mineurs

- feat(conditonal): ajoute l'operator d'inclusion dans une liste pour le conditionnel (#7767)
- fix(graphql): PersonneMorale#date_creation may be null (#7808)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220911134914_create_zone_labels.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220922151100_populate_zones.rake`, `spec/lib/tasks/deployment/20220922151100_populate_zones_spec.rake`).


--------------------------------------------

# 2022-09-23-01

Release [2022-09-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ Visiteur, je souhaite voir le footer de la home au DSFR (#7794)
- fix(etablissement.as_degraded_mode): backfill missing data via a cron (#7799)

### Technique

- T


--------------------------------------------

# 2022-09-22-01

Release [2022-09-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- DSFR: convert dossier top form elements (#7760)
- fix(data): backfill etablissement as degraded mode (#7797)
- fix(cron/procedure-declarative): don't auto accept a dossier still in degraded mode (#7796)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220922154831_backfill_etablissement_as_degraded_mode.rake`).


--------------------------------------------

# 2022-09-21-01

Release [2022-09-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Fix(dossier): don't accept/reject with an incomplete etablissement (degraded mode) (#7789)
- fix: AgentConnect button visibility (#7787) 
- fix: FranceConnect button visibility (#7784) 

### Technique

- T


--------------------------------------------

# 2022-09-19-01

Release [2022-09-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(export.xslx): add code commune to geo_area.label (#7773)
- feat(champ siret): permet la soumission du formulaire lorsque l'api est down (#7778)
- ETQ Usager, je souhaite voir le footer au design DSFR (#7747)

### Technique

- T


--------------------------------------------

# 2022-09-16-01

Release [2022-09-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-16-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- API entreprise: ajout d'un mode dégradé qui permet aux usagers de déposer un dossier concernant une personne morale malgré une indisponibilité des api INSEE (#7770)
- feat(api_entreprise): n'affiche que le siret si mode dégradé (#7772)


--------------------------------------------

# 2022-09-15-01

Release [2022-09-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- secure admin adding (#7737)
- fix: remove instructeur when admin is removed (#7757)
- fix(graphql): add graphql doc redirect (#7759)
- fix(db): remove bad data (#7635)
- fix(after_party): split foreign key cleanup task (#7764)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220914090549_cleanup_champs_types_de_champ_foreign_keys.rake`, `lib/tasks/deployment/20220914090601_cleanup_champs_dossier_foreign_keys.rake`, `lib/tasks/deployment/20220914090615_cleanup_champs_etablissement_foreign_keys.rake`, `lib/tasks/deployment/20220914090631_cleanup_etablissements_dossier_foreign_keys.rake`).


--------------------------------------------

# 2022-09-12-01

Release [2022-09-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-12-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(breadcrumb): extends breadcrumb libelle truncation from 4 to 10 words (#7751)
- fix(upload.custom-types): army has a custom format for crypted zip. not recognized as an application/octet-stream by the browser. html5[accept] attributes allows to use extensions. So we allow this extension (#7752)
- fix(typo): views/administrateurs/_groups_header.haml (#7754)

### Technique

- T


--------------------------------------------

# 2022-09-09-02

Release [2022-09-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-09-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(conditionnal): corrige les conditions sur les listes de nombre (#7746)
- fix(champ_label_component): add missing helpers (#7749)
- fix(style): applique un cursor text au textarea (#7750)


--------------------------------------------

# 2022-09-09-01

Release [2022-09-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(dsfr): input checkbox/radio visibility on old firefox (#7745)
- feat(dossier): autosave en construction (#7734)
- bug(instructeurs/dossiers#telecharger_pjs): zipline does not play well with not available active storage attachments (#7723)


--------------------------------------------

# 2022-09-08-01

Release [2022-09-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-08-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Header et fil d'ariane DSFR c119077987bcd8c649d3102ccac0fe6dde562657

### Mineurs

- style: retirer la baseline du header 3aec5fe41350605b5b959bb983946b982138e968
- Fix home hover buttons, convert to DSFR style 685ef54062932054e5d311810f576ed266e68d3a
- feat(DSFR.ie11): enable legacy mode with our own packaging system aa7a25712fc79b44d087200e76b9e11c3f0cb3de
- remove the confusing help ('nom@site.com') d4ab97f2707c9f910e3e9505113500d0486c7a19
- Choix parmi une liste' becomes 'Choix unique ffb445d074d7db7b70ce85a9b2249cbc45ca3881


### Technique

- perf(dossier): limite la verification des lignes aux seuls champs enfants 5a18e0e03ddab2eb91eb11fe8b9fd7aeafd2c1d9
- fix(revisions): improve changes information display c207474c8c4a57a34a6db7e3da1d1935ff3808b5


--------------------------------------------

# 2022-09-06-01

Release [2022-09-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- ETQ administrateur je dois associer un SIRET à un service (#7585)
- feat(carte/export): intègre geo labels aux exports pour les champs de type "carte" (#7704)

### Technique
- fix(preview): empeche d'accéder a la page aperçu si la revision est invalide (#7713)
- fix(api_entreprise job): don't fail with an entreprise without date_creation (#7722)
- fix: contact admin form (#7721)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220718134835_add_siret_to_services.rb`, `db/migrate/20220725090224_add_etablissement_infos_to_services.rb`, `db/migrate/20220725090225_backfill_add_etablissement_infos_to_services.rb`).


--------------------------------------------

# 2022-09-05-02

Release [2022-09-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-05-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(helpscout#1994272872): les exports pdf via la route instructeurs/dossier#telecharger_psj contenaient des point d'interogation en lieu et place des espaces insécable (non applicable dans notre cas )(#7717)


--------------------------------------------

# 2022-09-05-01

Release [2022-09-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ instructeur, je veux pouvoir recevoir un mail lorsqu'un expert dépose un avis (#7702)

### Technique

- fix(migration): correction de la migration avis email notification (#7715)
- fix(css): rename css file to fix legacy browsers (#7716)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220830125703_add_avis_email_notification.rb`, `db/migrate/20220902151143_backfill_add_avis_email_notification.rb`, `db/migrate/20220902151920_validate_backfill_add_avis_email_notification.rb`).


--------------------------------------------

# 2022-09-02-01

Release [2022-09-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-02-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dsfr): removed image on link (#7711)
- fix(service#index): DSFR table.caption absolute positionning does works well with long captions (#7712)

### Technique

- T


--------------------------------------------

# 2022-09-01-04

Release [2022-09-01-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-01-04)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(a11y): input background color issue (#7707)
- ajoute un résumé des erreurs présentes dans l'éditeur de champ (#7708)
- fix(dsfr): side effect (#7710)

### Technique

- T


--------------------------------------------

# 2022-09-01-03

Release [2022-09-01-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-01-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(dossier): fix user form component migration errors (#7706)

### Technique

- T


--------------------------------------------

# 2022-09-01-02

Release [2022-09-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-01-02)  sur GitHub

## Améliorations et correctifs

### Technique

- feat(dossier): passe les champs en composant (#7675)

:information_source: cette version compartimente la vue d'édition des champs dans des composants. Il faudra adapter vos champs dédiés de la mm façon


--------------------------------------------

# 2022-09-01-01

Release [2022-09-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-01-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Graphql: Fix champs order in repetitions (#7695)
- fix(champ): remove null byte before save (#7697)
- Nouvelle page pour les services (prenant en compte le design d'olivier) (#7677)
- fix(dossier/attestation_pdf): don't overlap DS name over logo (#7703)
- Refactor: RGAA quickwins (#7700)
- chore(npm): update dependencies (#7661)

### Technique

- T


--------------------------------------------

# 2022-08-29-01

Release [2022-08-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-29-01)  sur GitHub

## Améliorations et correctifs



### Technique

- add saml sp only if vars present (#7696)


--------------------------------------------

# 2022-08-24-02

Release [2022-08-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-24-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(brouillon.edit.form): novalidate (#7690)

### Technique

- T


--------------------------------------------

# 2022-08-24-01

Release [2022-08-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(manager#outdated_procedure): allow super admin to enable procedure_expires_when_termine_enabled for old procedure (#7676)
- feat(super-admin): re-add to procedure without any instructeur (#7693)
- fix(rubocop): filter does not exists on strong parameter... (#7694)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220824114151_readd_super_admin_to_their_procedure_without_instructeur.rb`).


--------------------------------------------

# 2022-08-23-02

Release [2022-08-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-23-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(export/archive): prevent creation of archives from user having a SuperAdmin user with the same email (#7557)
- feat(clean.super_admin.assign_tos): remove former assign to so our support team is happy :-) (#7605)
- update saml idp. (#7680)
- bug(instructeurs/archives#create): month should not be nil (#7689)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220712141913_add_manager_to_assign_tos.rb`, `db/migrate/20220712141945_backfill_assign_tos_manager.rb`, `db/migrate/20220725070026_cleanup_assign_tos_from_betagouv.rb`).


--------------------------------------------

# 2022-08-23-01

Release [2022-08-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(migration): following e764aade13985190b336bd4cbf14ac10893956e3 …we still need to remove those columns otherwise destroying a procedure fails due to index on types_de_champ.revision_id (#7684)
- fix(conditional): within section (dark blue bg) ; use white text (#7687)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220822135410_remove_unused_column_on_type_de_champs.rake`).


--------------------------------------------

# 2022-08-11-01

Release [2022-08-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(conditional): Ajoute l'opérateur "n'est pas égal" s'appliquant à un choix parmi une liste (#7670)
- Manager : permet au super admin de se retirer d'une démarche (#7672)
- fix(editeur_de_champ): permet d'ajouter à nouveau un template au pjs (#7674)


--------------------------------------------

# 2022-08-09-01

Release [2022-08-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Possibilité de générer un pdf vide pour une démarche même si elle est close(#7666)
- Corrige les champs obligatoires mais cachés (#7669)


--------------------------------------------

# 2022-08-04-01

Release [2022-08-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(champ): can condition on other option (#7653)

### Technique

- Fix random tests errors (#7659)
- refactor(spec): use new procedure.types_de_champ factory (#7660)
- fix(conditional): mandatory champs inside hidden repetition should not be validated (#7662)


--------------------------------------------

# 2022-08-03-02

Release [2022-08-03-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-03-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(service): destroy with discarded procedure (#7646)
- feat(Users::CommencerController#commencer): ensure to redirect to replaced procedure when it exisits (#7644)
- Revert "Merge pull request #7537 from tchak/graphql-doc" (#7560)
- chore(routes): remove unused routes (#7636)
- chore(db): remove ignored columns (#7641)
- Amelioration des factories de procedure/types_de_champ (#7608)
- chore(dev): add more pry features with pry-rails gem (#7650)
- fix(conditional): enable conditional per procedure (#7652)
- fix(agent_connect): constant path (#7654)
- chore(sentry): tag dossier & procedure ids on dossier endpoints (#7655)
- fix: dossier_transfer requires a valid email (#7647)
- fix: contact captcha invisibility, view_component update (#7643)
- fix(autosave): on check condition requests do not send file inputs (#7651)
- chore(npm): update dependencies (#7658)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220728084804_remove_ignored_columns.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220802133502_destroy_dossier_transfer_without_email.rake`).


--------------------------------------------

# 2022-08-03-01

Release [2022-08-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(stats): remove Helpscout from stats page (#7638)
- fix(db): remove bad attachments (#7640)
- Merge pull request #7642 from tchak/fix-should-not-be-able-to-remove-mandatory-files (#7642)
- OPEN DATA - Administrateurs, dossiers, instructeurs crées par mois (#7495)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220728062218_cleanup_attachments.rake`).


--------------------------------------------

# 2022-08-01-01

Release [2022-08-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-08-01-01)  sur GitHub

#### Mineurs

- fix(drop_down_list): Assurer de ne pas prendre en compte les inputs désactivés pour l'autosave (https://github.com/betagouv/demarches-simplifiees.fr/pull/7443)
- ETQ admin, je souhaite visualiser le recap des modifications apportées lorsque ma démarche close (https://github.com/betagouv/demarches-simplifiees.fr/pull/7446)

#### Technique
- fix(webhook.perform): enqueue WebHookJob with current state/updated_at for consistency reason (https://github.com/betagouv/demarches-simplifiees.fr/pull/7440)
- feat(a11y): Fix some a11y issues (feat(a11y): Fix some a11y issues  #7420)


--------------------------------------------

# 2022-07-27-01

Release [2022-07-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(admin/attestation): page layout (#7632)
- affiche un message d'alerte quand aucun email n'est donné pour une invitation sur un dossier (#7633)
- fix(archives): don't fail when there are no weight estimation (#7630)
- Fix: purge stuck exports & archives (#7629)

### Technique

- T


--------------------------------------------

# 2022-07-26-03

Release [2022-07-26-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-26-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix exports generation (#7623)
- fix(types_de_champ): fix exception on validation errors (#7624)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220726151017_add_job_status_to_exports.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220726154500_truncate_exports.rake`).


--------------------------------------------

# 2022-07-26-02

Release [2022-07-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-26-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Fix(procedure library): remplace la liste infinie des procédures par un recherche simple (#7602)
- perf(graphql): benchmark demarches_publiques query (#7598)
- feat(types_de_champ): allow siret in repetition blocks (#7593)
- refactor(vite): use turbo-polyfills (#7584)
- fix(export): fix PurgeStaledExportsJob regression with `stale` scope (#7613)

### Technique

- T


--------------------------------------------

# 2022-07-26-01

Release [2022-07-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-26-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- [opendata] job that publish opendata demarches to datagouv (#7576)
- fix(conditional): ne prend pas en compte les champs invalides (#7599)
- fix(export): when it takes more than 3 hours, exports are purged before being generated. make it possible to have an export that takes more than 3 hours. behaviour extracted and shared from/by archive (#7547)
- feat(etablissement) list entreprise "état administratif" (#7580)
- fix: drop down other input width (#7609)
- fix(autosave): morph instead of replace to preserve controller instances (#7611)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220707152632_cleanup_export_and_archive.rb`, `db/migrate/20220718093034_add_entreprise_etat_administratif_to_etablissements.rb`).


--------------------------------------------

# 2022-07-22-01

Release [2022-07-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(graphql): annotation edit mutation inside blocks (#7503)
- perf(dossier.pdf): use DossierPreloader to generate pdf (#7597)
- refactor(procedure): move cards to components (#7594)
- fix(dossier): decoralate dropdown behaviour from autosave (#7600)

### Technique

- T


--------------------------------------------

# 2022-07-21-01

Release [2022-07-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(admin/export): enable export for admin (#7523)

### Technique

- fix(dossier): do not include preview dossiers in dossiers counts (#7567)
- hide internal zone word to admin (#7587)
- fix(champ): allow to submit when secondary options are empty (#7590)
- fix(conditionnel): la valeur d'un champ est nul si le champ est caché (#7589)
- Faster pdf export (#7586)
- perf(graphql): improve dossier loading (#7596)


--------------------------------------------

# 2022-07-19-01

Release [2022-07-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat: Champs with realistic placeholders (#7532)
- feat(admin): list procedures with updated_at & auto_archive_on (#7549)
- Rollback customized generic placeholder (#7558)
- UX/input sizes (#7555)
- feat(conditionel): version beta du conditionnel (#7544, #7582, #7561, #7486, #7565, #7569)
- feat: allows user to edit civility / siret from a dossier en brouillon (#7571)
- feat(procedure.duree_conservation_dossiers_dans_ds): decrease max duree_conservation_dossiers_dans_ds from 36 to 12 (#7551)

### Technique

- Improve perf in editor (#7578)
- Fix a11y errors (#7545)
- fix: after_stable_id pb (#7546)
- refactor(autosave): improuve events handling (#7559)
- chore: bump rails to 6.1.6.1 (#7570)
- fix(ie11): webcomponents used template interpolation (#7574)
- more fix other ie11 issues (#7583)
- Us/fix event target polyfill (#7579)
- fix(preview): gon should not crash on preview pages (#7543)
- fix(gon): matomo key can be a number (#7581)
- fix(champ): use safe_join in champ helpers (#7568)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220708151654_drop_duree_conservation_dossiers_hors_ds.rb`, `db/migrate/20220708151802_add_column_duree_conservation_entendue_par_ds.rb`, `db/migrate/20220708151917_backfill_duree_conservation_entendue_par_ds.rb`, `db/migrate/20220708152039_new_default_duree_conservation_entendue_par_ds.rb`).


--------------------------------------------

# 2022-07-11-01

Release [2022-07-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- feat(tags): expose used_type_de_champ_tags (#7548)
- fix(targeted_user_link): on invite without user (#7556)
- bug(linked_dropdown): when mandatory, add an extra blank option (#7550)

### Technique
- Permet la génération json du descriptif des démarches publiques (#7489)
- feature: ajout du composant d'édition des conditons (#7522)


--------------------------------------------

# 2022-07-06-01

Release [2022-07-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(demarche): add possibility to reset draft revision (#7538) (#7542)
- fix(graphql): change doc location (#7537)


### Technique

- Revert "Add id to term" (#7535)
- task(dossier): remove orphan champs (#7536)
- fix(sentry/3394049118): apparently there is an Champs::PieceJustificative without content_type validation. Should be a bug, not found in db (#7534)
- chore(npm): update dependencies (#7530)
- chore(stimulus): stimulus controllers can be lazy loaded (#7526)
- feat(task/support:delete_adminstrateurs_procedures): simple task for simple cleanup (#7539)
- feat: conditionnel, ajout du controller (#7528)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220705164551_remove_unused_champs.rake`, `spec/lib/tasks/deployment/20220705164551_remove_unused_champs_spec.rb`).


--------------------------------------------

# 2022-07-05-01

Release [2022-07-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix: css, couleur au focus des inputs box (#7520)
- chore(graphql): build doc (#7519)
- ETQ admin, je souhaite pouvoir télécharger une archive.zip (mensuelle) des dossiers terminés (#7500)
- feat(export): add déparctement info to commune champ export (#7525)
- feat: amélioration sur le module de logic (#7521)

### Technique

- test(js): add vitest (#7531)
- feature: ajoute un identifiant aux termes d'une condition (#7533)
- chore(vite): add a coment with link to original legacy plugin code (#7527)
- fix: renommage de var id en stable_id (#7517)
- chore(bundle): update strscan to prevent rspec error (#7524)
- Small spec improvements (#7518)


--------------------------------------------

# 2022-07-01-02

Release [2022-07-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-01-02)  sur GitHub

## Améliorations et correctifs

### Technique

- corrige une erreur qui affichait l'option autre alors qu'elle n'était pas sélectionner [#7515](https://github.com/betagouv/demarches-simplifiees.fr/pull/7515)


--------------------------------------------

# 2022-07-01-01

Release [2022-07-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-01-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- update Pratique-de-dev for new_deployment (#7494)
- feat(EditComponent): for uploaded files, display and validate max_file_size and content_type for attached files on the front (#7471)
- feat(dubious): add RIB & IBAN and fix NRIPP istead of NIRPP (#7507)

### Technique

- chore(axe-core): add a11y reports in dev environement (#7504)

De nombreuse tentatives de correction de vite
- use vite_legacy_javascript to fix dynamic import for 60 <= ff < 67 (#7506)
- Revert "pull request #7506 from  brutally_fix_dynamic_import" (#7508)
- fix(js): dynamic import with import keyword does not work on ffx[60,68] - brutalify v2 (#7509)
- fix(vite): temporary use legacy build in all browsers (#7510)
- fix(ffx60-66): patch vite-legacy and cleanup of dynamic import (#7511)
- fix(vite): porte vite fallback logic from vite legacy plugin (#7513)


--------------------------------------------

# 2022-06-27-02

Release [2022-06-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-27-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(ChampEditor): add missing drop_down_other for  type_de_champ.drop_down_list_with_other? (#7501)


--------------------------------------------

# 2022-06-27-01

Release [2022-06-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- chore(dossier): remove dead code (#7498)
- fix(Rails.ujs): missing Rails.start (#7499)

### Technique


--------------------------------------------

# 2022-06-23-02

Release [2022-06-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-23-02)  sur GitHub

## Améliorations et correctifs



### Technique

- Use vitejs (#7469)


--------------------------------------------

# 2022-06-23-01

Release [2022-06-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(invite): wrap invitations with targeted_user_links (#7462)
- fix(spec): including Logic module broke spreadsheet architect (#7474)
- feat(GroupeInstructeurs.closed): add closed option to GroupeInstructeur in order to prevent usagers to submit dossier (#7475)
- fix(AddColumnClosedToGroupeInstructeurs): add column with change_column_default (#7480)
- ETQ Usager, je veux entrer mon Iban de manière plus simple (#7479)
- [administrateur] rend possible la publication en opendata du descriptif d'une démarche (#7485)
- fix(graphql): DemarcheDescriptorType can ba a revision or a procedure (#7488)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220613130322_change_column_null_for_targeted_user_links.rb`, `db/migrate/20220617142759_add_target_model_id_index_to_targeted_user_links.rb`, `db/migrate/20220620132646_add_column_closed_to_groupe_instructeurs.rb`, `db/migrate/20220620141238_add_index_on_procedure_id_and_closed_to_groupe_instructeurs.rb`, `db/migrate/20220621160241_backfill_add_column_closed_to_groupe_instructeurs.rb`, `db/migrate/20220622181047_add_opendata_to_procedures.rb`, `db/migrate/20220622183305_backfill_add_opendata_to_procedures.rb`).


--------------------------------------------

# 2022-06-17-02

Release [2022-06-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-17-02)  sur GitHub

## Améliorations et correctifs

### Technique

- fix(graphql): avoid n+1 on champ_descriptors (#7459)
- ajout du moteur de condition (#7424)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220531100040_add_condition_column_to_type_de_champ.rb`).


--------------------------------------------

# 2022-06-17-01

Release [2022-06-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- migration de l'éditeur de type de champ pour une utilisation de turbo (#7398)

### Mineurs

- correction: caractère surnuméraire dans le footer (#7464)

### Technique

- fix(export): corrige un crash sur les champs répétitions vides (#7472)


--------------------------------------------

# 2022-06-16-01

Release [2022-06-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- perf(dossier): improuve dossier preloading perf (#7453)

### Technique

- fix(20220614053743_fix_procedure_presentation_with_depose_since): missed case of procedure_presentation with Mis à jour depuis (#7468)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220614053744_fix_procedure_presentation_with_updated_since.rake`).


--------------------------------------------

# 2022-06-14-01

Release [2022-06-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-14-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(instructeurs/avis#revive): reactiver la demande d'avis sur un dossier ne fonctionnait pas (#7461)
- fix(procedure_presentation): la personnalisation d'une liste de dossier incluant "deposé depuis" cassait l'interface (#7465)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220614053743_fix_procedure_presentation_with_depose_since.rake`).


--------------------------------------------

# 2022-06-10-02

Release [2022-06-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-10-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(WebHookJob): missing find procedure (#7457)


--------------------------------------------

# 2022-06-10-01

Release [2022-06-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Fixe la restauration des démarches supprimées (#7454)
- bug(DeclarativeProceduresJob.perform_now): fail all next procedure on one dossier failure (#7455)


--------------------------------------------

# 2022-06-09-01

Release [2022-06-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-09-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- feat(procedure_presentation): add since date filters (#7449)


--------------------------------------------

# 2022-06-01-01

Release [2022-06-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Fixe l'erreur sur le nom du service dans le bandeau (#7441)

### Technique

- Technique : activation de l'estimation de la durée de remplissage pour toutes les démarches, et suppression du feature-flag `procedure-estimated-fill-time` (#7435)


--------------------------------------------

# 2022-05-31-02

Release [2022-05-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-31-02)  sur GitHub

## Améliorations et correctifs


### Technique

- fix(revision): fix clone démarches with repetitions (#7426)
- bump rake (#7425)
- feat(targeted_user_link): add targeted user link to wrap expert invitation in order to avoid access issue when the expert is connected with another account (#7369)
- Technique : correction d'une exception sur la page /patron (#7422)
- Technique : correction du nom du feature-flag `procedure_estimated_fill_duration` (#7432)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220520134041_enable_pgcrypto.rb`, `db/migrate/20220520134042_create_targeted_user_links.rb`).


--------------------------------------------

# 2022-05-31-01

Release [2022-05-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-31-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Admininistrateur : lorsque je publie une démarche, je peux indiquer si elle en remplace une autre (#7377)
- Admininistrateur : lorsque j'archive une démarche, je peux indiquer s'il existe une nouvelle démarche (#7417)
- Administrateur : affichage de la durée estimée de la démarche dans l'éditeur de champs (derrière un feature-flag) (#7409)

### Mineurs

- SuperAdmin : liste des démarches douteuses dans le manager (#7327)

### Technique

- fix(avis#instruction): avoid claiment(nil).email. (#7401)
- doc: add publiccode.yml (#6844, #7419)
- refactor(stimulus): use stimulus in message forms (#7413)
- Remove TypeDeChamp#types_de_champ (#7362)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220520173939_add_replacement_procedure_id_to_procedures.rb`).

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220517120321_remove_old_dubious_proc_job_from_delayed_job_table.rake`).


--------------------------------------------

# 2022-05-25-01

Release [2022-05-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : activation de l'attestation de dépôt pour toutes les démarches (#7348)

### Mineurs

- Usager : réduction du délai d'enregistrement automatique des brouillons (#7360)
- Usager : affichage du temps estimé pour remplir la démarche (derrière un feature flag) (#7352)
- SuperAdmin : affichage des infos depose_at, en_construction_at, en_instruction_at dans le Manager (#7399)

### Technique

- Administrateur : un dossier dans l'aperçu de la démarche peut être sauvegardé  (#7406)
- Refactor du rebase de dossiers sur une nouvelle révision (#7368)
- bug(expert/avis#instruction): raise when instructeur was merged (#7334)
- refactor(invites): use turbo (#7397)


--------------------------------------------

# 2022-05-24-01

Release [2022-05-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-24-01)  sur GitHub

## Améliorations et correctifs




### Mineurs
- fix(expert): expert should be allowed to delete messages (#7365)
- feat(graphql): expose repasser_en_construction and repasser_en_instruction (#7366)


### Technique

- refactor(revision) : utilise children_of sur les validateurs de répétition (#7353)
- refactor(graphql): fix types_de_champ usage (#7364)
- refactor(type_de_champ): use children_of when serializing type_de_champ (#7363)
- refactor(revision): types_de_champ for export (#7355)


--------------------------------------------

# 2022-05-19-01

Release [2022-05-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(revision): correction sur les révision ou un bug qui ajoutait en double les types de champ d'un bloc répétable  (#7354)


--------------------------------------------

# 2022-05-18-02

Release [2022-05-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-18-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(dossier_vide.pdf.prawn): avoid overlap on for multiline label on label (#7349)


### Technique

- Add champ v2 (#7302)
- Refactor(revision) : retire l'utilisation de type_de_champ.types_de_champ dans le manager (#7343)
- refactor(dossier): champs order by coordinate (#7345)
- Refactor(revision): enleve l'usage de types_de_champ dans le modèle type_de_champ (#7344)
- simplify children_of (#7346)
- Refactor(revision): retire l'usage de repetition.types_de_champ (#7342)
- Modèles : mise à jour des commentaires du schéma (#7347)


--------------------------------------------

# 2022-05-18-01

Release [2022-05-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(dossiers): remet l'export de tous les dossiers (#7339)

### Technique

- refactor(revision): remove type_de_champ#revision_id (#7307)
- fix(dossier): redirect on destroyed dossier -> NotFound (#7330)
- Merge pull request #7333 from betagouv/fix-webpack-dev-server (#7333)
- turbo poll controller (#7266)
- clean(data): remove old cron UpdateAdministrateurUsageStatisticsJob from delayed job table (#7329)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220517040643_remove_old_cron_job_from_delayed_job_table.rake`).


--------------------------------------------

# 2022-05-17-01

Release [2022-05-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-17-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- add link to solidarite-numerique (#7306)
- Usager : l'attestation de dépôt est envoyée en pièce jointe dans l'email de notification de dépôt (#7283)

### Technique

- fix(turbo): avoid crashing IE 11 with non get/post methods (#7315)
- Reapply improvements on procedure_revision (#7300)
- refactor(dossier): change state with turbo (#7316)
- fix(recherche): expert actions should be a menu-button (#7321)
- fix(sentry/3139111475): clean up some "corrupted" data, some dossier still have depose_at nil while en_construction_at is set (#7318)
- fix(autosave): prevent autosave if current champ is detached from the form (#7320)
- fix(recherche#index): move paginate outside of table row loop (#7322)
- Bump gems (#7312)
- Refactor(Revision Spec) : detailles les tests de comparaison entre les versions (#7311)
- sentry(3002560608): some Dossier ready to be purged are missing the hidden_by_reason (#7328)
- tech(Gemfile): unlock versions for kaminari and delayed_job_web (#7326)
- fix(spec): try fix dossier expiration spec (#7331)
- fix(fetch): prevent double parsing of fetch error messages (#7319)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220513135112_fix_depose_at_nil_on_dossier_state_not_brouillon.rake`, `lib/tasks/deployment/20220516160033_fix_hidden_by_reason_nil.rake`).


--------------------------------------------

# 2022-05-13-02

Release [2022-05-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-13-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(commencer#commencer): broken due to missing interpolated variable in i18n (#7313)


--------------------------------------------

# 2022-05-13-01

Release [2022-05-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-13-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Usager, je souhaite que mes emails transactionnels soient traduits (#7305)

### Technique

- Fix migrate parent (#7299)
- Merge branch 'main' into sentry/3267070633
- bug(commencer#commencer): raise when procedure does not have a service (#7303)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220512072704_fix_child_types_de_champ.rake`).


--------------------------------------------

# 2022-05-11-05

Release [2022-05-11-05](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-11-05)  sur GitHub

## Améliorations et correctifs



### Technique

- bring back find_type_de_champ_by_stable_id old implementation (#7297)


--------------------------------------------

# 2022-05-11-04

Release [2022-05-11-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-11-04)  sur GitHub

## Améliorations et correctifs



### Technique

- rollback type_de_champ revision changes (#7295)


--------------------------------------------

# 2022-05-11-03

Release [2022-05-11-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-11-03)  sur GitHub

## Améliorations et correctifs



### Mineurs

- cleanup(dossier): remove legacy export menu (#7286)
- Améliore l'accessibilité des boutons qui font apparaître un menu (#7289)

### Technique

- fix(upload): silence common client errors in sentry (#7292)
- feat(revision): migre remove_type_de_champ pour utiliser le nouveau système de révision (#7270)
- Move type de champ v2 (#7280)


--------------------------------------------

# 2022-05-11-02

Release [2022-05-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-11-02)  sur GitHub

## Améliorations et correctifs





### Technique

- fix(js): bring back js request for now (#7287)


--------------------------------------------

# 2022-05-11-01

Release [2022-05-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-11-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- Rewording for expiration messages in instruction panel (#7260)
- refactor(turbo): refactor autosave to use stimulus and turbo (#7257)


--------------------------------------------

# 2022-05-10-01

Release [2022-05-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-10-01)  sur GitHub

## Améliorations et correctifs


### Mineurs
- feat(administrateur) : ETQ Administrateur, je veux restaurer une démarche supprimée (#7278)
> <img width="728" alt="167623749-ca015b21-ea70-47d2-8c22-f14a539ba7bf" src="https://user-images.githubusercontent.com/125964/167667565-a632a58a-cf4a-465c-8427-68fa1f6bec80.png">

- feat(expert) : amélioration de l'en-tête des pages d'avis (#7277)
> <img width="973" alt="167614004-62cf11de-cac8-45ed-80a2-732395c65e46" src="https://user-images.githubusercontent.com/125964/167663263-e8654e52-dded-4286-91f8-ee8703c9950e.png">

- fix(multiselect): permet d'avoir plusieurs options avec un même label (#7276)
- fix(links): Corrige les link_to avec turbo (#7275)
- bug(export): expect dossiers_for_export not to include dossiers brouillon (#7279)

### Technique
- fix(revision) : corrige les liens entre parent enfant (#7268)
- fix(ga): include patches hash in github actions (#7282)
- feat(revision): partie technique une revision peut montrer les enfants d un groupe repetable (#7269)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220506105510_fix_wrong_parent.rake`).


--------------------------------------------

# 2022-05-06-01

Release [2022-05-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajoute dans l'interface usager le contact du service de la démarche (#7256)
> dans le cas d'une demarche close : <img width="1202" alt="demarche-close" src="https://user-images.githubusercontent.com/125964/167128945-b91ad9f2-3097-4578-8919-a5a21ddb9262.png">
> dans le cas d'une demarche supprimée : <img width="1218" alt="demarche-supprimee" src="https://user-images.githubusercontent.com/125964/167129029-a60c7723-c432-4ed7-a1e9-78037d9e0cc4.png">
> dans le cas d'une redirection utilisateur :  <img width="1225" alt="alert" src="https://user-images.githubusercontent.com/125964/167129084-175cf705-b229-4894-a5fd-7d3c165727d5.png">

- bug(type_de_champ.repetition): export avec repitition et nouveau champs commune. (#7259)
- corrige create_new_revision pour les enfants de type de champ répétition (#7265)

### Technique

- refactor(spec): lie les champs enfants avec leurs parents avec la nouvelle methode (#7243)
- refactor(revision): rename revision methods params to reflect usage of stable_id (#7263)
- cleanup(sendinblue): remove sendinblue tracking (#7258)
- refactor(procedure_revision_spec) (#7264)
- try codeql analysis (#7161)


--------------------------------------------

# 2022-05-05-02

Release [2022-05-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-05-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(procedure.expiration) ; pour toutes les nouvelles procédure, activation de l'expiration des dossiers une fois la duree de conservation passée. Rappel aux administrateurs qu'il doivent activer cette fonctionnalité (#6981)
> <img width="1066" alt="Screen Shot 2022-05-05 at 2 00 58 PM" src="https://user-images.githubusercontent.com/125964/166918921-32b08f89-ff4b-4fb1-b5b3-aaa8784bdd56.png">

### Technique

- fix(dossier): on dossier update render empty js response (#7254)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220222150340_set_default_procedure_expires_when_termine_enabled_to_true.rb`).


--------------------------------------------

# 2022-05-05-01

Release [2022-05-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : améliorations de l'affichage d'un dossier vide en PDF (#7247)
- Usager : l'accusé de dépôt affiche la première date de dépôt du dossier (#7248)

### Technique

- fix(js): prevent old Edge from crashing (#7250)
- fix(dossier): annotations privées form (#7252)
- Usager : correction du PDF de dossier vide quand un champ "Autre" est présent. (#7246)
- Improuve repetition champ (#7215)


--------------------------------------------

# 2022-05-04-03

Release [2022-05-04-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-04-03)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Usager : Ajout d'un accusé de dépôt téléchargeable (derrière un feature-flag) (#7238)
- Instructeur : Correction de l'enregistrement de blocs répétables dans les annotations privées (#7244)
- API : correction d'une exception sur les dossiers comportant des usagers supprimés (#7241)

### Technique

- refactor form inputs to use turbo (#7239)
- feat(user.merge): when user have an instructeur having dossier_opertions_logs containing instructeur_id (#7146)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220406144202_remove_column_instructeur_id_from_dossier_operation_log.rb`).


--------------------------------------------

# 2022-05-04-02

Release [2022-05-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-04-02)  sur GitHub

## Améliorations et correctifs

### Technique

- introduction de revision_types_de_champ (#7220)


--------------------------------------------

# 2022-05-04-01

Release [2022-05-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-04-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- feat(administrateurs) : supprime les droits admins non utilisés depuis 6 mois (#7228)


### Technique

- fix(js): add DocumentFragment.children polyfill for IE (#7227)
- Use fetch everywhere (#7216)
- refactor(dossier): use champ id as champ attributes key (#7200)
- clean(SMTP.balancing): remove some env vars (#7233)


--------------------------------------------

# 2022-05-03-01

Release [2022-05-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(locale) 'external opinion' english label (#7234)
- fix(manager): corrige le titre de la page dossier du manager (#7231)

### Technique

- renommage de types_de_champ en types_de_champs_public (#7209)


--------------------------------------------

# 2022-05-02-01

Release [2022-05-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-02-01)  sur GitHub

## Améliorations et correctifs

### Technique

- permet d'activer dolist (#7229)


--------------------------------------------

# 2022-04-29-04

Release [2022-04-29-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-29-04)  sur GitHub

## Améliorations et correctifs

### Technique

- fix patch-package on deploy


--------------------------------------------

# 2022-04-29-03

Release [2022-04-29-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-29-03)  sur GitHub

## Améliorations et correctifs


### Technique

- fix(js): event-target-polyfill should not patch Event (#7223)


--------------------------------------------

# 2022-04-29-02

Release [2022-04-29-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-29-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix typo in rgpd text (#7218)

### Technique

- fix(js): more polyfills for IE (#7219)
- fix(js): fix IE 11 fucked template element support (#7221)


--------------------------------------------

# 2022-04-29-01

Release [2022-04-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ameliore la decouvrabilité du code (#7213) (#7214)

### Technique

- Ajoute le prestataire de mail dolist (#7168)
- Ajoute le fichier SECURITY.md décrivant notre politque de sécurité (#7203)
- bump to rails 6.1.5.1 (#7212)


--------------------------------------------

# 2022-04-28-02

Release [2022-04-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-28-02)  sur GitHub

## Améliorations et correctifs



### Technique

- feat(dossier): use real dossier for preview (#7094)
- chore(devise): drop unmaintained dependencie (#7207)
- bug(commencer/test): with procedure without service nor organisation, it crashes (#7208)
- fix(react): too soon for strict mode (#7210)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220315124100_add_preview_to_dossiers.rb`).
Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220427195148_set_default_for_procedure_preview_on_dossiers.rake`).


--------------------------------------------

# 2022-04-28-01

Release [2022-04-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-28-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- feat(administrateur/procedures#publication): show dubious champs to administrateur (#7194)


### Technique

- fix(js): add turbo and stimulus polyfills (#7205)


--------------------------------------------

# 2022-04-27-01

Release [2022-04-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- fix(instructeur/procedure#export.zip): correctif pour l'export au format zip d'une selection de dossier de dossier (#7183)
- fix(procedure): une procédure cloné ne devrait pas hériter de l'auto_archive_on (#7185)
- fix(upload): errorFromDirectUploadMessage should take Error or string (#7192)
- feat(users/procedure/*): ajout du lien vers le texte_juridique ou la deliberation (#7190) et du lien vers le contact du DPO (#7197)
> ![Screenshot 2022-04-27 at 13-34-30 with list · demarches-simplifiees fr(1)](https://user-images.githubusercontent.com/125964/165541760-82267e40-41d5-4a5d-8d99-871222e9890b.png)
- feat(adminstrateurs#dossier_vide): ajout du bouton pour exporter un dossier vide au format PDF pour faciliter la vie des administrateurs afin d'inscrire la démarche au sein du registre de traitement.(#7188)
> <img width="1040" alt="164727749-b2b0e1e0-05a5-45b9-9597-a17ac4eca1f9" src="https://user-images.githubusercontent.com/125964/165542050-3d86c690-0f1a-44df-8ba4-bf7626ab769d.png">
- feat(type_de_champs::identite) Ajoute un avertissement sur les champs de type Titre Identité (#7196)
> <img width="1159" alt="165103842-213c03ac-af82-4899-8e5e-7af99ee2f362" src="https://user-images.githubusercontent.com/125964/165542735-822ee6e1-432a-4f51-9040-4fb48f2bbf11.png">
- fix(messagerie): Corrige l'exception lors de l'affichage de la messagerie (#7169)
- met a jour la mention accessibilité (#7201)

### Technique
- tech(ApiEntreprise): use dinum SIRET for all APIs call within the recipient params (#7195)
- fix(sentry#3144617191): Amélioration du rendu des archives dans le manager (#7184)
- tech(turbostream): Mise en place des turbostream rails (#7174)
- doc(ux): clarifie le process pour l'ux research (#7177)
- refactor(js): get rid of ds:page:update (#7186)
- ignore instructeur_id in dol (#7193)
- fix(migrate): lien administrateurs_procedures - administrateurs (#7191)
- refactor(champs): refactor champs components to use typescript (#7148)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220425140107_add_lien_dpo_to_procedure.rb`).


--------------------------------------------

# 2022-04-21-01

Release [2022-04-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- refactor(stimulus): initie les composants react avec stimulus (#7175)
- fix(a11y): Le tag HTML Lang correspond à la locale choisie(#7180)
- ETQ Administrateur, je veux que l'interface Instructeur et Usager soient entièrement traduite en anglais (#7179)


--------------------------------------------

# 2022-04-15-01

Release [2022-04-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs`

- Corrige le tri de dossier avec un filtre activé dans la vue instructeur (#7171)

### Technique

- chore(chartkick): upgrade to 4.1 and use built-in lazy loading (#7170)


--------------------------------------------

# 2022-04-14-02

Release [2022-04-14-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-14-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- correction: une procédure avec des pj manquantes peut être clonée (#7164)
- correction: l'ordre des colonnes dans la vue des dossiers instructeur est corrigé (#7166)


--------------------------------------------

# 2022-04-14-01

Release [2022-04-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-14-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration : traduction de plus d'éléments dans les pages Instructeur et Usager (#7158)
- Amélioration : traduction des colonnes des filtres de dossiers (#7162)
- Corrigé : corrige l'affichage des bannières de fermeture de démarche (#7163)


--------------------------------------------

# 2022-04-13-01

Release [2022-04-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-13-01)  sur GitHub

## Améliorations et correctifs

### Technique

- build(deps): bump devise-two-factor from 4.0.0 to 4.0.2 (#7155)
- passage a ruby 3.1.2 (#7157)
- rajoute des gems manquante de ruby 3.1.2 (#7159)

## Notes de déploiement

Montée de version de ruby de 3.0.3 à 3.1.2


--------------------------------------------

# 2022-04-12-02

Release [2022-04-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-12-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Fonctionnalité : permet d'exporter les dossiers au format .zip (#7123)

### Technique

- fix(graphql): fix AddressTypeType (#7151)
- Revert "Merge pull request #7137 from betagouv/faster_pdf" (#7152)


--------------------------------------------

# 2022-04-12-01

Release [2022-04-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajoute le controle du nonce et du state dans le flow openid d'agent connect (#7141)
- refactor(pdf): accélère la génération des pdf (*2 à *4) (#7137)
- fix(archive): exporte uniquement les fichiers sans virus (#7140)
- fix(graphql): detect custom champs in fragment definitions (#7142)

### Technique

- refactor(type_de_champs): use typescript in type de champs editor (#7136)
- Mise à jour de la dépendance Nokogiri (#7143)

## Notes de déploiement

Attention, cette version est buguée, en particulier elle empèche l'acceptation de certains dossiers. Passer à la release suivante.


--------------------------------------------

# 2022-04-08-01

Release [2022-04-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-08-01)  sur GitHub

## Améliorations et correctifs



### Technique

- accélere *30  la liste des documents par archive (#7115)
- Use the same env vars for setup and deploy (#7124)
- fix(dossiers): dossier extend_conservation actually works (#7138)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220408100411_fix_dossiers_expiration_dates.rake`).


--------------------------------------------

# 2022-04-07-01

Release [2022-04-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-07-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- feat: ETQ instructeur, je peux exporter une liste de dossier filtré (#7077)
> <img width="974" alt="Screen Shot 2022-04-07 at 10 41 57 AM" src="https://user-images.githubusercontent.com/125964/162158573-faca33fb-8a60-4e77-a222-c841239858e5.png">


### Mineurs
- feat: ETQ utilisateur, je peux changer de mail vers une adresse terminant en @assurance-maladie.fr (#7127)
- feat: ETQ instructeur, quand je demande un avis je suis le dossier automatiquement afin d'avoir les pastilles de notifications lorsqu'un expert/instructeur me répond. (#7125)

### Technique
- fix(exports): amélioration de migration pour ne pas bloquer la base de donnée lorsqu'on migre (#7130) (#7133)


## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20220323143325_add_procedure_presentation_and_state_to_exports.rb`, `db/migrate/20220405163206_add_procedure_presentation_snapshot_to_exports.rb`, `db/migrate/20220407081538_backfill_export_status.rb`).


--------------------------------------------

# 2022-04-05-02

Release [2022-04-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-05-02)  sur GitHub

## Améliorations et correctifs

### Technique

- grosses archives: appel une second fois le script d'envoi de l'archive si erreur (#7121)
- ajoute un index unique sur attestation dossier_id (#7117)
- refactorisation: expose filtered_sorted_ids method (#7114)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220405100354_add_uniq_index_on_attestation_dossier_id.rb`).


--------------------------------------------

# 2022-04-05-01

Release [2022-04-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat: ETQ instructeur, je peux télécharger/exporter un dossier et ses pjs sans limite de taille (#7106)
> <img width="292" alt="Screen Shot 2022-04-05 at 2 12 56 PM" src="https://user-images.githubusercontent.com/125964/161751184-41bd0ef0-9530-4aef-ad3a-aeba56e4cc99.png">
- fix: ETQ instructeur, les compteurs entre la synthese et les procedures etaient désynchronisés lorsqu'il y avait une procédure supprimée (#7109) 
> <img width="958" alt="Screen Shot 2022-04-05 at 2 14 18 PM" src="https://user-images.githubusercontent.com/125964/161751380-e7b068cf-0299-4bc5-a0a9-d4407184d9fe.png">
- fix: ETQ instructeur, les dossier traités du dernier jour du mois ne sont pas présents dans mon `archive.zip` (#7119)
- perf: ETQ instructeur, générer mes `archives.zip` plus rapidement (#7111)

### Technique
- data: tache pour supprimer les attestations en double (#7116)
- refacto: Amélioration du code de filtrage des dossiers (#7102)
- revert: On fait marche arrière sur l'introduction de Sorbet (#7118, #7105)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220405092317_remove_duplicate_attestations.rake`).


--------------------------------------------

# 2022-04-04-01

Release [2022-04-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- ETQ Expert, je souhaite pouvoir supprimer un message envoyé (#7101)

### Technique
- Téléchargement des fichiers des archives plus robuste. (#7104)
- Génération des archives plus rapide (#7103)


--------------------------------------------

# 2022-03-31-02

Release [2022-03-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-31-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Extraction du management de l'état d'une archive dans le job qui l'execute. Ajout des archive au manager afin de suivre l'usage temporairement (#7092)

### Technique

- Passage à React 18 (#7095)


--------------------------------------------

# 2022-03-31-01

Release [2022-03-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-31-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(archive): prepare la prod a un plus gros usage des archives (#7068)
- Fixe la typo lorsqu'un expert veut envoyer un message (#7090)
- Corrige la visibilité des dossiers (#7093)

### Technique

- build(deps): bump puma from 5.6.2 to 5.6.4 (#7096)
- Use vite (#6787)
- Revert "Merge pull request #6787 from tchak/use-vite" (#7098)
- fix(ProcedureArchiveService.zip_root_folder): should take archive instance otherwise when we generate many archive for the same procedure, errors may occures (#7065)
- build(deps): bump minimist from 1.2.5 to 1.2.6 (#7091)


--------------------------------------------

# 2022-03-29-02

Release [2022-03-29-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-29-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Une bannière est maintenant affichée quand on consulte un dossier d'une démarche supprimée (#7075)

### Technique

- Ajout de contraintes sur les colonnes des rôles en base, et suppression des colonnes `users.role_id` (#7088)

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données :
- `db/migrate/20220323064705_add_not_null_constraints_to_role_tables.rb`,
- `db/migrate/20220323113048_add_foreign_keys_to_role_tables.rb`,
- `db/migrate/20220323113152_validate_foreign_keys_to_role_tables.rb`,
- `db/migrate/20220323113327_add_index_to_role_tables.rb`,
- `db/migrate/20220323120846_remove_role_columns_on_user.rb`.


--------------------------------------------

# 2022-03-29-01

Release [2022-03-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-29-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Ajout d'une migration de données pour supprimer les rôles invalides en base (#7070)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220316105857_delete_roles_without_users.rake`).


--------------------------------------------

# 2022-03-24-01

Release [2022-03-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout de contraintes NOT NULL sur les colonnes de AdministrateursInstructeur (#7074)
- Correction de la tâche AfterParty fix_cloned_revisions (#7037)

### Technique

- durcit la validation des emails (#7078)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220322110900_add_not_null_constraints_to_administrateurs_instructeur.rb`).


--------------------------------------------

# 2022-03-22-02

Release [2022-03-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-22-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- API : correction d'une exception lors de l'authentification pour utiliser l'API GraphQL (#7072)

### Technique

- Correction de la tâche AfterParty migrate_revisions (#7033)


--------------------------------------------

# 2022-03-22-01

Release [2022-03-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-22-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de l'affichage des données GeoJSON sur la carte (#7067)


### Technique

- Changement du sens de la relation des Users aux rôles (#7050)
- fix(dossiers): mark as read dossier on update_annotations (#7061)


--------------------------------------------

# 2022-03-18-02

Release [2022-03-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-18-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Activation de la nouvelle méthode pour générer des archives (zip && typhoeus) (#7063


--------------------------------------------

# 2022-03-18-01

Release [2022-03-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Instructeur, lorsque l'usager a supprimé le dossier de son interface, je ne peux plus lui envoyer de messages (#7055)


### Technique

- Fix: Evitons de laisser des file descriptors ouvert lors de la création des archives, sinon on atteind la limite du systeme sur de gros volumes (#7058)


--------------------------------------------

# 2022-03-17-01

Release [2022-03-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-17-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- corrige un lien pointant vers les mentions légales (#7053)
- Afficher les modifications des formulaires (#7052)
- Correction des cartes IGN (#7056)

### Technique

- Échec de la tâche AfterParty process_expired_dossiers_en_construction (#7029)
- Échec de la tâche AfterParty add_traitements_from_dossiers (#7035)
- Échec de la tâche AfterParty fix_geo_areas_geometry (#7039)


--------------------------------------------

# 2022-03-16-01

Release [2022-03-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-16-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ETQ qu'instructeur je veux pouvoir télécharger une archive de ma procédure même si elle fait plus de 5Go (en test) (#6989)

### Technique

- Ajout de strong_migrations (linter automatique des migrations Rails) (#7046)


--------------------------------------------

# 2022-03-15-02

Release [2022-03-15-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-15-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- correction de la prévisualisation suite à un bug sur les numéros de section (#7042)


--------------------------------------------

# 2022-03-15-01

Release [2022-03-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-15-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- Améliorer la vue des dossiers (supprimés) pour les instructeurs (#7026)
- feat(graphql): expose more dossier informations (#7022)


### Technique

- perf(dossier): memoize champ.sections (#7023)
- Correction de la migration 20210402163003_exports_key_not_null (#6912)
- fix(instructeur): avoid n+1 in instructeur_dossiers#show et user_dossiers#show (#7024)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20210402000000_null_export_keys_to_uuid.rake`). Cette migration corrige une ancienne tâche de migration de schéma, et n'a pas d'incidence sur les systèmes en production à jour.


--------------------------------------------

# 2022-03-11-01

Release [2022-03-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-11-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Ne plus utiliser Discarded sur les Dossiers et utiliser les bons scopes (#7014)
- Les vues et les traductions de l'app sont maintenant facilement configurables par les instances (#6920)
- Ajout d'une documentation sur la gestion des données confidentielles (#7010)


--------------------------------------------

# 2022-03-10-01

Release [2022-03-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-10-01)  sur GitHub

## Améliorations et correctifs

### Technique

- bump rails to 6.1.4.7 (#7016)
- migrate(dossier): fill hidden_by_administration_at and hidden_by_user_at from hidden_at (#7017)
- fix(migration): add with_discarded to seed_hidden_at_dossiers (#7018)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220309101504_seed_hidden_at_dossiers.rake`).


--------------------------------------------

# 2022-03-08-01

Release [2022-03-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Rend la page de connexion visible en entier sur un écran de 1280 x 800 px (#7004)
- Correction de coquilles (#7008, #7009)

### Technique

- Suppression des AdministrateursProcedure quand une Procedure est supprimée (#7013)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220308110720_add_procedure_foreign_key_to_administrateurs_procedure.rb`).


--------------------------------------------

# 2022-03-02-02

Release [2022-03-02-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-02-02)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Suppression des AdministrateursInstructeur quand un Instructeur est supprimé (#7001)
- Améliorer le compteur des dossiers dans la vue administrateur (#7002)
- Corrigé le cas des dossiers impossible à déposer suite au changement sur l'état du routage (#7003)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220302101337_add_foreign_keys_to_administrateurs_instructeurs.rb`).


--------------------------------------------

# 2022-03-02-01

Release [2022-03-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout de documentation pour le déploiement (#6971)
- Correction des composants multi-select (#6999)
- Correction de l'affichage des numéros de téléphone des DROM (#6997)
- Usager : l'interface des cartes n'attend plus que le fond de carte soit chargé pour s'afficher (#6987)

### Technique

- Suppression des AdministrateursProcedure quand un Administrateur est supprimé (#6992)
- chore(deps): bump image_processing from 1.12.1 to 1.12.2 (#6998)
- Bump nokogiri from 1.13.1 to 1.13.3 (#6993)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220301160753_add_administrateur_foreign_key_to_administrateurs_procedure.rb`).


--------------------------------------------

# 2022-02-28-01

Release [2022-02-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs
- feat(dossier): gestion de la mise à jour des dossiers en construction and en instruction lors de la publication d'une nouvelle revision de procédure (#6972)
- feat(agentconnect): Intégration des maquettes AgentConnect (#6816, #6986)

### Mineurs
- feat(email): Améliorations de l'email de notification "Nouveau commentaire sur un dossier en brouillon" (#6968)
- feat(api-entreprise.warning): Affichage d'un message clair en cas d'erreur `APIEntreprise::API::Error::ServiceUnavailable` (#6984)
- feat(administrateurs.dossier-submitted-message): Possibilité de rediriger l'usager vers un site externe après dépôt de son dossier (#6913)
- fix(avis_controller#*): missing ACL (#6990)

### Technique
- tech(js.warnings): Javascript : correction des avertissements ESLint (#6985)
- test(user): correctif d'un test qui passait aléatoirement (#6973)
- tech(migration): Restauration des anciennes tâches after_party supprimée du code (#6979)
- tech(test.remote): Blocage des connexions vers des services externes pendant les tests (#6983)
- tech(dependences): suppression des mentions de commits spécifiques pour les gems (#6975)
- tech(node): passage a volta pour faciliter la gestion des dépédances pour les contributeurs
- tech(rails): Mise à jour de Rails vers la version 6.1.4.6 (#6978)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220204130722_create_dossier_submitted_messages.rb`).

Cette version restaure d'ancienne tâches de migrations du contenu des données. Ces tâches sont supposées avoir déjà tourné en production, et ne sont typiquement pas exécutées. (Voir #6979)


--------------------------------------------

# 2022-02-15-04

Release [2022-02-15-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-15-04)  sur GitHub

## Améliorations et correctifs

### Technique

- Correction des requêtes Matomo bloquées par la Content Security Policy (#6966)
- Amélioration de la fiabilité du test automatisé "Remplir un dossier" (#6965)
- Dépendances : mise à jour de `puma` de la v5.5.1 à la v5.6.2 (#6956)


--------------------------------------------

# 2022-02-15-03

Release [2022-02-15-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-15-03)  sur GitHub

## Améliorations et correctifs
### Technique

- Javascript : correction de la gestion d'erreur dans OperationQueue.js (#6963)
- Utilisation des variables d'environnement lors de la déclaration des Content Security Policies (à nouveau) (#6946)


--------------------------------------------

# 2022-02-15-02

Release [2022-02-15-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-15-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction du réglage "cookieDomain" de Matomo (#6960)


--------------------------------------------

# 2022-02-15-01

Release [2022-02-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction du bandeau indiquant l'expiration du dossier (#6955)

### Technique

- Rend l'environnement de développement accessible depuis un hôte autre que localhost:3000 (#6902)
- Javascript : mise à jour de `follow-redirects` de la v1.14.7 à la v1.14.8 (#6957)
- Ajout d'un réglage pour désactiver ClamAV (#6927)
- Ajout d'un réglage pour configurer le domaine de Matomo (#6915)
- Ajout d'un réglage pour utiliser MailCatcher dans l'environnement de production (#6914)
- Ajout d'un réglage pour configurer le type de stockage ActiveStorage (#6929)
- Ajout d'un réglage pour masquer des sections de la landing page administration (#6917)

### Notes de déploiement

Cette release ajoute des variables d'environnement obligatoires :

- `ACTIVE_STORAGE_SERVICE`
- `MATOMO_DOMAIN`
- `MATOMO_COOKIE_DOMAIN`
- `MAILCATCHER_ENABLED`
- `MAILCATCHER_HOST`
- `MAILCATCHER_PORT`
- `CLAMAV_ENABLED`

[Voir le diff](https://github.com/betagouv/demarches-simplifiees.fr/pull/6958/files#diff-f0d9dfc2f9c0665f8d8630f30effad8d583820349481a7d5169fc0f98e986759)

Elle rajoute également des variables d'environnement optionnelles :

- `LANDING_TESTIMONIALS_ENABLED`
- `LANDING_USERS_ENABLED`

[Voir le diff](https://github.com/betagouv/demarches-simplifiees.fr/pull/6958/files#diff-d10170506b925b305d94a0f0389b85c674b448548dd1b721f08fded2bba6f86a)


--------------------------------------------

# 2022-02-09-01

Release [2022-02-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Replace mapbox-gl with maplibre-gl (#6876)
- L'URL de l'iframe Matomo est maintenant configurable (#6931)
- OpenStack : utilisation de la variable d'environnement FOG_OPENSTACK_URL (#6932)
- Crée les DeletedDossier dans la méthode Dossier.purge_discarded (#6938)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220204093401_add_hidden_by_reason_to_dossiers.rb`).


--------------------------------------------

# 2022-02-08-03

Release [2022-02-08-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-08-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Correction de l’envoi des pièces jointes, par un revert des changements récents de Content Security Policy (#6944)


--------------------------------------------

# 2022-02-08-02

Release [2022-02-08-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-08-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Versionnage des templates d’attestation (à nouveau) (#6841)

### Mineurs

- Amélioration du contraste de la landing page administration (#6918)

### Technique

-  L'option --no-sandbox du pilote Selenium Chrome est maintenant configurable lors des tests (#6899)
- Nettoyage des dépendances de Rubocop (#6908)
- Remplacement de la gem `rubocop-rspec-focused` dépréciée (#6905)
- Utilisation des variables d'environnement lors de la déclaration des Content Security Policies (#6933)
- Correction d'un test qui échouait si la variable d'environnement `UNIVERSIGN_API_URL` était définie sans valeur (#6900)
- Amélioration de la configuration des fournisseurs d'email (#6821)
- AfterParty : correction de la tâche `20210118142539_backfill_experts_procedure_id_on_avis_table` (#6922)
- AfterParty : amélioration de la tâche `20210307144755_backfill_claimant_type_on_avis_table` (#6923)
- AfterParty : amélioration de la tâche `20210311085419_backfill_claimant_id_for_experts_on_avis_table` (#6924)
- AfterParty : amélioration de la tâche `20210324081552_backfill_experts_procedure_id_on_avis_table` (#6925)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20211214145059_add_attestation_template_id_to_procedure_revisions.rb`).

Cette version comporte une migration du contenu des données (`spec/lib/tasks/deployment/20220112184331_revise_attestation_templates_spec.rb`).


--------------------------------------------

# 2022-02-08-01

Release [2022-02-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration du rendu du pied-de-page (#6919)
- SuperAdmin : les administrateurs ajoutés pour une journée sont correctement supprimés  (#6892)
- Statistiques : amélioration visuelle de l'affichage du temps de traitement habituel (#6852)

### Technique

- Ignore les avertissements GitGuardian de fuite de secrets de test (#6939)
- Possibilité de charger un filigrane depuis une URL distante (#6928)
- Correction de tests sur les instructeurs qui échouent aléatoirement (#6907)
- Correction de feature tests touchant aux emails qui échouent aléatoirement (#6903)
- Correction de tests France Connect qui échouent quand la variable d'env APPLICATION_NAME a une autre valeur que celle par défaut (#6906)
- Tri alphabétique des règles Rubocop (#6909)
- AfterParty : amélioration de la tâche `20210429172327_rename_conservation_extension` (#6926)
- Unicité du nom des tâches de déploiement (#6930)
- Déplacement de toutes les mentions de la DINUM dans des variables (#6916)


--------------------------------------------

# 2022-02-04-01

Release [2022-02-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de l'ordre des mois sur la page des statistiques (#6851)

- Instructeur : le dossier supprimé dans la recherche n'est pas cliquable (#6894)

### Technique

- Fixe a11y-8.9.1 (#6827)


--------------------------------------------

# 2022-02-02-01

Release [2022-02-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-02-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Instructeur : correction de la recherche de dossiers, cassée dans la release précédente (#6850)


--------------------------------------------

# 2022-02-01-01

Release [2022-02-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-02-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'une nouvelle variable d'environnement `SAML_IDP_ENABLED` (#6836)
- ETQ instructeur, je veux pouvoir restaurer un dossier supprimé (#6843)
- Administrateur : sépare la validation des champs publics et des annotations privées (#6840)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220127135056_add_supprimes_recemment_to_procedure_presentations.rb`).

**⚠️ Attention :** Cette version contient un problème lié à la PR #6843, qui empêche l'affichage de certains résultats de recherche.


--------------------------------------------

# 2022-01-26-01

Release [2022-01-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-26-01)  sur GitHub

## Améliorations et correctifs

### Hotfix

- hotfix(attestation): add missing attestations on dossiers (#6838)


--------------------------------------------

# 2022-01-25-02

Release [2022-01-25-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-25-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Javascript : mise à jour de node-fetch de la v2.6.6 à la v2.6.7 (#6832)
- Revert de la migration des templates d'attestation de la release précédente (#6834)


--------------------------------------------

# 2022-01-25-01

Release [2022-01-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- test(system): Simplifie le selecteur de menu (#6814)
- fix(archives): Seulememnt les dossiers archivés des groupes instructeurs sont exportables (#6813)
- feat(procedure): Seuls les démarches n'ayant plus de dossiers sont supprimables (#6810)
- Amélioration de la description des variables d’environnement (#6807)
- ETQ Administrateur, lorsque je teste ma démarche, je souhaite que l'import CSV n'envoie pas de mails aux instructeurs (#6819)
- Amélioration des variables d'environnement (ajout et renommage) (#6820)
- fix(dossier): N'envoie pas de mail avertissant de l'expiration du dossier si il est déjà supprimé (#6826)
- Versionner les attestations (#6751)
- feat(MonAvisEmbedValidator): Autorise le bouton mon avis a ne pas voir de titre (#6831)
- Instructeur : corrige l'affichage des dossiers masqués par l'usager dans l'onglet 'tous' de la liste des dossiers (#6830)
- Fixe les mails de suppression de dossiers (#6825)

### Technique

- Javascript : mise à jour de `nanoid` de la v3.1.30 à la v3.2.0 (#6829)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20211214145059_add_attestation_template_id_to_procedure_revisions.rb`).

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20220112184331_revise_attestation_templates.rake`).

**🛑 Attention :** dans cette version, la migration des templates d'attestations comporte une erreur qui dissocie les templates attestations des démarches. Ce problème est corrigé (par un revert) dans la version suivante.


--------------------------------------------

# 2022-01-13-01

Release [2022-01-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-13-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'une erreur dans le formulaire de contact (#6804)
- fix(parallel_download_queue): typhoeus does not like raise from a request handler [crash straight to first frame] (#6805)
- Correction de Administrateurs::ProcedureAdministrateursController#destroy (#6801)
- Fix multiselect menu (#6809)
- ETQ usager, je souhaite restaurer un dossier supprimé (#6797)

### Technique

-Mise à jour : follow-redirects de 1.14.6 à 1.14.7 (#6811)


--------------------------------------------

# 2022-01-06-01

Release [2022-01-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de l'accessibilité concernant les libellés et descriptions des champs
- Administrateur : possibilité de désactiver une attestation même une fois la démarche publiée
- Instructeur : la pastille comme quoi un dossier a été mis à jour manquait quand un utilisateur avait mis à jour une pièce justificative


--------------------------------------------

# 2022-01-04-01

Release [2022-01-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-04-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Correction d'un bug javascript qui affectait les champs pays/nationalité  (#6791)


--------------------------------------------

# 2022-01-03-02

Release [2022-01-03-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-01-03-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Instructeur, je veux pouvoir cacher un dossier terminé de mon interface (#6716)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20211202133139_add_hidden_by_administration_to_dossiers.rb`).


--------------------------------------------

# 2021-12-23-01

Release [2021-12-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : corrige la configuration des notifications si l'instructeur appartient à plusieurs groupe d'instruction d'une même démarche (#6770)
- SuperAdmin : affichage et édition des zones des démarches dans le Manager (#6779)

### Mineurs

- Matomo : désactivation du suivi des clics sur les liens externes (#6766)
- Améliorations sur l'accessibilité (#6765)
- Correction d'un problème d'accessibilité sur la page de contact (#6760)
- Correction d’une coquille typographique dans un texte de l’API Particulier (#6762)

### Technique

- Correction d'un test automatique sur la suppression des dossiers (#6769)
- Javascript : utilisation d'imports dynamiques (#6768)
- Correction des tests automatisés des zones (#6764)
- Mise à jour des dépendances Javascript (#6767)


--------------------------------------------

# 2021-12-18-01

Release [2021-12-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(bulk_messages): destroy bulk messages with procedures
- Répare le fichier csv des stats accessible en super admin
- ETQ administrateur ; je souhaite pouvoir publier un lien vers monavis qui utilise les URL de vosusagers
- Merge pull request #6737
- Merge pull request #6748 from betagouv/better-stats-text
- Corrige un affichage non nettoyé d'email d'un usager
- Ajoute une tâche manuelle pour renseigner la zone des procédures données en entrée
- rend le zonage disponible derrière le feature flag _zonage_
-  [API Particulier] (4/4) Support du statut étudiant
- affiche le bouton "supprimer la démarche" pas uniquement lorsqu'elle en brouillon (#6752)

### Technique

- compresse les archives en utilisant l'utilitaire zip et non la libraire ruby
- [corrige un test pour les stats](https://github.com/betagouv/demarches-simplifiees.fr/pull/6756) 
- corrige un test pour le service d'archivage de procedure


--------------------------------------------

# 2021-12-09-01

Release [2021-12-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- [API Particulier] (3/4) Support de Pôle emploi (#6710)
- Administrateur : ajout d'une classification des démarches par "Zone administrative" (#6733)

### Mineurs

- Instructeurs : correction de la requête de notification quotidienne (#6740)
- Petites corrections textuelles (#6709)

### Technique

- GraphQL : correction d'une erreur lorsque l'attestation est nil (#6738)
- Helpscout : ajout d'un tag aux messages envoyés depuis le formulaire de contact


--------------------------------------------

# 2021-12-07-03

Release [2021-12-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-07-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Désactivation temporaire de l'expiration des dossiers (#6727)


--------------------------------------------

# 2021-12-07-02

Release [2021-12-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-07-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- [API Particulier] (2/4) Support de DGFiP

### Mineurs

- Instructeur : correction d'un problème lors de l'affichage des dossiers (#6724)


--------------------------------------------

# 2021-12-07-01

Release [2021-12-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Utiliser depose_at au lieu de en_construction_at
- Display commune departement information


### Technique

- Add parent to procedure revision type de champ

## Notes de déploiement

⚠️ Cette version comporte une erreur, due à un bug dans une tâche de migration. Ne pas déployer.


--------------------------------------------

# 2021-12-06-01

Release [2021-12-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- 6194 ETQ administrateur ajout des zones sur la déclaration d'une démarche
- 6671 ETQ instructeur, j'ai de la visibilité et je peux gérer les dossiers expirants


--------------------------------------------

# 2021-12-02-01

Release [2021-12-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-12-02-01)  sur GitHub

## Améliorations et correctifs
### Mineurs

-  Administrateur : empêche la publication d'une démarche comportant un champ "Choix" sans valeur sélectionnable (#6703)
- Usager : la date d'expiration d'un dossier n'est affichée que si la fonctionnalité `procedure_process_expired_dossiers_termine` est activée (#6684)
- Suppression des statistiques visibles uniquement par les Super-Admins (#6712)

### Technique

- Remplacement de la feature Flipper `instructeur_bypass_email_login_token` 3/3 (#6681)
- Amélioration de la fiabilité des tests des statistiques (#6711)


--------------------------------------------

# 2021-11-30-04

Release [2021-11-30-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-30-04)  sur GitHub

### Technique

- Correction du nom de la dernière tâche `after_party` en date (#6701)

### Notes de déploiement

Cette version contient une tâche `after_party` de migration de données, qui migre les données du feature flag `Flipper.instructeur_bypass_email_login_token` vers une colonne `Instructeur.bypass_email_login_token`.


--------------------------------------------

# 2021-11-30-03

Release [2021-11-30-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-30-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : empêche de publier un formulaire comportant un bloc répétable vide (#6473)
- Instructeur : il est possible de voir les documents joints à une demande d'avis

### Technique

-  Remplissage de la colonne `Instructeur.bypass_email_login_token` 2/3 (#6680)


--------------------------------------------

# 2021-11-30-02

Release [2021-11-30-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-30-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Passage a ruby 3.0.3 (#6675)
- Ajoute une gem manquante avec ruby 3 (#6697)

### Notes de déploiement

:information_source: Cette version contient une migration majeur de ruby de 2.7.3 à 3.0.3


--------------------------------------------

# 2021-11-30-01

Release [2021-11-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige la réappartition d'un dossier supprimé par un utilisateur mais repassé en instruction (#6691)
- Dans le manager, redirige correctement sur après avoir mergé des utilisateurs (#6692)

### Technique

- Supprime la table obsolète `feedbacks` (#6687)
- Renommage du namespace `NewAdministrateur` en `Administrateurs` (#6682)


--------------------------------------------

# 2021-11-26-01

Release [2021-11-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- N'affiche pas les départements DOM lorsqu'on sélectionne une commune
- ETQ Utilisateur, j'aimerais fusionner mes comptes FC/DS avec un mail de confirmation
- ETQ usager, je veux pouvoir supprimer les dossiers terminés


--------------------------------------------

# 2021-11-25-01

Release [2021-11-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Prise en charge d'agent connect (#6027)

### Mineurs
- Simplification des messages d’erreur en cas de fichier trop volumineux
- GraphQL : affiche la cause des erreurs de syntaxe des requêtes
- 6649 ETQ usager instructeur rendre la suppression plus visible
- Fix add depose_at to dossiers
- fix(ComboCOmmunesSearch.typo): fix combo communes search typo

### Technique

- Suppression du code obsolète pour l'auto-validation du chemin des démarches
- fix(graphql): fix and improve query parsing for logs
- Ajoute une variable d'env manquante a AgentConnect
- Merge pull request #6664 from betagouv/save-new-procedure-revision


--------------------------------------------

# 2021-11-17-02

Release [2021-11-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-17-02)  sur GitHub

## Améliorations et correctifs

### Technique

- fix(champs): save departement info on champ commune


--------------------------------------------

# 2021-11-17-01

Release [2021-11-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-17-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- un administrateur peut désactiver lui même le routage
- un instructeur peut supprimer un message envoyé par erreur #6624 
- un administrateur peut modifier la durée de conservation des dossiers une fois la procédure en ligne

### Technique

- Correction d'un bug qui empêchait d'accéder au profil et de renouveler son jeton api
- Correction d'un qui affectait le merge de compte avec des fichiers cachés
- Correction d'un bug d'upload de fichier (pj stale)
- Correction de l'affichage des traitements d'un dossier
- Mise a jour de dépendances js


--------------------------------------------

# 2021-11-12-01

Release [2021-11-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-12-01)  sur GitHub

## Améliorations et correctifs





### Technique

- Correction de colonnes non désirées dans les démarches clonées comportant des blocs répétables (#6629)
- Reset notifications when changing dossier state


--------------------------------------------

# 2021-11-05-01

Release [2021-11-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-05-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- feat(champ): Demande un département avant de demander une commune
- FIX #6557 CONTRIBUTING Décrit les best practices de dev
- Corrige l'environnement de développement pour apple silicon
- Permet à un usager de merger son compte (#6550)
- fix(graphql): add graphql_operation to lograge



### Technique

- bump ruby to 2.7.3
- FIX PJ - Lancer les jobs d'antivirus et analyse rails indépendamment
- Merge branch 'main' into dependabot/npm_and_yarn/color-string-1.6.0
- chore(deps): bump color-string from 1.5.3 to 1.6.0
- chore(deps): bump striptags from 3.1.1 to 3.2.0


--------------------------------------------

# 2021-11-04-01

Release [2021-11-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-04-01)  sur GitHub

## Améliorations et correctifs

### Technique

- feat(graphql): add graphql_operation to rails logs
- fix(grope_instructeur): can not destroy groupe_instructeur with discarded dossiers
- fix(dossiers): wrap dossier discard in a transaction
- fix(revisions): fix repetitions export with revisions
- Amélioration de la vitesse d'exécution des tests GraphQL (#6610)
- task(dossiers): delete MAE procedures expired dossiers
- fix(avis): destroy avis for discarded en_construction dossiers


--------------------------------------------

# 2021-10-29-01

Release [2021-10-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ usager, je veux voir les titres des démarches en plus grand dans mon interface


--------------------------------------------

# 2021-10-28-01

Release [2021-10-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-28-01)  sur GitHub

## Améliorations et correctifs



### Mineurs
- Supprime le vieux compte lors d'un merge
- Unfollow les dossiers lorsqu’un instructeur est retiré d'un groupe instructeur

### Technique

- fix(bulk_messages): disable pj on bulk messages
- fix(typo): addresse -> adresse
- fix(rebase): fix repetition champ rebase
- Fix repetition add row button


--------------------------------------------

# 2021-10-27-01

Release [2021-10-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateur : lors de la modification d'une démarche publiée, les dossiers en brouillon existants sont mis à jour avec la dernière révision.
- Administrateur : possibilité d'ajouter une option "Autre" à une liste de choix
- Administrateur : possibilité de spécifier le libellé et la description des menus déroulants secondaires

### Mineurs

- Usager : les dossiers désactivés ne sont plus transférés lors d'une fusion de compte

### Technique

- Fix(dossier): fixe dossier.avis cascade
- Suppression de code obsolète dans la configuration du serveur web puma
- Mise en cache du dictionnaire de complexité des mots de passe
- Tests : Migration des tests d'intégration vers des "system specs" (#6584)
- Tests : amélioration de certaines factories


--------------------------------------------

# 2021-10-20-02

Release [2021-10-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-20-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Améliorer les perfs des exports


--------------------------------------------

# 2021-10-20-01

Release [2021-10-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-20-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- i18n: affichage du sélecteur de langues pour les navigateurs non-francophones

### Mineurs

- un super admin peut a nouveau ajouter un admin à une démarche
- les instructeurs / administrateurs peuvent changer leur emails vers des domaines autorisés
- un super administrateur peut changer l'email d'un administrateur

### Technique

- Le merge de compte FranceConnect ne lève pas d'exception lorsqu'il concerne un compte qui n'existe pas
- Gems : mise à jour de phonelib (#6560)


--------------------------------------------

# 2021-10-14-01

Release [2021-10-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-14-01)  sur GitHub

#:exclamation: Cette mise à jour est recommandée pour toutes les instances qui utilisent France Connect.

## Améliorations et correctifs

### Majeurs

- Ajoute le champ CNAF
- Corrige le connecteur FranceConnect dans le cas de différent comptes FC utilisant le même mail de contact

### Mineurs

- Corrige une erreur lors de la fusion de compte dans le manager
- Usager : les informations FranceConnect sont maintenant plus discrètes dans le formulaire (#6538)
- Retire une description superflu dans un champ oui/non

### Technique

- Nettoyage : déplacement de la classe ActiveJobLogSubscriber en dehors des initializers (#6533)
- Merge pull request #6540 from betagouv/dependabot/bundler/puma-5.5.1
- fix(avis): add foreign key to avis dossier_id


--------------------------------------------

# 2021-10-11-01

Release [2021-10-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Un super administrateur peut devenir un administrateur d'un procédure démarche 24h (#6530)
- Un super administrateur peut changer le mail d'un instructeur (#6515)


--------------------------------------------

# 2021-10-07-01

Release [2021-10-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-07-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- ETQ Administrateur je veux télécharger au format CSV tous les groupes instructeurs d'une démarche (FIX)
- Corrige les jobs de suppression de dossiers et démarches

### Technique

- build(deps): Mise à jour de  url-parse from 1.5.1 to 1.5.3


--------------------------------------------

# 2021-10-06-02

Release [2021-10-06-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-06-02)  sur GitHub

## Améliorations et correctifs



### Technique

- fix(dossier): never write dossier logs on dossiers brouillons


--------------------------------------------

# 2021-10-06-01

Release [2021-10-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ expert, je veux télécharger le dossier en pdf et ses pj
- ETQ Administrateur, je souhaite exporter tous les groupes instructeurs d'une démarche au format CSV


--------------------------------------------

# 2021-10-05-01

Release [2021-10-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : localisation de l'import des groupes instructeurs par fichier CSV (#6479)
- Instructeur : une pastille de notification est maintenant affichée lorsque l'usager met à jour son identité (#6511)
- Administrateur : l’aperçu d'une démarche s’ouvre maintenant dans un nouvel onglet (#6512)

### Technique

- Mise à jour de webpacker (#6474)
- Correction d'un test de feature qui échouait aléatoirement (#6507)
- Correction d'une erreur lors de la suppression d'une demande de transfert de fichier
- Empêche les jobs d'export de réessayer indéfiniment les exports inexistants


--------------------------------------------

# 2021-09-30-01

Release [2021-09-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-30-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction du fonctionnement du champ "Pays" sous certaines anciennes version des navigateurs Edge et Safari (#6506)

### Mineurs

- Le message d'erreur en cas de pièce jointe trop volumineuse mentionne maintenant la taille maximale de la pièce jointe (#6499)
- Correction d'une faute d'orthographe dans une notification (#6505)
- Correction du fond de l'icône "Langue" (#6501)

### Technique

- Correction d'un échec aléatoire dans un test des transferts de dossier (#6497)
- Mise à jour de `nokogiri` vers la version 1.12.5


--------------------------------------------

# 2021-09-22-01

Release [2021-09-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : il est possible de transférer d'un coup tous ses dossiers à un autre usager (#6482)
- Administrateur : le routage d'une démarche peut être activé directement depuis l'interface d'administration, sans passer par le support (#6469)

### Mineurs

- Amélioration des messages d'erreurs, qui mentionnent maintenant la taille maximale des pièces justificatives (#6468)
- SuperAdmin: le lien SendInBlue n'est plus affiché lorsque SendInBlue est désactivé (#6472)

### Technique

- Un administrateur peut ajouter un jeton api particulier à une procédure
- Montre les sources disponibles pour un jeton api particulier
- Un administrateur peut sauvegarder ses sources API particulier
- Sauvegarde les scopes api particulier
- Ajout d'un linter pour vérifier que toutes les chaînes localisées sont traduites en français (#6470) et qu'aucune n'est inutilisée dans les autres langues (#6477)
- Correction d'une erreur lors de la ré-initialisation de mot de passe avec un jeton expiré (#6476)
- Suppression des anciennes constantes pour la rétro-compatibilité des Jobs (#6475)


--------------------------------------------

# 2021-09-14-01

Release [2021-09-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-14-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Permet à un utilisateur de transférer ses dossiers (#6359)

### Mineurs

- Administrateur : rétablissement de l'indicateur de complexité lors d'un changement de mot de passe (#6417)
- Ajout d'une variable d'environnement pour désactiver l'OTP des Super-Admin (#6454)

### Technique

- Correction de messages d'avertissement dans les tests
- i18n : l'interface technique de prévisualisation des emails permet de les afficher dans la bonne langue (#6459)


--------------------------------------------

# 2021-09-08-02

Release [2021-09-08-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-08-02)  sur GitHub

## Améliorations et correctifs



### Mineurs

- fix(i18n): add Kosovo to countries list


--------------------------------------------

# 2021-09-08-01

Release [2021-09-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-08-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Correction d'erreurs Javascript mineures
- i18n : correction des traductions manquantes dans l'email "Retrouvez votre brouillon"


--------------------------------------------

# 2021-09-07-03

Release [2021-09-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-07-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- GraphQL: ajout d'une option pour changer l'état d'un dossier sans envoyer d'email de notification à l'usager

### Mineurs

- Correction de la génération PDF de dossiers comportant des champs "Pays"

### Technique

- Ajoute un test a `dossier_projection_service` concernant l'`external_id`


--------------------------------------------

# 2021-09-07-02

Release [2021-09-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-07-02)  sur GitHub

## Améliorations et correctifs

### Technique

- delete features flag option : 'make_experts_notifiable'
- fix(dossier_projection_service): select external_id on champs


--------------------------------------------

# 2021-09-07-01

Release [2021-09-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-09-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Affiche la liste des pays en anglais
- i18n: ajout des traductions pour la page reset-link-sent
- feat(i18n): utilisation de la langue choisie par l'utilisateur pour lui envoyer des notifications

### Technique

- Mise à jour des dépendances Javascript (#6432)
- Correction d'une erreur lors d'un échec de l'enregistrement automatique d'un brouillon
- Suppression des anciens contrôleurs ancien-design dans `admin` (#6435)
- Mise à jour de Rails vers la version 6.1.4.1
- Dans une liste déroulante liée, on s'assure que la réinitialisation de la valeur primaire, réinitialise la valeur secondaire


--------------------------------------------

# 2021-08-27-01

Release [2021-08-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-27-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- Correction de l'affichage du texte de réinitialisation de mot de passe (#6413)
- Amélioration du sélecteur de langues (#6414)
- GraphQL: expose demarche descriptor on dossier type
- Send expiration notifications 2 weeks prior to supression instead of a month
- Translate devise related emails
- i18n: request new password translation
- Traduction de certains emails de notification (#6422)

### Technique
- Migration de données : suppression de la clef `'migrated'` dans ProcedurePresentation#filters (#6411)
- Correction d'un fichier de test dont les tests n'étaient pas exécutés (#6423)


--------------------------------------------

# 2021-08-24-01

Release [2021-08-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- Ajouter la possibilité de changer la locale via un paramètre dans l’URL


### Technique
- #6407 - Fix flash message typo in new_administrateur/groupe_instructeurs_controller and its related spec
- Mise à jour de `path-parse` de la version 1.0.6 à 1.0.7
- Remplacement de la gem dépréciée `axe-matchers` par `axe-core-spec` (#6394)


--------------------------------------------

# 2021-08-20-01

Release [2021-08-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Supprime les anciens  mails SendinBlue
- Internationalisation de la page *Commencer*
- fix(i18n): supprime l'espace avant la question mark en anglais
- Internationalisation de la création du compte
- Fixe la sauvegarde local Storage des Bulk Messages


--------------------------------------------

# 2021-08-19-01

Release [2021-08-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- corrige le composant combo-search: accepte désormais une valeur vide en entrée
- active la fonctionnalité dossiers termine expiration derrière un feature flab
- ajoute une colonne *Code insee* pour l'export d'un champ commune


--------------------------------------------

# 2021-08-18-01

Release [2021-08-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ usager, instructeur ou expert, je ne veux pas perdre le texte d'un message ou d'un avis


--------------------------------------------

# 2021-08-13-01

Release [2021-08-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-13-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- stats: précise légende et périodes considérées


--------------------------------------------

# 2021-08-12-01

Release [2021-08-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- archives: déplace dans le fichier zip le rapport d'incident
- statistiques : rend plus lisible le graphique *répartition par semaine*
- statistiques  : rend plus lisible le graphique *taux d'acceptation*


--------------------------------------------

# 2021-08-04-01

Release [2021-08-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Améliorations sur l'envoi des messages aux usagers en brouillon


--------------------------------------------

# 2021-08-03-01

Release [2021-08-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-08-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ajoute un rapport de bug dans l'archive d'une démarche

### Technique

- ajoute un jeton api particulier chiffré


--------------------------------------------

# 2021-07-29-01

Release [2021-07-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-29-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Debugging archive generation


--------------------------------------------

# 2021-07-28-01

Release [2021-07-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-28-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction des clés
- ETQ admin/instructeur, je veux envoyer un message à tous les usagers en brouillon ayant choisi un groupe instructeur


--------------------------------------------

# 2021-07-23-02

Release [2021-07-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-23-02)  sur GitHub

## Améliorations et correctifs


### Technique

- Revert "Suppression de la clef `"migrated": true` sur les filtres des ProcedurePresentation" (#6355)
- Corrige le trie dans la vue instructeur pour les démarches avec des revisions


--------------------------------------------

# 2021-07-23-01

Release [2021-07-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : en cas de déconnexion, l'Usager en train de remplir un formulaire est redirigé vers la page de connexion (#6360)
- Administrateur : correction de faux positifs dans l'avertissement de balises non utilisées (#6357)

### Technique

- Ajout de contraintes d'unicités supplémentaires sur Champs, DeletedDossiers et Etablissements (#6362)
- Suppression de la clef "migrated": true sur les filtres des ProcedurePresentation (#6355)
- Suppression d'anciennes tâches Rake (#6354)


--------------------------------------------

# 2021-07-20-03

Release [2021-07-20-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-20-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- i18n : internationalisation des pages de création et d'édition de dossiers

### Mineurs

- Get more results from communes API and use local matcher
- Instructeurs : limitation de la valeur d'un filtre à 100 caractères


--------------------------------------------

# 2021-07-20-02

Release [2021-07-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-20-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Correctif pour diminuer le nombre d'erreurs InvalidAuthenticityToken ("La requête a été rejetée") (#6332)


--------------------------------------------

# 2021-07-20-01

Release [2021-07-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de l'affichage des cases à cocher dans les listes multiples lorsque plusieurs options ont des valeurs similaires (#6334)


### Technique

- Mise à jour de `addressable` de la version 2.7.0 à 2.8.0 (#6337)
- Mise à jour du SDK de Sentry (#6339)


--------------------------------------------

# 2021-07-15-01

Release [2021-07-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Autorise le content type windows concernant les imports CSV


--------------------------------------------

# 2021-07-13-02

Release [2021-07-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-13-02)  sur GitHub

## Améliorations et correctifs

- Revert "Instructeurs : limitation de la valeur d'un filtre à 100 caractères" (#6341)


--------------------------------------------

# 2021-07-07-01

Release [2021-07-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajouter un point sur la carte en saisissant les coordonnées
- Un usager peut accéder aux statistiques d'une démarche close

### Technique

- Erreurs ActionController::InvalidAuthenticityToken : lorsqu'il n'y a pas de cookies, la page d'erreur par défaut est affichée (#6325)
- Correction de l'affichage de couches cadastrales


--------------------------------------------

# 2021-07-06-02

Release [2021-07-06-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-06-02)  sur GitHub

## Améliorations et correctifs


### Mineurs

- archives : ajoute un poids moyen minimum pour les procédures sans champ pièce justificative

### Technique

- Correction d'une erreur Javascript lors de l'auto-remplissage des menus déroulants (#6323)
- Amélioration du log des erreurs ActionController::InvalidAuthenticityToken (#6321)


--------------------------------------------

# 2021-07-06-01

Release [2021-07-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-06-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- Redesign et traduction de la page 'Erreur 422'
- ETQ Expert, je ne peux plus me connecter après avoir accepté une demande d'avis
- Ajouter la possibilité de cacher ou régler l'opacité des couches cartographiques IGN
- Ajouter la possibilité de tester les démarches avec les changements d'une révision
- Instructeur : désactivation temporaire de la génération d'une archive complète d'une démarche. L'archivage mensuel (en beta) reste activé. (#6317)
- Experts : l'onglet "Avis" est maintenant visible quel que soit le profil (#6315)


### Technique

- Refactor clone_attachments
- Remonte les exceptions des jobs dans Sentry


--------------------------------------------

# 2021-06-28-01

Release [2021-06-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-28-01)  sur GitHub

## Améliorations et correctifs


### Technique

- Fix Expose dossier PDF export as IO


--------------------------------------------

# 2021-06-24-03

Release [2021-06-24-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-24-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Revert "Expose dossier PDF export as IO"


--------------------------------------------

# 2021-06-24-02

Release [2021-06-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-24-02)  sur GitHub

## Améliorations et correctifs

### Technique

- archives : expose l'export pdf d'un dossier pour l'instructeur comme un IO


--------------------------------------------

# 2021-06-24-01

Release [2021-06-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-24-01)  sur GitHub

## Améliorations et correctifs



### Mineurs
- Improve procedure revisions
- [GraphQL] expose deleted dossiers


### Technique

- Mise à jour de `bindata` de la version 2.4.8 à 2.4.10 (#6297)

- Mise à jour de `delayed_job_web` de 1.4.3 à 1.4.4 (#6298)


--------------------------------------------

# 2021-06-23-02

Release [2021-06-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-23-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Enable publish revisions


--------------------------------------------

# 2021-06-23-01

Release [2021-06-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-23-01)  sur GitHub

## Améliorations et correctifs



### Mineurs


- archives: corrige une faute d'orthographe
- Renvoie un message d'erreur si les headers du fichier CSV ne sont pas au format attendu
- Permettre l’export des dossiers sur les 30 derniers jours

### Technique

- CI : mise à jour de l'action 'rebase' (#6283)
- README : mise à jour des instructions d'installation concernant `geos` (#6291)
- Utilisation des réglages par défaut de Rails pour configurer la protection contre les formulaires invalides (#6290)


--------------------------------------------

# 2021-06-18-01

Release [2021-06-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Expose sous forme de graphe l'évolution du temps de traitement d'une procédure pour les usagers et les instructeurs
- Compte les dossiers archivés parmi les dossiers présents dans les archives
- Ne met pas à jour un dossier après la génération de l'export pdf par un instructeur
- permet à l'instructeur de supprimer un dossier

### Technique

- Les URLs qui comportaient des adresses email dans le chemin insèrent maintenant l'email comme query-param (#6276)
- Mise à jour de `postcss` de la version 7.0.30 à 7.0.36
- Tests : correction de l'usage de `Timecop.freeze` dans les tests de Traitement (#6282)


--------------------------------------------

# 2021-06-16-01

Release [2021-06-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Administrateur, je peux ajouter des groupes instructeurs via un fichier CSV
- Rends possible la recherche des communes avec de multiples codes postaux


--------------------------------------------

# 2021-06-15-01

Release [2021-06-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-15-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Les numéros de téléphone entrés dans un dossier sont automatiquement formatés (par exemple via l'ajout d'espaces) (#6267)
- La valeur entrée dans un champ de sélection avec autocomplétion est automatiquement sélectionnée quand on soumet le formulaire (#6266)
- Instructeur / Administrateur : légères améliorations graphiques de l'interface (#6270)

### Technique

- [Rubocop] ETQ développeur, je souhaiterais pouvoir exploiter le plein potentiel de Ruby 2.7
- Mise à jour de `ws` de la version 6.2.1 à 6.2.2


--------------------------------------------

# 2021-06-09-01

Release [2021-06-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Fixe un bug dans le users/statistiques controller
- optimise la page qui liste les archives (toujours uniquement accessible via feature flag)


--------------------------------------------

# 2021-06-08-01

Release [2021-06-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- corrige un data type dans l'api education

### Technique

- optimise les stats du nb de dossiers termines par semaine


--------------------------------------------

# 2021-06-07-01

Release [2021-06-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Usager / Instructeur, je peux voir les stats de dossiers acceptés/refusés/sans-suite pour chaque démarche
- Préviens les doublons pour les dossiers déclaratifs


--------------------------------------------

# 2021-06-03-01

Release [2021-06-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-03-01)  sur GitHub

## Améliorations et correctifs





### Mineurs

- Manager : correction de l'affichage de la page Emails lorsque l'utilisateur n'est pas encore activé (#6248)

### Technique

- Mise à jour de highcharts de la version 8.1.1 à 9.0.0 (#6183)
- Handle GeoJSON validation errors
- GraphQL byte_size should be BigInt
- Fix publishing revisions


--------------------------------------------

# 2021-06-01-01

Release [2021-06-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction du format des apostrophes (#6234)
- SuperAdmin : ajout de traductions manquantes pour le nom du modèle User (#6244)
- SuperAdmin : correction de l'effacement de démarches supprimées (#6242)

### Technique

- Correction d'un test automatisé échouant aléatoirement (#6237)
- i18n : ajout d'un test pour le sélecteur de langues (#6231)
- Correction de l'environnement remonté par Sentry (#6239)
- Mise à jour de Sentry vers la version 4.4.2
- Mise à jour de dns-packet de la version 1.3.1 à 1.3.4


--------------------------------------------

# 2021-05-26-01

Release [2021-05-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-26-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

-  i18n : ajout d’un sélecteur de langue (désactivé par défaut) (#6209)
- Ajouter le champ description aux cadastres
- Rend les "tag" compatibles avec les revisions





### Technique

- Update dossier serialize query to stop using deprecated fileds
- Do not crash when properties is nil


--------------------------------------------

# 2021-05-25-01

Release [2021-05-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Use cadastre information contained in layers



### Technique

- Bump browserslist from 4.12.0 to 4.16.6
- Refactorage de la déclaration des balises HTML autorisées (#6199)
- verify avis privacy


--------------------------------------------

# 2021-05-21-01

Release [2021-05-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Améliore l'usage du composant multi select


--------------------------------------------

# 2021-05-20-02

Release [2021-05-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-20-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans la vue instructeur, n'affiche que les avis du dossier en question


--------------------------------------------

# 2021-05-20-01

Release [2021-05-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige un bug sur l'affichage d'avis confidentiel
- Corrige un bug sur le scroll de la messagerie (Fix messagerie scroll to last message)
- Accélère la recherche sur les dossiers en évitant une étape de stockage de dossiers_id

### Technique

- Bump nokogiri from 1.11.3 to 1.11.4
- Bump puma from 5.2.1 to 5.3.1


--------------------------------------------

# 2021-05-19-02

Release [2021-05-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-19-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- corrige le tableau de bord d'une procédure dans le cas ou un champ adresse est affiché


--------------------------------------------

# 2021-05-19-01

Release [2021-05-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige un bug d'affichage des dossiers supprimés pour les utilisateurs
- ETQ expert, je veux pouvoir trouver un dossier spécifique via la barre de recherche
- Corrige un mauvais affichage de champ dans le tableau de bord d'une procédure


--------------------------------------------

# 2021-05-11-02

Release [2021-05-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-11-02)  sur GitHub

## Améliorations et correctifs





### Technique

- Rename en_construction_conservation_extension to conservation_extension – part 2
- Dossier without user should be valid


--------------------------------------------

# 2021-05-11-01

Release [2021-05-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-11-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Mise à jour de la dépendance Javascript `url-parse` de 1.4.7 à 1.5.1
- Mise à jour de la dépendance Javascript `hosted-git-info` de la version 2.8.8 à 2.8.9
- Mise à jour de la dépendance Javascript `lodash` de 4.17.19 à 4.17.21
- Rename en_construction_conservation_extension to conservation_extension

### ℹ️ Notes de déploiement

Cette version contient une migration de base de données.


--------------------------------------------

# 2021-05-10-01

Release [2021-05-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-10-01)  sur GitHub

## Améliorations et correctifs

### Technique

- #6187 corrige un text API Entreprise qui échouait de manière aléatoire
- corrige une traduction manquante pour l'état d'un dossier dans les exports csv/excel


--------------------------------------------

# 2021-05-07-01

Release [2021-05-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-07-01)  sur GitHub

## Améliorations et correctifs

### Technique

- SuperAdmin : correction de l'affichage de la page `/manager/dossiers`
- Suppression des tâches de déploiement `after_party` les plus anciennes (#6177)
- Corrige une configuration dans le smtp de dev
- mise à jour d'administrate


--------------------------------------------

# 2021-05-06-01

Release [2021-05-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : petites améliorations de l'interface du tableau de bord d'une démarche (#6135)
- SuperAdmin : réparation du manager après la mise à jour à Rails 6.1.3.2 (#6178)

### Technique

- Permet d'utiliser HELO comme serveur SMTP en développement local
- API Éducation : corrige le lien annuaire lorsque le SIREN n'est pas connu


--------------------------------------------

# 2021-05-05-02

Release [2021-05-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-05-02)  sur GitHub

## Améliorations et correctifs

### Technique

- bump to rails 6.1.3.2 (patch secu)


--------------------------------------------

# 2021-05-05-01

Release [2021-05-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : corrige une duplication des dossiers dans le tableau d'une démarche (#6169)

### Technique

-  rajoute des contraintes manquantes à la table france_connect_informations (#6084)
- activate rack_mini_profiler in dev


### Notes de déploiement

ℹ️ Cette version contient une migration du schéma de base de données, qui ajoutent un index unique à france_connect_informations/user_id.


--------------------------------------------

# 2021-05-04-01

Release [2021-05-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- active l'extension de la conservation des brouillons
- active la destruction des comptes utilisateurs
- prend en compte uniquement les pj pour estimer la taille d'un dossier
- Revert "Merge pull request #6142 from tchak/enable_brouillon_extend_conservation"

### Technique

- Mise à jour de la dépendance rexml de 3.2.4 à 3.2.5 (#6160)
- Jobs : ajout de Excon::Error::Socket à la liste des erreurs ré-essayées automatiquement (#6163)
- Correction d'un test de l'API GraphQL (#6162)


--------------------------------------------

# 2021-04-29-03

Release [2021-04-29-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-29-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- exporte tous les dossiers d'une démarche (et corrige le problème de performance)


--------------------------------------------

# 2021-04-29-02

Release [2021-04-29-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-29-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Revert "Export de tous les dossier d'une démarche" pour cause de problèmes de performance (#6155)

### Technique

- Les jobs ActiveStorage et ApplicationJob ré-essaient automatiquement plus tard en cas d'erreur réseau (#6153)
- Remplacement de fontawesome par heroicons


--------------------------------------------

# 2021-04-29-01

Release [2021-04-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- ameloration des perfs du tableau de bord instructeur (#6131)

### Technique
- Update and pin mapbox-gl  (#6096)


--------------------------------------------

# 2021-04-28-01

Release [2021-04-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- archive tous les dossiers d'une démarche (pour les démarches de moins d'1 Go)

### Mineurs

- Amélioration des styles pour les sélecteurs de valeurs multiples


--------------------------------------------

# 2021-04-27-02

Release [2021-04-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-27-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : la page d'un dossier s'affiche plus rapidement (#6134)
- Page "Lien de réinitialisation du mot de passe envoyé" : ajout d'espaces insécables (#6124)
- Page "Lien de connexion envoyé" : correction d'un texte manquant
- Suppression des boutons de Feedback (rouge/orange/vert)

### Technique

- Suppression du lien entre un commentaire et l'utilisateur
- Ajout d'un message de déboguage en cas de token invalide dans Users::SessionsController (#6129)
- Ajout de contraintes d'unicité sur les tables Invites, Procedures et Individuals (#6136)

### Notes de déploiement

ℹ️ Cette version contient trois migrations du schéma de base de données, qui ajoutent des index uniques à certaines colonnes.


--------------------------------------------

# 2021-04-22-01

Release [2021-04-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-22-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de l'interface de ré-inititialisation de mot de passe (#6106)
- Usager : les communes comportant de nombreux noms similaires (ex. Beaulieu, Ste-Marie) peuvent maintenant être sélectionnés
- Instructeur : améliore le temps de réponse de la vue d'une démarche par un instructeur
- Administrateur : le tableau de bord d'une démarche affiche maintenant le n° de la démarche, ainsi que le lien public permettant de commencer la démarche

### Technique

- spec: correction d'un test aléatoire sur les experts
- Dépendances : mise à jour de ssri de 6.0.1 à 6.0.2 (#6115)
- Correction d'un message d'avertissement "Initialization autoloaded the constants ActionText::ContentHelper"
- Ajoute un index sur la colonne etablissement_id de la table exercices

:information_source: Cette version contient une création d'index


--------------------------------------------

# 2021-04-19-01

Release [2021-04-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-19-01)  sur GitHub

## Améliorations et correctifs


### Majeurs

- Administrateur : possibilité de gérer une liste d'experts pré-définie par démarche


--------------------------------------------

# 2021-04-16-01

Release [2021-04-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction du lien vers la documentation de l'API (#6103)

### Technique

- Développeur : documente l'option de rspec pour ne faire passer que les tests échoués précédemment
- CI : teste automatiquement la conformité des classes avec Zeitwerk (#6101)
- Mise à jour de la gem `devise-two-factor` (#6104)
- Users that are experts should not be deletable
- amélioration des temps de réponse instructeur procedure show


--------------------------------------------

# 2021-04-15-01

Release [2021-04-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-15-01)  sur GitHub

## Améliorations et correctifs

### Majeures

- Expert : les experts notifiables reçoivent un email leur indiquant la décision rendue
- Manager : affichage et suppression des SuperAdmin

### Technique

- Correction des données liées aux dossiers qui doivent être effacées lors de la suppression (#6091)
- Ajout d'utilitaires pour faciliter l'écriture de migrations de base de données (#6058)
- Les colonnes supprimées ne sont plus mentionnées dans le code (#6074)
- #6092 - Test: use APPLICATION_NAME to deal nicely with custom configurati


--------------------------------------------

# 2021-04-13-01

Release [2021-04-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-13-01)  sur GitHub

## Améliorations et correctifs

### Mineur

- Instructeur : le tableau de bord d'une démarche s'affiche plus rapidement
- SuperAdmin : correction de l'affichage des emails envoyés (#6076)
- SuperAdmin : en plus des emails envoyés par Sendinblue, ceux envoyés par Mailjet sont également affiché (#6076)

### Technique

- N'utilise plus flipper pour passer outre le login par jeton (1er partie)
- Remplissage des dates manquantes dans `france_connect_informations` (#6084)
- L'email de demande d'avis n'est plus envoyé si le dossier a été supprimé

### ℹ️ Notes de déploiement

Cette release contient :
- deux migrations du schéma de la base de données,
- une moulinette de migration des données (redressage des données `france_connect_information`).

Attention : sur les bases Postgres < 11, la migration de base de donnée peut potentiellement être lente.


--------------------------------------------

# 2021-04-09-01

Release [2021-04-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-09-01)  sur GitHub

## Améliorations et correctifs

### Technique

- S'assure que les adresses to_s ne retournent jamais `nil`
- Suppression de la colonne obsolete `procedure_id` de la table `assign_to`


--------------------------------------------

# 2021-04-08-01

Release [2021-04-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-08-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Améliorer le menu des exports


### Technique

- Remove unused columns


--------------------------------------------

# 2021-04-07-03

Release [2021-04-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-07-03)  sur GitHub

## Améliorations et correctifs


### Technique

- Maybe fix notifications


--------------------------------------------

# 2021-04-07-02

Release [2021-04-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-07-02)  sur GitHub

## Améliorations et correctifs


### Technique

- Try to fix orange badge hell


--------------------------------------------

# 2021-04-07-01

Release [2021-04-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-07-01)  sur GitHub

## Améliorations et correctifs



### Mineurs


- Améliorations d'accessibilité (icône pour les liens externes, statut d'accessibilité) (#6054)
- Marque les fichiers qui ont un pb d'intégrité comme corrompus et prévient l'utilisateur


### Technique
- Diminue l'impact de flipper sur les temps de réponse
- Prevent crash in preview where there is no siblings
- `ExpertsProceduresController` utilise maintenant les actions par défaut de Rails (#6056)


--------------------------------------------

# 2021-04-06-02

Release [2021-04-06-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-06-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : correction du numérotage des sections de formulaire (#6049)

### Technique

- Ajout d'une variable d'environnement `ACCESSIBILITE_URL` (optionnelle)
- Mise à jour de la configuration Rails pour utiliser les réglages par défaut de Rails 6.1 (#6036)


--------------------------------------------

# 2021-04-06-01

Release [2021-04-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ Admin, je peux ajouter/supprimer des experts notifiables

### Technique

- Migration : ajout d'une contrainte 'NOT NULL' sur la colonne exports#key
- Nettoyage de la relation inverse Dossier ↔︎ Champs (#6045)

### ℹ️ Notes de déploiement

Cette version contient une migration de base de données.


--------------------------------------------

# 2021-04-02-01

Release [2021-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-02-01)  sur GitHub

## Améliorations et correctifs


### Mineurs


- Mise à jour de la déclaration d'accessibilité (#6039)
- Améliore le rendu des dossiers en PDF

### Technique

- Mise à jour de y18n de 4.0.0 à 4.0.1
- Maj de rails 6.3.1 et calcxls pour enlever mimemagic dep, maj skylight
- Simplify export unicity check and use create_or_find_by
- Correction de modifications mineures oubliées lors de mises à jour précédentes de la base de donnée (#6034)
- Base de donnée : ajout d'une contrainte 'not null' sur la colonne `service_name` de la table `active_storage_blobs` (#6035)

### ℹ️ Notes de déploiement

Cette PR retire la dépendance à système à shared-mime-info sur les linux du à mimemagic.


--------------------------------------------

# 2021-03-29-02

Release [2021-03-29-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-29-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Répare le déploiement (maj mimemagic et test du fichier i_am_a_webserver)

### ℹ️ Notes de déploiement

Cette PR ajoute une nouvelle dépendance à système à shared-mime-info sur les linux du à un changement au niveau de mimemagic (https://github.com/mimemagicrb/mimemagic#dependencies et https://github.com/rails/rails/issues/41750)

mimemagic n'est plus utilisé dans la release rails suivante (https://github.com/rails/rails/releases/tag/v6.1.3.1) mais encore par une autre gem `caxlsx`, donc l'installation de `shared-mime-info` reste nécessaire pour l'instant.

On pourrait retirer cette dépendance avec rails >= 6.1.3.1 et caxls >= 3.1.0 .

Par ailleurs, pour ceux qui utilisent mina pour déployer l'app, le redémarrage des puma est maintenant conditionné à la présence d'un fichier `i_am_a_webserver` dans le répertoire shared 

(https://github.com/betagouv/demarches-simplifiees.fr/blob/edbb5c325d4a90f6b9716b55f3b85a69e1b9f806/config/deploy.rb#L7)


--------------------------------------------

# 2021-03-29-01

Release [2021-03-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : amélioration des performances de l'export des dossiers (#6022)
- Ajoute l'onglet avis sur l'interface instructeur


--------------------------------------------

# 2021-03-25-03

Release [2021-03-25-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-25-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Passage à Rails 6.1 (#5955)
- Correction de la migration ActiveStorage incluse dans le passage à Rails 6.1 (#6025)
- CI : envoie le tag de version à Sentry (au lieu du SHA1) (#6020)

### ℹ️ Notes de déploiement

Cette PR inclut des migrations de base de données.

Sur les bases ayant un grand nombre de données concernant les pièces jointes (ActiveStorage::Blob), ces migrations peuvent prendre plusieurs dizaines de minutes à s'exécuter. Le site reste néanmoins accessible pendant la durée des migrations.


--------------------------------------------

# 2021-03-25-01

Release [2021-03-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usagers : traduction de la page `/users/sign_in` en anglais (derrière un feature-flag) (#5991)

### Technique

- Nettoyage du code de migration des Experts (#6002)


--------------------------------------------

# 2021-03-24-02

Release [2021-03-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-24-02)  sur GitHub

## Améliorations et correctifs


### Technique

- Ajout d'une variable d'environnement `DS_ENV` pour détecter si l'environnement courant est `staging`


--------------------------------------------

# 2021-03-24-01

Release [2021-03-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : correction du message d'avertissement lorsqu'un champ d'attestation est manquant (#6010)
- Export PDF : les valeurs des champs sont maintenant affichées sur deux colonnes (#6011)
- Tâche `after_party` qui remplit les `experts_procedure_id` de la table avis `nil`


--------------------------------------------

# 2021-03-23-01

Release [2021-03-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- Instructeur : les champs non-exportables sont correctement exclus de l'export (corrige l'export de certaines démarches) (#6007)

### Technique

- Ajout d'un validateur pour rendre obligatoire la relation ExpertsProcedure


--------------------------------------------

# 2021-03-22-01

Release [2021-03-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ajout d'un profil "Expert". Les personnes externes à qui un Instructeur demande un Avis sont maintenant enregistrées non plus comme un Instructeur, mais comme un Expert (#5937)

### Mineurs

- Usager : un champ "Liste déroulante à choix multiple" est correctement détecté comme vide si aucun élément n'a été ajouté (#5997)


### Technique

- Utilisation de Bootsnap pour accélérer le démarrage de l'application Rails
- Nettoyage : suppression d'un mailer inutilisé

### ⚠️ Notes de déploiement

La moulinette after_party qui convertit les Instructeurs en Experts ne prend pas en compte certains comptes qui devraient pourtant être convertis. La moulinette se termine avec succès, mais certaines données ne sont pas migrées. un correctif supplémentaire sera disponible dans la prochaine version.


--------------------------------------------

# 2021-03-18-01

Release [2021-03-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-18-01)  sur GitHub

## Améliorations et correctifs



### Mineurs
- Usager : le champ Pièce justificative est maintenant accessible
- Instructeur : affichage de plus d’informations sur les champs Adresse, Commune et Région (#5993)
- Instructeur : ajoute le PDF du dossier à l'export des pièces justificatives
- API : ajout de filtres pour récupérer les révisions des dossiers

### Technique

- Return empty strings for incomplete api entreprise adresses
- Sentry : dans les jobs, ignore les exceptions que l'on gère déjà manuellement (#5994)
- After_party : correction de la tâche `remove_invalid_geometries after_party`, qui échouait après une heure (#5989)


--------------------------------------------

# 2021-03-16-01

Release [2021-03-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-16-01)  sur GitHub

## Améliorations et correctifs

### Technique

- config: avoid blocking legitimate requests from mobile ISPs
- Jobs : lorsqu'une erreur ActiveStorage::IntegrityError se produit pendant le scan des virus, le job est maintenant automatiquement retenté plus tard.
- Jobs : refactor de la méthode utilisée pour déclencher les jobs de scan anti-virus et de filigranage (#5979)
- Ajout de tests automatisés pour la validation des polygones GeoJSON (#5984)
- API : rend optionnelle la présence du n° de TVA intra-communautaire


--------------------------------------------

# 2021-03-11-01

Release [2021-03-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-11-01)  sur GitHub

## Améliorations et correctifs




### Technique
- capital_social and numero_tva_intracommunautaire can be null
- Try to reduce the number of external data fetches
- Fix invalid GeoJSON handling


--------------------------------------------

# 2021-03-05-01

Release [2021-03-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-05-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Corrige la transpilation mapbox-gl
- Suppression de nouveaux messages d'avertissements pendant les tests et lors de l'exécution de commandes `bin/rails` (#5960)
- Corrige la date limite pour la validation des notices des procedures
- Rend les associations des champs plus "nullable"


--------------------------------------------

# 2021-03-04-03

Release [2021-03-04-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-04-03)  sur GitHub

## Améliorations et correctifs



### Technique
- Pas de validation pour les anciennes démarches
- GraphQL : corrige la date de création des associations
- ActiveStorage : correction de la manière dont sont déclenchés les jobs externes (anti-virus, filigranage) (#5953)


--------------------------------------------

# 2021-03-04-02

Release [2021-03-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-04-02)  sur GitHub

## Améliorations et correctifs





### Technique

- Some addresses have no street_address
- When procedure is reset delete only draft revision dossiers


--------------------------------------------

# 2021-03-04-01

Release [2021-03-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-03-04-01)  sur GitHub

## Améliorations et correctifs



### Mineurs
- Implementer le feature flag pour permettre le routage via l'API
- Le groupe instructeur par défaut est déterminé par l'ordre alphabétique




### Technique

- specs: fix test failing on March 1st
- Operation serializer
- Fix JSON.parse crashing on empty values
- Add proper external data API errors handling
- procedure: fix query for finding champs without stable_id


--------------------------------------------

# 2021-02-25-02

Release [2021-02-25-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-25-02)  sur GitHub

## Améliorations et correctifs


### Technique
- remove unused print.css reference


--------------------------------------------

# 2021-02-25-01

Release [2021-02-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-25-01)  sur GitHub

## Améliorations et correctifs


### Technique

- specs: fix rspec warning about raise_error specificity
- Suppression d'un avertissement zeitwerk au démarrage de l'app en environnement de développement local (#5921)
- Add github actions CI
- Nettoyage : déplacement de la configuration du nom de l'app dans un initialiseur (#5931)
- CI : séparation des groupes de tests en trois
- Mise à jour vers le processeur d'assets Sprockets v4 (#5932)
- Correction de messages d'avertissement liés à Zeitwerk (#5936)
- Suppression et mise à jour de gems
- Try to parse SIB email date when it is a string


--------------------------------------------

# 2021-02-19-03

Release [2021-02-19-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-19-03)  sur GitHub

## Améliorations et correctifs



### Technique

- Fix multiselect labels


--------------------------------------------

# 2021-02-19-02

Release [2021-02-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-19-02)  sur GitHub

## Améliorations et correctifs



### Technique

- fix constant


--------------------------------------------

# 2021-02-19-01

Release [2021-02-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Structured address

### Mineurs

- 5911 allonge conservation dossiers
- Improuve multi select


--------------------------------------------

# 2021-02-17-01

Release [2021-02-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- Amelioration de l'accessibilité des champs multi-select

### Technique

- README : mise à jour des informations techniques sur le déploiement de l'application
- CI : mise à jour de l'action Github utilisée pour publier les releases sur Sentry
- GraphQL : modification de l'annotation
- Sentry : ajustement du taux d'échantillonnage des traces
- préparation du mécanisme pour rendre les instructeurs notifiables (ou non) de la décision finale sur un dossier (#5904)
- Ne cache pas les classes durant les tests
- Mise à jour des dépendances Ruby
- Mise à jour de react
- Suppression de jquery et select2
- Suppression des chemins non utilisés dans la configuration de l'assets pipeline


--------------------------------------------

# 2021-02-11-02

Release [2021-02-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-11-02)  sur GitHub

## Technique

- Utilisation de zeitwerk, l'auto-loader de Rails 6 (#5887, #5899)

## ⚠️ Notes de déploiement

Si votre fork a rajouté des classes Ruby au code, vérifiez que ces classes sont nommées d'une façon compatible avec zeitwerk en lançant `bin/rails zeitwerk:check` (voir #5887).


--------------------------------------------

# 2021-02-11-01

Release [2021-02-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Il n'est plus possible pour les Administrateurs et les Instructeurs de se connecter par France Connect. Au moment de la connexion, l'administrateur ou l'instructeur sera invité à créer un mot de passe sécurisé à la place.

  _La raison est que le niveau de sécurité des différents fournisseurs France Connect est assez variable, et ne respecte pas forcément le niveau de sécurisation que nous demandons pour les Instructeurs et les Administrateurs._

### Mineurs

- SuperAdmin : correction de la génération initiale du code OTP (#5897)


--------------------------------------------

# 2021-02-10-03

Release [2021-02-10-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-10-03)  sur GitHub

## Améliorations et correctifs





### Technique

- Update sentry


--------------------------------------------

# 2021-02-10-02

Release [2021-02-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-10-02)  sur GitHub

## Améliorations et correctifs


### Technique

- Add sentry traces (20% for now)
- fix print css


--------------------------------------------

# 2021-02-10-01

Release [2021-02-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-02-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Statistiques : correction de l'affichage visuel du nombre de dossiers (#5879)

### Technique

- Merge pull request #5881 from betagouv/ruby-2.7-keyword-arguments
- Permet au logo dans le pied-de-page des emails d'être configuré par une variable d'environnement (#5874)
- Tests : correction d'un avertissement Rspec sur l'assertion des exceptions (#5880)
- Renommage des constantes pour Zeitwerk
- Fix unstable tests
- Add throttling for api entreprise


--------------------------------------------

# 2021-01-27-02

Release [2021-01-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-27-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Make siren_siret on annuaire education optional
- Stop crashing if attachment can not be watermarked
- Stop crashing if a selection utilisateur contains no geometry
- Stop crashing when removed repetition row is submitted


--------------------------------------------

# 2021-01-20-01

Release [2021-01-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-20-01)  sur GitHub

## Améliorations et correctifs

### Technique

- s'assure qu'une erreur lors d'une fermeture d'une procedure n'affecte pas la fermeture des autres.
- tache technique pour importer les instructeurs avec un csv


--------------------------------------------

# 2021-01-18-02

Release [2021-01-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-18-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Restreint les content type autorisés des pièces jointes


--------------------------------------------

# 2021-01-18-01

Release [2021-01-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-18-01)  sur GitHub

## Améliorations et correctifs

### Technique

- ajoute une tache after party pour ignorer la validation du content type validation pour les vieilles pj
- Bump nokogiri from 1.10.10 to 1.11.1


--------------------------------------------

# 2021-01-15-01

Release [2021-01-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-15-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Annuaire Education

### Mineurs

- Add DateTime champ to GraphQL

### Technique

- Improuve annuaire education


--------------------------------------------

# 2021-01-12-01

Release [2021-01-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de l'interface des buttons multiples

### Technique

- Fix type_de_champ repetition revision after clone
- Fix a crash when champ carte has no options


--------------------------------------------

# 2021-01-04-02

Release [2021-01-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-04-02)  sur GitHub

## Améliorations et correctifs

### Technique

- ajoute les images comme content type valide pour les notices et les deliberations


--------------------------------------------

# 2021-01-04-01

Release [2021-01-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- GraphQL: Retour des erreurs d'API au format JSON
- [GraphQL] Cacher les dossiers brouillons dans l'API
- ETQ Administrateur, je peux voir la liste des experts invités sur tous les dossiers de ma démarche


--------------------------------------------

# 2020-12-18-01

Release [2020-12-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Enable filters across revisions
- Add titre identite champ to GraphQL

### Technique

- Add cron to complete missing analysis
- Fix stable filters migration


--------------------------------------------

# 2020-12-16-01

Release [2020-12-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-16-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- feat/5720 - un instructeur peut supprimer un dossier terminé

### Mineurs

- #5746 force une sécurité élevé du password super admin
- l'export des pièces jointes d'un dossier comprend a présent les attestations, justificatifs, operation_logs et certificat d'horodatage

### Technique

- #5799 - Fix(form /contact-admin): use APPLICATION_NAME
- #5801 - Allow configuration of the URLs of FAQ and documentation websites in .env file
- GraphQL: make parts of address nullable


--------------------------------------------

# 2020-12-14-01

Release [2020-12-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-14-01)  sur GitHub

## Améliorations et correctifs

### Technique

- #5793 - Fix(.gitignore): exclude /public/assets directory
- #5795 - Allow default logo of a procedure to be configured in .env file
- nouvel essai pour réparer l'horodatage
- #5797 - Form add/edit a service: use APPLICATION_NAME


--------------------------------------------

# 2020-12-11-03

Release [2020-12-11-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-11-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Répare l'horodatage


--------------------------------------------

# 2020-12-11-02

Release [2020-12-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-11-02)  sur GitHub

## Améliorations et correctifs

### Technique

- réparer les erreurs de cadastre dans le module cartographique
- rollback des montés de version des dépendances js suite à un problème d'empreinte des assets.


--------------------------------------------

# 2020-12-11-01

Release [2020-12-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ajoute dans la cartographie les couches Mnhn

### Mineurs

- présente la civilité par ordre alphabétique
- Redémarrage de delayed_job seulement pour les workers
- Supprime les titres identite une fois un dossier traité
- Affiche les titre d'identité qu'une fois le filigrane apposé.

### Technique

- #5752 - Doc: add DEMANDE_INSCRIPTION_ADMIN_PAGE_URL to env.example.optional file
- #5764 - Allow logos (mail, webapp) and favicons to be configured in .env file
- Update js dependencies
- Appelle l'API entreprise avec le token en Header
- Better graphql mutation error messages
- Bump ini from 1.3.5 to 1.3.7
- Répare et test la gestion des erreurs de fichier lors d'upload par API


--------------------------------------------

# 2020-12-08-02

Release [2020-12-08-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-08-02)  sur GitHub

#!! Suite au renommage des jobs cron, il faut supprimer ceux qui tournent avant la MEP et les relancer après avec les nouveau nom

## Améliorations et correctifs

### Technique

- répare des cron mal namespacé


--------------------------------------------

# 2020-12-08-01

Release [2020-12-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-08-01)  sur GitHub

#!! Attention ne pas prendre cette version mais la suivante, suite a un pb de namespace

## Améliorations et correctifs

### Mineurs

- Rend le champ pays accessible

### Technique

- Change les conditions de réessais sur api entrepirse
- change l'émtteur 2FA pour les environnements local et de staging
- déplace tous les crons job dans leur propre répertoire


--------------------------------------------

# 2020-12-02-01

Release [2020-12-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-12-02-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ajoute un filigrane sur les documents d'identité

### Mineurs

- rend plus compréhensible le mail quotidien des nouveautés aux insructeurs

### Technique

- moins d'erreur api entreprise (#5675 for all api entreprise job, retry on 502, 503)
- Corrige les seeds


--------------------------------------------

# 2020-11-25-01

Release [2020-11-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-11-25-01)  sur GitHub

## Améliorations et correctifs

### Technique

- #5754 - Allow CGU URL and Legal Notice URL to be configured in .env file
- [GraphQL] fix attachment upload


--------------------------------------------

# 2020-11-18-01

Release [2020-11-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-11-18-01)  sur GitHub

## Améliorations et correctifs





## Technique

- [GraphQL] add better errors when attachments are not properly used
- Remove foreign key dossier  on dossier operations logs
- add specific totp label for dev env


--------------------------------------------

# 2020-10-30-02

Release [2020-10-30-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-30-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Accelère le temps de chargement sur la page d'accueil
- Accelère le temps de chargement sur la vue instructeur
- Cherche a télécharger l'attestation sociale tous les jours pendant 5 jours
- fix / Modifie la description dans le champ titre identité

## Technique

- Supprime la GEM rack_mini_profiler de l'environnement de production


--------------------------------------------

# 2020-10-23-01

Release [2020-10-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-23-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Fixe les CSP pour bootstrap et Jquery


--------------------------------------------

# 2020-10-15-02

Release [2020-10-15-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-15-02)  sur GitHub

## Améliorations et correctifs





## Technique

- replace deprecated react-loadable with suspense
- Mount react components on page updates


--------------------------------------------

# 2020-10-15-01

Release [2020-10-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-15-01)  sur GitHub

## Améliorations et correctifs

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5664 Montée de version puma 5.0.2


--------------------------------------------

# 2020-10-12-01

Release [2020-10-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Revert "feat/5635 - Supprime la possibilité pour l'expert invité d'envoyer un message"


--------------------------------------------

# 2020-10-07-01

Release [2020-10-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration des performances de la pages de statistiques
- [GraphQL] expose dossier pdf and attestation
- Informe l'administrateur si la procédure a un jeton spécifique
- ETQ administrateur, je peux modifier un service depuis une démarche


--------------------------------------------

# 2020-10-06-01

Release [2020-10-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-10-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Il est possible de refuser des avis externes sur une démarche
- Cartographie : Fixe les calculs sur les geo areas
- Ajoute un feature flag pour qu'un expert ne puisse pas inviter un autre
- Fixe la longueur de l'url de la démarche


--------------------------------------------

# 2020-09-30-02

Release [2020-09-30-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-30-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5643 Correction pour permettre à un administrateur d'envoyer une copie de démarche à un autre administrateur
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5547 Les exports contiennent les types_de_champ de toutes les revisions
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5627 Réduction de la taille de police dans les attestations au long footer
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5652 Correction des problèmes d'upload de champs Pièce justificative dans les champs répétition

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5650 Renvoi de l'erreur de parsing lors des appels API GraphQL incorrects


--------------------------------------------

# 2020-09-30-01

Release [2020-09-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un * pour le second menu du champ déroulants liés lorsqu'il est obligatoire
- Correction des noms de feuilles excel - generation de noms valides
- Correction du lien vers le choix du service
- Correction de bugs de deserialization dans certains envois d'email

## Technique

- montées de versions de gems:
  - Bump flipper from 0.18.0 to 0.19.0
  - Bump gon from 6.3.2 to 6.4.0
  - Bump webpacker from 5.1.1 to 5.2.1


--------------------------------------------

# 2020-09-29-01

Release [2020-09-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-29-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- #4926 Réactivation des validateurs sur la taille des pièces justificatives
- Corrections de plusieurs bugs liés aux exports:
  - Correction d'un bug dans la génération des rapports de statistiques pour certains administrateurs
  - Correction des erreurs ApiEntreprise pour les etablissements manquants
  - Temps de conservation des exports passé à 1h
  - Suppression d'une boucle dans la génération des exports

## Technique

- montées de versions de gems:
  - Bump rubocop -> 0.92.0
  - Bump browser from 4.2.0 to 5.0.0
  - Bump geocoder from 1.6.2 to 1.6.3
  - Bump jwt from 2.2.1 to 2.2.2


--------------------------------------------

# 2020-09-25-01

Release [2020-09-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5613 Redirection de la création de comptes vers le formulaire DEMANDE_INSCRIPTION_ADMIN_PAGE_URL
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5614 Limitation de la longueur des noms de feuilles (qui empêchait la génération de certains exports)

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5623 Fix pour les exports manquants (ActiveRecord::RecordNotFound Couldn't find Export) lors de leur création
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5617 Mise en place de `dependabot`
- https://github.com/betagouv/demarches-simplifiees.fr/pull/5621 Montée de version graphql-batch 0.4.2 -> 0.4.3


--------------------------------------------

# 2020-09-23-01

Release [2020-09-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajoute la possibilité de jouer les jobs dans leur propre queue
- Ajout du type de champ Iban
- Supprime la page demandes/new
- Corrige des bugs au niveau des géométries


--------------------------------------------

# 2020-09-22-01

Release [2020-09-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-22-01)  sur GitHub

## Améliorations et correctifs



### Mineurs
- #5600 - Fix GraphQL entreprise nil values
- [GraphQL] Add archiver mutation
- add aria disclosure for contact page


### Technique
- Do not enqueue web hooks for empty urls
- Fix geometry errors
- procedure.dossiers through revisions
- Remove TypeDeChamp.to_stable_id


--------------------------------------------

# 2020-09-21-01

Release [2020-09-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5585 − Ajout du champ de formulaire "titre d'identité"

### Mineurs

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5583 − Modification de la création de comptes administrateurs pour qu'ils aient nécessairement des mots de passe solides

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/5589 − Plusieurs montées de versions de gems


--------------------------------------------

# 2020-09-18-01

Release [2020-09-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Redirections pour les routes dépréciées de l'éditeur de démarche

## Technique

- Montées de versions de gems
- #5571 - Nettoyage de portes `Flipper` inutilisées


--------------------------------------------

# 2020-09-16-01

Release [2020-09-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-16-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Cartographie : utilisation de la V2 des plans IGN et ajout des sources MNHN
- Rendre accessible les menus déroulants
- Interface administrateur : page de publication d'une démarche migrée vers la nouvelle interface

## Technique

-  Update rails


--------------------------------------------

# 2020-09-14-01

Release [2020-09-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-14-01)  sur GitHub

## Améliorations et correctifs

## Technique

- passage à ruby 2.7.1


--------------------------------------------

# 2020-09-11-01

Release [2020-09-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- autorise les numéros de téléphone polynésien
- #5529 Replacement des tabulations par des espaces dans l’attestation
- Amélioration de la perf de la page d'accueil
- Améliorations des performances du tableau de bord instructeur
- Suppression du lien contactez nous depuis la page d'administration

## Technique

- build(deps): bump http-proxy from 1.18.0 to 1.18.1
- Add a missing test on job retry to the excon err
- Mise a jour des docs Graphql


--------------------------------------------

# 2020-09-08-02

Release [2020-09-08-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-08-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Expose revisions sur GraphQL API
- Met à jour le tableau de bord instructeur pour améliorer les performances

## Technique

- variabilise le message de bannière
- Fix uninitialized excon constant


--------------------------------------------

# 2020-09-04-01

Release [2020-09-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Suppression des doublons d'annotations privées


--------------------------------------------

# 2020-09-03-01

Release [2020-09-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-09-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : traduction des boutons d'édition de la carte en français
- Administrateur : migration de la page de configuration des emails à la nouvelle interface
- Administrateur : correction de l'affichage du logo des démarches sous Safari

### Technique

- Révisions : simplification et nettoyage du code
- Reprise silencieuse des jobs en erreur dûe à un problème réseau
- Internationalisation : traduction de la page /contact-admin en anglais
- Internationalisation : Nettoyage des fichiers de localisation
- Internationalisation : Adoption de la gem devise-i18n lorsque c'est possible
- Intégration continue :  ajout d'un workflow GitHub Actions pour publier les releases sur Sentry
- Mailers : utilisation de la variable APPLICATION_NAME dans la signature
- Utilisation de APPLICATION_NAME dans views/users/sessions/new
- Ajout d'une variable d'environnement optionnelle `FRANCE_CONNECT_ENABLED` pour activer ou non France Connect
- Usager : lorsque qu'une barre qui affiche la progression de l'envoi d'un fichier disparait de la page, la Console ne remonte plus d'erreurs Javascript
- Jobs: lors de l'analyse antivirus, ignore les fichiers supprimés


--------------------------------------------

# 2020-08-27-01

Release [2020-08-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : correction d'un problème où décocher toutes les cases d'une liste n'enregistrait pas les changements.
- Localisation : quand le feature-flag `localization` est activé, la langue par défaut est choisie en fonction des préférences exprimées par le navigateur.

## Technique

- Mise en place de l'infrastructure de localisation de la page `/contact`.
- Documentation : ajout d'un schéma de la base de données en PDF.
- Suppression d'un commentaire à propos d'un refactoring possible de l'intégration ActiveStorage.


--------------------------------------------

# 2020-08-26-01

Release [2020-08-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : un champ radio optionnel peut maintenant être dé-sélectionné
- Usager : correction d'erreurs lors de l'enregistrement automatique d'un dossier en brouillon après la suppression d'un bloc répétable
- Super-admin : ajout de termes médicaux au détecteur automatique de données sensibles

## Technique

- build(deps): bump highcharts from 8.1.0 to 8.1.1


--------------------------------------------

# 2020-08-20-01

Release [2020-08-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Date champs: adding a placeholder for IE and Safari
- Sign-in page: autofocus on 'input: password' removed
- prend en compte le nom de l'enseigne pour l'établissement
- renforce la validation des numéros de téléphone

## Technique

- Rails : les relations `belongs_to` sont maintenant requises par défaut. (Pour une relation optionnelle, mentionner explicitement `belongs_to :table, optional: true`)


--------------------------------------------

# 2020-08-12-01

Release [2020-08-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-12-01)  sur GitHub

## Améliorations et correctifs

## Technique

- Annotation des modèles avec le résumé du schema active_record
- Suppression des colonnes précédemment retirées de la base de donnée
- Ajout de la tâche rake tmp_set_dossiers_last_updated_at (en lieu et place du job)


--------------------------------------------

# 2020-08-10-01

Release [2020-08-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- corrige l'intégration entre helpscout et le manager
- corrige mise en forme des commentaires de la messagerie


--------------------------------------------

# 2020-08-07-01

Release [2020-08-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Adapte la première partie de la page de configuration des emails (interface administrateur) au nouveau design


--------------------------------------------

# 2020-08-06-01

Release [2020-08-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-06-01)  sur GitHub

## Technique

- Utilisation de la configuration par défaut de Rails 6.0

## Notes de version

- Cette version ne permet plus à Internet Explorer <= 8 de soumettre des formulaires.
- Cette version se met à générer des cookies qui ne sont pas compatibles avec les versions précédentes. Les anciens cookies restent compatibles. Après cette version, revenir à Rails 5 (ou à une version précédente de Rails 6) invalidera les cookies générés entre temps.


--------------------------------------------

# 2020-08-05-02

Release [2020-08-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-05-02)  sur GitHub

## Technique

- Utilisation de la configuration Rails 5.1 par défaut


--------------------------------------------

# 2020-08-05-01

Release [2020-08-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : amélioration de l'accessibilité de la page de confirmation de l'adresse email
- Administrateur : corrige l'affichage "Activée/Désactivée" des attestations sur la page de description de la démarche

### Technique

- Mise à jour d'elliptic de la version 6.5.2 à la version 6.5.3
- Attestations : correction du rendu des PDFs


--------------------------------------------

# 2020-08-03-01

Release [2020-08-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-08-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Passage de la page admin/attestation_templates/edit au nouveau design
- Ajout du fond de carte IGN pour le module cartographique

### ⚠️ Notes de versions

Cette version comporte un bogue dans la génération des attestations, et a été retirée. La version suivante corrige le problème.


--------------------------------------------

# 2020-07-30-01

Release [2020-07-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #5428 - France connect avant les boutons de login/signup
- Optimisation du tableau de bord de l'interface instructeur
- Ajoute la possibilité de cloner une démarche lorsqu'elle est close, dans la nouvelle interface d'administrateurs


--------------------------------------------

# 2020-07-29-01

Release [2020-07-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Création d'un toggle-switch
- Suppression du message d'alerte suite aux problèmes d'upload
- Ajoute une propriété  "scroll-padding" pour les pages disposant d'un "sticky-bottom"
- Ajoute la possibilité de filtrer les dossiers archivés sur Graphql
- Ajoute un bouton envoyer une copie à la nouvelle interface admin
- L'interface administrateur passe sur le nouveau design concernant la liste des démarches
- Fixe un bug sur les notifications email dans le footer


--------------------------------------------

# 2020-07-28-02

Release [2020-07-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-28-02)  sur GitHub

## Améliorations et correctifs





### Technique

- Fix cloned revisions


--------------------------------------------

# 2020-07-28-01

Release [2020-07-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-28-01)  sur GitHub

## Améliorations et correctifs

## Technique

- correction de la déclaration du linter haml


--------------------------------------------

# 2020-07-27-01

Release [2020-07-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- instructeurs: correction du lien de démarche active
- Ajout de `cdn.jsdeliver.net` dans les Content-Security Policy, afin de pouvoir accéder à nouveau à la playground Graphql

## Technique

- Le nom d'application est paramètrable
- #5408 - Les expéditeurs d'emails sont paramètrables dans un fichier .env


--------------------------------------------

# 2020-07-23-02

Release [2020-07-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-23-02)  sur GitHub

## Améliorations et correctifs



### Technique

- fix active_revision_id -> active_revision.id


--------------------------------------------

# 2020-07-23-01

Release [2020-07-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-23-01)  sur GitHub

## Améliorations et correctifs





### Technique

- #5379 - Allow status page URL to be configured in the .env file
- Fix revision migration job


--------------------------------------------

# 2020-07-22-01

Release [2020-07-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Add revision migration and models

### Mineurs

- Administrateur : indique plus clairement si des champs de formulaire ont été ajouté à la démarche

### Technique

- Fix CI accessibility errors
- Ajout d'un test pour le telechargement de zips de dossier
- Différent fix lié au types_de_champ (surtout révisions)


--------------------------------------------

# 2020-07-21-01

Release [2020-07-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : possibilité de relancer un·e expert·e pour une demande d'avis

### Mineurs

- API GraphQL : les blocs répétables et les options sont maintenant exposés sur `champ_descriptors`

## Technique

- Activation des options par défaut de Rails 5.0


--------------------------------------------

# 2020-07-20-01

Release [2020-07-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige le bug sur la drop down list du patron
- [Revoque une demande d'avis à un expert](https://github.com/betagouv/demarches-simplifiees.fr/pull/5386)
- Instructeur : correction du titre lorsque l'onglet "Avis externe" est affiché
- Corrige l'export Zip
- Améliore la nouvelle interface d'administrateur en ajoutant les boutons archiver et tester

## Technique

- T


--------------------------------------------

# 2020-07-17-01

Release [2020-07-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-17-01)  sur GitHub

## Améliorations et correctifs

### Mineurs


- Résoud un bug sur la nouvelle interface d'administrateur sur la possibilité de tester la démarche (en brouillon)

## Technique

- Mise à jour de lodash de 4.17.15 à 4.17.19


--------------------------------------------

# 2020-07-16-01

Release [2020-07-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager: revoque un invité sur un dossier
- Affiche un nouveau design pour l'interface administrateur (détail d'une démarche) 

## Technique

- build(deps): mise à jour de puma de 4.3.3 à 4.3.5
- build(deps):  mise à jour de rack de 2.0.9 à 2.2.3


--------------------------------------------

# 2020-07-16-01-2

Release [2020-07-16-01-2](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-16-01-2)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Retire les drop_down_list inutilisées
- Ajout des annotations privées dans l'écran d'administration


--------------------------------------------

# 2020-07-15-01

Release [2020-07-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : ré-activation du temps de traitement des dossiers (désactivé pendant quelques jour pour cause de bogue)

## Technique

- Mailers : d'autres d'erreurs SMTP sont maintenant ignorées
- Jobs : les jobs d'analyse anti-virus ne sont plus relancés si le fichier cible n'existe plus
- Les pièces justificatives existantes sont marquées comme devant ignorer les validations qui seront mises en place prochainement


--------------------------------------------

# 2020-07-13-01

Release [2020-07-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateur : retour à l'ancien design de la page des détails d'une démarche (le nouveau design fonctionne mal sous Internet Explorer 11, mais va rapidement être amélioré)


--------------------------------------------

# 2020-07-12-03

Release [2020-07-12-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-12-03)  sur GitHub

## Améliorations et correctifs

### Technique

- disable usual_traitement_time


--------------------------------------------

# 2020-07-12-02

Release [2020-07-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-12-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Revert "build(deps): bump puma from 4.3.3 to 4.3.5"


--------------------------------------------

# 2020-07-12-01

Release [2020-07-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-12-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateur : nouveau design de la page "Détails d'une démarche"

### Technique

- Suppression d'anciennes tâches de migration de données
- Revert "build(deps): bump rack from 2.0.9 to 2.2.3"


--------------------------------------------

# 2020-07-09-01

Release [2020-07-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #4897 Affiche l'instructeur qui a rendu la décision sur un dossier

### Mineurs

- Changement du lien de contact sur la page d'accueil
- #5347 Validation du jeton API entreprise spécifique à une procédure fourni par l'administrateur

## Technique

- Les assets (CSS, Javascript) ne sont plus compressés dans le cadre d'un environnement de développement
- #5333 Ne remonte plus les erreurs api-entreprise à Sentry
- Remove repetition types_de_champ on type_champ change
- Suppression de la dépendance à `overmind`
- montées de versions:
  - bump rack from 2.0.9 to 2.2.3
  - puma-4.3.5


--------------------------------------------

# 2020-07-08-01

Release [2020-07-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-08-01)  sur GitHub

## Technique

- Mise à jour de Rails vers la version 6


--------------------------------------------

# 2020-07-07-03

Release [2020-07-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-07-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : correction de l'affichage des pages de notification et de statistiques de la démarche


--------------------------------------------

# 2020-07-07-02

Release [2020-07-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-07-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateur : correction d'une erreur sur la page "Nouvelle démarche"


--------------------------------------------

# 2020-07-07-01

Release [2020-07-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-07-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Les adresses web des pages administrateur  sont maintenant toutes préfixées par `/admin`. (détail ci-dessous)

### Mineurs

- Amélioration de la visibilité des onglets de l'interface
- Instructeur : amélioration de l'apparence des badges de synthèse des dossiers
- Superadmin: correction de l'affichage des demandes lorsque la page est vide
- Superadmin: correction de l'affichage de la page des liasses
- Usager : le triangle à côté de la date de clôture de la démarche est maintenant correctement masqué

### ⚠️ Note sur le changement des url administrateur

Les adresses web des pages admin sont maintenant toutes préfixées par `/admin`. 
Voici la liste complète des url affectées:
_(format: ancienne url (ancien helper) => nouvelle url (nouveau helper))_
- **/procedures/:procedure_id/groupe_instructeurs/:id/** (procedure_groupe_instructeur_path) **=> /admin/procedures/:procedure_id/groupe_instructeurs/:id/** (admin_procedure_groupe_instructeur_path)

- **/procedures/:id/champs** (champs_procedure_path) **=> /admin/procedures/:id/champs** (champs_admin_procedure_path)

- **/services** (services_path) **=> /admin/services** (admin_services_path)

- **/services/new** (new_service_path) **=> /admin/services/new** (new_admin_service_path)

- **/procedures/:procedure_id/types_de_champ** (procedure_types_de_champ_path) **=> /admin/procedures/:procedure_id/types_de_champ** (admin_procedure_types_de_champ_path)

- **/procedures/:procedure_id/mail_templates/:id/preview** (preview_procedure_mail_template_path) **=> /admin/procedures/:procedure_id/mail_templates/:id/preview** (preview_admin_procedure_mail_template_path)

- **/procedures/:id/annotation** (annotations_procedure_path) **=> /admin/procedures/:id/annotations** (annotations_admin_procedure_path)

- **/procedures/:id/apercu** (apercu_procedure_path) **=> /admin/procedures/:id/apercu** (apercu_admin_procedure_path)

- **/procedures/:id/monavis** (procedure_monavis_path) **=> /admin/procedures/:id/monavis** (monavis_admin_procedure_path)

- **/procedures/:id/update_monavis** (procedure_update_monavis_path) **=> /admin/procedures/:id/update_monavis** (update_monavis_admin_procedure_path)

- **/procedures/:id/jeton** (procedure_jeton_path) =>** (jeton_admin_procedure_path)

- **/procedures/:id/update_jeton** (procedure_update_jeton_path) **=> /admin/procedures/:id/update_jeton** (update_jeton_admin_procedure_path)

- **/procedures/:procedure_id/groupe_instructeurs/:id/reaffecter_dossiers** (reaffecter_dossiers_procedure_groupe_instructeur_path) **=> /admin/procedures/:procedure_id/groupe_instructeurs/:id/reaffecter_dossiers** (reaffecter_dossiers_admin_procedure_groupe_instructeur_path)

- **/procedures/:procedure_id/groupe_instructeurs/:id/reaffecter** (reaffecter_procedure_groupe_instructeur_path) **=> admin/procedures/:procedure_id/groupe_instructeurs/:id/reaffecter** (reaffecter_admin_procedure_groupe_instructeur_path)

**Helper modifié pour instructeur:**
- /procedures/:procedure_id **(procedure_path) =>** /procedures/:procedure_id **(instructeur_procedure_path)**


--------------------------------------------

# 2020-07-06-01

Release [2020-07-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- La date de fin de support d'Internet Explorer 11 (le 31 janvier 2021) est maintenant annoncée dans le bandeau de dépréciation

### Mineurs

- Usager : correction de la largeur maximale des menus déroulants
- Correction des exports PDF des dossiers, dans le cas où l'effectif mensuel était présent


--------------------------------------------

# 2020-07-02-01

Release [2020-07-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-02-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : affichage d'une synthèse du nombre de dossiers en attente sur toutes les démarches

### Mineurs

- 5325 : meilleure présentation des dossiers exportés en PDF


--------------------------------------------

# 2020-07-01-02

Release [2020-07-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-01-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Annulation de "5319: meilleure présentation pour les pdf dossier"

## Technique

- Tests : utilisation de `fixture_file_upload` plutôt que `Rack::Test::UploadedFile`


--------------------------------------------

# 2020-07-01-01

Release [2020-07-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-07-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- groupe les avis par démarche pour les experts
- supprime la précision à 2 chiffres après la virgule pour les données financières de l'identité entreprise
- 5319: meilleure présentation pour les pdf dossier
- 5316: améliore l'accessibilité en corrigeant les aria-labels manquant dans le pied de page

**⚠️ Cette release a été retirée, car elle introduit un bogue dans la génération des attestations PDF.**


--------------------------------------------

# 2020-06-29-01

Release [2020-06-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Supprime la notification de la création d'administrateurs
- Suppression d'un avertissement de Crisp dans la console Javascript
- Renomage du menu déroulant
- Usager : amélioration de l'accessibilité de la liste des dossiers
- Usager : empêche le bouton "Supprimer la pièce jointe" d'être cliqué deux fois
- Applique le nouveau design pour la gestion des instructeurs lorsque la démarche n'est pas routée

## Technique

- Ajoute des tests d'accessibilité WCAG pour les pages usager


--------------------------------------------

# 2020-06-23-02

Release [2020-06-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-23-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Accessibilité : les pages publiques passent maintenant la validation W3C


--------------------------------------------

# 2020-06-23-01

Release [2020-06-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs


- Usager : correction de certains textes faussement détectés comme des liens
- Instructeur : les annotations privées "Oui/Non" peuvent maintenant également être filtrées


## Technique

- Ajoute un mode maintenance (404) a l'url de ping
- Suppression de la restriction par IP pour voir le ping
- Correction du mécanisme de rollback des mises en production
- Mise à jour vers Rails 5.2.4.3


--------------------------------------------

# 2020-06-19-01

Release [2020-06-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Accessibilité : #5267 - couleurs plus constratées
- Accessibilité : #5277 - correction de balises html

## Technique

- #5279  [CARTO] fix the GPX and KML imports in order to manage multiples features
- #5280 utilisation de keystone v3


--------------------------------------------

# 2020-06-16-01

Release [2020-06-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-16-01)  sur GitHub

## Améliorations et correctifs
### Mineurs
- Instruct filter yes no
- Contact acessibility

### Technique

- no sentry report when error 400 for exercices
- Remove dossier procedure_id
- Fix job max attempts


--------------------------------------------

# 2020-06-11-03

Release [2020-06-11-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-11-03)  sur GitHub

## Améliorations et correctifs

## Technique

- Prepare to remove dossier procedure_id
- more verbose exception when RequestFailed for apientreprise occurs


--------------------------------------------

# 2020-06-11-02

Release [2020-06-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-11-02)  sur GitHub

## Améliorations et correctifs

## Technique

- build(deps): bump geocoder from 1.6.0 to 1.6.1
- add destroy dependent option for exports


--------------------------------------------

# 2020-06-11-01

Release [2020-06-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-11-01)  sur GitHub

## Améliorations et correctifs

## Technique

- supprime l'option `dependent: destroy` entre les groupes_instructeurs et les instructeurs


--------------------------------------------

# 2020-06-10-01

Release [2020-06-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout des descriptions aux zones géographiques
- récupère les effectifs d'avril 2020 via api_entreprise


--------------------------------------------

# 2020-06-09-01

Release [2020-06-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de la vue identite_entreprise

## Technique

- build(deps): bump websocket-extensions from 0.1.4 to 0.1.5


--------------------------------------------

# 2020-06-04-03

Release [2020-06-04-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-04-03)  sur GitHub

## Améliorations et correctifs

## Technique

- Revert "Suppression de l'utilisation de Keystone v2"


--------------------------------------------

# 2020-06-04-02

Release [2020-06-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-04-02)  sur GitHub

## Améliorations et correctifs

### Mineurs
- #5162 - Précisions sur le format attendu de login et mot de passe
- #5162 - Amélioration des libellés vers l'accueil
- #5162 - Meilleurs titres HTML pour écrans sign in/sign up
- ajoute des infos dans l'identité d'une entreprise tirées du bilan banque de france
- exporte les bilans banque de france aux formats csv, xls et ods

## Technique

- Suppression de l'utilisation de Keystone v2
- Suprression de la carto obsolète


--------------------------------------------

# 2020-06-04-01

Release [2020-06-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- tri des bilans bdf CSV
- https://github.com/betagouv/demarches-simplifiees.fr/issues/5224 gestion des effectifs d'entreprise seulement pour février 2020

## Technique

- Précision des essais max par job
- #5207 - Ajout de la gestion du protocole Openstack Keystone v3


--------------------------------------------

# 2020-06-02-01

Release [2020-06-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-06-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Outil cartographique : Ajout de fonds de cartes sans cadastres et du lien vers les tutoriels vidéo à destination des usagers
- Vues : modernisation.gouv.fr -> numerique.gouv.fr

### Technique

- Mise à jour (deps):  kaminari de 1.1.1 à 1.2.1
- Mise à jour (deps):  puma de 3.12.4 à 3.12.6


--------------------------------------------

# 2020-05-28-02

Release [2020-05-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-28-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : affichage de la date limite des démarches dans la description et les emails

## Technique

- Mise à jour de l'action GitHub "Rebase"
- Fix remaining etablissements with shared dossier


--------------------------------------------

# 2020-05-27-02

Release [2020-05-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-27-02)  sur GitHub

## Améliorations et correctifs 

### Technique

- fix le fix


--------------------------------------------

# 2020-05-27-01

Release [2020-05-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Do not send draft norifications to users on inactive démarches

## Technique

- fix champ etablissement


--------------------------------------------

# 2020-05-26-01

Release [2020-05-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Précisions sur le caractère obligatoire des infos de service
- Ajout de precisions pour les emails non reçus lors de la création de compte
- Fix. Ne met plus à jour l'établissement du dossier lors de la modif d'un champ siret

## Technique

- Correction de crashes lors de l'utilisation de Spring
- appels à api entreprise : précise l'url appelée lorsqu'une exception est levée
- specs: supprime des cas de test inutilisés


--------------------------------------------

# 2020-05-20-02

Release [2020-05-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-20-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Passe la récupération des données API Entreprise en asynchrone
- Usager : amélioration de l'interface d'import de données cartographiques


--------------------------------------------

# 2020-05-20-01

Release [2020-05-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige le non affichage des adresses dans le module cartographique


--------------------------------------------

# 2020-05-18-01

Release [2020-05-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : le service administratif de la démarche est maintenant accessible aux lecteurs d'écrans

## Technique

- Javascript : évite de remonter dans Sentry les erreurs de Direct Upload (en plus des erreurs d'auto-upload)
- Rake : ajout d'une tâche pour déclencher un rollback


--------------------------------------------

# 2020-05-15-01

Release [2020-05-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige l'aperçu du champ carte
- Mise à jour des logos, pour conformité avec la Charte Graphique de l’État
- Fixe le problème d'enregistrement du dossier lorsque le champ carte est obligatoire

### Technique

- GraphQL : correction d'un problème lors de l'introspection de certaines requêtes
- Retire le feature flag pour l'auto upload des pièces justificatives


--------------------------------------------

# 2020-05-14-01

Release [2020-05-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-14-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : le nom de fichier des pièces justificatives est maintenant indiqué dans les exports tableur
- Instructeur :la limite de téléchargement du ZIP de pièces jointes d'un dossier passe de de 50 Mo à 100 Mo

## Technique

- Javascript : mise à jour des dépendances à nouveau (sans casser IE 11)
- Upload des fichiers : seuls les messages d'erreurs inattendus sont enregistrés dans Sentry


--------------------------------------------

# 2020-05-13-02

Release [2020-05-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-13-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #4965- Clarification de l'explication du l'écran des dossiers archivés

### Technique

- Remove legacy carto code
- Rollback js update


--------------------------------------------

# 2020-05-13-01

Release [2020-05-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-13-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : amélioration de l'apparence des lignes et des points sur les cartes
- Instructeur : un dossier archivé qui est repassé en instruction est maintenant également désarchivé

## Technique

- Mise à jour des dépendances Javascript
- Suppression du code des tooltips dans l'ancien design
- Désactivation de Turbolinks


--------------------------------------------

# 2020-05-11-01

Release [2020-05-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- les attestations sociales et fiscales sont désormais disponibles via l'API
- import de fichier gpx (cartographie) en tant qu'usager
- Corrige l'erreur 404 sur les avis. Permet aux experts de consulter les bilans bdf

### Mineurs

- ajout des effectifs de l'entreprise dans l'export pdf du dossier
- Opérations atomiques possible dans l'editeur de carte

## Technique

- Corrige test aléatoire pour DossierSearchService


--------------------------------------------

# 2020-05-06-01

Release [2020-05-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-05-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- PDF vide - Ajout de descriptions manquantes
- N'appelle pas api-entreprise si jeton expiré

### Technique

- Use a “safe” reference date in expiration tests
- Update circle CI postgres version
- Fix a crash in case of invalid geometry


--------------------------------------------

# 2020-04-30-01

Release [2020-04-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-30-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- appelle api entreprise avec un jeton spécifique à une démarche
- affiche les attestations sociales aux instructeurs pour les procédures autorisées
- affiche les attestations fiscales aux instructeurs pour les procédures autorisées
- affiche les bilans bdf aux instructeurs pour les procédures autorisées
- active la republication sur les démarches dépubliées
- Corrige le souci de compatibilité pour Safari and Firefox compatibility pour mapbox-gl js

### Mineurs

- permet aux instructeurs de télécharger un document GeoJSON pour un dossier donné

## Technique

- build(deps): bump jquery from 3.4.1 to 3.5.0


--------------------------------------------

# 2020-04-28-01

Release [2020-04-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Migration vers un nouvel éditeur de carte (Mapbox-GL)

### Mineurs

- Précision dans le mail suppression des dossiers en construction


--------------------------------------------

# 2020-04-23-01

Release [2020-04-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur: les données SIRET comprennent maintenant l'effectif moyen pour l'année antérieure

### Mineurs

- Usager : les zones sélectionnées sur la carte indiquent maintenant la surface et la longueur de la sélection
- Instructeur : correction d'un bug où la notification de dépôt de dossier était envoyée à tous les instructeurs (plutôt qu'uniquement à ceux du groupe d'instruction concerné)
- Instructeur : corrige la présence de démarches supprimées dans la liste des démarches

## Technique

- Les dossiers expirés sont maintenant marqués comme notifiés dès que l'email de notification est envoyé.
- Les dossiers terminés dont la durée de conservation est expirée sont maintenant supprimés.
- `Champ.dossier` renvoie le dossier parent même lorsque le dossier est à la corbeille.
- Specs : nettoyage de instructeurs/procedures_controller_spec.rb #show
- Javascript : nettoyage des stacktraces des FileUploadError


--------------------------------------------

# 2020-04-22-01

Release [2020-04-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : à partir du SIRET, les effectifs mensuels de l'entreprise sont maintenant affichés dans l'interface. Les effectifs mensuels sont également disponibles dans l'API.
- Administrateur : les dossiers en construction ayant dépassé la date d'expiration sont maintenant supprimés (en conformité avec le RGPD)

### Mineurs

- Instructeur : la description de l'onglet "Tous" est plus claire

## Technique

- Correction d'une erreur sur la recherche texte libre
- Correction des règles CSP pour le développement local
- Les erreurs de lecture des pièces justificatives sont remontées dans Sentry


--------------------------------------------

# 2020-04-17-01

Release [2020-04-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-17-01)  sur GitHub

## Améliorations et correctifs

## Technique

- L'éditeur de carte envoie les FeatureCollection au server
- Nettoyage des validations de procedure pour `duree_conservation_dossiers_dans_ds`
- Fix du serializer de carte pour l'API v1 suite aux modifs de carto


--------------------------------------------

# 2020-04-16-02

Release [2020-04-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-16-02)  sur GitHub

## Améliorations et correctifs

## Technique

- Javascript : amélioration du formattage des erreurs d'upload


--------------------------------------------

# 2020-04-16-01

Release [2020-04-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-16-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : possibilité de sélectionner plusieurs zones sur une même carte

### Mineurs

- Usager : amélioration des erreurs de l'envoi automatique des pièces jointes
- Administrateur : correction des icônes de boutons manquantes

### Technique

- Refactor de l'uploader de pièces jointes


--------------------------------------------

# 2020-04-15-01

Release [2020-04-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-15-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : le nom du demandeur est affiché dans liste des dossiers
- API : nouvel explorateur de requêtes GraphQL (pour remplacer graphiql)

### Mineurs

- Manager: les administrateurs avec des procédures archivées peuvent être supprimés
- Administrateur : dé-activation des quartiers prioritaires sur la carte


## Technique

- Tentative de correction des erreurs ActionController::InvalidAuthenticityToken à l'upload
- Javascript: suppression de code périmé autour de jQuery.active


--------------------------------------------

# 2020-04-09-04

Release [2020-04-09-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-09-04)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeurs : possibilité de recevoir une notification par email à chaque dépôt de dossier

## Technique

- Améliore la manière dont les erreurs d'upload sont groupées dans Sentry


--------------------------------------------

# 2020-04-09-03

Release [2020-04-09-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-09-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : les instructeurs le souhaitant sont notifiés à chaque nouveau commentaire sur les dossiers qu'ils suivent.

### Mineurs

- Superadmin : correction de la suppression des démarches en test

## Technique

- Javascript: les erreurs DirectUpload sont mieux remontées dans Sentry


--------------------------------------------

# 2020-04-09-02

Release [2020-04-09-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-09-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Migrate mapReader to mapbox-gl with react

### Mineurs

- Fix middle-click on "Delete attachment" button

### Technique

- Update ruby-graphql
- remove WarnExpiringDossiersJob
- Process expired dossiers en construction


--------------------------------------------

# 2020-04-09-01

Release [2020-04-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- [GraphQL] Permettre de récupérer les dossiers d'un groupe instructeur
- Permettre aux usagers de rechercher dans le contenu de leurs dossiers

### Technique

- Javascript : mise à jour des dépendances de Webpacker
- [GraphQL] fix groupe instructeur type definition


--------------------------------------------

# 2020-04-08-01

Release [2020-04-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : affiche une page de redirection quand le bouton "Supprimer la pièce jointe" est ouvert dans un nouvel onglet
- Usager : améliorations sur le bouton "Réessayer l'envoi" des pièces justificatives


--------------------------------------------

# 2020-04-07-01

Release [2020-04-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-07-01)  sur GitHub

## Notes de version

Cette release corrige le problème de compilation du Javascript pour les anciens navigateurs introduit dans la version précédente (2020-04-06-01).

## Améliorations et correctifs

### Majeurs

- Administrateur : quand un administrateur clone une de ses propres démarches, les groupes instructeurs sont clonés aussi
- Usager : Désactivation des cartes Mapbox-GL, et retour vers l'ancien format de cartes

### Mineurs

- Usager : corrige une erreur Javascript au moment du Feedback, liée à l'utilisation de `window.scroll`
- Experts : dans l'email d'invitation, le lien pour donner son avis est maintenant un grand bouton bien visible

## Technique

- Ajoute une validation pour vérifier la cohérence des champs (#4994)
- Usager : les erreurs de vérification du statut de l'anti-virus ne sont plus remontées dans Sentry
- Jobs : utilisation d'expressions en pseudo-langage humain pour configurer les jobs récurrents


--------------------------------------------

# 2020-04-06-01

Release [2020-04-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-06-01)  sur GitHub

## ⚠️ Notes de version

Cette release contient un changement de la configuration Webpack, qui casse le Javascript dans les anciens navigateurs.

Ce changement est retiré dans la release suivante, en attendant qu'il soit corrigé proprement.

## Améliorations et correctifs

### Majeurs

- Usager : migration des cartes vers Mapbox-GL

### Mineurs

- Usager : simplification de l'apparence des champs sur la page Identité
- Usager : correction de l'envoi automatique de pièces justificatives dans un bloc répétable
- Instructeurs: amélioration de la découvrabilité du fichier d'export téléchargeable

## Technique

- Les dossier.dossier_operation_logs sont par défaut ordonnés par ordre de création
- Nettoyage du service d'expiration des dossiers


--------------------------------------------

# 2020-04-02-01

Release [2020-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-04-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : permet au navigateur web de remplir automatiquement les champs prénom, nom et mot de passes
- Usager : corrige le remplissage des champs Carte, SIRET et pièce justificative pour les invités
- Superadmin : possibilité de remiser (et de rétablir) des dossiers et des démarches dans le Manager
- Administrateur : correction de la suppression des groupes instructeurs, dans le cas où le groupe a eu des dossiers affectés puis supprimés par la suite

## Technique

- Nettoyage du code de suppression des dossiers
- Nettoyage du code des mailers de suppression de dossier


--------------------------------------------

# 2020-03-31-02

Release [2020-03-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-31-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : envoi automatique des pièces jointes au dossier (désactivé par défaut)


--------------------------------------------

# 2020-03-31-01

Release [2020-03-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-31-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : correction de la prévisualisation des attestations
- Administrateur : correction des champs masqués en bas de l'éditeur de champs

## Technique

- Ajout d'une tâche `rake jobs:schedule` pour programmer les tâches récurrentes
- Tests : correction du test des dossiers expirants pour les fins de mois
- Tests : suppression du helper inutilisé `wait-for-ajax`
- Tests : simplification de la configuration rspec
- Javascript : mise à jour d'eslint et de ses dépendences
- Nettoyage des validations de PJ


--------------------------------------------

# 2020-03-27-01

Release [2020-03-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Utilisation consistente de `’` au lieu de `'`
- #4955 - ajout d'un bandeau d'informations coronavirus


--------------------------------------------

# 2020-03-26-03

Release [2020-03-26-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-26-03)  sur GitHub

## Améliorations et correctifs

### Technique

- Spécification du niveau eIDAS lors des appels à France Connect
- Rename delete_and_keep_track -> discard_and_keep_track
- Remove non-existant columns from manager dashboards


--------------------------------------------

# 2020-03-26-02

Release [2020-03-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-26-02)  sur GitHub

## Améliorations et correctifs

### Mineurs
- Instructeur : affiche les dossiers supprimés pour une procédure
- Instructeur : afficher la raison de la suppression des dossiers
- Usager : amélioration de l'accessibilité des labels du formulaire
- Usager : permettre à l'usager de prolonger la durée de conservation d'un dossier en construction
- Usager : avertir 48h avant la clôture des démarches les usagers qui n'ont pas déposé de brouillon
- Ajouter la raison à la suppression des dossiers
- Améliorations d'accessibilité (CGU, pieds-de-page, sélection du genre)
- Amélioration du label des champs pièces jointes
- Les dossiers en brouillon supprimés ne génèrent plus de notifications ou d'opérations dans les journaux
- Ajout d'une tache de suppression des dossiers “discarded”
- Ne pas permettre la suppression des usagers avec des dossiers cachée par la démarche
- Correction des traductions des champs

### Technique
- Déplacement de fichiers Javascript d'un dossier à l'autre
- Garder les journaux après la suppression du dossier
- bump administrate
- README : mise à jour de la liste des taches
- Refactor scopes with intervals and use Time.zone.now


--------------------------------------------

# 2020-03-26-01

Release [2020-03-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-26-01)  sur GitHub

## Améliorations et correctifs

Cette release corrige la release précédente, qui pouvait échouer à cause de timeout lors d'une migration

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/4948 Add default to en_construction_conservation_extension


--------------------------------------------

# 2020-03-23-01

Release [2020-03-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- https://github.com/betagouv/demarches-simplifiees.fr/issues/3366 permet à un instructeur de remplir une demarche en cliquant sur un lien
- Préparation de la suppression périodique des dossiers en construction (suite)
- https://github.com/betagouv/demarches-simplifiees.fr/issues/4872  - fix the email notifications data bug
- https://github.com/betagouv/demarches-simplifiees.fr/issues/4227 - add files validations to models

## Technique

- #4916 db: correction du diff dans schema.rb
- #4913 Nettoyage des `mailers` pour les dossiers
- #4917 Tests : suppression d’un paramètre Chrome en double
- #4919 javascript: `utils.ajax()` renvoit une promise
- https://github.com/betagouv/demarches-simplifiees.fr/pull/4924 Renommage de `process_expired_dossiers_brouillon` dans `ExpiredDossiersDeletionJob`
- #4923 Mise à jour mineure de Rails (5.2.4.1 -> 5.2.4.2)


--------------------------------------------

# 2020-03-18-01

Release [2020-03-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3653 - Usager - Dossier PDF vide d'une procédure pour dépot papier
- #4879 - Usager - Permet à un usager de type "entreprise" de s'inscrire à DS même si cette entreprise n'a pas souhaité que ses infos soient diffusables
- #4883 - Instructeur : réception des emails quotidiens pour les démarches déclaratives
- #4900 - Instructeur: remplir une démarche en cliquant sur son lien
- #4892 - Administrateur : possibilité de repasser une démarche déclarative en non-déclaratif
- #4889 - Superadmin - La suppression d'un administrateur supprime les services qui n'ont pas d'administrateur 

### Technique

- #4882 CI: Possibilité de consulter les captures d'écran des tests échoués
- #4903 Suppression des tâches `after_party` périmées
- #4899 La configuration `Content Security Policy` autorise le live-reload
- #4901 Correction de l'initialisation de la base de donnée locale
- Montées de versions:
  - #4885 build(deps): bump acorn from 6.4.0 to 6.4.1
  - https://github.com/betagouv/demarches-simplifiees.fr/pull/4864 Mise à jour `rspec-rails`
  - https://github.com/betagouv/demarches-simplifiees.fr/pull/4866 Suppression de `restclient`
  - https://github.com/betagouv/demarches-simplifiees.fr/pull/4859 Montée de version de plusieurs gems


--------------------------------------------

# 2020-03-16-01

Release [2020-03-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- correction de bug dans l'ajout de nouveaux administrateurs

## Technique

- #4780 ajoute une mutation graphql pour changer le groupe instructeur d'un dossier


--------------------------------------------

# 2020-03-05-01

Release [2020-03-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-05-01)  sur GitHub

## Améliorations et correctifs

### Majeur

- Instructeur : possibilité d'ajouter une pièce jointe lorsque d'une demande d'avis.

### Mineurs

- Instructeur : Affichage d'une pastille orange quand le groupe instructeur a changé
- Administrateur : Amélioration des explications sur les procédures déclaratives
- Administrateur : les fins de procédures ont lieu à 23h59 au lieu de minuit

## Technique

- Correction d'un bug qui empêchait l'envoi du mail AdministrationMailer#dossier_expiration_summary
- Correction d'un bug lors des réaffectations de dossiers
- mise à jour activestorage-openstack
- mise à jour de puma (3.12.2 -> 3.12.4)


--------------------------------------------

# 2020-03-04-01

Release [2020-03-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-03-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- [GraphQL] add groupeInstructeur on dossier

### Technique

- fix/4814-fix-bug-error
- Update gems
- Use dossier.assign_to_groupe_instructeur
- Rend plus robuste un test pour `procedure_presentation`


--------------------------------------------

# 2020-02-27-01

Release [2020-02-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-27-01)  sur GitHub

## Améliorations et correctifs

### Technique

- feat/4796 - remove email_notifications_enabled column
- Revert "Update gems"


--------------------------------------------

# 2020-02-26-01

Release [2020-02-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #4814: Interdiction d'envoyer un logo d'attestation en GIF
- #4784: Les dossiers qui changent de groupe apparaissent dans la colonne "A suivre" des instructeurs du nouveau groupe

## Technique

- Mise à jour des dépendances ruby
- #4832: Amélioration des performances des écrans de listes de dossiers


--------------------------------------------

# 2020-02-25-02

Release [2020-02-25-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-25-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : correction du lien vers la démarche après l'inscription
- Administrateur : cloner une démarche supprime son lien à des révisions précédentes de la démarche


--------------------------------------------

# 2020-02-25-01

Release [2020-02-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Réduction de la taille du Javascript chargé sur chaque page
- Usager : redirige vers la démarche après l'email de confirmation, même lorsque le navigateur web est différent

### Mineurs

- Instructeur : ajout de précisions sur le contenu de l'export CSV
- Instructeur : les exports sont immédiatement marqués comme sûrs par l'antivirus
- Administrateur : amélioration du design et de la clarté de la page "Description de la démarche"
- Administrateur : les champs "Nombre" modernes sont activés sur toutes les démarches

## Technique

- Mise à jour de core-js
- Mise à jour de nokogiri


--------------------------------------------

# 2020-02-24-01

Release [2020-02-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un séparateur des milliers sur l'onglet de la vue générale des instructeurs
- #4785 - Ajout d'un tag 'groupe instructeur' pour le emails
- Correction d'un bug qui empechait d'ajouter un administrateur depuis le manager

## Technique

- Suppression des erreurs d'email pour cause de syntaxe erronée
- Suppression les colonnes ignorées des classes `Administrateur` et `Instructeur`
- Tests : corrige une erreur aléatoire dans les tests automatisés des Experts
- Ajout de mailers pour les opérations de suppression
- Suppression des mailers `notify_unhide_to_user` et `notify_undelete_to_user`
- Suppression du code d'export des dossiers obsolète
- #4796 - Ajout de colonnes dans `Assign_tos`


--------------------------------------------

# 2020-02-17-02

Release [2020-02-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-17-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : correction d'une erreur lors de l'affichage d'un titre de section dans un bloc répétable

## Technique

- Base de données : supprime la colonne "email" des Administrateurs


--------------------------------------------

# 2020-02-17-01

Release [2020-02-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-17-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- La possibilité pour un instructeur de se désabonner du récap par email hebdomadaire
- Usager : dans l'email « Le compte existe déjà », ajoute un lien vers la démarche
- Formulaire : affiche l'index de section dans l'en-tête
- GitHub : les utilisateurs sont invités à utiliser https://demarches-simplifiees.featureupvote.com pour leurs demandes d’améliorations
- Les nombres peuvent être copiés/collés sans inclure les espaces des séparateurs de milliers

## Technique

- Suppression des migrations antérieures à 2019
- Utilisation de la gem discard au lieu de DIY solution


--------------------------------------------

# 2020-02-12-02

Release [2020-02-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-12-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : corrige l'affichage des annotations privées contenant des titres de section


--------------------------------------------

# 2020-02-12-01

Release [2020-02-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : amélioration de la clarté et de l'accessibilité des formulaires
- Instructeur : corrige le fonctionnement de la case "Inviter les experts aussi sur les dossiers liés"
- Usager, Instructeur : affichage d'une note sur les dossiers dont des pièces jointes ont été perdues


--------------------------------------------

# 2020-02-11-02

Release [2020-02-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-11-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Manager : permet de visualiser le contenu d'un type de champ répétable
- Rajoute une aide contextuelle à propos de la suggestion de feature sur feature-upvote


--------------------------------------------

# 2020-02-11-01

Release [2020-02-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat/4501 -ajoute un séparateur de millier dans les pages du dossier + pdf
- inscription : retire l'aide sur la longueur du password
- Remplacement des dernières références restants à la DINSIC par DINUM

## Technique

- technique: ajoute un test à la consultation des demandes en attente
- Supprime de vielles clés de configuration de fog qui n’étaient utilisées que par CarrierWave


--------------------------------------------

# 2020-02-06-01

Release [2020-02-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Manager: correction d'un bug qui empêche la validation de demandes de créations de comptes


--------------------------------------------

# 2020-02-05-01

Release [2020-02-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Superadmin : peut supprimer un compte via le manager
- Usager : corrige quelques espaces disgracieux dans les emails

## Technique

- #4658 - balises avec accents et apostrophes
- ajoute un timeout dans les autocomplete
- modification des temps de rafraichissement pour l'export
- #4532 supprime la colonne email de la table admin
- #4705 - investigation des problèmes de mail


--------------------------------------------

# 2020-02-03-01

Release [2020-02-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-02-03-01)  sur GitHub

## Améliorations et correctifs

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/4732: mise à jour des dépendances JS − passage de node 8 à 12
- https://github.com/betagouv/demarches-simplifiees.fr/pull/4739: linter: amélioration de la détection de rubocop


--------------------------------------------

# 2020-01-30-01

Release [2020-01-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Mise à jour automatique des boutons de téléchargement des exports

## Technique

- Mise à jour de la règle Rubocop `unscope` pour qu'elle ne soit chargée qu'en développement development


--------------------------------------------

# 2020-01-28-01

Release [2020-01-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #4698 - Possibilite de selectionner un resultat quand il n'y a rien de suggéré dans le champ adresse

## Technique

- https://github.com/betagouv/demarches-simplifiees.fr/pull/4706 - Correction d'un bug circleci sur la répartition des tests


--------------------------------------------

# 2020-01-27-01

Release [2020-01-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- https://github.com/betagouv/demarches-simplifiees.fr/pull/4699 - L'autocomplete de recherche de departement fonctionne par numéro et par nom de département


--------------------------------------------

# 2020-01-21-02

Release [2020-01-21-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-21-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de la formulation de la bannière de grève, pour signaler que c'est uniquement le personnel de DS qui est en grève.
- Désactivation de la suppression de compte par les super-admin, à cause d'un bogue.

## Technique

- Tests : amélioration des factories de dossier


--------------------------------------------

# 2020-01-21-01

Release [2020-01-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-21-01)  sur GitHub

#**CETTE VERSION COMPORTE UN BOGUE SUR LA SUPPRESSION DES DOSSIERS, NE PAS DÉPLOYER.**

## Améliorations et correctifs

### Majeurs

- #3245 - Export PDF d'un dossier pour usager et instructeur
- #4686 - Champ communes
- #4601 - Suppression des groupes instructeurs

### Mineurs

- #4666 - Le récap hebdomadaire filtre les dossiers par groupe instructeur
- #4127 - Suppression de compte usager pour un superadmin


--------------------------------------------

# 2020-01-13-01

Release [2020-01-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ajout de champs communes

### Mineurs

- Les champs liés à un lieu utilisent tous un autocomplete
- Possibilité pour un superadmin de supprimer un compte usager

## Technique

- Nettoyage de la création des objets `Individual`
- Expiration des URLs ActiveStorage après une heure
- Correction de l'URL locale de France Connect sur les environnements de développement
- Nettoyage des demarches archivées


--------------------------------------------

# 2020-01-07-01

Release [2020-01-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-07-01)  sur GitHub

## Améliorations et correctifs

### Mineur

- La bannière de grève est derrière un feature-flip


--------------------------------------------

# 2020-01-06-01

Release [2020-01-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2020-01-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- Instructeur : meilleur messages d'erreur lorsque le changement d’état d’un dossier échoue.
- Usager : correction d'une erreur lorsque l'usager change de page alors qu'une suggestion d'email était affichée.

## Technique

- build(deps): bump rack from 2.0.7 to 2.0.8


--------------------------------------------

# 2019-12-18-01

Release [2019-12-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Rend l'export de dossier compatible avec les procédures routées

## Technique

- bump excon from 0.68.0 to 0.71.0


--------------------------------------------

# 2019-12-16-01

Release [2019-12-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Clarification du bouton de création d'annotations privées
- Corrige un bug concernant les destinataires du mail annoncant un nouvel instructeur dans un groupe


--------------------------------------------

# 2019-12-12-01

Release [2019-12-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Suppression de la possibilité pour instructeur de changer son propre e-mail

## Technique

- Suppression la clé étrangère administration_id sur DossierLogs
- Corrige un bug concernant les timezones dans les test
- Corrige un bug qui empêchait les mails concernant les dossiers dont la démarche n'avait pas de tel de partir


--------------------------------------------

# 2019-12-11-01

Release [2019-12-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- [GraphQL] fix a crash in mutations with an attachment

### Technique

- build(deps): bump puma from 3.12.0 to 3.12.2


--------------------------------------------

# 2019-12-04-03

Release [2019-12-04-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-04-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un état « Dépublié » aux démarches
- Ajout d’une bannière pour prévenir des perturbations liée à la grève


--------------------------------------------

# 2019-12-04-02

Release [2019-12-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-04-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration des bannières affichées globalement sur le site

### Technique

- Always load IntersectionObserver to fix old browsers


--------------------------------------------

# 2019-12-04-01

Release [2019-12-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Suppression des dossier en mode brouillon expiré et notification de la suppression
- Laisser les administrateurs déclarer eux-mêmes les démarches comme déclaratives

### Mineurs

- explique en détail l'importance de renseigner des infos de contact valides
- Afficher la description du champ répétable
- [GraphQL]: informations du demandeur du dossier
- [GraphQL]: informations du service


--------------------------------------------

# 2019-12-02-01

Release [2019-12-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-12-02-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Améliore l'accessibilité de la page d'accueil

### Mineurs

- GraphQL : normalise le nom des dates en français
- Corrige une erreur lors de l'affectation d'instructeur avec une adresse vide
- Corrige la fonction de sauvegarde automatique qui n'ignorait pas les bons champs
- Correction d'une petite faute
- Rename demarche archivée to démarche close
- Corrige la création d'un administrateur dans l'interface manager

### Technique

- Mise à jour de Capybara
- Clean up procedure states
- Simplify React components loader


--------------------------------------------

# 2019-11-25-01

Release [2019-11-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout des fichiers de la messagerie dans l'export zip du dossier

## Technique

- Correction des propriétés email utilisées pour `Instructeur` et `Administrateur`
- Correction des fautes syntaxiques html
- Correction d'un test bout en bout peu fiable sur le routage


--------------------------------------------

# 2019-11-21-02

Release [2019-11-21-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-21-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : permet d'enregistrer automatiquement les brouillons des dossiers (désactivé pour l'instant)

## Technique

- Devise : ignore les messages envoyés à des adresses email invalides


--------------------------------------------

# 2019-11-21-01

Release [2019-11-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : lorsqu'un dossier est envoyé à un autre Instructeur, ce dernier est ajouté aux instructeurs qui suivent ce dossier.
- Instructeur : l'ancien format de l'export des dossiers en fichier tableur n'est plus disponible.

### Mineurs

- Usager : améliorations diverses sur l'accessibilité.

## Technique

- DossiersController : enregistre par défaut en brouillon
- DossiersController : évite de générer du HTML en cas de requête JSON
- Documentation : ajout de commentaires et d'explications pour env.example
- Javascript : les fonctions utilitaires fonctionnent lorsque l'élément n'existe pas
- Fix UnknownFormat durant les exports de procédures


--------------------------------------------

# 2019-11-20-01

Release [2019-11-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-20-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ajoute un champ autocomplété pour l'ajout des gestionnaires
- [GraphQL] Add dossierEnvoyerMessage mutation
- [GraphQL]: add dossier state change mutations

### Mineurs

- [GraphQL]: fix some types and add some doc
- Améliore l'accessibilité en encadrant les liens sous focus
- Maj message quand messagerie désactivée
- Log l'email du super admin lors des actions dans le manager
- Supprime le champ `Service.siret` du Manager
- individual: raise when the individual object cannot be created

### Technique

- Suppression de CarrierWave
- Bump rails and activestorage-openstack
- build(deps): bump json-jwt from 1.10.0 to 1.11.0
- Merge branch 'dev' into 4482-echec-initilaisation-env-dev
- Fixes missing database on initialization: closes #4482.


--------------------------------------------

# 2019-11-12-01

Release [2019-11-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- API GraphQL : ajout de plus d'options de filtrage à la récupération de dossiers

## Technique

- Suppression des notification vers report-uri
- Correction d'une exception `ActiveStorage::FileNotFoundError`


--------------------------------------------

# 2019-11-07-02

Release [2019-11-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-07-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : l'éditeur de formulaire insère les nouveaux champs à l'endroit attendu
- Corrige des bugs sur l'export

## Technique

- Renommage de l'option routage afin de l'activer sur les pages manager/admins
- [GraphQL] Add create direct upload mutation


--------------------------------------------

# 2019-11-07-01

Release [2019-11-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Rgaa 3.0 : corrections pour les pages non connectés

## Technique

- On arrête de relancer les envois d'email lorsqu'ils échouent car le destinataire est inexistant


--------------------------------------------

# 2019-11-06-02

Release [2019-11-06-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-06-02)  sur GitHub

## Technique

- Correction de crashes apparaissant dans Sentry
- Une partie des emails est maintenant envoyée par SendInBlue (plutôt que Mailjet)


--------------------------------------------

# 2019-11-06-01

Release [2019-11-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : les erreurs lors de l’édition d’une carte sont mieux remontées
- Administrateur : correction de la recherche dans la liste des instructeurs

## Technique

- Mise à jour de la documentation d'installation
- Bump de loofah
- Use webdrivers gem to keep webdrivers updated


--------------------------------------------

# 2019-11-05-01

Release [2019-11-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Manager : répare l'invitation des administrateurs
- Instructeurs : la casse de l'email est correctement ignorée lors d'une invitation

## Technique

- Webhook : correction d'une exception lorsque l'adresse email du client est inconnue
- Déplacement de la méthode `active?` sur le model User


--------------------------------------------

# 2019-11-04-01

Release [2019-11-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-11-04-01)  sur GitHub

## Technique

- Dépréciation de la colonne Instructeur.email


--------------------------------------------

# 2019-10-31-02

Release [2019-10-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-31-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : amélioration de la mise en page du champ "Lien de rappel HTTP"
- Manager: allow hidden procedures to be searched
- ETQ Instructeur, lorsque je fais l'export excel, j'aimerais avoir le nom de la personne morale qui dépose dans la première feuille


--------------------------------------------

# 2019-10-31-01

Release [2019-10-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-31-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateur : nouvelle page de création et d'édition des démarche


--------------------------------------------

# 2019-10-30-01

Release [2019-10-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-30-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Gestion des groupes instructeurs par les instructeurs

## Technique

- Bump openstack
- Add a DS_PROXY_URL env variable


--------------------------------------------

# 2019-10-29-01

Release [2019-10-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-29-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction des noms de pièces jointes qui sautent

### Mineurs

- Amélioration du bouton "Ajouter une ligne" des blocs répétables
- Corrections de quelques fautes d'orthographe

## Technique

- Suppression des derniers defaut_groupe_instructeur illégitimes
- Corrige le bug qui maintenait les administrateurs dans un état inactif


--------------------------------------------

# 2019-10-24-02

Release [2019-10-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-24-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #4434 - Rend possible le fait de télécharger 2x un export

## Technique

- Fix un pb de rendu des types de champs dans l'interface admin
- Downgrade  de Capybara
- Modifie le déploiement pour autoriser des longues migrations (5min)
- Les erreurs d'envoi de pièces jointes sont signalées à Sentry


--------------------------------------------

# 2019-10-24-01

Release [2019-10-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Gestion des groupes d'instructeurs
- Invite experts to linked dossiers

### Mineurs

- Admin : clarifie le nom des différents types de champs "Nombre"

### Technique

- Update activestorage-openstack
- Decommission ActiveStorage proxy service and use openstack service


--------------------------------------------

# 2019-10-22-01

Release [2019-10-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-22-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Export asynchrone d'une procédure

### Mineurs

- Rajout de la liste déroulante qui permet à l'usager d'utiliser le routage

## Technique

- Suppression des requètes qui font plus de 60s en base
- Ajout de tests d'acceptation pour les Experts
- Migration vers Sendinblue API v3


--------------------------------------------

# 2019-10-22-01-b

Release [2019-10-22-01-b](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-22-01-b)  sur GitHub

## Technique

- Ajout d'un job de suppression des vieux exports


--------------------------------------------

# 2019-10-16-01

Release [2019-10-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-16-01)  sur GitHub

## Améliorations et correctifs

## Technique

- Ajoute une colonne dans la table procédure qui contiendra le label de la liste de routage
- Add web3 and web4


--------------------------------------------

# 2019-10-15-01

Release [2019-10-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-15-01)  sur GitHub

## Améliorations et correctifs

- Instructeur : Les instructeurs ont à nouveau accès aux attestations des dossiers terminés
- Instructeur : Il est maintenant possible de voir le justificatif même s'il n'y a pas de motivation. (Avant le lien n'apparaissait pas.)

### Mineurs

- Instructeur : Amélioration du menu de changement d'état d'un dossier
- Instructeur : Il est maintenant possible de repasser un dossier en instruction même s'il n'y a pas de motivation ou de justificatif.
- Instructeur : Amélioration de l'apparence du formulaire de demande d'Avis
- Instructeur : La prévisualisation de l'attestation est plus simple


--------------------------------------------

# 2019-10-09-01

Release [2019-10-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Un instructeur peut passer un dossier accepté en instruction (#4058)
- Corrige la notion d'administrateur actif (#4361)

### Mineurs

- Optimisations de performances
- Génération automatique de liens dans les champs textes
- Instructeur : corrige l'utilisation des cartes dans les annotations privées (#4388)

## Technique

- Suppression du feature flag `download_as_zip`
- Refactoring : dans les tasks, remplacement des `puts` par `rake_puts`
- Ajout de modèles par défaut lors de la création d'une issue
- Mise à jour des dépendances Javascript
- Mise à jour des dépendances Ruby :
  - rubyzip passe de 1.2.2 à 1.3.0
  - suppression de la gem 'simple_form'


--------------------------------------------

# 2019-10-03-01

Release [2019-10-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout du bouton "Continuer" dans la création de formulaire
- Les textes des dossiers non modifiables sur les écrans `merci` et `invitation` reflètent le caractère non modifiable du dossier
- Modifications diverses sur les liens externes (nouveaux onglets, ajouts/suppressions, lien vers la doc de DS)


--------------------------------------------

# 2019-09-26-02

Release [2019-09-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-26-02)  sur GitHub

## Améliorations et correctifs

## Technique

- [GraphQL] Add groupe_instructeurs to demarche
- Amélioration du requetages des notifications


--------------------------------------------

# 2019-09-26-01

Release [2019-09-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les dossiers archivés ne sont plus 'dé suivis'
- Correction de l’affichage des diagrammes des stats (Disable turbolinks for links to Stats pages)
- Dans la vue des dossiers, on peut maintenant trier par “a des notifications” y compris dans l’onglet “dossiers traités”.
- Ajout de liens vers entreprise.data.gouv.fr en bas de la fiche entreprise lors du dépot et du traitement d’un dossier pour une personne morale

## Technique

- Prépare l'export des dossiers au routage
- Ajoute la colonne groupe d'instructeur dans les procédures routées
- Api v2 graphql
- Optimisation des requêtes sur les pages /procedures#index et /procedures#show


--------------------------------------------

# 2019-09-19-01

Release [2019-09-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Prépare la vue instructeur procedure show à l'arrivée du routage

### Technique

- Revert "Upload through proxy"


--------------------------------------------

# 2019-09-18-01

Release [2019-09-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dashboard de statistiques pour les instructeurs

### Mineurs

- Administrateur : Simplification la gestion des liens de procédure
- Administrateur : Mise à jour de la validation des liens "Mon Avis"
- Administrateur : Correction de la fonctionnalité réinviter un administrateur [fix #4311]
- Usager : Boutons flottant dans le formulaire de dossier [fix #3975]
- Instructeur : Les statistiques des instructeurs utilisent les groupes instructeurs

### Technique

- Upload through proxy
- Remove duplicated attachments (Step 2)
- Fix procedure_locked_spec
- Remove unused columns and tables


--------------------------------------------

# 2019-09-17-01

Release [2019-09-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-17-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Perf index procedures
- Use built in active_storage helper
- Prepare to drop columns


--------------------------------------------

# 2019-09-16-01

Release [2019-09-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Serialize champ repetition for tags

## Technique

- Correctly create new flipper flags
- Bump devise from 4.6.1 to 4.7.1
- Add Style/CollectionMethods to rubocop
- Replace old migrations with a single “recreate_structure”


--------------------------------------------

# 2019-09-12-01

Release [2019-09-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-12-01)  sur GitHub

## Mineur

- Instructeur : corrige des notifications manquantes dans le tableau de bord

## Technique

- Mise à jour de sécurité mixin-deep de 1.3.1 à 1.3.2
- Tentative de correction des tests d'acceptance qui deadlock
- Nettoyage de vielle données FlipFlop
- Statistiques: correction du taux de contact usager


--------------------------------------------

# 2019-09-10-01

Release [2019-09-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : correction de la suppression des démarches de test
- Messagerie : le texte explicatif indiquant le destinataire du message est  plus clair
- Emails : les emails transactionnels d'une démarche ont maintenant une adresse de réponse en NO_REPLY ; cette adresse renvoie un message d'orientation automatique en cas de réponse.
- API : le service d'une démarche est maintenant exposé dans l'API

## Technique

- Suppression des uploaders CarrierWave (étape 1)


--------------------------------------------

# 2019-09-09-01

Release [2019-09-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-09-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Emails : la signature des messages n'est plus transformée en lien cliquable

## Technique

- correction du déploiement des groupes instructeurs


--------------------------------------------

# 2019-09-02-01

Release [2019-09-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- enlève l'option parcelle agricole dans la carto

## Technique

- Insère un niveau d'indirection entre la procédure et les instructeurs (routage)


--------------------------------------------

# 2019-08-28-02

Release [2019-08-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-28-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Utiliser un unique compte pipedrive
- Mentionner clairement que les dossiers en test sont supprimés lorsqu'on modifie la démarche
- message plus clair pour la cloture automatique

### Technique

- Fix attestation preview


--------------------------------------------

# 2019-08-28-01

Release [2019-08-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-28-01)  sur GitHub

## Améliorations et correctifs

### Technique

- Migrate attestation files to active_storage
- Fix file path in README
- Fix invalid characters in repetition champs sheet_name for xls export
- Fix logo blobs


--------------------------------------------

# 2019-08-27-01

Release [2019-08-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3536 Suppression de la possibilité de se créer un compte depuis la home
- #4238 Modifie la connexion par jeton pour éviter l'envoi de mails intempestifs
- Corrige un texte sur la carto
- #4242 supprime les demandes de date de naissance sur les nouvelles procédures

## Technique

- Utilise active storage pour le logo de la procédure
- Fait marcher capybara sur linux
- Bump eslint-utils from 1.3.1 to 1.4.2
- Ajoute crisp au domaine autorisée


--------------------------------------------

# 2019-08-21-01

Release [2019-08-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-21-01)  sur GitHub

## Améliorations et correctifs

## Technique

- fix flaky spec in ip_service_spec
- Improve clone of procedure attachements
- Fix carte initialize


--------------------------------------------

# 2019-08-20-02

Release [2019-08-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-20-02)  sur GitHub

## Améliorations et correctifs

## Technique

- Corrige une tache after party
- Mise à jour de la Gem Nokogiri


--------------------------------------------

# 2019-08-20-01

Release [2019-08-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction dans le texte des statistiques et du mail d'invitation des experts

## Technique

- Nettoyage du Controller de Session
- Nettoyage global du au suppression des roles devise instructeur et administrateur
- Suppression du commentaire file uploader
- Corrige un bug dans le filtre de la vue instructeur
- Corrige la suppression d'un administrateur par une administration
- Corrige un bug dans le manager concernant le reset de password
- Simplifie la création de role instructeur ou administrateur
- Ajout de la gem rack attack pour prévenir les attaque par brut force
- Suppression de taches after party obsolètes


--------------------------------------------

# 2019-08-14-01

Release [2019-08-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-14-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Activation des blocs répétables et de l'export tableur v2 pour toutes les démarches

### Mineurs

- Administrateur : il n'est plus obligatoire de renseigner un SIRET lors de la création d'un service
- Corrige un bug concernant l'invitation d'un instructeur depuis le manager
- Le formulaire de contact peut être utilisé même si l'utilisateur à un appareil non confirmé

## Technique

- Suppression du role devise administrateur
- Exécute les scripts UJS après une modification asynchrone de la page


--------------------------------------------

# 2019-08-13-01

Release [2019-08-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-13-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : nouveau tutoriel pour l'intégration de « Mon Avis »
- Traduction de la page 404 en français
- Administrateur : ajout de liens vers la documentation et l’aide
- Rend le bouton "Archiver" visible dès la validation du dossier

## Technique

- Supprime le compte instructeur devise
- Renomme Gestionnaire en Instructeur dans un commentaire du code
- Documentation technique : renomme Gestionnaire en Instructeur
- Seeds : renomme Gestionnaire en Instructeur
- Suppression de contrôleurs Devise inutiles


--------------------------------------------

# 2019-08-12-01

Release [2019-08-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

-  Vérification de la complexité lors du changement d'un mot de passe administrateur
- #4015 - Pré-remplir nom/prénom/civilité à partir des infos France Connect

## Technique

- Ajout de test sur la création d'un compte administrateur
- Renomme les références internes à `Gestionnaire` en `Instructeur`


--------------------------------------------

# 2019-08-01-01

Release [2019-08-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-01-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Superadmin : possibilité de repasser un dossier Accepté en Instruction
- API : ajout de l'URL de l'attestation dans les données d'un dossier
- API : ajout des champs répétables

### Mineurs

- Instructeur : correction de l'export tableur des dossiers v2
- Administrateur : ajout d'une confirmation avant la suppression d'un champ

## Technique

- Ajout de pundit
- Suppression de feature flags inadaptés
- Migration des sélections cartographiques au format geo_area


--------------------------------------------

# 2019-07-30-01

Release [2019-07-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-30-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Après un certain nombre d'authentifications infructueuses, le compte utilisateur est temporairement verrouillé

### Mineurs

- Suppression des pages sur le Tour de France

## Technique

- Suppression du code des anciennes pièces justificatives.

  _Note : avant de déployer cette version, vérifier qu'il n'y a plus aucun objet `PieceJustificative` ni `TypeDePieceJustificative` en production. Au besoin, regarder le code de `pj_migration.rake` pour effectuer la migration._


--------------------------------------------

# 2019-07-29-01

Release [2019-07-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : le lien vers la page sur laquelle sera publiée la démarche est maintenant demandé à la publication (plutôt qu'à la création de la démarche)
- Usager : corrections mineures sur le bouton "Modifier le dossier"

## Technique

- Mise à jour du descriptif du schéma de la base de données
- Fait en sorte que Procedure.path soit toujours présent en base


--------------------------------------------

# 2019-07-25-01

Release [2019-07-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : ajout de boutons d'actions aux emails transactionnels
- Usager : ajout d'un message (optionnel) lors de l'invitation à co-construire un dossier

### Mineurs

- Usager : ajout d'un lien et d'un bouton pour modifier le dossier une fois déposé
- Usager : ajout d'infos pratiques pour aller chercher son SIRET
- Administrateur : amélioration de la formulation par défaut des emails transactionnels


--------------------------------------------

# 2019-07-24-01

Release [2019-07-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un message lorsque le dossier fait plus de 50 Mo
- Ameliorations cosmétiques des écrans « MonAvis »
- Amélioration de formulation sur la page « Suivi »
- Administrateur : à la première connexion, un message de bienvenue s'affiche dans le chat
- Superadmin : Ajout d'un bouton de suppression des admin dans le manager

## Technique

- Replacement du endpoint api-carto
- Simplification de la hiérarchie de vue des emails


--------------------------------------------

# 2019-07-18-01

Release [2019-07-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : affiche un bouton "Répondre" dans la Messagerie
- Administrateur : ajout d'une balise `--lien document justificatif--`
- Permet d'insérer un lien vers MonAvis dans les démarches

### Mineurs

- Usager : améliore le titre des emails transactionnels
- Usager : améliore les emails de nouveau brouillon et de nouveau message

## Technique

- NodeJS v8 est maintenant le minimun requis pour faire tourner l'application


--------------------------------------------

# 2019-07-17-01

Release [2019-07-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : ajout d'un bouton pour télécharger toutes les pièces d'un dossier (derrière un feature-flag)

### Mineurs

- Usager : clarifie le sens du délai de traitement estimé
- Usager : clarifie les explications des différents états du dossier
- Usager : l'action « Soumettre le dossier » est renommée en « Déposer le dossier »

## Technique

- Mise à jour de lodash vers la version 4.17.14
- Configure OmniAuth pour éviter de potentielles CSRF
- Refactor de la suppression de dossier (#4099)
- Migration des pièces jointes : re-tente de changer le type MIME en cas d'erreur


--------------------------------------------

# 2019-07-15-01

Release [2019-07-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-15-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : le bouton "Publier" n'est plus masqué par l'aide contextuelle
- Manager : corrige l'email notifiant la suppression d'un dossier, qui n'était plus envoyé à l'usager

## Technique

- Suppression de colonnes dépréciées sur la table `procedures`


--------------------------------------------

# 2019-07-11-01

Release [2019-07-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : ré-active la messagerie sur les démarches archivées

### Mineurs

- Usager : améliore le message d'erreur lorsque la valeur d'un champ numérique est invalide
- Instructeur : des messages d'avertissement clairs sont affichés lorsque deux instructeurs effectuent en même temps une action contradictoire sur le même dossier
- Instructeur : ajoute des explications sur le rôle de chaque onglet
- Administrateur : corrige l'affichage du radio-button "Personne morale"

## Technique

- Suppression du code de `individual_with_siret` sur `Procedure`
- Ignore la colonne `:expects_multiple_submissions` sur le bon modèle
- Bumpe lodash.template vers la version 4.5.0


--------------------------------------------

# 2019-07-10-01

Release [2019-07-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-10-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : rend plus facile de déposer un autre dossier sur la même démarche

### Mineurs

- Amélioration du changement d'adresse email
- Usager : corrige une erreur lorsque l'email France Connect n'a pas la même casse que sur demarches-simplifiees.fr
- Instructeur : correction de l'orthographe dans le menu "Passer en instruction"
- Administrateur : meilleure valeur par défaut pour les menus déroulants liés

## Technique

- Mise à jour de l'équipe dans Pipedrive
- Correction d'un test aléatoire sur les dossiers
- Ajout d'une tâche pour migrer les pièces jointes de la Messagerie vers ActiveStorage
- Amélioration des factories des champs


--------------------------------------------

# 2019-07-09-01

Release [2019-07-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Possibilité de changer l'adresse email de son compte

### Mineurs

- Il n'est plus possible d'envoyer par erreur des messages sur un dossier où la Messagerie n'est pas disponible
- Usager : la mise en page des attestations est plus nette, et le logo occupe mieux l'espace
- Instructeur : corrige un crash quand un dossier est déjà en instruction
- Instructeur : Correction des dates sur l'export des dossiers
- Administrateur : le choix entre "Démarche pour les particuliers" et "Démarche pour les structures professionnelles" est plus clair
- Administrateur : suppression du n° de téléphone du standard

## Technique

- Suppression de la référence obsolète `Procedure.administrateur_id` dans la base de données


--------------------------------------------

# 2019-07-03-01

Release [2019-07-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Export de spreadsheets v2 avec les champs répétables et les avis
- Les instructeurs ont la possibilité de repasser un dossier rejeté en instruction

### Mineurs

- Support (un peu plus) officiel pour les démarches déclaratives

## Technique

- MAJ des dépendances JavaScript
- Ajout d'une machine à états aux dossiers
- Migration des templates mail vers `actiontext`


--------------------------------------------

# 2019-07-01-02

Release [2019-07-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-01-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- [#4008] active la connexion sécurisée pour tous les instructeurs


--------------------------------------------

# 2019-07-01-01

Release [2019-07-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-07-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout des avis dans l'API
- Désactivation de la messagerie dans les dossiers et procédures archivées
- #3986 Calcul du ROI pour les administrateurs

## Technique

- Activation des règles `Content-Security-Policy`


--------------------------------------------

# 2019-06-24-01

Release [2019-06-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- API : un paramètre `order` permet maintenant de contrôler l'ordre dans lequel les dossiers sont renvoyés

## Technique

- Admin : correction d'un plantage dans l'email `activate_before_expiration`
- Suppression des pièces justificatives orphelines en base
- Migration des pièces jointes : créer un champ vide ne déclenche plus de notification sur le dossier


--------------------------------------------

# 2019-06-20-01

Release [2019-06-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- Usager : correction d'une erreur lors de l'enregistrement simultané d'un champ invalide et d'une pièce jointe
- Admin : correction d'un problème lors de la création d'une démarche sans cadre juridique

## Technique
- Migration des PJ : gestion des erreurs qui peuvent se produire pendant le rollback
- Migration des PJ : corrige le rollback dans le cas où les dossiers sont cachés
- Correction du mailer_preview pour prendre en compte les administrateurs multiples


--------------------------------------------

# 2019-06-19-01

Release [2019-06-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-19-01)  sur GitHub

## Améliorations et correctifs

## Technique

- Ajustement des restrictions de Content-Security-Policy


--------------------------------------------

# 2019-06-17-02

Release [2019-06-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-17-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Horodatage des operations des dossiers

### Mineurs

- Correction d'un problème où la date de modification d'un dossier n'était pas mise à jour lorsque seulement les pièces justificatives étaient modifiées
- Suppression d'un paragraphe dupliqué dans le fichier README
- Petites améliorations au CONTRIBUTING.md

## Technique

- javascript: fix dependancies compilation using yarn 1.16.0


--------------------------------------------

# 2019-06-17-01

Release [2019-06-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-17-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans toute l'interface, les dates sont maintenant affichées au format « 12 juillet 2019 » (plutôt que « 12/07/2019 »)
- Usager : clarifie les explications sur l'enregistrement du brouillon pour le reprendre plus tard
- Usager : meilleur message d'erreur lorsque les informations d'une entreprise ne sont pas publiques
- Instructeur : affiche tous les instructeurs ayant suivi le dossier à un moment donné
- Administrateur : le champ "Ajouter un nouvel administrateur" est correctement réinitialisé après l'ajout

## Technique

- Mise à jour de Chartkick
- Correction de la tâche de migration des pièces justificatives
- Mise à jour de la politique CSP


--------------------------------------------

# 2019-06-12-01

Release [2019-06-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-12-01)  sur GitHub

## Technique

- Corrige une erreur Javascript concernant ActiveStorage sous Internet Explorer 11


--------------------------------------------

# 2019-06-06-01

Release [2019-06-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : les bloc répétables ne disparaissent plus lorsqu'on appuie sur la touche Entrée dans un champ
- Manager : ajout de colonnes "nombre de dossiers par an" et "échéance souhaitée".

## Technique

- Intégration de l'API Crisp
- Corrige les notifications Instructeur sur les dossiers migrés le 4 juin


--------------------------------------------

# 2019-06-04-01

Release [2019-06-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeurs : il est maintenant possible de supprimer une pièce jointe sur les avis
- API : l'ordre des dossiers renvoyés dans les résultats paginés est maintenant stable
- Correction de l'affichage des menus déroulants dans Firefox

## Technique

- Amélioration de la fiabilité des tests d'acceptance
- Améliore la rapidité et la fiabilité des tests d'acceptance
- Correction de certains tests automatisés qui échouaient aléatoirement
- Bump de la dépendance indirecte `fstream`


--------------------------------------------

# 2019-05-29-01

Release [2019-05-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-29-01)  sur GitHub

## Technique

- Corrections et améliorations de robustesse sur la tâche de migration des PR


--------------------------------------------

# 2019-05-28-02

Release [2019-05-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-28-02)  sur GitHub

## Technique

- Correction des instructions pour mettre les jobs récurrents en queue.


--------------------------------------------

# 2019-05-20-01

Release [2019-05-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-20-01)  sur GitHub

## Améliorations et correctifs

## Technique

- CSP: ajout de domaines manquants dans la whitelist
- Suppression d'une erreur au démarrage de la console Rails


--------------------------------------------

# 2019-05-16-01

Release [2019-05-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout de plus d'informations utilisateur pour télécharger le justificatif côté utilisateur

## Technique

- Mise à jour des dépendances JS
- Changement de l'URL de report-uri
- Suppression d'une locale inutile
- Suppression d'un import de script sendinblue superflu
- Correction d'une typo dans la configuration carrierwave


--------------------------------------------

# 2019-05-15-01

Release [2019-05-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-15-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateurs : nouvel éditeur de champs de formulaire

### Technique

- Usager : corrige une erreur mineure à la fin de l'envoi d'un fichier sous Safari


--------------------------------------------

# 2019-05-14-01

Release [2019-05-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-14-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateurs : dans l'éditeur de champs, les nouveaux champs sont insérés à l'emplacement actuel dans le formulaire

### Mineurs

- Administrateurs : ajout de vidéos et de liens vers les webinaires
- Manager : correction de l'activation ou désactivation de fonctionnalités aux administrateurs

## Technique

- L'état des dossiers est consigné dans un journal lors de l'acceptation, du refus ou du classement sans suite


--------------------------------------------

# 2019-05-13-02

Release [2019-05-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-13-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- La page d'inscription résiste mieux à la possibilité de vérifier si une adresse email est déjà enregistrée comme compte

## Technique

- Correction d'une erreur rare lors du login France Connect
- Lorsque le logo d'une démarche est manquant, l'identifiant de la démarche est remonté


--------------------------------------------

# 2019-05-13-01

Release [2019-05-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- Rétablissement du filtrage des extensions sur les anciennes pièces justificatives
- Corrige l'affichage d'une page d'erreur après l'envoi d'un message via le formulaire de contact

## Technique

- Ajustement des en-têtes CSP envoyés au navigateur
- Remonte les informations en cas d'erreur lors de l'exécution de l'antivirus


--------------------------------------------

# 2019-05-03-01

Release [2019-05-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ajouter une valeur par défaut aux champs menus déroulants liés pour éviter une erreur de validation a la création
- le message d'alerte en mode test apparait désormais en rouge

## Technique

- charger le code de `leaflet` dans un chunk séparé
- activer l'antivirus sur tous les attachements `active_storage`


--------------------------------------------

# 2019-04-30-01

Release [2019-04-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- amélioration du texte dans la page de login

## Technique

- Workaround pour réparer l'api insee qui ne marche pas


--------------------------------------------

# 2019-04-18-01

Release [2019-04-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Modification de texte dans la page de saisie d'un dossier
- Amélioration de l’icône de déconnexion
- Possibilité de changer le modèle d'un champ pj dans le manager
- Correction d'une erreur survenant lorsqu'un gestionnaire cherche à suivre plusieurs fois le même dossier
- Correction d'un bug dans la notification par mail qui produisait de fausses notifications


--------------------------------------------

# 2019-04-11-03

Release [2019-04-11-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-11-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Corrige un bug lors de l'envoi d'un email de notification si le logo de la démarche n'est pas récupérable

## Technique

- Correction d'un bug qui affectait la création de type de champ avec menu déroulant.


--------------------------------------------

# 2019-04-11-02

Release [2019-04-11-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-11-02)  sur GitHub

## Améliorations et correctifs

## Technique

- Correction d'un bug affectant l'envoi des emails automatiques avec un service


--------------------------------------------

# 2019-04-11-01

Release [2019-04-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : permet de filtrer des dossiers par email de l’instructeur
- Les emails de changement d'état mettent moins en avant DS et plus la démarche

### Mineurs

- Instructeur : masque les détails lors de l'alternance entre plusieurs motivations différentes


--------------------------------------------

# 2019-04-04-02

Release [2019-04-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-04-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : l'écran de réglage des notifications est plus clair

## Technique

- Utilise l'API INSEE v3 à nouveau


--------------------------------------------

# 2019-04-04-01

Release [2019-04-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Instructeur : affiche les avis en respectant les sauts de ligne
- Usager : correction d'une erreur lors de la navigation sur la page /commencer

## Technique

- Correction d'une erreur Javascript
- Envoie uniquement l'identifiant utilisateur à Sentry (mais plus l'email)


--------------------------------------------

# 2019-04-03-03

Release [2019-04-03-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-03-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : améliorations de l'éditeur de champs :
  - Les blocs sont mieux séparés
  - Ajout de bulles d'aides aux actions
  - Amélioration des boutons "Haut" et "Bas"
  - Amélioration du drag/drop
  - Le bouton "Ajouter un champ" a une icône
  - Élargissement de la barre d'enregistrement


--------------------------------------------

# 2019-04-03-02

Release [2019-04-03-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-03-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : affiche la description de la démarche sur les petits écrans
- Usager : Ajoute le bouton France Connect sur la page commencer
- Instructeur : Whitelist de réseaux pour éviter les connexions sécurisées (#3733)
- Administrateur : amélioration du nouvel éditeur de champ (#3647)

### Mineurs

- Administrateur : Amélioration de l'affichage d'un placeholder dans les services


--------------------------------------------

# 2019-04-03-01

Release [2019-04-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : affiche un message d'erreur pour informer de la panne d'API Entreprise
- Usager : améliore l'affichage des boutons d'action sur mobile
- Stats : ajuste les intervalles affichés

## Technique

- Déplace les analytics dans un pack séparé
- Renommage du fichier de license


--------------------------------------------

# 2019-04-02-01

Release [2019-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3625 réécriture de la page d'inscription des administrateur

## Technique

- #3722 suppression d'un code inutile concernant la cartographie
- #3718 correction d'un bug affectant les champs siret
- #3724 annulation du code qui affichait les dates en anglais


--------------------------------------------

# 2019-03-28-03

Release [2019-03-28-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-28-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3698 Validation du champ SIRET (il doit comporter exactement 14 chiffres)

## Technique

- #3702 factorisation des méthodes de clonage de démarches
- #3703 évite de générer une notification à l’instructeur lorsqu’une PJ est migrée


--------------------------------------------

# 2019-03-28-02

Release [2019-03-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-28-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateurs : possibilité de configurer une démarche pour les dépôts récurrents
- Usagers : ajout d'un bouton "Commencer un autre dossier" pour les démarches à dépôts récurrents
- Experts : Ajout de la possibilité de joindre un document à un avis

## Technique

- Correction de la tâche de migration des pièces jointes


--------------------------------------------

# 2019-03-28-01

Release [2019-03-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-28-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3563 corrige les problèmes pour accéder à un active storage local protégé par SSO lors du clonage d’une procédure comportant des « pièces jointes »
- #3349 amélioration du formatage des dates
- #3583 propose à l'usager d'envoyer une capture d'écran quand il demande du support
- #3646 ajout des contacts administrations au menu Aide des pages /commencer
- #3695 la bibliothèque de procédures passe au nouveau design
- #3696 redesign du menu de choix de rôle dans l’interface administrateur
- #3694 corrige la redirection pour s'inscrire sur une démarche de test

## Technique

- #3666 tâche rake permettant de migrer une procédure depuis les anciennes PJ vers les champs PJ
- #3697 les tâches after_party sont jouées quand on lance `bin/update` en local


--------------------------------------------

# 2019-03-27-01

Release [2019-03-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Envoi d'un email pour prévenir l'usager si son dossier est repassé en instruction
- Administrateurs : modification du texte de l'email de bienvenue
- Ajout d'une boite de confirmation avant la suppression d'une démarche en test
- Correction d'un bug qui empêchait le clonage des modèles de champs PJ

## Technique

- Correction du code appelant Sendinblue
- Renommage new_gestionnaire -> gestionnaire
- Renommage du repository GitHub


--------------------------------------------

# 2019-03-25-01

Release [2019-03-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-25-01)  sur GitHub

## Améliorations et correctifs
### Mineurs
- Administrateurs : les annotations privées peuvent maintenant comporter des champs répétables.
- Descriptif des démarches : correction de l'affichage de certains logos sous Chrome

## Technique
- Renommage de `new_user` en `users`
- Correction d'un bug lors du calcul de la durée d'instruction d'une démarche


--------------------------------------------

# 2019-03-20-03

Release [2019-03-20-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-20-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de l'affichage du logo "Démarches Simplifiées" en haut à gauche de la page.


--------------------------------------------

# 2019-03-20-02

Release [2019-03-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-20-02)  sur GitHub

## Améliorations et correctifs

- Usager : ajout d'un bouton d'aide contextualisé

### Mineurs

- Support : les comptes utilisateurs affichés par HelpScout sont maintenant insensibles à la casse de l'email
- API : les urls des pièces jointes sont tout le temps disponibles sauf fichier infecté


--------------------------------------------

# 2019-03-20-01

Release [2019-03-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-20-01)  sur GitHub

## Améliorations et correctifs
### Majeurs
- Usagers : correction de l'enregistrement des informations des champs SIRET dans un formulaire.

### Mineurs
- Usagers : correction de l'estimation du temps de passage en instruction des dossiers.

## Technique
- Mise à jour de Rails et Devise.


--------------------------------------------

# 2019-03-19-01

Release [2019-03-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Instructeur : vous avez la possibilité de recevoir un email journalier qui vous informe des nouveaux dossiers ou ceux récemment modifiés

### Mineurs

- Suppression de l'option SIRET pour les personnes physiques
- Affiche correctement le formulaire de contact sur mobile
- Correction d'un bug qui affectait la suppression de filtres instructeurs avec un email

## Technique

- Amélioration du layout à deux colonnes


--------------------------------------------

# 2019-03-18-01

Release [2019-03-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3604 Corrige le crash de l'app lors d'un SIRET en erreur
- Changement de terme sur l'archivate
- Suppression du délai exact de conformité RGAA
- Suppression des statistiques concernant les avis et les motivations
- #3611 Création de démarche : déplace le champ "Webhook" à la fin du formulaire
- #3615 augmentation du contraste

## Technique

- Modification de la configuration de puma pour permettre le mode cluster en production


--------------------------------------------

# 2019-03-12-02

Release [2019-03-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-12-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug affectant la page de garde pour les administrations


--------------------------------------------

# 2019-03-12-01

Release [2019-03-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-12-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ouverture de la fonctionnalité démarches de test à tout le monde
- Les instructeurs peuvent filtrer les dossiers avec plusieurs critères sur une même colonne. Les critères sont combinés avec un "OU". #3479
- Une démarche peut avoir plusieurs admin (beta)

### Mineurs

- Renommage de FAQ en Aide
- Amélioration du header sur mobile

## Technique

- Supprime une colonne plus utilisée 
- Suppression du code lié à la STI sur TypeDeChamp #3589
- Correction de test intermittent introduit par #3479
- Active FlipFlop pour tous les types d'utilisateurs


--------------------------------------------

# 2019-03-11-01

Release [2019-03-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration du texte dans les mails de connexions
- Ajout d'un batch d'envoi de statistique à notre prestataire de mail
- Amélioration du texte de la page d'accueil, dans les footers et dans un lien de signalisation de problème
- Ajout du template d'attestation dans la vue super admin
- Correction d'un style dans les emails qui abîmait les images
- Les templates ne proposent plus les tagues des champs statiques
- Les filtres de recherches sont maintenant triées

## Technique

- les liens taget blank sont maintenant protégés d'un rel=nooponer
- Utilisation de constante pour certains urls
- Correction d'un bug dans le tracking matomo
- Correction d'un bug qui faisait perdre la timezone
- Correction d'un bug qui affectait certaines balises dans les templates
- Simplification d'un chargement de données json en base


--------------------------------------------

# 2019-03-05-01

Release [2019-03-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Le champ région peut rester vide #3523 
- L'usager peut modifier son identité #3495 
- L'usager peut modifier son SIRET #1874  
- Ajout d'un lien vers la page d'historique de disponibilité du site


--------------------------------------------

# 2019-03-04-01

Release [2019-03-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-03-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un lien vers la faq pour les connexions sécurisée et modification du texte de l'email de connexion sécurisée

## Technique
- Correction d'un test
- Mise a jour de gems


--------------------------------------------

# 2019-02-28-01

Release [2019-02-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-28-01)  sur GitHub

## Améliorations et correctifs

## Technique

- ajout d'un index sur les types de pièces justificatives pour améliorer les performances
- augmentation de la durée de validité des urls temporaires d'upload pour corriger les problèmes d'upload des usagers avec une petite bande passante


--------------------------------------------

# 2019-02-26-02

Release [2019-02-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-26-02)  sur GitHub

## Améliorations et correctifs
### Majeurs
- #3513 Permet aux administrateurs sans démarche de changer de rôle

## Technique
- #3512 Corrige la création de procédures en mode multi-administrateur


--------------------------------------------

# 2019-02-26-01

Release [2019-02-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-26-01)  sur GitHub

## Améliorations et correctifs
### Mineurs
* Correction d'un bug affectant les PJ sans extension

## Technique
* Ajout d'un index sur la table `types_de_champ`


--------------------------------------------

# 2019-02-25-02

Release [2019-02-25-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-25-02)  sur GitHub

## Technique
* Ruby 2.6.1


--------------------------------------------

# 2019-02-25-01

Release [2019-02-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3375 L'API ne propose plus que les pièces jointes sans virus
- Ajout de liens vers la faq et la documentation dans le footer
- #3463 Ajout d'une balise nom du service
- #3370 Admin: meilleur affichage du lien de changement de profile
- Correction d'un bug sur l'affichage des pièces jointes
- #3489 Ajout d'un texte pour inciter les usagers à ne pas créer de compte admin
- #3446  formulaire de contact usager avec réponse automatique

## Technique

- Correction de tests
- Caché : plusieurs administrateurs sont liés à une procédure
- Amélioration du Readme
- Mise à jour de bootstrap


--------------------------------------------

# 2019-02-19-02

Release [2019-02-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-19-02)  sur GitHub

## Améliorations et correctifs
### Majeurs
* Amélioration de l'UI des fichiers uploadés

### Mineurs
* Je ne dois pas voir en prévisualisation les parcelles cadastrales si la case à cocher Cadastre n’a pas été cochée pour le champ « Carte » associé
* Ajout des liens de doc et de FAQ dans le footer

## Technique
* Amélioration de la vue super admin d'un dossier
* Tâche pour activer la fonctionnalité des démarches de test à grande échelle


--------------------------------------------

# 2019-02-18-01

Release [2019-02-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Les jetons d'authentification ont maintenant une durée de vie d'une semaine.

### Mineurs

- #3417 les champs départements peuvent avoir une valeur vide
- la barre de progression d'upload des fichiers est réparée
- #3427 les super admins peuvent supprimer un dossier
- #1140 wording sur la page attestation
- les super admin ont un lien vers l'aperçu des procédures et peuvent chercher des procédures par url
- #3315 mises à jour des organismes des services

## Technique

- correctif: affichage de la date de dépôt d'une dossier uniquement lorsque celle-ci est disponible


--------------------------------------------

# 2019-02-14-01

Release [2019-02-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-14-01)  sur GitHub

## Améliorations et correctifs
### Mineurs
- Amélioration du formatage du contenu des champs "Adresse"
- Utilisation du nom officiel de la République de Macédoine du Nord
- Usager : la date de dépôt du dossier est affichée clairement
- Usager : la surface de la parcelle est affichée dans les cartes cadastrales
- Usager : suppression du bouton "Nouvelle démarche", perturbant pour les utilisateurs
- Nouvel éditeur de champs : les boutons "Ajouter un champ" et "Enregistrer" sont visibles en permanence en bas de l'écran

## Technique
- Amélioration des instructions d'installation dans le README
- Déploiement : suppression d'une option inutilisée
- Formulaire de contact : le numéro de dossier est correctement transmis à HelpScout
- Statistiques : correction d'une erreur Javascript dans le code Matomo
- Envoi des pièces jointes via `www.demarches-simplifiees.fr` (plutôt qu'un sous-domaine)


--------------------------------------------

# 2019-02-11-01

Release [2019-02-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usager : possibilité de supprimer un dossier en construction
- [Nouvel éditeur de champ en beta] Amélioration de la fiabilité de l'enregistrement automatique

### Mineurs
- Amélioration de la gestion des erreurs lors de la modification d'une carte
- [Blocs répétables en beta] Correction du fonctionnement des champs "Adresse" dans un bloc répétable
- [Blocs répétables en beta] Amélioration de la prévisualisation du formulaire


--------------------------------------------

# 2019-02-06-01

Release [2019-02-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- le clonage d'une procédure conserve les champs à l'intérieur d'un bloc répétable

## Technique

- réutilisation de gems entre déploiement pour accélérer les mise en production


--------------------------------------------

# 2019-02-05-02

Release [2019-02-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-05-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- diverses améliorations dans la gestion des champs répétables (cachés pour l'instant)
- ajout de tags pour sélectionner la première ou la deuxième valeur d'un champ deux listes déroulantes liées
- correction d'un bug qui affectait la description coté usager d'un dossier lié

## Technique

- le job clamav loggue à présent les erreurs qu'il rencontre


--------------------------------------------

# 2019-02-05-01

Release [2019-02-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-05-01)  sur GitHub

## Technique

- Ajout de logs techniques sur l'analyse antivirus


--------------------------------------------

# 2019-02-04-01

Release [2019-02-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-04-01)  sur GitHub

## Nouveautés

- Bloc répétable


--------------------------------------------

# 2019-02-01-01

Release [2019-02-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Meilleur affichage des statistiques de satisfaction usager
- [fix #3282] Les champs datetime peuvent avoir une valeur vide

## Technique

- Meilleure comparaison de date pour le mécanisme de connexion sécurisé


--------------------------------------------

# 2019-01-30-01

Release [2019-01-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-30-01)  sur GitHub

## Améliorations et correctifs
### Mineurs

- Répare la génération d’attestation comportant des caractères spéciaux (hors Windows 1252)


--------------------------------------------

# 2019-01-29-01

Release [2019-01-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-29-01)  sur GitHub

## Améliorations et correctifs
### Mineurs

- API : correction d'un bug qui changeait le formatage des champs Oui/Non
- Carto : Affichage des erreurs d'API carto à l'usager 
- Attestations : Changements de la font et de l'alignement horizontal pour respecter la charte de l'Etat
- Champ téléphone : il y a désormais une validation sur les champs numéro de téléphone
- Suppression du bouton pour supprimer un brouillon de la page de détail d'un dossier
- Suppression d'un bug qui empêchait parfois d'enregistrer la description d'une procédure clonée
- Suppression d'un bug qui affectait les super administrateurs


--------------------------------------------

# 2019-01-23-03

Release [2019-01-23-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-23-03)  sur GitHub

## Technique

- Corrige la création de dossiers sans démarches lors de la suppression d'une démarche en test
- Supprime les dossiers sans démarches en production


--------------------------------------------

# 2019-01-23-02

Release [2019-01-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-23-02)  sur GitHub

## Améliorations et correctifs
### Mineurs

- Administrateur : clarification d'un texte sur la page de création d'une démarche
- Instructeur : correction d'un plantage lors de certaines recherches
- Usager : ajout de la date de création du dernier dossier sur la page de garde d'une démarche
- Amélioration de la fiabilité des statistiques récoltées


--------------------------------------------

# 2019-01-23-01

Release [2019-01-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-23-01)  sur GitHub

## Nouveautés

- Les instructeurs peuvent maintenant prévisualiser l'attestation qui sera envoyée à l'usager avant d'accepter un dossier.

## Améliorations et correctifs

### Mineurs
- Formulaire de contact: correction de l'envoi de messages sur la messagerie du dossier
- Augmentation de la taille du texte sur la page de garde des démarches


--------------------------------------------

# 2019-01-21-01

Release [2019-01-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-21-01)  sur GitHub

## Nouveautés

- Usager : les démarches ont maintenant une page d'informations dédiée (plutôt que de rediriger directement vers le formulaire de connexion).
- Administrateur : ajout d'un nouveau type de champ « SIRET », pour obtenir les informations complètes d'une entreprise.

## Améliorations et correctifs

### Majeurs

- Usager : quand l'usager a déjà commencé un brouillon ou déposé un dossier, la page d'informations de la démarche lui propose de retrouver le dossier en cours (plutôt que d'en commencer un nouveau).
- Usager : un nouveau brouillon n'est plus automatiquement créé quand on clique sur le lien de la démarche ; l'usager doit cliquer explicitement sur un bouton pour créer un brouillon.
- Usager : les dossiers en brouillons peuvent maintenant être supprimés depuis la liste des dossiers.

### Mineurs

- Administrateur : lorsqu'une démarche est clonée, les pièces justificatives sont automatiquement converties en champs « Pièce jointe ».
- Administrateur : correction d'une coquille sur la page de demande de compte.
- Administrateur : ajout d'un texte explicatif sur la suppression des attestations.

## Technique

- Suppression du feature-flag pour le formulaire de support
- Correction d'une erreur lors de l'initialisation du nouvel éditeur de champs
- Correction d'une erreur du nouvel éditeur de champ lors de l'édition d'anciennes démarches
- Ajout d'une moulinette pour corriger le suivi des dossiers processés par un job automatique


--------------------------------------------

# 2019-01-16-01

Release [2019-01-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-16-01)  sur GitHub

### Améliorations et correctifs
#### Majeurs

- Évite d’exposer les adresses en `ovh.net` pour les champs pièce jointe


--------------------------------------------

# 2019-01-10-03

Release [2019-01-10-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-10-03)  sur GitHub

### Améliorations et correctifs
#### Mineurs

- Correction du calcul des statistiques de satisfaction sur la page https://www.demarches-simplifiees.fr/stats


--------------------------------------------

# 2019-01-10-02

Release [2019-01-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-10-02)  sur GitHub

## Correctifs

- correction d'un bug qui affectait la récupération par API d'un dossier lorsqu'une pièce jointe était absente


--------------------------------------------

# 2019-01-10-01

Release [2019-01-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- un administrateur qui clone une procédure y est automatiquement affecté en tant qu'instructeur
- l'interface administrateur affiche plus clairement les étapes manquantes pour pouvoir publier la démarche
- les mails de connexion sécurisée sont dorénavant uniquement envoyé toutes les 15 minutes

## Technique

- diverses nettoyages de code
- certains logs ne sont plus affichés lors de tests
- dans les tests, la création d'un administrateur créé le gestionnaire associé
- correction d'un bug qui affectait les text area dans la vue usager et instructeur


--------------------------------------------

# 2019-01-08-01

Release [2019-01-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-08-01)  sur GitHub

## Améliorations et correctifs
### Majeurs

- Lorsqu'une nouvelle démarche est créée, l'Administrateur est automatiquement affecté en tant qu'Instructeur de la démarche.

### Mineurs

- Les statistiques de contact usager sont calculées à partir du 1er janvier 2018
- Administrateurs :
  - Améliorations cosmétiques sur la page "Nouvelle démarche"
  - À la connexion, les Administrateurs n'ayant aucune sont redirigés vers la page "Nouvelle démarche"
  - Les liens dans la colonne de gauche sont ordonnés de manière plus logique

- Usagers :
  - Dans la messagerie, le bouton "Envoyer un message" est correctement affiché sur mobile
  - Améliorations cosmétiques aux pages de Connexion et d'Inscription
  - Ajout d'une marge en bas de la page "Merci d'avoir soumis votre demande"
 
## Technique

- Activation de plus de règles Rubocob
- Renommage des tâches "Admin" en "Superadmin"
- Suppression des Décorateurs
- La page /stats ne plante plus quand on la visite sans identifiants HelpScout
- Remplacement de la gem `nacl-libsodium` par `ActiveSupport::MessageVerifier`
- Suppression de la table inutilisée `rna_informations`
- Bump des dépendances
- Suppression des dépendances inutilisés `web-console` et `rack-handlers`
- Améliorations cosmétiques au Gemfile
- Remontée de l'état du parcours Administrateur à SendInBlue
- Unification du layout entre les pages "Siret" et "Identité"


--------------------------------------------

# 2018-12-26-01

Release [2018-12-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-26-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil Instructeur, correction d'une vulnérabilité dans la connexion par email

### Mineurs

- Dans le profil Administrateur, le lien de contact dans le header ne redirige plus vers notre adresse email mais vers le formulaire de contact administrateur
- Dans le Manager, le numéro de téléphone est désormais affiché sur la page des demandes de compte administrateur
- Dans le Manager, le pipe par défaut sur la page des demandes de compte administrateur est désormais "supect"
- Amélioration du style graphique de plusieurs emails
- Amélioration des textes de plusieurs emails

## Technique

- Ajout de nombreuses previews d'emails
- Une partie des previews d'emails est désormais consultable avec une BDD vide


--------------------------------------------

# 2018-12-20-01

Release [2018-12-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-20-01)  sur GitHub

## Améliorations et correctifs
### Majeurs
- #3194 et #3206 corrige les services partagés par erreur entre administrateurs

### Mineurs
- #3199 homogénéisation des signatures des mails automatiques
- #3204 liens cliquables dans les mails
- #3173 applique le même style graphique à tous les emails
- #3202 invite les administrateurs à nous contacter avant de faire une demande d’inscription avec un mail grand public (type wanadoo, gmail...)

## Technique
- #3186 suppression de code mort
- #3177 modèle en vue des champs répétables
- #3197 évite le double encodage YAML + JSON des options d’un type de champ
- #3201 le scope type de champ est maintenant trié par défaut


--------------------------------------------

# 2018-12-19-02

Release [2018-12-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-19-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Suppression du bandeau sur le Tour de France sur la landing administrateur
- Dans le profil Administrateur, dans l'onglet "Pièces jointes" d'une démarche, ajout d'un message invitant l'administrateur à utiliser les champs "pièce justificative"
- Le lien d'inscription à la newsletter dans le footer pointe désormais vers la page d'inscription de SendInBlue

## Technique

- Utilisation d'un `link_to` dans une vue à la place d'un lien `%a`
- Ajout d'une tache de traitement de balises obsolètes
- Bump de rubocop
- Activation de plusieurs cops Rubocop
- Ajout de colonnes timestamps aux tables qui n'en avaient pas


--------------------------------------------

# 2018-12-19-01

Release [2018-12-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil instructeur, amélioration du placeholder du champ "motivation", qui explicite désormais que la motivation sera visible par l'usager

## Technique

- Suppression du code lié à la gestion d'ActiveStorage sur CleverCloud, maintenant que nous sommes sur un OpenStack
- Simplification et amélioration de plusieurs parties du code
- Suppression de tables désormais inutilisées
- Renommage d'un module pour plus de clarté


--------------------------------------------

# 2018-12-18-01

Release [2018-12-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil administrateur, le type de champ "pièce jointe" est désormais disponible à tous les administrateurs

### Mineurs

- Dans les statistiques, ajout du taux de contact
- Dans le manager, affichage du formulaire "annotations privées"

## Technique

- Harmonisation d'une route de suppression de pièce jointe
- Ajout d'un test pour tester que l'inscription avec un mot de passe trop court renvoie une erreur


--------------------------------------------

# 2018-12-14-01

Release [2018-12-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-14-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui empêchait l'affichage de la page "annotations privées" sur un dossier avec un champ pièce justificative rempli


--------------------------------------------

# 2018-12-12-01

Release [2018-12-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil instructeur, correction d'une faute d'orthographe sur la page de confirmation de dépôt de dossier


--------------------------------------------

# 2018-12-11-01

Release [2018-12-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #3123 Les noms de fichier des pièces jointes en champ sont préservés

### Mineurs

- #3159 Clarifie la formulation du message affiché à l’usager lors du passage en construction


--------------------------------------------

# 2018-12-10-02

Release [2018-12-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-10-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil instructeur, correction d'un bug d'ordre des colonnes sur les exports CSV et XLS
- Dans le profil administrateur, activation du type de champ "listes liées" pour tous les administrateurs

## Technique

- On cache désormais les requêtes à l'API Géo
- Ajout d'un CONTRIBUTING.md


--------------------------------------------

# 2018-12-10-01

Release [2018-12-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-10-01)  sur GitHub

## Technique
- #3138 Passage à l’API INSEE V3 pour récupérer des résultats plus complets
- #3148 Optimisation du script de migration des PJ


--------------------------------------------

# 2018-12-07-03

Release [2018-12-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-07-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #3144 Dans le profil administrateur, correction d'un bug affectant le clonage des démarches

## Technique

- #3145 Amélioration du script de migration des PJ


--------------------------------------------

# 2018-12-07-02

Release [2018-12-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-07-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #3142 Dans le profil administrateur, correction d'un bug affectant le clonage des démarches

## Technique

- #3141 Amélioration du script de migration des PJ


--------------------------------------------

# 2018-12-07-01

Release [2018-12-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil instructeur, après la connexion, l'instructeur est redirigé sur la page à laquelle il souhaitait accéder
- Dans le profil usager, il est désormais possible de supprimer les pièces jointes ajoutées sur les champs de type "pièce jointe"

## Technique

- Le clonage d'une démarche ne garde plus le "lien notice" qui n'est plus utilisé dans les nouvelles démarches
- Migration de Rails en 5.2.2
- Ajout de statistiques concernant les pages vues par les usagers


--------------------------------------------

# 2018-12-05-01

Release [2018-12-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-05-01)  sur GitHub

## Nouveautés

- #3046 (Instructeurs) les informations SIRET sont maintenant incluses dans l’export
- #3111 (Instructeurs) les instructeurs peuvent maintenant voir les démarches en test

## Technique

- #3118 Mémorise l’identité des auteurs des messages sur les dossiers
- #3112 script de migration des PJs vers le nouveau fournisseur de stockage
- #3121 passage de sass-rails à sassc-rail
- #3120 faire tourner after_party en post_deploy plutôt qu’au cours du script de déploiement
- #3117 mise à jour de webpacker(babel 7)
- #3114 bump de gemmes
- Suppression de code legacy


--------------------------------------------

# 2018-12-04-01

Release [2018-12-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil instructeur, lorsque l'on invite plusieurs experts à donner leur avis, les points virgule qui séparent les emails sont désormais correctement gérés
- Dans le profil administrateur, le champ "date de fermeture automatique" de la démarche est désormais un champ date, ce qui facilite son remplissage
- Dans le profil administrateur, correction de vocabulaire sur les pages de gestion des Services

## Technique

- Mise à jour de Rails en version 5.2.1.1
- Bump de capybara et capybara-screenshot


--------------------------------------------

# 2018-12-03-01

Release [2018-12-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de bugs sur les champs de type "Carte"


--------------------------------------------

# 2018-12-01-01

Release [2018-12-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-12-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de bugs sur les champs de type "Carte"
- Correction d'un bug sur l'export en GeoJSON de l'API

## Technique

- Désactivation d'after_party


--------------------------------------------

# 2018-11-29-01

Release [2018-11-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de bugs sur l'export en GeoJSON de l'API
- Correction d'un bug la validation sur les types de champ nombre entier et nombre décimal
- L'API expose désormais des ids stables pour les types de champ

## Technique

- Utilisation de containers différents pour ActiveStorage et Carrierwave


--------------------------------------------

# 2018-11-28-01

Release [2018-11-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil administrateur, le bug qui empechait la publication d'une démarche sur laquelle des dossiers de test avaient été déposés est corrigé

### Mineurs

- Ajout d'une page de contact dédiée aux administrateurs
- Amélioration de la landing administrateurs
- Correction d'un bug sur la suppression de pièces jointes

## Technique

- Les développeurs peuvent désormais s'ils le souhaitent overrider `config.active_job.queue_adapter`
- Les attestations sont désormais supprimées lorsque le dossier auquel elles sont rattachées sont supprimées
- Les traces des opérations sont désormais supprimées lorsque le dossier auquel elles sont rattachées sont supprimées


--------------------------------------------

# 2018-11-27-03

Release [2018-11-27-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-27-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration du footer des emails de notification

## Technique

- Suppression de fichiers et de code obsolètes suite à la migration de cartographie sur des champs


--------------------------------------------

# 2018-11-27-02

Release [2018-11-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-27-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Suppression de footers désormais obsolètes sur les messages envoyés. Ils n'apparaitront plus dans la messagerie.


--------------------------------------------

# 2018-11-27-01

Release [2018-11-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration du style de l'email de demande d'avis
- Corrections de bugs sur le champ Carte
- Correction d'un bug sur les pièces jointes

## Technique

- On ne requiert plus `mina` par défaut, ce qui rend la sortie des taches rake plus propre
- Déplacement de code sur le modèle
- On trace désormais les changements de statut d'un dossier
- Autorisation de l'utilisation de variables d'environnement pour overwriter certaines constantes


--------------------------------------------

# 2018-11-23-01

Release [2018-11-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- #3047 La page super-admin d’un dossier comporte maintenant un lien vers la page usager
- #3049 Quand un expert répond à une demande d’avis public, on lui annonce bien que celui-ci sera public et non privé

## Technique
- #3033 suppression de la colonne `commentaires.champ_id`
- #3052 Lors d’une mise en production, les anciennes releases sont automatiquement effacées pour éviter de saturer le disque


--------------------------------------------

# 2018-11-22-03

Release [2018-11-22-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-22-03)  sur GitHub

## Technique

- Préparation de la suppression de la colonne `champ_id` sur la table `commentaires`


--------------------------------------------

# 2018-11-22-02

Release [2018-11-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-22-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil instructeur, correction d'un bug sur les notifications suite à l'ajout des champs Carte

### Mineurs

- Dans le profil usager, on affiche désormais le pied de page sur la page de confirmation de soumission de dossier
- Dans la manager, correction d'un bug lors de la suppression de démarches

## Technique

- Suppression de toute référence aux facades, désormais inexistantes


--------------------------------------------

# 2018-11-22-01

Release [2018-11-22-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-22-01)  sur GitHub

## Technique

- Ajout de paramètres pour pouvoir suivre les soumissions de formulaires de contact et de demande de compte administrateur
- Correction du nom d'un dossier
- Dans les vues, généralisation de l'utilisation de `l` au lieu de `I18n.l`
- Conversion du dernier email encore au format texte au format HTML
- Suppression de vieux fichiers PDF inutilisés
- Suppression d'une route morte
- Suppression d'un fichier inutile
- Suppression de toute référence à `unicorn` car nous utilisons désormais `puma`
- Déplacement de la gestion de deux pages statiques dans le `RootController` au lieu de controlleurs dédiés
- Amélioration d'une route


--------------------------------------------

# 2018-11-21-01

Release [2018-11-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-21-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Les images incluses dans nos emails s'affichent désormais correctement
- Dans le profil instructeur, il est désormais possible d'accéder aux dossiers qui comportent des pièces jointes sans extensions

### Mineurs

- Correction de bugs sur le champ "Carte"
- Dans le manager, amélioration de l'affichage des démarches
- Dans le manager, ajout d'un bouton de suppression des démarches

## Technique

- Correctif d'un test qui échouait
- Modification de robots.txt pour que les moteurs de recherche ne référencent pas les liens directs vers les démarches


--------------------------------------------

# 2018-11-20-01

Release [2018-11-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil administrateur, correction d'un bug lors du filtrage de la liste des instructeurs sur la page "Instructeurs" d'une démarche
- Pour les usagers, amélioration du message affiché en bas de l'email de notification de nouveau message

## Technique

- Ajout d'une tache qui migre les modules cartographiques vers les nouveaux champs carte
- Refactor de certains mailers


--------------------------------------------

# 2018-11-16-03

Release [2018-11-16-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-16-03)  sur GitHub

## Améliorations et correctifs
### Majeurs
- #2999 Mauvaise page après connexion d’un instructeur


--------------------------------------------

# 2018-11-16-02

Release [2018-11-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-16-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- La page d'erreur 500 est reprend désormais le message de notre site et affiche un message en français

## Technique

- Bump de plusieurs gems liés à la gestion des fichiers uploadés en vue de la migration totale vers de l'upload sur nos serveurs via ActiveStorage et OpenStack
- Bump de plusieurs gems (ce qui permet par ailleurs de ne plus avoir une version de rake qui présente une vulnérabilité)


--------------------------------------------

# 2018-11-16-01

Release [2018-11-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil usager, les estimations de temps de traitement se basent désormais uniquement sur les dossiers traités pendant les 30 derniers jours
- Dans le profil instructeur, correction de l'affichage des filtres dont le libellé est très long
- Dans le manager, affichage de "Démarche" et "Instructeurs" au lieu de "Procédures" et "Gestionnaires"
- Dans le manager, les features flags du groupe "production" sont désormais cachés des pages administrateurs
- Dans le manager, les features "pre_maintenance_mode" et "maintenance_mode" sont désormais dans le groupe "production"

## Technique

- Les traces Rake sont désormais totalement désactivées dans les tests
- Refactor de code et de tests
- Amélioration de la robustesse de plusieurs test
- Déplacement de code relatif aux invitations
- Configuration de CircleCI pour qu'il affiche les commandes utilisées pour le lancement des tests afin de pouvoir plus facilement reproduire les cas qui échouent
- Rspec utilise désormais l'option `:shell` pour les bisect afin d'éviter des échecs sur les tests de feature


--------------------------------------------

# 2018-11-13-01

Release [2018-11-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Pour les administrateurs et les instructeurs, envoi d'un email de vérification mensuel lors de la connexion, et envoi immédiat lors de la connexion à partir d'un nouveau navigateur.

### Mineurs

- Dans le profil Usager, ajout d'une page à propos de la confirmation du compte suite à l'inscription
- Dans le profil Usager, les temps de traitements sont désormais basés sur le 90e percentile au lieu de la moyenne 
- Amélioration de la présentation du footer


--------------------------------------------

# 2018-11-08-01

Release [2018-11-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil Usager, quand un usager clique sur le lien d'activation de son compte dans l'email qu'on lui a envoyé, il est désormais automatiquement connecté à son compte
- Dans le manager, les éléments affichés sont désormais de nouveau triés par date de création
- Ajout d'analyse d'audience avec Matomo

## Technique

- Amélioration des performances de l'API
- Suppression de toutes les facades
- Ajout d'une tache de traitement de masse des dossiers d'une démarche particulière
- Correction d'une tache rake
- Refactor de l'affichage du `state` des dossiers


--------------------------------------------

# 2018-11-06-01

Release [2018-11-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-11-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ajout des types de champ "nombre décimal" et "champ cartographique"
- Dans le profil instructeur, il est désormais possible d'envoyer des demandes d'avis à plusieurs expert à la fois

### Mineurs

- Correction d'un bug qui provoquait un crash lors de la suppression d'un logo d'une démarche en prod
- Correction d'un bug qui provoquait une mauvaise valeur dans la sérialisation de champs de type "oui / non" dans l'attestation

## Technique

- Déplacement de la page commencer `commencer` dans `new_user`
- Suppression de la table `procedure_paths`
- Suppression de tache relative à l'infra structure de demarches-simplifiees.fr
- Mise a jour de gem `loofah`
- Variabilisation de l'URL de l'API Géo


--------------------------------------------

# 2018-10-31-02

Release [2018-10-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-31-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug dans l'API qui empêchait d'obtenir le détail d'un dossier dont au moins un message ne comportait pas de pièce jointe.


--------------------------------------------

# 2018-10-30-01

Release [2018-10-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-30-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil Usager, amélioration du message d'aide concernant les pièces jointes
- Dans le profil Administrateur, on ne perd plus les données renseignées dans le formulaire en cas d'erreur lors de la création d'un service
- Dans l'API, on expose désormais la `geo_reference_id` associée à une GeoArea, quand elle existe
- Dans l'API, on expose désormais les pièces jointes associées aux messages
- Dans le manager, suppression du bouton de confirmation de compte

## Technique

- Suppression de `ProcedurePath`
- Refactor de la gestion du path d'une démarche
- Lorsqu'une recherche concerne une date, on ne considère plus l'heure associée à la date
- Simplification de la syntaxe de certains tests
- Correction d'un test peu fiable
- Ajout de `geo_reference_id` sur GeoArea


--------------------------------------------

# 2018-10-26-02

Release [2018-10-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-26-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug qui affectait la liste des dossiers lorsqu'on le filtrait avec un nombre sur une date


--------------------------------------------

# 2018-10-26-01

Release [2018-10-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un avertissement sur les dossiers liés à une démarche en brouillon
- Correction de bugs relatifs aux fuseaux horaires

## Technique

- Gros passage sur la gestion des dates et temps dans l'application
- Les pièces jointes sont désormais accessibles à l'adresse `static.demarches-simplifiees.fr`
- Le serveur web de développement utilise le port 3000 avec overmind


--------------------------------------------

# 2018-10-25-01

Release [2018-10-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-25-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil Instructeur, correction d'un bug sur le tri des dossiers par présence de notifications
- Correction d'un bug sur le module cartographique lorsqu'aucun tracé n'est réalisé par l'usager
- Dans le profil Administrateur, amélioration de la description du webhook

## Technique

- Refactor des scripts de déploiement
- Refactor de `urls.rb`


--------------------------------------------

# 2018-10-24-01

Release [2018-10-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug affectant les formulaires avec plusieurs dossiers liés

## Technique

- Suppression du code de déploiement par CircleCI
- Mise en place du déploiement sur la nouvelle infra
- Refactor des scripts de déploiement
- Passage au serveur web puma
- Bump de `mina`


--------------------------------------------

# 2018-10-23-02

Release [2018-10-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-23-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Les champs de type Carte peuvent désormais remonter les parcelles agricoles (issues du RPG)

### Mineurs

- Dans le profil instructeur, meilleure gestion des préférences d'affichage du tableau en cas de problème (changement du formulaire, etc.)


--------------------------------------------

# 2018-10-23-01

Release [2018-10-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans l'API, on expose désormais les informations récupérées pour les champs de type “SIRET”
- Dans l'API, on expose désormais les informations récupérées pour les champs de type “Carte”
- Dans l'API, on expose désormais le “state” d’une démarche
- Ajout du champ Carte, caché pour le moment

### Mineurs

- Dans le profil Usager, correction d’erreurs dans le module cartographique
- Dans le profil Instructeur, dans le header, “Contact” a été renommé en “Aide”
- Dans le profil Instructeur, le pop-over d’aide ne contient plus notre numéro de téléphone
- Dans le profil Instructeur, amélioration du message dans le pop-over d’aide
- Amélioration de l’email de création de compte administrateur
- Amélioration de l’email de création de compte instructeur
- Dans l’email de refus de création de compte administrateur, on invite désormais l’utilisateur à passer par notre formulaire de contact

## Technique

- Refactor de la gestion de la cartographie pour partager plus de code entre le champ et le module
- Explicitation d’une foreign_key sur `Champs::CarteChamp`
- Léger refactor de `ProcedureSerializer` et `Procedure`


--------------------------------------------

# 2018-10-18-02

Release [2018-10-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-18-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil Instructeur, les dossiers archivés sont désormais remontés par la recherche 
- Dans le profil Instructeur, l'état de suivi / d'archivage d'un dossier est désormais affiché dans les résultats de recheche
- Dans le profil Administrateur, on affiche désormais notre adresse email et notre numéro de téléphone dans le header

## Technique

- Suppression de code de l'ancienne UI Usager, désormais mort
- Documentation de l'utilisation de letter_opener dans le README


--------------------------------------------

# 2018-10-18-01

Release [2018-10-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil administrateur, amélioration de la partie "Cadre juridique" de la description d'une démarche
- Administration des performances de la page Statistiques pour les utilisateurs non-super-administrateurs
- Correction d'une coquille sur la page de réinitialisation de mot de passe Usager
- On cache désormais, dans l'onget Résumé, les temps moyens pour les dossiers d'une démarche particulière


--------------------------------------------

# 2018-10-17-02

Release [2018-10-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-17-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil Instructeur, il est désormais possible de personnaliser, trier et filtrer le tableau avec "en construction le", date à laquelle un dossier est passé en construction
- Dans le profil Usager, les exercices remontés via l'API Entreprise depuis un numéro SIRET ne sont plus affichés aux usagers

## Technique

- Étoffement de plusieurs tests


--------------------------------------------

# 2018-10-17-01

Release [2018-10-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans le profil Usager, la page SIRET est désormais dans le style de la nouvelle UI

### Mineurs

- Dans le profil Administrateur, correction d'un bug lors de la création d'une démarche invalide avec un logo, une notice ou un fichier juridique

## Technique

- Correction d'un bug dans le script de déploiement qui inhibait la tache after_party


--------------------------------------------

# 2018-10-16-07

Release [2018-10-16-07](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-07)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug sur les stats

## Technique

- Mise à jour de divers gems


--------------------------------------------

# 2018-10-16-06

Release [2018-10-16-06](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-06)  sur GitHub

## Technique

- Ajout d'une tache pour migrer des dossiers de démarches supprimées vers les démarches qui les ont remplacées
- Déplacement de fichiers de `/spec/support` vers `/spec/fixtures`
- Ajout du champ carte et du modèle GeoArea


--------------------------------------------

# 2018-10-16-05

Release [2018-10-16-05](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-05)  sur GitHub

## Technique

- Refactor et renommante de `Ban::SearchController` en `AddressController`
- Simplification de `ApiCarto::API.call`
- Suppression des options de bypass vérifications SSL lors de certains appels à des APIs externes


--------------------------------------------

# 2018-10-16-04

Release [2018-10-16-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-04)  sur GitHub

## Technique

- Simplification de scopes : `ApiCarto::Cadastre::Adapter → ApiCarto::CadastreAdapter`, `ApiCarto::
QuartiersPrioritaires::Adapter` → `ApiCarto::QuartiersPrioritairesAdapter`
- Unification de la gestion du scoping
- Renommage des classes `Driver` en `API`
- Refactor de `ApiAdresse`
- Organisation de fichiers de mock dans des dossiers
- Renommage des classes `Retriever` en `Adapter`
- Ménage (suppression de méthodes inutiles, suppression de commentaires, ajouts de lignes vides)


--------------------------------------------

# 2018-10-16-03

Release [2018-10-16-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-03)  sur GitHub

## Technique

- Scission de `lib/carto` en `lib/api_geo`, `lib/api_adresse`, `lib/api_carto`
- Utilisation de parenthèses
- Extraction de l'adresse de l'API Adresse dans une constante, et utilisation du HTTPS pour cette dernière
- Légers refactors
- Suppression d'une cassette dupliquée
- Ajout d'un délai (debounce) sur les requêtes de suggestions d'adresses pour ne pas trop solliciter notre API


--------------------------------------------

# 2018-10-16-02

Release [2018-10-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de la performance de la page Statistiques
- Ajout de messages d'état vide pour la cartographie

## Technique

- Tests des taches rake déplacés de `/spec/lib/rake` à `/spec/lib/tasks`
- Suppression de cassettes inutilisées
- Refactor de `Carto::SGMAP` et `ModuleApiCartoService`
- Suppression de code mort
- Ajout de parenthèses pour les appels de méthodes


--------------------------------------------

# 2018-10-16-01

Release [2018-10-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil administrateur, correction de messages affichés après modification d'une démarche

## Technique

- Refactor conséquent du module de carto qui utilise désormais ujs
- Suppression de code mort


--------------------------------------------

# 2018-10-12-01

Release [2018-10-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2818 Correction d'un bug ou les usagers pouvaient vider un champ obligatoire d'un dossier soumis


--------------------------------------------

# 2018-10-11-01

Release [2018-10-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #2805 Dans la page de connexion Usager, le bouton d'inscription a été remonté pour être plus visible
- #2810 Dans le profil instructeur, les champs prénom, nom et civilité du demandeur sont désormais disponibles pour le filtrage, le tri et la personnalisation du tableau
- #2803 Dans le profil instructeur, correction du filtrage par date de création et date de mise-à-jour des dossiers

### Mineurs

- #2804 Dans le profil instructeur, correction d'un bug où l'URL de la démarche s'affichait dans le tableau de dossiers à la place des valeurs vides
- #2812 Dans le manager, dans la liste de démarches, affichage du service à la place de l'organisation

## Technique

- #2811 Suppression de code mort


--------------------------------------------

# 2018-10-10-03

Release [2018-10-10-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-10-03)  sur GitHub

## Technique

- #2794 Suppression de InviteGestionnaire et InviteUser, il ne reste plus que des Invite (qui concernent uniquement les Usagers)
- #2726 Suppression de l'utilisation de jQuery dans une bonne partie de notre code JavaScript


--------------------------------------------

# 2018-10-10-02

Release [2018-10-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-10-02)  sur GitHub

## Technique

- #2793 Suppression des InviteGestionnaire en base en vue de leur suppression du code
- #2791 Suppression d'une facade inutile
- #2795 Refactor de code


--------------------------------------------

# 2018-10-10-01

Release [2018-10-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-10-01)  sur GitHub

## Technique

- #2756 Tâche rake de support pour transférer toutes les procédures d’un administrateur
- #2789 Suppression de code mort
- #2770 Vérification par le modèle de la validité des filtres instructeurs


--------------------------------------------

# 2018-10-09-01

Release [2018-10-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- #2781 Les usagers peuvent désormais déposer des dossiers sur les démarches en brouillon
- #2778 Dans l'API, l'attribut `state` des dossiers est désormais exposé

### Mineurs

- #2784 Dans le profil Administrateur, suppression du bouton de suppression d'une démarche
- #2783 La doc de l'API est désormais hébergée sur GitBook
- #2780 Dans la manager, ajout d'un bouton pour repasser une démarche en brouillon

## Technique

- #2783 Suppression de `apipie` et `maruku`
- #2787 Ajout de taches pour réenvoyer les mails envoyés par Mailjet avec une IP de confiance faible


--------------------------------------------

# 2018-10-08-01

Release [2018-10-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2776 Correction d'une date sur la page du Tour de France

## Technique

- #2773 Ajout d'une tache pour changer l'adresse email d'un compte
- #2774 Ajout d'une tache pour changer le numéro SIRET d'un dossier
- #2732 Refactor de la gestion du code JavaScript pour l'ancienne UI


--------------------------------------------

# 2018-10-05-01

Release [2018-10-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-05-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

-  #2764 Les champ de type “lien dossier” acceptent désormais les numéros de dossier dont l’utilisateur n’est pas le créateur
- #2758 Dans le profil Instructeur, il n’est plus possible de demander un avis une fois le dossier passé dans un état final

## Technique

- #2761 Utilisation de `letter_opener_web` à la place de `mailcatcher`
- #2763 Refactor de `DossierFieldService` pour corriger des tests qui échouait de manière aléatoire
- #2759 #2766 Meilleure gestion des réponses partielles aux appels à l’API Entreprise
- https://github.com/betagouv/tps/commit/d93f69693581075d37a095b82ee82a897060ad23 https://github.com/betagouv/tps/commit/22c132febed91ba6d08d6d0b559f4af9234b9607 https://github.com/betagouv/tps/commit/0927905d2e03ebb762f8a21ee0e2b047c391c9a0 Amélioration du script de deploy


--------------------------------------------

# 2018-10-04-02

Release [2018-10-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-04-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

* #2720
  * Dans le profil instructeur :
    * les colonnes civilité, prénom et nom ne sont plus disponibles dans les préférences d'affichage, de filtrage et de tri des tableaux de dossiers
    * la recherche ne cherche plus dans les champs prénom et nom fournis par France Connect
  * Dans le profil usager, suppression temporaire du code de remplissage automatique de l'identité à partir des informations FranceConnect

## Technique

* #2720
  * Sur User, suppression de la délégation de plusieurs attributs à FranceConnectInformation
  * Ajout d'une tache pour supprimer les champs relatifs à France Connect des préférences d'affichage, de filtrage et de tri des tableaux de dossiers instructeurs


--------------------------------------------

# 2018-10-04-01

Release [2018-10-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2581 Pour le profil instructeur, le changement de statut d'un dossier ne recharge plus la page du dossier

## Technique

- #2754 Ajout des informations d'intégrité au fichier `yarn.lock`


--------------------------------------------

# 2018-10-03-03

Release [2018-10-03-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-03-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2743 Migration des pages de session Usager vers la nouvelle UI
- #2689
  * Dans le profil administrateur, l'onglet "Informations" s'appelle désormais "Publication"
  * Dans l'onglet "Publication", suppression des infos redondantes sur la démarche
- #2752 Correction de régression (tri instructeur sur l’id et le statut d’un dossier)


--------------------------------------------

# 2018-10-03-02

Release [2018-10-03-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-03-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2744 Dans le profil Administrateur, ajout d'un texte qui invite les administrateurs à ne diffuser que le lien vers leur démarche

## Technique

- #2747 Suppression d'une colonne désormais obsolète
- #2722 Activation de plusieurs cops Rubocop (et modifification du code invalide)


--------------------------------------------

# 2018-10-03-01

Release [2018-10-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2733 Dans le profil instructeur, le bouton d'enregistrement des annotations privées est désormais plus visible
- #2737 Dans le manager, affichage du service des démarches
- #2736 Dans le manager, affichage des emails des démarches
- #2731 Suppression de la case d'acceptation des CGV pour l'indentification par SIRET

## Technique

- #2742 Correction de failles dans le code relatif à la personnalisation et au filtrage du tableau de dossiers instructeur
- #2734 Suppression de code relatif au profil usager désormais obsolète
- #2746 Suppression d'une vue SQL désormais obsolète


--------------------------------------------

# 2018-10-02-01

Release [2018-10-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #2708 Pour le role Administrateur, ajout d'une astuce dans l'onglet "Instructeurs" d'une démarche

## Technique

- #2727 Amélioration de la sécurité des appels aux méthodes de suppression des notices, logos et délibérations d'une démarche
- #2723 Suppression du feature flag de la nouvelle UI usager


--------------------------------------------

# 2018-10-01-01

Release [2018-10-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-10-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les liens usagers pointent désormais tous vers la nouvelle interface
- Correction d'une erreur sur la page du Tour de France

## Technique

- Suppression de l'utilisation de modifiers
- Refactor du dropdown avec un vrai élément `button`


--------------------------------------------

# 2018-09-27-02

Release [2018-09-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-27-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Ajout d'un texte explicatif à propos des liens d'une démarche pour les administrateurs

## Technique

- Les jetons d'API sont dorénavant uniquement stockés sous forme hashée
- Correction des données de seed pour répondre aux contraintes de complexité des mots de passe
- Utilisation d'ujs pour afficher la complexité d'un mot de passe


--------------------------------------------

# 2018-09-27-01

Release [2018-09-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Le jeton de connexion à l'API n'est maintenant affiché seulement lors de sa génération

### Mineurs

- Fonctionnalité cachée : lors d'une réinitialisation d'une procédure, les dossiers en brouillon sont correctement supprimés.
- Fonctionnalité cachée : avancée ergonomique sur la publication de procédure en brouillon

## Technique

- Le jeton de connexion à l'API est sauvegardé hashé
- Le jeton n'est plus généré automatiquement à la création d'un compte mais à la demande


--------------------------------------------

# 2018-09-26-01

Release [2018-09-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-26-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Lorsqu'un administrateur définit son mot de passe, une certaine complexité est désormais requise pour ce dernier
- Dans l'interface administrateur, ajout d'un message d'erreur en cas de champ "listes liées" mal formatté


--------------------------------------------

# 2018-09-25-03

Release [2018-09-25-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-25-03)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui rendait la cartographie inutilisable par les usagers


--------------------------------------------

# 2018-09-25-02

Release [2018-09-25-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-25-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Tentative de correction d'un bug qui rendait la cartographie inutilisable par les usagers

### Mineurs

- Ajout d'une précision dans le formulaire de demande de compte administrateur

## Technique

- Ajout de tests pour la visualisation de l'identification SIRET et de la page de carto dans la nouvelle UI Usager
- Mise-à-jour des dépendances JS
- Suppression du gem `therubyracer`, désormais inutile


--------------------------------------------

# 2018-09-25-01

Release [2018-09-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans l'interface instructeur, le filtrage de dossiers par date de création d'entreprise fontionne désormais correctement
- Pour les démarches déclaratoires, on génère désormais l'attestation et on envoie un email à l'usager lors du dépôt de son dossier

### Mineurs

- Ajout d'une bannière à propos du Tour de France sur la landing administrations

## Technique

- Utilisation de délégation au lieu de STI pour `TypeDeChamp`
- Suppression de code mort
- Amélioration de certains tests
- Ajout d'une tache de nettoyage des préférences d'affichage des instructeurs suite à la correction de cette partie du code
- Ajout de tests pour l'invitation des usagers dans la nouvelle UI
- Suppression de la table `entreprises`


--------------------------------------------

# 2018-09-24-04

Release [2018-09-24-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-24-04)  sur GitHub

## Technique
- #2622 les colonnes `displayed_fields`, `sort` et `filters` de la table `procedure_presentations` sont désormais stockées en JSONB


--------------------------------------------

# 2018-09-24-03

Release [2018-09-24-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-24-03)  sur GitHub

## Nouveautés

- Ajout de la page [Tour de France](https://www.demarches-simplifiees.fr/tour-de-france)

## Améliorations et correctifs

### Mineurs

- Ajout de la gestion des usagers invités dans la nouvelle interface Usager


--------------------------------------------

# 2018-09-24-02

Release [2018-09-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-24-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les administrateurs peuvent désormais supprimer le logo d'une démarche
- Amélioration du texte du footer des emails de demande d'avis


--------------------------------------------

# 2018-09-24-01

Release [2018-09-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-24-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les administrateurs peuvent désormais supprimer la notice d'une démarche
- Les administrateurs peuvent désormais supprimer le logo d'une attestation
- Les administrateurs peuvent désormais supprimer la signature d'une attestation
- Multiples améliorations, pour la plupart graphiques, à la page de demande de compte administrateur
- Lorsqu'un usager rentre un mot de passe trop court, le message d'erreur qui lui est affiché est désormais plus clair
- Pour les administrateurs, ajout d'une précision concernant la "durée de conservation hors demarches-simplifiees.fr" qu'ils doivent renseigner

## Technique

- Suppression d'un warnings dans la tache `dev:console`
- Le logo DS n'est plus affiché sur nos pages, il a été remplacé à certains endroits par une Marianne
- Utilisation d'un unique fichier pour la Marianne, au lieu de deux


--------------------------------------------

# 2018-09-21-03

Release [2018-09-21-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-21-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les notices des démarches s'ouvrent désormais dans un nouvel onglet


--------------------------------------------

# 2018-09-21-02

Release [2018-09-21-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-21-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Rollback de la vérification du format des champs Nombre qui posait des problèmes de soumission de formulaire et d'annotations privées


--------------------------------------------

# 2018-09-21-01

Release [2018-09-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Pour le profil Administrateur, le type de champ "Nombre" s'appelle désormais "Nombre entier"
- Ajout d'un message d'erreur dans le cas où un usager soumettrait autre chose qu'un nombre entier dans un champ de type "Nombre"
- Pour le profil Usager, dans la nouvelle interface (cachée pour le moment), meilleur affichage du bouton vers la notice dans le formulaire de modification d'un dossier déposé
- Pour le profil instructeur, la fonction d'impression d'un dossier affiche désormais le nom des pièces jointes fournies par l'usager au lieu du texte "Pièce fournie"

## Technique

- Ajout d'une tache envoyant d'un email mensuel à l'équipe avertissant des dossiers à supprimer sur notre site


--------------------------------------------

# 2018-09-20-01

Release [2018-09-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug sur les champs "dossier lié"
- Correction de l'affichage des sauts de ligne dans le dialogue de confirmation de suppression d'un dossier
- Ajout de sauts de ligne dans le dialogue de confirmation de suppression d'un dossier
- Harmonisation et correction de plusieurs textes

## Technique

- Ajout et utilisation d'un linter d'editorconfig
- Flipflop est désormais remis à zero avant chaque test
- Modification du script de clone de base pour qu'il pointe vers notre nouvelle base
- Extraction des scripts de déploiement et de lint dans des fichiers
- Suppression de scripts inutiles
- Nettoyage du script de déploiement
- Modification de la clé SSH fournie à CircleCI pour le déploiement


--------------------------------------------

# 2018-09-19-03

Release [2018-09-19-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-19-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans le profil usager, dans l'onglet Résumé d'un dossier, on affiche désormais à l'usager les temps de vérification et d'instruction moyens pour la démarche en question
- Lorsqu'une personne clique sur un lien d'invitation, le formulaire d'inscription est désormais pré-rempli avec son adresse email

## Technique

- Déplacement de la création des liens entre les invitations et l'usager du callback `after_confirmation_path_for` au callback `after_confirmation`


--------------------------------------------

# 2018-09-19-02

Release [2018-09-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-19-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les numéros SIRET des établissements de La Poste ne sont plus considérés comme invalides (ce sont les seuls qui ne valident pas la formule de Luhn)
- Amélioration de la signalisation qu'une démarche est une démarche en brouillon dans les interfaces instructeur et usager

## Technique

- Refactor de la validation des numéros SIRET


--------------------------------------------

# 2018-09-19-01

Release [2018-09-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Changement de "procédure" en "démarche" à un endroit où le changement avait été oublié

## Technique

- Extraction du code relatif au tri et au classement des dossiers dans l'interface instructeur dans un service dédié
- Ajout de l'attribut `path` à `Procedure`
- Généralisation du feature switch concernant la nouvelle interface usager pour pouvoir facilement activer la fonctionnalité
- Ajout du gem `rails-i18n` pour avoir des messages d'erreur traduits
- Bump de plusieurs gems


--------------------------------------------

# 2018-09-18-02

Release [2018-09-18-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-18-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans l'interface Usager, on affiche désormais le service dans la liste des démarches
- Dans l'interface Usager, amélioration des textes affichés dans l'onglet "Résumé" des dossiers

## Technique

- Correction d'une bug dans la tache `2018_09_12_fix_templates`


--------------------------------------------

# 2018-09-18-01

Release [2018-09-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-18-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de l'affichage des états "terminés" dans l'onglet "Résumé" de la nouvelle interface Usager
- Le footer est désormais affiché sur toutes les pages de la nouvelle interface Usager
- Amélioration de l'affichage du footer sur les pages de la nouvelle interface Usager
- Ajout de la page "Liste des démarches" dans la nouvelle interface Usager

## Technique

- Refactor des partials de footer usager
- Ajout d'une tache qui migre une balise désormais obsolète des templates de mails et d'attestations vers la balise désormais utilisée


--------------------------------------------

# 2018-09-13-01

Release [2018-09-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-13-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui rendait certains boutons inactifs sur Safari iOS
- Pour les instructeurs, la rechrche est désormais insensible à la casse

### Mineurs

- Correction d'une coquille sur le formulaire de demande de compte administrateur

## Technique

- Refactor du champ SIRET
- Correction de la tache qui corrigeait la démarche FTAP


--------------------------------------------

# 2018-09-12-03

Release [2018-09-12-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-12-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Le texte qui indique le poids maximum d'une PJ dans la messagerie est désormais plus proche du champ d'ajout de la PJ

## Technique

- Ajout d'un endpoint pour connaitre la disponibilité d'une URL pour une démarche, en préparation de l'ajout de publication des démarches en brouillon
- Ajout d'une tache pour effectuer des modifications à la démarche FTAP


--------------------------------------------

# 2018-09-12-02

Release [2018-09-12-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-12-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Côté Usager, dans l'onglet "Résumé" d'un dossier (caché pour le moment), on affiche désormais le dernier message de la messagerie

## Technique

- Ajout de polyfills
- La configuration webpack est désormais lintée
- Changement de la signature de `CommentaireHelper#commentaire_is_from_me_class`
- Extraction de code dans `CommentaireHelper`


--------------------------------------------

# 2018-09-12-01

Release [2018-09-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Améliorations de la fonctionnalité d'envoi d'un dossier à un autre instructeur :
  * il est désormais possible d'envoyer un dossier à plusieurs instructeurs en une fois
  * il est désormais possible de filtrer la liste des instructeurs à qui envoyer un dossier

## Technique

- Refactor du footer usager pour améliorer son affichage
- Dans les vues relatives aux messages, on passe désormais le `connected_user` en variable locale plutôt que son email


--------------------------------------------

# 2018-09-11-01

Release [2018-09-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Améliorations à la page [Statistiques](https://www.demarches-simplifiees.fr/stats) :
  * la satisfaction usager est désormais plus explicitement affichée en pourcentage
  * les données utiliser pour la satisfaction usager ne sont désormais basées que sur des données complètes (et non plus avec une semaine pas encore terminée)

## Technique

- Refactor du CSS de la messagerie en vue de l'affichage d'un message dans l'onglet "Résumé" d'un dossier côté Usager
- Mini-refactor de FeedbacksController qui suit désormais nos règles stylistiques et s'assure qu'un usager est bien connecté
- Refactor des champs `dossier_link` :
  * Moins de dépendance à jQuery
  * Unification avec ce qui a été fait pour le champ SIRET
  * Meilleure protection des données
- Refactor de la publication des démarches en vue de la publication des démarches en brouillon


--------------------------------------------

# 2018-09-07-02

Release [2018-09-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-07-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Dans la nouvelle interface usager (cachée pour le moment), ajout d'une page dédiée à la modification d'un dossier déposé

### Mineurs

- Nombreuses améliorations sur la messagerie instructeur (et donc usager, cachée pour le moment) :
  * bouton d'envoi de message plus visible
  * meilleur affichage du bouton d'envoi de message
  * message de confirmation d'envoi de message plus clair
  * ajout d'une explication pour les usagers expliquant l'onglet "Messagerie"
  * meilleur affichage des dates
  * l'adresse email des instructeurs n'est plus totalement affiché
  * meilleure détéction du cas où l'emetteur est un invité
- On précise désormais aux usagers et aux accompagnateurs si une démarche est en brouillon ou pas

## Technique

- Distinction avec deux méthodes au lieu d'une de l'update de brouillon et l'update de dossier
- Le helper `dynamic_tab_item` gère désormais les tableaux d'URLs
- Renommage de certaines méthodes pour que leurs noms soient plus clairs


--------------------------------------------

# 2018-09-07-01

Release [2018-09-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-07-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction du lien de contact sur la page Accessiblité
- La validation de l'URL d'une démarche n'est désormais plus déclenchée tous les formulaires de démarche, seulement dans le bon formulaire

## Technique

- Ajout du support du debounce au handler ujs
- Bump de `spreadsheet_architect` et `axlsx` (permet de bumper `rubyzip`, qui présentait une vulnérabilité)


--------------------------------------------

# 2018-09-06-01

Release [2018-09-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-06-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui faisait planter l'impression d'un dossier pour les instructeurs


--------------------------------------------

# 2018-09-05-03

Release [2018-09-05-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-05-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les emails de notifications affichent désormais un lien vers la messagerie du dossier en pied de page
- Vocabulaire :
  * on ne parle plus de "procédure" mais de "démarche"
  * on ne parle plus de "commentaire" mais de "message"


--------------------------------------------

# 2018-09-05-02

Release [2018-09-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-05-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les nouvelles pages Usager ont désormais un titre précis
- L'onglet "Formulaire" dans la nouvelle interface Usager s'appelle désormais "Demande"
- Ajout d'une première version l'onglet "Messagerie" dans la nouvelle interface Usager, cachée pour le moment 
- Précision de la légende d'une carte dans la page de statistiques

## Technique

- Renommage de partials pour plus de précision et une compréhension plus immédiate de leurs rôles
- Extraction de la messagerie dans des fichiers dédiés pour qu'elle soit facilement exploitable à la fois par les interfaces usagers et instructeurs


--------------------------------------------

# 2018-09-05-01

Release [2018-09-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-05-01)  sur GitHub

## Technique

- Mutualisation du code de création de message dans un service en vue de la réutilisation de ce code dans la partie usagers
- Correction des factories suite aux warnings affichés par FactoryBot


--------------------------------------------

# 2018-09-04-01

Release [2018-09-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-04-01)  sur GitHub

## Technique

- Le code de calcul des stats est désormais plus robuste
- Travail sur les variables d'environnement et les secrets :
  * Suppression de variables inutiles
  * Ajout de variables manquantes
  * Extraction de secrets dans des variables d'environnement
  * Extraction de la conf de la base de données dans des variables d'environnement
  * Suppression de secrets inutiles
  * Au démarrage de l'application, on vérifie désormais la présence des variables d'environnement
- Bump de gems
- Utilisation de `ENV["APP_NAME"]` au lieu de `Rails.env` à certains endroits pour éviter des confusions maintenant que le site de staging tourne sous un env proche de la production


--------------------------------------------

# 2018-09-03-01

Release [2018-09-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-09-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Pour les nouvelles interfaces, amélioration de l'affichage du bandeau "navigateur obsolète"

## Technique

- Mise-à-jour des scripts rails d'installation et de mise-à-jour de l'application
- Documention des scripts rails d'installation et de mise-à-jour de l'application dans le README
- Passage d'une variable d'environnement à une constante


--------------------------------------------

# 2018-08-31-01

Release [2018-08-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-31-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui rendait de nombreux boutons inactifs dans Safari

### Mineurs

- Pour le profil usager, les durées de conservation des données sont désormais affichées dans le footer des dossiers
- Pour le profil usager, dans le formulaire, un message d'avertissement concernant l'ajout des pièces jointes est désormais affiché
- Pour le profil administrateur, dans la partie "Informations" d'une démarche, le message d'avertissement à propos de la suppression de la démarche affiche désormais le bon nombre de dossiers et de brouillons


--------------------------------------------

# 2018-08-30-02

Release [2018-08-30-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-30-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Le role "accompagnateur" s'appelle désormais "instructeur"
- Amélioration significative des performances de l'export de dossiers pour les instructeurs

### Mineurs

- Multiples petites améliorations du formulaire de contact

## Technique

- Ajout et configuration du gem `after_party`
- Utilisation d'une relation au lieu d'une méthode pour `Dossier.followers_gestionnaires`
- Les champs d'un dossier sont désormais toujours récupérés triés dans l'ordre
- Mutiples petites améliorations du code


--------------------------------------------

# 2018-08-30-01

Release [2018-08-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-30-01)  sur GitHub

## Nouveautés

- Ajout d'un formulaire de contact qui remplace les liens vers notre adresse email

## Améliorations et correctifs

### Mineurs

- Sur la page [Statistiques](https://www.demarches-simplifiees.fr/stats), ajout de l'évolution sur 30 jours des démarches publiées et des dossiers déposés

## Technique

- Ajout d'une API de webhook HelpScout pour pouvoir afficher des liens vers le manager dans HelpScout
- Généralisation des appels du type `MonModel.un_enum.fetch(:enum_key)` au lieu de `"enum_value"` afin d'éviter l'utilisation de valeurs inexistantes d'un enum
- Ajout d'une tache pour permettre la migration de dossiers d'une démarche Nutriscore vers une autre
- Suppression de l'API de stats, désormais inutilisée


--------------------------------------------

# 2018-08-29-01

Release [2018-08-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-29-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui rendait impossible la connexion via France Connect

### Mineurs

- Dans la partie Administrateur, suppression de la partie "Statistiques" d'une procédure car les chiffres présentés étaient faux et le graphique ne se chargeait pas

## Technique

- Suppression de le la configuration d'environnement `staging` : notre environnement de staging utilise désormais une configuration similaire à celle de production
- Suppression de code mort


--------------------------------------------

# 2018-08-28-02

Release [2018-08-28-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-28-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug affectant les formulaire avec plusieurs champs Adresse
- Correction d'un bug affectant la fermeture des boites de dialogue


--------------------------------------------

# 2018-08-28-01

Release [2018-08-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-28-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction des couleurs du graph de satisfaction usagers sur la page de stats
- Suppression de l'encadré de demandes de retour sur la nouvelle liste de dossiers pour les usagers après 2 mois d'affichage

## Technique

- Fin de la migration de `mark` vers `rating` sur `Feedback` :
  * suppression de la colonne `mark`
  * validations ActiveRecord et SQL de la présence de `rating`
- Améliorations sur les variables d'environnement :
  * Passage en constante d'une variable d'environnement non-secrète et qui ne varie pas
  * Suppression d'une variable d'environnement désormais inutile
  * Amélioration du nom d'une variable d'environnement
- Suppression de tâches désormais inutiles
- Petites améliorations stylistiques du code


--------------------------------------------

# 2018-08-27-02

Release [2018-08-27-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-27-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug introduit à la dernière release qui rendait la page Statistiques inaccessible


--------------------------------------------

# 2018-08-27-01

Release [2018-08-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Améliorations de [la page Stats](https://www.demarches-simplifiees.fr/stats) :
  * Ajout d'un graphique avec la répartition des états des dossiers
  * Ajout d'un graphique avec l'évolution du sentiment usager
  * Pour les super-administrateurs, ajout d'un bouton pour télécharger des statistiques au format CSV

### Mineurs

- Petites améliorations au widget de collecte de sentiment usager
  * reformulation de la question pour qu'elle soit plus précise et pertinente
  * les usagers peuvent désormais évaluer une fois par mois, et pas une fois "pour toujours"
  * les usagers sont désormais incités à nous écrire un email après leur évaluation
  * après l'évaluation, la page scroll désormais en haut de page afin de voir le message de confirmation
- Petites améliorations aux deux landing pages :
  * amélioration du style d'un bouton dans la landing usagers
  * rajout d'un CTA en bas de page de la landing administateurs
  * les administrateurs sont désormais redirigés vers la landing administrateurs après soumission du formulaire de demande de compte, et non plus la landing usagers
  * le bouton vers le formulaire de demande de compte administrateur ne s'ouvre plus dans un nouvel onglet mais dans l'onglet actuel
- Ajout de points finals manquants dans des emails

## Technique

- Utilisation d'un enum textuel et non pas d'une simple colonne d'entiers pour gérer les évaluations usager
- Ajout de parenthèses dans le code
- Suppression de code CSS dupliqué
- Suppression d'un appel à Google Analytics désormais inutile


--------------------------------------------

# 2018-08-24-02

Release [2018-08-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-24-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui faisait planter l'upload de fichiers

## Technique

- Nettoyage du fichier `.gitignore`
- Nettoyage du README


--------------------------------------------

# 2018-08-24-01

Release [2018-08-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-24-01)  sur GitHub

## Technique

- Utilisation de la gem dotenv pour configurer les environnements


--------------------------------------------

# 2018-08-23-02

Release [2018-08-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-23-02)  sur GitHub

## Améliorations et correctifs
### Majeurs
- Le formulaire d’inscription reflète la structure du CRM
- La recherche de dossiers pour les accompagnateurs fonctionne à nouveau

### Mineurs
- Le bouton « envoyer une copie » d’une procédure fonctionne à nouveau
- Le texte des boutons reste le même après avoir cliqué dessus


--------------------------------------------

# 2018-08-23-01

Release [2018-08-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-23-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de l'affichage du bandeau de navigateur obsolète

## Technique

- Ajout de helpers pour la gestion des onglets
- Correction d'un bug lié au search terms suite à la modification d'un dossier
- Le fichier `feature.yml`, qui n'existe plus, n'est plus mentionné dans le script de déploiement
- Activation de dotenv sur tous les environnements
- Utilisation de nouvelles variables d'environnement
- Amélioration de la gestion des navigateurs obsolètes
- Ajout de la protection CSRF pour les requêtes initiées par jQuery
- Refactor de vieux code JS
- Suppression de la table `cerfas`


--------------------------------------------

# 2018-08-16-03

Release [2018-08-16-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-16-03)  sur GitHub

## Technique

- Activation de du fichier `.env` au deploy
- Suppression de logstasher et utilisation d'un logger custom


--------------------------------------------

# 2018-08-16-02

Release [2018-08-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-16-02)  sur GitHub

## Technique

- Corrige une erreur Javascript sur l'ancien design


--------------------------------------------

# 2018-08-16-01

Release [2018-08-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-16-01)  sur GitHub

## Améliorations et correctifs
### Majeurs
- Séparation de la page d'accueil en 2 pages : une dédiée aux usagers, une autre aux adminsitrations

### Mineurs
- Augmentation du nombre maximum de champs possibles a 250 (!)

## Technique
- Import explicite de jquery
- Refactoring JavaScript
- Logging des backtraces dans lograge
- Ajout du feature flag pour le nouveau processus de procédure en brouillon
- Suppression des dossiers associés lors de la modification d'une procédure en brouillon
- Stabilisation de tests
- Amélioration de la nouvelle page cachée dossier pour les utilisateurs


--------------------------------------------

# 2018-08-13-01

Release [2018-08-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-13-01)  sur GitHub

## Améliorations et correctifs
### Majeurs
- Accompagnateur : Il est maintenant obligatoire de renseigner la motivation lors du refus ou du classement sans suite d'un dossier.
- Usager : La motivation de refus ou de classement sans suite est maintenant mentionnée directement dans l'email de notification.
- Amélioration de la compatibilité Javascript avec les vieux navigateurs (Internet Explorer et Safari).

### Mineurs
- Usager : Les boutons de satisfaction ne sont plus affichés quand l'Usager n'a aucun dossier, et ils sont désormais affichés en bas de page.
- Usager : Correction de la sérialisation des cases à cocher dans l'attestation.

## Technique
- Ajout de helpers `render_to_element` et `render_flash` pour les formulaires remotes.
- Utilisation de delegated events au lieu de local handlers pour les interactions JS.
- Ajout de l'onglet "Résumé" de la future page des détails d'un dossier.
- Ajout de Sentry pour les erreurs Javascript (pas encore activé).
- Ajout d'une tâche `bin/rake lint` pour exécuter tous les linters.
- Remplacement de logstasher par lograge.


--------------------------------------------

# 2018-08-10-02

Release [2018-08-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-10-02)  sur GitHub

## Améliorations et correctifs

### Mineur
- Correction d'un bug avec l'exporter de log qui bloquait le manager et d'autres actions


--------------------------------------------

# 2018-08-10-01

Release [2018-08-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-10-01)  sur GitHub

## Améliorations et correctifs
### Majeurs

- Les usagers sont invités à donner leurs avis sur le service

### Mineur
- Activation du clic droit sur la liste des procédures dans l'interface admin
- Correction d'un mauvais lien dans l'invitation d'un gestionnaire à donner un avis
- Correction d'un mauvais lien dans l'email de création de brouillon
- Les boutons "soumettre un dossier" et "sauvegarder un brouillon" gardent désormais leurs textes au clic
- Le type de champ "checkbox" s'appelle désormais "case à cocher"

## Technique
- Correction d'erreur JavaScript (pour IE11 principalement)
- Ajout de données et corrections sur l'exporter de log
- Ajout d'un page cachée pour la nouvelle interface des détails d'un dossier
- Amélioration diverses du README


--------------------------------------------

# 2018-08-07-01

Release [2018-08-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-07-01)  sur GitHub

## Technique

- Suppression du besoin d'avoir une route `/dossiers/:id` fictive
- Suppression du code pour le remplissage des formulaires usagers sur l'ancienne interface
- Amélioration de la fiabilité des tests Capybara
- Découpage de `routes.rb` en différentes sections


--------------------------------------------

# 2018-08-06-01

Release [2018-08-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-06-01)  sur GitHub

## Améliorations et correctifs
### Majeurs

- L'Administrateur ne reçoit plus d'email de notification lorsqu'un Usager supprime un dossier en brouillon.
- Le bouton "Soumettre le dossier" fonctionne maintenant correctement sur les anciens navigateurs web (au lieu de simplement enregistrer le brouillon).
- Le bandeau rouge indiquant "Votre navigateur web est obsolète" est maintenant réparé, plus visible, et comporte un lien aidant les utilisateurs à mettre à jour leur navigateur.

## Technique

- Internet Explorer 10 affiche maintenant correctement le bandeau "Navigateur périmé".
- Webpack est explicitement configuré pour émettre du Javascript compatible avec tous les navigateurs que nous gérons.
- Suppression de code périmé et redondant pour la détection des vieilles version d'Internet Explorer.


--------------------------------------------

# 2018-08-02-01

Release [2018-08-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-02-01)  sur GitHub

## Nouveautés

- Les usagers peuvent désormais inviter des personnes à modifier leur brouillon

## Technique

- Création et mise-à-jour de binstubs afin de corriger d'un bug au déploiement
- On cache désormais les packages yarn à la CI


--------------------------------------------

# 2018-08-01-02

Release [2018-08-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-01-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- L'email de notification de création de brouillon envoyé aux usagers n'encourage plus à poser ses questions "sur la plateforme", car les brouillons n'ont pas accès à la messagerie

## Technique

- Activation de l'option `defer` de Chartkick pour éviter des problèmes d'affichage sur la page Stats
- Le JS du widget Mailjet est désormais packagé avec webpack
- La CI réessaie désormais l'opération `yarn install --non-interactive` si elle échoue une première fois


--------------------------------------------

# 2018-08-01-01

Release [2018-08-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-08-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration du champ "SIRET" :
  * Lorsque l'usager vide un champ SIRET, le message d'erreur est correctement ré-initialisé ;
  * Le message d'erreur n'est plus bloqué lorsque l'usager a vidé le champ une fois ;
  * Meilleure formulation des messages d'erreur ;
  * Les messages d'erreur sont affichés en rouge ;
  * L'établissement trouvé est affiché dans un encadré (indiquant qu'il n'est pas modifiable) ;
  * Le spinner est correctement aligné.
- Les adresses emails des accompagnateurs suivant un dossier ne sont plus affichées en entier dans la messagerie del'interface usager
- Les adresses emails des accompagnateurs suivant un dossier ne sont plus affichées dans le panneau "Personnes impliquées" de l'interface usager
- Correction d'un problème d'alignement dans le menu de changement d'état de l'interface accompagnateur
- Correction d'un bug qui faisait planter l'appli en cas de double acceptation d'un dossier

## Technique

- Ajout et utilisation de webpacker
- Travail préparatoire sur le refactor de la recherche accompagnateur
- Bump de gems
- Nombreuses améliorations sur les tests, leur execution et leur fiabilité
- Correction de l'usage global de jQuery
- Amélioration du script de déploiement
- Amélioration du script de la config de CI


--------------------------------------------

# 2018-07-30-01

Release [2018-07-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-30-01)  sur GitHub

## Améliorations et correctifs
### Majeurs

- Usager : correction de l'affichage du bouton « Soumettre le dossier » sous Internet Explorer 11


--------------------------------------------

# 2018-07-25-01

Release [2018-07-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Usagers : les usagers peuvent à nouveau supprimer un dossier en brouillon.
- Accompagnateurs : lorsqu'un Usager supprime son dossier, les Accompagnateurs qui suivent le dossier reçoivent un email de notification.

### Mineurs

- Dans l'application, les boutons "Éditer" sont renommés en "Modifier", pour être plus clair.
- Emails : suppression d'une ancien texte mentionnant "Téléprocédures Simplifiées".
- Administrateur : correction d'un plantage lorsqu'on rentre des données invalide pour un Service.


--------------------------------------------

# 2018-07-24-01

Release [2018-07-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Manager : 
  - La recherche par dossier fonctionne maintenant correctement
  - Il est possible de rechercher un dossier ou une procédure par son identifiant
  - Les dossiers supprimés sont affichés lors d'une recherche

### Mineurs

- Correction d'une coquille : `une astérisque` → `un astérisque`
- Ajout de marges entre les champs de type "Pièce jointe"
- Manager : correction de la pluralisation de "Dossier"


--------------------------------------------

# 2018-07-20-01

Release [2018-07-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Le SIRET est désormais demandé lors de la création d'un Service


--------------------------------------------

# 2018-07-18-01

Release [2018-07-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Ajout d'un champ de recherche par numéro de dossier dans la partie usager

### Mineurs

- Correction d'un bug avec le champ SIRET sur la page "Prévisualisation d'une démarche"
- Dans le manager, on affiche désormais si le compte d'un usager est confirmé ou nom
- Dans le manager, on affiche désormais 2 boutons pour renvoyer un email de confirmation ou confirmer le compte d'un usager

## Technique

- Refactor de certaines routes
- Bump de gems


--------------------------------------------

# 2018-07-16-01

Release [2018-07-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-16-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : correction d'une coquille sur la page de prévisualisation du formulaire.
- L'email qui notifie de la suppression d'un dossier spécifie désormais le nom de la démarche concernée.

## Technique

- Ajout de tests


--------------------------------------------

# 2018-07-13-02

Release [2018-07-13-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-13-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui empêchait d'utiliser plus d'une "liste déroulante liée"
- Correction d'un bug qui empêchait la sauvegarde des valeurs d'une "liste déroulante liée" dans les annotations privées


--------------------------------------------

# 2018-07-13-01

Release [2018-07-13-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-13-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Les paragraphes sont désormais correctement mis en forme dans les descriptions des champs du formulaire
- Ajout d'un bouton vers la notice dans le formulaire
- Dans l'explication de la signification de l'astérisque dans le formulaire, cette dernière est désormais rouge
- Correction d'une faute d'orthographe dans l'interface administrateur

## Technique

- Ajout d'icones manquantes au patron
- Refactor d'un test


--------------------------------------------

# 2018-07-06-02

Release [2018-07-06-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-06-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'une couleur de fond sur la page d'accueil


--------------------------------------------

# 2018-07-06-01

Release [2018-07-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Reformulation de certains textes de la page d'accueil


--------------------------------------------

# 2018-07-04-02

Release [2018-07-04-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-04-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de fautes d'orthographe sur le terme "pièce jointe"


--------------------------------------------

# 2018-07-04-01

Release [2018-07-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-04-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateurs : sur la page "Cloner une procédure" :
  - amélioration des performances ;
  - les procédures sont maintenant correctement groupées en fonction du nom de l'organisation ou du service.
- Administrateurs : sur la page "Accompagnants", ajout d'un texte pour clarifier les actions possibles sur la page.
- Usagers : mise en place d'une redirection de l'ancienne liste des dossiers vers la nouvelle.
- Usagers : sur la page "Identité", les paragraphes descriptifs de la procédure sont correctement espacés.
- Manager : une confirmation est maintenant demandée avant de changer l'état d'un dossier.

## Technique

- Les tests d'acceptance qui échouent génèrent maintenant automatiquement une capture d'écran de la page, et un export du code HTML présent au moment de l'échec.
- Améliorations stylistique mineures du code sur la liste des dossiers.
- Suppression du code gérant l'ancienne liste des dossiers.


--------------------------------------------

# 2018-07-03-01

Release [2018-07-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- L'aperçu dans la messagerie usager affiche désormais le dernier message reçu
- Dans la manager, ajout d'un bouton "retour en instruction" pour les dossiers traités
- Correction d'un bug avec l'interconnexion Pipedrive


--------------------------------------------

# 2018-07-02-01

Release [2018-07-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-07-02-01)  sur GitHub

## Nouveautés

- Nouvelle présentation de la liste des dossiers pour les Usagers.

![screenshot_2018-07-02 demarches-simplifiees fr 1](https://user-images.githubusercontent.com/179923/42164203-97ddd740-7e05-11e8-8df1-2d65b95b526b.png)

## Améliorations et correctifs

### Mineurs

- Une fois qu'une procédure est archivée, il n'est plus possible de soumettre un dossier pour cette procédure, même si le dossier a été commencé avant.
- Il est désormais possible d'enregistrer un brouillon avec une pièce jointe même si tous les champs obligatoires n'ont pas été renseignés.
- La paramétrisation du nombre de résultats par page dans l'API fonctionne maintenant correctement.
- L'email de réinitialisation de mot de passe est maintenant traduit en français.


--------------------------------------------

# 2018-06-29-01

Release [2018-06-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-29-01)  sur GitHub

## Nouveautés

- Listes déroulantes liées (conditionnel simple) - sous réserve d’activation de la feature pour l’administrateur

## Améliorations et correctifs

### Mineurs

- Le service n’est plus cloné quand on clone une procédure via la bibliothèque

## Technique

- Suppression du vieux code cerfas


--------------------------------------------

# 2018-06-28-01

Release [2018-06-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-28-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Conversion des anciens _cerfas_ en pièces jointes
- Correction d’un lien de header qui pointait prématurérment vers la nouvelle interface usager
- Meilleur lien « qu’est-ce que France Connect ? »

## Technique

- Rétablissement de X-Ray en dev
- Progrès sur les listes déroulantes conditionnelles
- Calcul plus simple des `ordered_champs` (rendu possible par le passage au nouvel aperçu)


--------------------------------------------

# 2018-06-27-01

Release [2018-06-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-27-01)  sur GitHub

## Technique

- Nettoyage de champs factices créés en base par l’ancien aperçu de procédures
- Amélioration, pas encore visible, de divers aspects du fonctionnement de la nouvelle liste des dossiers pour l'usager :
  - Cliquer sur un dossier dans la liste redirige vers la page individuelle d'un dossier,
  - Simplification de l'affichage quand il n'y a pas de dossiers invités,
  - Correction des liens dans le header modernisé,
  - Ajout d'un état "Aucun dossier",
  - Ajout d'un bouton pour ajouter un nouveau dossier,
  - Utilisation de active_link pour gérer les onglets de la navigation,
  - Amélioration de la vitesse de certains tests.


--------------------------------------------

# 2018-06-26-01

Release [2018-06-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-26-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Affichage de deux établissements dans l’interface accompagnateur : l’établissement correspondant au SIRET, ainsi que le siège social (si celui-ci est différent)
- Le caractère obligatoire de la case à cocher des CGU est désormais signalé à l’utilisateur par une astérisque
- Correction du lien vers l'email de contact dans l'interface accompagnateur
- Le bouton « créer un compte » s’affiche désormais sur une seule ligne sur la page de connexion

## Technique

- Travail préparatoire sur champs conditionnels simples (deux listes déroulantes liées)
- Constantisation du numéro de téléphone de contact


--------------------------------------------

# 2018-06-21-02

Release [2018-06-21-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-21-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de la mise en page sur certains écrans du nouveau design


--------------------------------------------

# 2018-06-21-01

Release [2018-06-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Si un usager inscrit mais non confirmé tente de se réinscrire, on lui envoie désormais un nouvel email de confirmation
- Dans la partie accompagnateur, le SIRET affiché est désormais bien le SIRET de l'établissement, pas celui du siège social de l'entreprise
- Dans le manager, mise en évidence des gestionnaires d'une procédure sur les pages des gestionnaires et des procédures

## Technique

- Ajout de spring aux binstubs par défaut
- Optimisation de la compilation des assets en environnement de dev
- Correction d'un test qui échouait de manière aléatoire
- Suppression de code mort


--------------------------------------------

# 2018-06-20-01

Release [2018-06-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction de la faille CVE-2018-3760 qui n'affectait pas directement demarches-simplifiees.fr


--------------------------------------------

# 2018-06-19-03

Release [2018-06-19-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-19-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction d'un bug d'affichage sur les listes déroulantes


--------------------------------------------

# 2018-06-19-02

Release [2018-06-19-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-19-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Rajout d'un footer dans l'interface usager qui indique le service gestionnaire de la procédure et les moyens de le contacter

## Technique

- Déplacement de code spécifique à certains types de champs de la classe génériqye à des classes spécifiques


--------------------------------------------

# 2018-06-18-01

Release [2018-06-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug qui envoyait les mails relatifs à la gestion des comptes utilisateurs en anglais

### Mineurs

- Correction d'un bug sur la saisie d'une durée de conservation supérieure à 36 mois 

### Interne

- Possibilité de renvoyer une invitation à un accompagnateur


--------------------------------------------

# 2018-06-12-01

Release [2018-06-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-12-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Mise en place d'une analyse antivirus sur le nouveau champ pièces jointes

### Mineurs

- Amélioration de la documentation développeur


--------------------------------------------

# 2018-06-11-01

Release [2018-06-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-11-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Correction d'un bug dans l'email d'envoi de dossier à un autre accompagnateur

### Mineurs

- Tous les emails HTML ont désormais également une version texte


--------------------------------------------

# 2018-06-08-02

Release [2018-06-08-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-08-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Corrige une faille qui permettait à un usager de modifier un dossier en instruction ou déjà instruit

## Technique

- Corrige une typo dans les tests


--------------------------------------------

# 2018-06-08-01

Release [2018-06-08-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-08-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Pré-confirmation de certains comptes dont l'activation était compliquée (bug corrigé depuis) et notification des utilisateurs impactés


--------------------------------------------

# 2018-06-07-05

Release [2018-06-07-05](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-07-05)  sur GitHub

## Technique

- Ultime correction de bug dans une tache


--------------------------------------------

# 2018-06-07-04

Release [2018-06-07-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-07-04)  sur GitHub

## Technique

- Correction de bugs dans 2 taches


--------------------------------------------

# 2018-06-07-03

Release [2018-06-07-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-07-03)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Dans Pipedrive, le deals et le contacts sont désormais assignés automatiquement au super-administrateur qui a accepté la demande de création de compte administrateur dans Manager

## Technique

- Correction d'une tache : le nom de la tache est désormais identique au nom de fichier
- Correction d'un bug dans une tache


--------------------------------------------

# 2018-06-07-02

Release [2018-06-07-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-07-02)  sur GitHub

## Technique

- Correction d'un bug dans une tache rake
- Passage d'un envoi d'email en asynchrone


--------------------------------------------

# 2018-06-07-01

Release [2018-06-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-07-01)  sur GitHub

## Nouveautés

- Les nouvelles procédures doivent maintenant renseigner une durée de rétention des donnés dans et hors DS

## Améliorations et correctifs

### Majeurs

- Correction du bug qui exigeait une double validation des accompagnateurs et administrateurs nouvellement invités

### Mineurs

- Amélioration de la mise en page de l'email de demande d’avis
- Améliroration du wording et de la présentation des emails
- Les emails d’invitation des administrateurs sont maintenant signés par le business developer à l’origine de l’invitation
- Ajout d’une tache pour renvoyer des attestations suite au changement de template de ces attestations
- Ajout d’une tache pour renvoyer des attestations suite à des réouvertures puis instructions de dossiers déjà instruits

## Technique

- Suppression des préfixes des sujets des emails
- Amélioration de plusieurs sujets d’emails
- Refactor des mailers et des templates d’emails
- Unification des noms des dossiers contenant les mailers
- Suppression d’un fichier inutile
- Unification du format des templates d’emails
- Suppression de traductions redondantes
- Bump de Sinatra, qui comble une potentielle faille de sécurité XSS


--------------------------------------------

# 2018-06-01-02

Release [2018-06-01-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-01-02)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Rendre le fondement juridique non-bloquant pour les procédures existantes (corrige #2016, #2021, et #2023)


--------------------------------------------

# 2018-06-01-01

Release [2018-06-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-06-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Meilleure gestion des erreurs lors du clonage d'une procédure

## Technique

- Refactor des mailers et constantisation des adresses emails
- Suppression du verouillage de version et bump de `delayed_job_web`


--------------------------------------------

# 2018-05-31-03

Release [2018-05-31-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-05-31-03)  sur GitHub

## Nouveautés

- Les procédures doivent maintenant avoir un fondement juridique

## Améliorations et correctifs

### Mineurs

- Il est désormais possible de consulter les accompagnateures et les usagers dans le Manager

## Technique

- Refactor de `DossierHelper#delete_dossier_confirm`
- Ajout de `User#owns? `
- Ajout de `User#owns_or_invite?`


--------------------------------------------

# 2018-02-19-01

Release [2018-02-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-02-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- ajout de la date de dépôt pour l'instructeur 

## Technique

- Ajout d'une règle Timecop concernant les dates
- correction d'un bug sur la connexion sécurisée pour les nouveaux instructeurs et administrateurs


--------------------------------------------

# 2018-01-17-01

Release [2018-01-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2018-01-17-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Sur les démarches déclaratives, l'Administrateur n'est plus automatiquement affecté aux dossiers

### Mineurs

- Ajout du nouvel éditeur de champ (derrière un réglage optionnel pour l'instant)
- La page de Création de compte affiche maintenant le contexte de la démarche
- Améliorations d'usabilité pour les démarches en test

## Technique

- Refactor du layout des pages sign_in et sign_up
- Refactor de l'extraction du contexte de la démarche
- Correction des variables envoyées à SendInBlue pour suivre l'onboarding des administrateurs


--------------------------------------------

# 2016-06-19-01

Release [2016-06-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2016-06-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- La prévisualisation d'une démarche utilise désormais le même design que la version "usager" de la démarche
- La suppression d'un dossier côté usager n'est plus disponible si le dossier est entré en instruction
- Réouverture des dossiers supprimés qui avaient passé l'instruction

### Mineurs

- Les checkboxes sont désormais affichées à gauche de leur libellé dans les formulaires
- Corrections de petits bugs graphiques

## Technique

- Ajout d'une tâche pour changer l'administrateur d'une procédure
- Amélioration du bouchonnage et du README
