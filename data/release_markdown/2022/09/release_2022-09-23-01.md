# 2022-09-23-01

Release [2022-09-23-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-23-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ Visiteur, je souhaite voir le footer de la home au DSFR (#7794)
- fix(etablissement.as_degraded_mode): backfill missing data via a cron (#7799)

### Technique

- T
