# 2022-09-01-04

Release [2022-09-01-04](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-09-01-04)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(a11y): input background color issue (#7707)
- ajoute un résumé des erreurs présentes dans l'éditeur de champ (#7708)
- fix(dsfr): side effect (#7710)

### Technique

- T
