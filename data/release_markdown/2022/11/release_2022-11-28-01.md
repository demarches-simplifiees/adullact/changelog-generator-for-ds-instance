# 2022-11-28-01

Release [2022-11-28-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-28-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(graphql): fix playground when admin has no procedures or dossiers (#8127)
- chore(dossier): cleanup clone champ (#8099)
- feat(demarche): enable revisions for all (#8133)
- feat(graphql): make demarche schema public (#8124)
- fix(pdf): strip html tags in type_de_champ descriptions (#8137)
- fix: escape characters when showing page title (#8102)
- chore: instructions sur le transfert de dossier plus précises (#8138)
- fix(graphql): check if tokens are revoked (#8141)

### Technique

- T
