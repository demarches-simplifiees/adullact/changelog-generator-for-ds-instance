# 2022-11-24-01

Release [2022-11-24-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-11-24-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- fix(procedure): rattrape la donnée manquante relative à la duree de conservation (#8091)
- feat(attestation): validate attestation tags (#8003)
- chore: maj skylight (#8120)
- fix(procedure): ajoute des contraintes not null sur les colonnes duree_conservation (#8092)
- fix(invite): do not render menu when invite not found (#8126)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20221122145030_clean_duree_conservation.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20221108114545_assign_attestation_templates_to_procedures.rake`, `lib/tasks/deployment/20221121163201_clean_invalid_procedures.rake`, `spec/lib/tasks/deployment/20221108114545_assign_attestation_templates_to_procedures_spec.rb`).
