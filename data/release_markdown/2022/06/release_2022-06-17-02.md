# 2022-06-17-02

Release [2022-06-17-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-17-02)  sur GitHub

## Améliorations et correctifs

### Technique

- fix(graphql): avoid n+1 on champ_descriptors (#7459)
- ajout du moteur de condition (#7424)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220531100040_add_condition_column_to_type_de_champ.rb`).
