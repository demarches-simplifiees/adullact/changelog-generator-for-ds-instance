# 2022-06-27-01

Release [2022-06-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-06-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- chore(dossier): remove dead code (#7498)
- fix(Rails.ujs): missing Rails.start (#7499)

### Technique

