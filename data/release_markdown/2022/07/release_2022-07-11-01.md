# 2022-07-11-01

Release [2022-07-11-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-07-11-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- feat(tags): expose used_type_de_champ_tags (#7548)
- fix(targeted_user_link): on invite without user (#7556)
- bug(linked_dropdown): when mandatory, add an extra blank option (#7550)

### Technique
- Permet la génération json du descriptif des démarches publiques (#7489)
- feature: ajout du composant d'édition des conditons (#7522)
