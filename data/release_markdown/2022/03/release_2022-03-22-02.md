# 2022-03-22-02

Release [2022-03-22-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-03-22-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- API : correction d'une exception lors de l'authentification pour utiliser l'API GraphQL (#7072)

### Technique

- Correction de la tâche AfterParty migrate_revisions (#7033)