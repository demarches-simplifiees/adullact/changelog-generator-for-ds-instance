# 2022-04-27-01

Release [2022-04-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-04-27-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- fix(instructeur/procedure#export.zip): correctif pour l'export au format zip d'une selection de dossier de dossier (#7183)
- fix(procedure): une procédure cloné ne devrait pas hériter de l'auto_archive_on (#7185)
- fix(upload): errorFromDirectUploadMessage should take Error or string (#7192)
- feat(users/procedure/*): ajout du lien vers le texte_juridique ou la deliberation (#7190) et du lien vers le contact du DPO (#7197)
> ![Screenshot 2022-04-27 at 13-34-30 with list · demarches-simplifiees fr(1)](https://user-images.githubusercontent.com/125964/165541760-82267e40-41d5-4a5d-8d99-871222e9890b.png)
- feat(adminstrateurs#dossier_vide): ajout du bouton pour exporter un dossier vide au format PDF pour faciliter la vie des administrateurs afin d'inscrire la démarche au sein du registre de traitement.(#7188)
> <img width="1040" alt="164727749-b2b0e1e0-05a5-45b9-9597-a17ac4eca1f9" src="https://user-images.githubusercontent.com/125964/165542050-3d86c690-0f1a-44df-8ba4-bf7626ab769d.png">
- feat(type_de_champs::identite) Ajoute un avertissement sur les champs de type Titre Identité (#7196)
> <img width="1159" alt="165103842-213c03ac-af82-4899-8e5e-7af99ee2f362" src="https://user-images.githubusercontent.com/125964/165542735-822ee6e1-432a-4f51-9040-4fb48f2bbf11.png">
- fix(messagerie): Corrige l'exception lors de l'affichage de la messagerie (#7169)
- met a jour la mention accessibilité (#7201)

### Technique
- tech(ApiEntreprise): use dinum SIRET for all APIs call within the recipient params (#7195)
- fix(sentry#3144617191): Amélioration du rendu des archives dans le manager (#7184)
- tech(turbostream): Mise en place des turbostream rails (#7174)
- doc(ux): clarifie le process pour l'ux research (#7177)
- refactor(js): get rid of ds:page:update (#7186)
- ignore instructeur_id in dol (#7193)
- fix(migrate): lien administrateurs_procedures - administrateurs (#7191)
- refactor(champs): refactor champs components to use typescript (#7148)


## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220425140107_add_lien_dpo_to_procedure.rb`).
