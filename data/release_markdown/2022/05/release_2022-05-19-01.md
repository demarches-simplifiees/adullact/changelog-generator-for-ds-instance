# 2022-05-19-01

Release [2022-05-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-19-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- fix(revision): correction sur les révision ou un bug qui ajoutait en double les types de champ d'un bloc répétable  (#7354)
