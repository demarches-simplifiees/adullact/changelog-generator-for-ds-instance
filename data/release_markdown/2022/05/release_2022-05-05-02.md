# 2022-05-05-02

Release [2022-05-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-05-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- feat(procedure.expiration) ; pour toutes les nouvelles procédure, activation de l'expiration des dossiers une fois la duree de conservation passée. Rappel aux administrateurs qu'il doivent activer cette fonctionnalité (#6981)
> <img width="1066" alt="Screen Shot 2022-05-05 at 2 00 58 PM" src="https://user-images.githubusercontent.com/125964/166918921-32b08f89-ff4b-4fb1-b5b3-aaa8784bdd56.png">

### Technique

- fix(dossier): on dossier update render empty js response (#7254)

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20220222150340_set_default_procedure_expires_when_termine_enabled_to_true.rb`).
