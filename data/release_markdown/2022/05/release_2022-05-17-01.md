# 2022-05-17-01

Release [2022-05-17-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2022-05-17-01)  sur GitHub

## Améliorations et correctifs



### Mineurs

- add link to solidarite-numerique (#7306)
- Usager : l'attestation de dépôt est envoyée en pièce jointe dans l'email de notification de dépôt (#7283)

### Technique

- fix(turbo): avoid crashing IE 11 with non get/post methods (#7315)
- Reapply improvements on procedure_revision (#7300)
- refactor(dossier): change state with turbo (#7316)
- fix(recherche): expert actions should be a menu-button (#7321)
- fix(sentry/3139111475): clean up some "corrupted" data, some dossier still have depose_at nil while en_construction_at is set (#7318)
- fix(autosave): prevent autosave if current champ is detached from the form (#7320)
- fix(recherche#index): move paginate outside of table row loop (#7322)
- Bump gems (#7312)
- Refactor(Revision Spec) : detailles les tests de comparaison entre les versions (#7311)
- sentry(3002560608): some Dossier ready to be purged are missing the hidden_by_reason (#7328)
- tech(Gemfile): unlock versions for kaminari and delayed_job_web (#7326)
- fix(spec): try fix dossier expiration spec (#7331)
- fix(fetch): prevent double parsing of fetch error messages (#7319)

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20220513135112_fix_depose_at_nil_on_dossier_state_not_brouillon.rake`, `lib/tasks/deployment/20220516160033_fix_hidden_by_reason_nil.rake`).
