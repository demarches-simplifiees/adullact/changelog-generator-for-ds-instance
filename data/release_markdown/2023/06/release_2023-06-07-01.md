# 2023-06-07-01

Release [2023-06-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-06-07-01)  sur GitHub

## Améliorations et correctifs

### Admin

- Ajustements d'UI dans la liste et le détail des groupes d'instructeurs (#9133)

### Technique

- Correction rapide des pages de présentation pour les pages avec une notice_url mais pas de notice attaché (#9145)