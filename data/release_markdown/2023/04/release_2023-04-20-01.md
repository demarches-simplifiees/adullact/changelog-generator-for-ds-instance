# 2023-04-20-01

Release [2023-04-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-20-01)  sur GitHub

## Améliorations et correctifs

### Instructeurs
- ETQ instructeur, je veux voir l'état des dossiers affiché sur une seule ligne (#8920)

### Administrateurs
- ETQ administrateur, je peux specifier un niveau de titre a mes titres de sections (#8695)
- ETQ administrateur, je peux changer le groupe d'instructeur par défaut (#8916)

### Usagers
- ETQ usager, Tableau de bord - Déplacer la barre de recherche proche des dossiers (#8912)
- ETQ usager, Simplifie l'implémentation des champs "liste d'options" et "listes d'options liées" (#8850)
- ETQ usager, je voudrais avoir accès aux statistiques des démarches closes (#8928)
- ETQ usager, regroupe les champs précédés d'un titre de section dans un fieldset (#8695)
- ETQ usager, je voudrai pouvoir remplir un code postal avec des espaces (#8930)

### API 
- ETQ intégrateur API, je ne veux pas avoir d'erreurs lorsque j'interroge les métadonnées de certains fichiers (#8919)
- ETQ intégrateur API, je voudrais avoir accès aux dossiers récemment supprimés d’un groupe instructeur (#8888)
- ETQ intégrateur API, je voudrais avoir des codes d'erreur plus précis (#8918)
- ETQ DS API, log dossier and procedure id on dossier fetch endoint (#8927)

### Correctif
- ETQ usager, Améliorer l'affichage des resultats de recherche (#8913)
- ETQ visiteur, ne plus afficher le telephone de la dinum sur la page declaration d'accessibilite (#8922)
- ETQ usager, désactive la checkbox pour tout séléctionner quand une action mutliple est en cours (#8902)



### SuperAdmin
- ETQ opérateur, je voudrais pouvoir consulter plus facilement l’état d’une démarche (#8929)

### Technique
- amelioration(dolist): ne log erreurs pas les erreurs dans sentry lorsque le contact chez dolist est injoingable ou hardbounce (#8907)
- chore(procedure): remove duplicate code (#8925)
- Tech: update rubocop, active nouveaux cops Rails/* (#8924)
