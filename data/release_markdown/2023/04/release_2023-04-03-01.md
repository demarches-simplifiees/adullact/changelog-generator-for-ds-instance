# 2023-04-03-01

Release [2023-04-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-04-03-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- Webinaire link fix (#8814)
- correctif(export.pdf): ETQ expert, lorsque j'exporte un dossier au format PDF, celui ci contenient les avis non confidentiels ainsi que mes avis (#8817)
- Fix: autolink manuellement les liens qui ne sont pas des urls (#8820)
- Fix avis: n'affiche pas le form de réponse à la question s'il n'y a pas de question (#8816)
- FIX - Garde les tags dans les params lors de la pagination (#8815)
- Webinaire link fix (#8814)
- correctif(export.pdf): ETQ expert, lorsque j'exporte un dossier au format PDF, celui ci contenient les avis non confidentiels ainsi que mes avis (#8817)
- Fix: autolink manuellement les liens qui ne sont pas des urls (#8820)
- refactor(commune): choisir la commune par son code postal (#8783)
- feat: allow classic SMTP for email sending (#8818)
- feat(graphql): log type and timeout errors (#8822)
- chore(npm): update dependencies (#8823)
- fix(rails): smtp config should be optional (#8825)
- Fix champ explication: cache le texte qui doit être caché (#8826)
- Fix (manager): ne trie pas par created_at pour ne pas casser la prod (#8828)
- [Fix] Petites améliorations de la vue expert (#8829)
- Fix: met à jour des liens du pied de page des démarches (#8830)
- ETQ instructeur je veux pouvoir supprimer la pj pendant l'instruction (#8811)
- patch(demarche.lien_demarche): ignore cette colonne pr la supprimer plus tard (#8649)
- Add routing rules to groupe instructeurs (#8774)
- a11y(bloc-repetable): amélioration des interactions avec les répétitions d'un bloc répétable (#8699)
- chore(données): supprime la table `drop_down_lists` qui est inutilisée (#8832)
- correctif(revision.validation-des-conditions): les conditions dans un bloc répétable ne remontenpt pas dans le composant ErrorsSummary (#8833)
- [fix] bug dans le sujet des mails de notification si le libelle de la procedure contient un apostrophe (#8837)
- fix(commune): improuve label and error message (#8835)
- fix(dossier): add value_json to dossier projection (#8842)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230228073530_add_routing_column_to_groupe_instructeur.rb`, `db/migrate/20230331075755_drop_table_drop_down_lists.rb`).
