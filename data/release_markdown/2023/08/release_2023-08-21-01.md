# 2023-08-21-01

Release [2023-08-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-08-21-01)  sur GitHub

## Améliorations et correctifs

### EQT instructeur

- ETQ instructeur, je peux envoyer un message a un utilisateur ayant un dossier qui n'a pas encore de groupe d'instructeur (#9314)



### correctif
- correctif(dolist): utilise des liens vers les logos des procedures plutôt que des attachements.inlined (#9369)
- correctif(export): corrige bug création archives (#9402)
- correctif(email.dolist): expose public logo url (#9376)

### technique
- chore(pipedrive): remove pipedrive (#9396)
- fix(after_party): this job timouts in prod. Drop it for now. (#9399)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230718143350_reset_dossier_brouillon_groupe_instructeur_id.rake`).
