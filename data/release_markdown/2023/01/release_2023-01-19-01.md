# 2023-01-19-01

Release [2023-01-19-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-01-19-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- feat(graphql): expose more information on demarche descriptor (#8117)
- fix(after_party): backfill children Champs without row_ids (#8460)
- fix(graphql): demarche with pj should return schema (#8461)
- ETQ Administrateur, je souhaite filtrer mes démarches par tag (#8392)
- Fix: prévient l'instructeur lorsqu'un dossier n'est pas terminable à cause de champ SIRET incomplet (#8462)

### Technique

- T

## Notes de déploiement

Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230103170917_normalize_pays_values.rake`, `lib/tasks/deployment/20230118102035_backfill_repetition_champ_without_row_id.rake`).
