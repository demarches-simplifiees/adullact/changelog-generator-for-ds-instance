# 2023-03-06-01

Release [2023-03-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-06-01)  sur GitHub

## Améliorations et correctifs


### Mineurs

- Administrateur:  je peux filtrer avec plusieurs tags (#8720)
- Instructeur: Uniformise les actions pour les instructeurs sur la page tableau et dossier (#8630)
- instructeur: amélioration UX du bouton d'instruction (#8701)
- Instructeur: integre les avis dans l'export au format pdf du dossier (#8744)
- Instructeur & Usager: implémentation des classes DSFR pour les badges d'état des dossiers (#8710)
- Fix: un groupe instructeur inactif est valide tant qu'il reste un groupe actif (#8729)
- Fix: affichage de yes/no en anglais (#8735)
- Préremplissage: fait du get sans stored query  (#8622)

### Technique
- Corrige des erreurs récurrentes dans des jobs, notamment dans les jobs type "cron" (#8723)
- Schema: ajoute la clé etrangère entre active_storage_attachements et active_storage_blobs uniquement si elle est manquante (#8721)

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230302161322_fix_again_hidden_by_reason_nil.rake`).
