# 2023-03-16-02

Release [2023-03-16-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-02)  sur GitHub

## Améliorations et correctifs

### Technique

Fix déploiement de la release précédente [2023-03-16-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-03-16-01) (#8772)

Cette version peut être considérée comme **stable**.

## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230316125244_fix_private_champ_type_mismatch.rake`).
