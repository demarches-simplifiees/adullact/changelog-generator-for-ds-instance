# 2023-05-31-02

Release [2023-05-31-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-31-02)  sur GitHub

## Améliorations et correctifs

### Administrateurs

- Majeur : - Nouvelle UX pour le routage (#8940) !

### Technique

- tech(recovery.list_blob_ids): ajoute une tache pour exporter les clés des fichiers a restaurer (#9090)