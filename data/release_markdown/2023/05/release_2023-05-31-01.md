# 2023-05-31-01

Release [2023-05-31-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-31-01)  sur GitHub

## Améliorations et correctifs


### Usager 
- Merge branch 'main' into 8054-a11y-ways-of-navigating
- a11y : 8054 a11y ways of navigating (#8979)
- 8588 search engine footer optimization (#9096)
- [Refonte page accueil demarche] Détailler la description pour plus de clarté pour l'usager (#9074)

### Instructeur

### Administrateur
- Etq admin, lors de la création ou modification d'une démarche, des zones par défaut me sont suggérées (#9014)

### Super Admin
- ETQ SuperAdmin, je vois le groupe dans le détail d'un dossier (#9091)

### Technique
- ETQ intégrateur d’API, je voudrais voir un message d’erreur si le changement d’état échoue (#9085)
- ETQ tech je mets à jour Sentry pour essayer d'avoir de meilleures traces (#9089)
- ajoute un exemple de config pour utiliser l'environnement de staging d'API Entreprise (#9094)
- Api Entreprise : Migration Entreprise Tva  et Kbis (#9092)
- API Entreprise : migration "exercices" (#9099)
- API Entreprise : migration "attestation fiscale" (#9098)
- Migre les données pour le nouveau mode de routage (#8923)

## Notes de déploiement

:information_source: Il y a une grosse migration du a nouveau système de routage : un champ est ajouté à tous les dossiers routés, voir #8923

Cette version comporte des migrations du schéma de la base de données (`db/migrate/20230508103932_add_tchap_hs_to_zones.rb`, `db/migrate/20230508160551_create_default_zones_administrateurs.rb`, `db/migrate/20230516132925_add_description_target_audience_to_procedure.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230322172910_populate_zones_with_tchap_hs.rake`, `lib/tasks/deployment/20230417083259_migrate_data_for_routing_with_dropdown_list.rake`, `spec/lib/tasks/deployment/lib/tasks/deployment/20230417083259_migrate_data_for_routing_with_dropdown_list_spec.rake`).
