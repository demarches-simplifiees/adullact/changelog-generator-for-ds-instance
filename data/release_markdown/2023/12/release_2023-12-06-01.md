# 2023-12-06-01

Release [2023-12-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-12-06-01)  sur GitHub

## Améliorations et correctifs

### Correctif

- Tentative de correction de  l'upload de fichiers pour certains navigateurs et config réseau (chore(yarn): patch update vite) (#9803)

### Instructeur

- ETQ instructeur, lorsque je motive une décision, la motivation est désormais bien formatée (#9799)
