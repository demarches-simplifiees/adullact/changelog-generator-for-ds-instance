# 2023-07-25-01

Release [2023-07-25-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-07-25-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- M

### Mineurs

- ETQ administrateur, je souhaite que la page "publier" soit plus claire (#9323)
- ETQ operateur, je peux piloter le niveau de log à partir de la variable DS_LOG_LEVEL (#9342)
- rend plus actionnable message d'erreur (#9332)
- fix(views): fix dead link to configure API entreprise token (#9343)
- Ameliore le wording des personnes impliquées (#9340)
- API graphql: expose les demandes de corrections (#9310)
- build(deps): bump word-wrap from 1.2.3 to 1.2.4 (#9327)
- chore(deps): bump semver from 5.7.1 to 5.7.2 (#9302)
- ETQ instructeur, je veux que la motivation soit effacée lorsque je repasse un dossier en instruction (#9329)
- [fix] Les demarches supprimées s'affichent encore dans l'onglet "en test" pour les instructeurs (#9337)
- refactor: move submit en_construction logic to the model (#9348)
- [refonte usager] Tableau de bord - filtrer les dossiers par démarche (#9338)
- feat(administrateur): add environment variable for Administrateur::UNUSED_ADMIN_THRESHOLD (#9352)
- chore(geo_area): purge invalid geo_areas (#9346)
- fix spec (% 10000 and assert_performed_jobs) (#9350)

### Technique

- T

## Notes de déploiement

Cette version comporte une migration du schéma de la base de données (`db/migrate/20230712095037_add_reason_to_dossier_corrections.rb`).
Cette version comporte des migrations du contenu des données (`lib/tasks/deployment/20230712095348_backfill_dossier_correction_reason.rake`, `lib/tasks/deployment/20230721142825_purge_motivation_dossiers_not_termine.rake`, `lib/tasks/deployment/20230721145042_purge_invalid_geo_areas.rake`).
