# 2023-09-05-01

Release [2023-09-05-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-09-05-01)  sur GitHub

## Améliorations et correctifs

💣 Instable sur vieux navigateurs, passez directement à 2023-09-05-02

### Correctifs
- ~~Amélioration de l'utilisation des menus déroulants sur les anciens navigateurs (#9452)~~
- Correction du lien qui renvoie vers la FAQ dans le message de confirmation de compte à chaque connexion (#9447)

### Instructeur
- Dans une démarche SVA/SVR, meilleur suivi des dossiers terminés repassés en instruction (#9450)

### Administrateur
- Correction UX du formulaire de sélection de zones (#9453)


### Manager
- Visualisation de la liste des instructeurs et des groupes d'instructeurs pour une procédure spécifique (#9448)

### Technique
- Mise à jour des dépendances npm (#9454)
