# 2023-02-27-01

Release [2023-02-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-02-27-01)  sur GitHub

## Améliorations et correctifs

Cette version est considérée comme **instable**.

### Mineurs

- Titres de section: la numérotation automatique tient compte du conditionnel (#8659)
- Accessibilité: traduit les menus contextuels d'aide, normalise l'affichage et les icônes (dsfr) (#8673)
- fix(autocomplete): avoid double escape of query params (#8698)
- fix(procedure): fix translations in changes component (#8689)
- fix(filter): fix find type de champ by stable_id (#8694)
- Prefill repeatable (#8513)
- feat(dossier): prefill communes champ (#8685)
- feat(dossier): prefill siret champ (#8388)
- feat(dossier): prefill rna champ (#8424)
- feat(dossier): prefill dossier link champ (#8679)


## Notes de déploiement

Cette version comporte une migration du contenu des données (`lib/tasks/deployment/20230221100840_strip_type_de_champ_libelle.rake`).
