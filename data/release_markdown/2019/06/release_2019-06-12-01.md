# 2019-06-12-01

Release [2019-06-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-12-01)  sur GitHub

## Technique

- Corrige une erreur Javascript concernant ActiveStorage sous Internet Explorer 11
