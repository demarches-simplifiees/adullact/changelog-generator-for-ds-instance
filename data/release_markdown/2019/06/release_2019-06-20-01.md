# 2019-06-20-01

Release [2019-06-20-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-06-20-01)  sur GitHub

## Améliorations et correctifs

### Mineurs
- Usager : correction d'une erreur lors de l'enregistrement simultané d'un champ invalide et d'une pièce jointe
- Admin : correction d'un problème lors de la création d'une démarche sans cadre juridique

## Technique
- Migration des PJ : gestion des erreurs qui peuvent se produire pendant le rollback
- Migration des PJ : corrige le rollback dans le cas où les dossiers sont cachés
- Correction du mailer_preview pour prendre en compte les administrateurs multiples