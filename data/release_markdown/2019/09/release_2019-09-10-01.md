# 2019-09-10-01

Release [2019-09-10-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-09-10-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Administrateur : correction de la suppression des démarches de test
- Messagerie : le texte explicatif indiquant le destinataire du message est  plus clair
- Emails : les emails transactionnels d'une démarche ont maintenant une adresse de réponse en NO_REPLY ; cette adresse renvoie un message d'orientation automatique en cas de réponse.
- API : le service d'une démarche est maintenant exposé dans l'API

## Technique

- Suppression des uploaders CarrierWave (étape 1)