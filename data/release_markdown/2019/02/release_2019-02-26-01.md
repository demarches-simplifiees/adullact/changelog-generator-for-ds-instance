# 2019-02-26-01

Release [2019-02-26-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-26-01)  sur GitHub

## Améliorations et correctifs
### Mineurs
* Correction d'un bug affectant les PJ sans extension

## Technique
* Ajout d'un index sur la table `types_de_champ`