# 2019-02-05-02

Release [2019-02-05-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-05-02)  sur GitHub

## Améliorations et correctifs

### Mineurs

- diverses améliorations dans la gestion des champs répétables (cachés pour l'instant)
- ajout de tags pour sélectionner la première ou la deuxième valeur d'un champ deux listes déroulantes liées
- correction d'un bug qui affectait la description coté usager d'un dossier lié

## Technique

- le job clamav loggue à présent les erreurs qu'il rencontre