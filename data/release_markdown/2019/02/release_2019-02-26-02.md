# 2019-02-26-02

Release [2019-02-26-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-26-02)  sur GitHub

## Améliorations et correctifs
### Majeurs
- #3513 Permet aux administrateurs sans démarche de changer de rôle

## Technique
- #3512 Corrige la création de procédures en mode multi-administrateur
