# 2019-02-18-01

Release [2019-02-18-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-02-18-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Les jetons d'authentification ont maintenant une durée de vie d'une semaine.

### Mineurs

- #3417 les champs départements peuvent avoir une valeur vide
- la barre de progression d'upload des fichiers est réparée
- #3427 les super admins peuvent supprimer un dossier
- #1140 wording sur la page attestation
- les super admin ont un lien vers l'aperçu des procédures et peuvent chercher des procédures par url
- #3315 mises à jour des organismes des services

## Technique

- correctif: affichage de la date de dépôt d'une dossier uniquement lorsque celle-ci est disponible
