# 2019-01-23-02

Release [2019-01-23-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-23-02)  sur GitHub

## Améliorations et correctifs
### Mineurs

- Administrateur : clarification d'un texte sur la page de création d'une démarche
- Instructeur : correction d'un plantage lors de certaines recherches
- Usager : ajout de la date de création du dernier dossier sur la page de garde d'une démarche
- Amélioration de la fiabilité des statistiques récoltées