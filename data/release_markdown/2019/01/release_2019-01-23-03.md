# 2019-01-23-03

Release [2019-01-23-03](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-23-03)  sur GitHub

## Technique

- Corrige la création de dossiers sans démarches lors de la suppression d'une démarche en test
- Supprime les dossiers sans démarches en production