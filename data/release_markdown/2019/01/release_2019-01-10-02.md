# 2019-01-10-02

Release [2019-01-10-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-01-10-02)  sur GitHub

## Correctifs

- correction d'un bug qui affectait la récupération par API d'un dossier lorsqu'une pièce jointe était absente