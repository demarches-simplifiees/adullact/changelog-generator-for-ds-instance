# 2019-04-02-01

Release [2019-04-02-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-02-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- #3625 réécriture de la page d'inscription des administrateur

## Technique

- #3722 suppression d'un code inutile concernant la cartographie
- #3718 correction d'un bug affectant les champs siret
- #3724 annulation du code qui affichait les dates en anglais