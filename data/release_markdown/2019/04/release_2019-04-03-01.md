# 2019-04-03-01

Release [2019-04-03-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-04-03-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Usager : affiche un message d'erreur pour informer de la panne d'API Entreprise
- Usager : améliore l'affichage des boutons d'action sur mobile
- Stats : ajuste les intervalles affichés

## Technique

- Déplace les analytics dans un pack séparé
- Renommage du fichier de license