# 2019-08-21-01

Release [2019-08-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-08-21-01)  sur GitHub

## Améliorations et correctifs

## Technique

- fix flaky spec in ip_service_spec
- Improve clone of procedure attachements
- Fix carte initialize