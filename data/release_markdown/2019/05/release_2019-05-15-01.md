# 2019-05-15-01

Release [2019-05-15-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-05-15-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateurs : nouvel éditeur de champs de formulaire

### Technique

- Usager : corrige une erreur mineure à la fin de l'envoi d'un fichier sous Safari