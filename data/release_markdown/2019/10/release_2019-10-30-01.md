# 2019-10-30-01

Release [2019-10-30-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-30-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Gestion des groupes instructeurs par les instructeurs

## Technique

- Bump openstack
- Add a DS_PROXY_URL env variable
