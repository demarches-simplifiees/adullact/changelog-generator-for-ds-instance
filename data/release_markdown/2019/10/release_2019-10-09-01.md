# 2019-10-09-01

Release [2019-10-09-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2019-10-09-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Un instructeur peut passer un dossier accepté en instruction (#4058)
- Corrige la notion d'administrateur actif (#4361)

### Mineurs

- Optimisations de performances
- Génération automatique de liens dans les champs textes
- Instructeur : corrige l'utilisation des cartes dans les annotations privées (#4388)

## Technique

- Suppression du feature flag `download_as_zip`
- Refactoring : dans les tasks, remplacement des `puts` par `rake_puts`
- Ajout de modèles par défaut lors de la création d'une issue
- Mise à jour des dépendances Javascript
- Mise à jour des dépendances Ruby :
  - rubyzip passe de 1.2.2 à 1.3.0
  - suppression de la gem 'simple_form'
