# 2021-06-24-02

Release [2021-06-24-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-24-02)  sur GitHub

## Améliorations et correctifs

### Technique

- archives : expose l'export pdf d'un dossier pour l'instructeur comme un IO
