# 2021-06-01-01

Release [2021-06-01-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-06-01-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Correction du format des apostrophes (#6234)
- SuperAdmin : ajout de traductions manquantes pour le nom du modèle User (#6244)
- SuperAdmin : correction de l'effacement de démarches supprimées (#6242)

### Technique

- Correction d'un test automatisé échouant aléatoirement (#6237)
- i18n : ajout d'un test pour le sélecteur de langues (#6231)
- Correction de l'environnement remonté par Sentry (#6239)
- Mise à jour de Sentry vers la version 4.4.2
- Mise à jour de dns-packet de la version 1.3.1 à 1.3.4