# 2021-01-12-01

Release [2021-01-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-01-12-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Amélioration de l'interface des buttons multiples

### Technique

- Fix type_de_champ repetition revision after clone
- Fix a crash when champ carte has no options
