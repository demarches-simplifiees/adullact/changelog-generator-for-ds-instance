# 2021-07-20-02

Release [2021-07-20-02](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-07-20-02)  sur GitHub

## Améliorations et correctifs

### Technique

- Correctif pour diminuer le nombre d'erreurs InvalidAuthenticityToken ("La requête a été rejetée") (#6332)
