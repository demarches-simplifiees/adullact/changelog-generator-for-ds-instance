# 2021-10-06-01

Release [2021-10-06-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-06-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ expert, je veux télécharger le dossier en pdf et ses pj
- ETQ Administrateur, je souhaite exporter tous les groupes instructeurs d'une démarche au format CSV

