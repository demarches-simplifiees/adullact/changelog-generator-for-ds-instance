# 2021-10-14-01

Release [2021-10-14-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-14-01)  sur GitHub

#:exclamation: Cette mise à jour est recommandée pour toutes les instances qui utilisent France Connect.

## Améliorations et correctifs

### Majeurs

- Ajoute le champ CNAF
- Corrige le connecteur FranceConnect dans le cas de différent comptes FC utilisant le même mail de contact

### Mineurs

- Corrige une erreur lors de la fusion de compte dans le manager
- Usager : les informations FranceConnect sont maintenant plus discrètes dans le formulaire (#6538)
- Retire une description superflu dans un champ oui/non

### Technique

- Nettoyage : déplacement de la classe ActiveJobLogSubscriber en dehors des initializers (#6533)
- Merge pull request #6540 from betagouv/dependabot/bundler/puma-5.5.1
- fix(avis): add foreign key to avis dossier_id