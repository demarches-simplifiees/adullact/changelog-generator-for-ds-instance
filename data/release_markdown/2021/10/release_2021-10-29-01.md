# 2021-10-29-01

Release [2021-10-29-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-29-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- ETQ usager, je veux voir les titres des démarches en plus grand dans mon interface

