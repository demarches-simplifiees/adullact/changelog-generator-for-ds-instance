# 2021-10-27-01

Release [2021-10-27-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-10-27-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

- Administrateur : lors de la modification d'une démarche publiée, les dossiers en brouillon existants sont mis à jour avec la dernière révision.
- Administrateur : possibilité d'ajouter une option "Autre" à une liste de choix
- Administrateur : possibilité de spécifier le libellé et la description des menus déroulants secondaires

### Mineurs

- Usager : les dossiers désactivés ne sont plus transférés lors d'une fusion de compte

### Technique

- Fix(dossier): fixe dossier.avis cascade
- Suppression de code obsolète dans la configuration du serveur web puma
- Mise en cache du dictionnaire de complexité des mots de passe
- Tests : Migration des tests d'intégration vers des "system specs" (#6584)
- Tests : amélioration de certaines factories
