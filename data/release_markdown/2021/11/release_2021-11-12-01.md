# 2021-11-12-01

Release [2021-11-12-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-11-12-01)  sur GitHub

## Améliorations et correctifs





### Technique

- Correction de colonnes non désirées dans les démarches clonées comportant des blocs répétables (#6629)
- Reset notifications when changing dossier state
