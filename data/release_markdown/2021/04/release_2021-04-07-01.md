# 2021-04-07-01

Release [2021-04-07-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-04-07-01)  sur GitHub

## Améliorations et correctifs



### Mineurs


- Améliorations d'accessibilité (icône pour les liens externes, statut d'accessibilité) (#6054)
- Marque les fichiers qui ont un pb d'intégrité comme corrompus et prévient l'utilisateur


### Technique
- Diminue l'impact de flipper sur les temps de réponse
- Prevent crash in preview where there is no siblings
- `ExpertsProceduresController` utilise maintenant les actions par défaut de Rails (#6056)
