# 2021-05-21-01

Release [2021-05-21-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-21-01)  sur GitHub

## Améliorations et correctifs

### Mineurs

- Améliore l'usage du composant multi select

