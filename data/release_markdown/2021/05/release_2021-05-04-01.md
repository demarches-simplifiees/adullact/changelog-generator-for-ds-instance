# 2021-05-04-01

Release [2021-05-04-01](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2021-05-04-01)  sur GitHub

## Améliorations et correctifs

### Majeurs

### Mineurs

- active l'extension de la conservation des brouillons
- active la destruction des comptes utilisateurs
- prend en compte uniquement les pj pour estimer la taille d'un dossier
- Revert "Merge pull request #6142 from tchak/enable_brouillon_extend_conservation"

### Technique

- Mise à jour de la dépendance rexml de 3.2.4 à 3.2.5 (#6160)
- Jobs : ajout de Excon::Error::Socket à la liste des erreurs ré-essayées automatiquement (#6163)
- Correction d'un test de l'API GraphQL (#6162)
