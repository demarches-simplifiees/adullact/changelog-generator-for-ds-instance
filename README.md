# CHANGELOG generator for DS software

## Data

- [CHANGELOG in **HTML** format](data/release_html/CHANGELOG.html)
- [CHANGELOG in **Markdown** format](data/release_markdown/CHANGELOG.md)

## Documentation

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Code of Conduct](CODE_OF_CONDUCT.md)

## License

[**AGPL v3** - GNU Affero General Public License](LICENSE)
